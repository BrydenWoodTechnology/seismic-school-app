﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace BrydenWoodUnity {
    [InitializeOnLoad]
    public class EditorSettingSetup {

        static EditorSettingSetup()
        {
            PlayerSettings.SetApiCompatibilityLevel(BuildTargetGroup.Standalone, ApiCompatibilityLevel.NET_2_0);
            PlayerSettings.companyName = "Bryden Wood";
            //Debug.Log("API compatibility level set to 2.0");
        }

    }
}