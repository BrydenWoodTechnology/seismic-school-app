﻿using BrydenWoodUnity.UIElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BrydenWoodUnity.GeometryManipulation
{

    /// <summary>
    /// A Mesh Vertex Unity component
    /// </summary>
    public class MeshVertex : MonoBehaviour
    {
        public float snapThreshold = 1.0f;
        /// <summary>
        /// The initial position of the vertex component
        /// </summary>
        public Vector3 initialPosition { get; set; }

        /// <summary>
        /// The current position of the polygon Vertex
        /// </summary>
        public Vector3 currentPosition { get; private set; }

        public Vector3 currentLocalPosition { get; private set; }

        /// <summary>
        /// The index of this vertex
        /// </summary>
        public int index { get; private set; }
        public ControlPointUI cpui { get; private set; }

        #region Events
        /// <summary>
        /// Triggered when the vertex has been moved
        /// </summary>
        public event MeshVertexMoved vertexMoved;
        /// <summary>
        /// Called when the vertex is being moved in order to trigger the corresponding event
        /// </summary>
        /// <param name="sender">The sender of the event</param>
        public void OnMoved(object sender)
        {
            currentPosition = transform.localPosition;
            //VertexSnap();
            if (vertexMoved != null)
            {
                vertexMoved(this);
            }
            if (moved != null)
            {
                moved(this);
            }
        }

        public void OnMoved()
        {
            OnMoved(this);
        }

        public static event MeshVertexMoved moved;
        #endregion

        private void Start()
        {
            currentPosition = transform.localPosition;
        }

        private void Update()
        {

        }

        private void OnDisable()
        {
            if (currentPosition != transform.localPosition)
            {
                currentPosition = transform.localPosition;
            }
        }

        #region Public Methods
        public void Initialize(int index)
        {
            this.index = index;
            initialPosition = transform.position;
            currentPosition = transform.position;
            currentLocalPosition = transform.localPosition;
        }

        /// <summary>
        /// Sets the GUI element to be used for moving the vertex
        /// </summary>
        /// <param name="cpui">The Control Point UI element for moving the vertex</param>
        public void SetGUIElement(ControlPointUI cpui)
        {
            this.cpui = cpui;
            this.cpui.moved += OnMoved;
        }

        public void SetPosition(Vector3 pos)
        {
            transform.localPosition = pos;
            currentPosition = pos;
            currentLocalPosition = transform.localPosition;
            cpui.UpdatePositions(transform);
            OnMoved(this);
        }

        public void UpdatePosition(Vector3 pos)
        {
            transform.localPosition = pos;
            currentPosition = pos;
            currentLocalPosition = transform.localPosition;
        }
        #endregion
    }
}