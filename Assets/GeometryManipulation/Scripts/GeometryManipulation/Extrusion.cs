﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BrydenWoodUnity.GeometryManipulation
{
    /// <summary>
    /// An extrusion Unity component
    /// </summary>
    [RequireComponent(typeof(MeshFilter))]
    public class Extrusion : SelectableGeometry
    {
        #region Public Fields
        /// <summary>
        /// The mesh filter which will be used for the mesh
        /// </summary>
        [Header("Unity specific fields:")]
        public MeshFilter meshFilter;
        /// <summary>
        /// Whether the resulting mesh should be flipped
        /// </summary>
        public bool flip = false;

        /// <summary>
        /// The total height of the extrusion
        /// </summary>
        [Space(10)]
        [Header("Geometry specific fields:")]
        [Tooltip("The total height of the polygon (read-only)")]
        public float totalHeight;
        /// <summary>
        /// The total facade of the extrusion
        /// </summary>
        [Tooltip("The facade area of the polygon (read-only)")]
        public float totalFacade;

        [Tooltip("Whether the extrusion should be capped or not")]
        public bool capped = true;

        [Tooltip("An offset for the reversed vertical faces in order to avoid Z-fighting")]
        public float reversedOffset = 0.02f;

        [Space(10)]
        [Header("Materials and Prefabs")]
        public Material vertexColor;

        /// <summary>
        /// The polygon which is being extruded
        /// </summary>
        [HideInInspector]
        public Polygon polygon;
        /// <summary>
        /// The editable mesh that is being extruded
        /// </summary>
        [HideInInspector]
        public EditableMesh editableMesh;
        /// <summary>
        /// The overall mesh of the polygon
        /// </summary>
        [HideInInspector]
        public Mesh overallMesh;
        /// <summary>
        /// The top face of the extrusion
        /// </summary>
        [HideInInspector]
        public Mesh topFace;
        /// <summary>
        /// A List including the vertical faces of the extrusion
        /// </summary>
        [HideInInspector]
        public List<Mesh> verticalFaces;

        [HideInInspector]
        public List<Mesh> reverseVerticalFaces;
        
        #endregion

        #region Private Fields
        private GameObject m_gameObject;
        #endregion

        private void Start()
        {

        }

        private void Update()
        {

        }

        #region Public Methods
        public void Initialize(Polygon polygon)
        {
            this.polygon = polygon;
            transform.localPosition = Vector3.zero;
            this.polygon.updated += OnPolygonUpdated;
            GetComponent<MeshRenderer>().material = polygon.gameObject.GetComponent<MeshRenderer>().material;
            GenerateMeshes();
        }

        public void Initialize(EditableMesh editableMesh)
        {
            this.editableMesh = editableMesh;
            transform.localPosition = Vector3.zero;
            this.editableMesh.updated += OnEditableMeshUpdated;
            GetComponent<MeshRenderer>().material = editableMesh.gameObject.GetComponent<MeshRenderer>().material;
            GenerateExtrusionMeshes();
        }

        public void DisplayAnalysisResults()
        {
            List<CombineInstance> m_instances = new List<CombineInstance>();

            GetComponent<MeshRenderer>().material = vertexColor;

            if (capped)
            {
                m_instances.Add(new CombineInstance() { mesh = topFace, transform = Matrix4x4.identity });
            }

            overallMesh = new Mesh();
            overallMesh.CombineMeshes(m_instances.ToArray(), true, true);
            Vector2[] m_uvs = new Vector2[overallMesh.vertexCount];
            for (int i = 0; i < m_uvs.Length; i++)
            {
                m_uvs[i] = new Vector2(overallMesh.vertices[i].x, overallMesh.vertices[i].y);
            }
            overallMesh.uv = m_uvs;
            overallMesh.uv2 = m_uvs;
            overallMesh.uv3 = m_uvs;
            overallMesh.uv4 = m_uvs;
            UpdateMeshComponents();
        }

        public void DisplayStandardMode()
        {
            GenerateMeshes();
        }
        #endregion

        #region Private Methods
        private void GenerateExtrusionMeshes()
        {
            Mesh mesh = editableMesh.mesh;
            List<CombineInstance> m_instances = new List<CombineInstance>();

            List<Vector3> bottomVerts = new List<Vector3>();
            for (int i = 0; i < editableMesh.outterEdges.Count; i++)
            {
                var edge = editableMesh.outterEdges[i];
                bottomVerts.Add(mesh.vertices[edge.v1]);
            }

            bool clockWise = Polygon.IsClockWise(bottomVerts);
            if (!clockWise && capped)
            {
                flip = true;
            }

            if (capped)
            {
                Vector3[] topVerts = new Vector3[mesh.vertexCount];
                for (int i = 0; i < topVerts.Length; i++)
                {
                    topVerts[i] = mesh.vertices[i] + new Vector3(0, totalHeight, 0);
                }
                Mesh topMesh = new Mesh();
                topMesh.vertices = topVerts;
                int[] topTriangles = new int[mesh.triangles.Length];
                for (int i = 0; i < mesh.triangles.Length; i += 3)
                {
                    topTriangles[i] = mesh.triangles[i + 2];
                    topTriangles[i + 1] = mesh.triangles[i + 1];
                    topTriangles[i + 2] = mesh.triangles[i];
                }
                topMesh.triangles = topTriangles;
                topMesh.colors = mesh.colors;
                m_instances.Add(new CombineInstance() { mesh = topMesh, transform = Matrix4x4.identity });
            }

            verticalFaces = new List<Mesh>();
            for (int i = 0; i < editableMesh.outterEdges.Count; i++)
            {
                var edge = editableMesh.outterEdges[i];
                Mesh verticalFace = new Mesh();
                Vector3[] verts = new Vector3[]
                {
                    mesh.vertices[edge.v1],
                    mesh.vertices[edge.v2],
                    mesh.vertices[edge.v2] + new Vector3(0,totalHeight,0),
                    mesh.vertices[edge.v1] + new Vector3(0,totalHeight,0),
                };
                verticalFace = GeometryHelper.GetMeshFrom4Points(verts, flip, Color.white);
                verticalFaces.Add(verticalFace);
                m_instances.Add(new CombineInstance() { mesh = verticalFace, transform = Matrix4x4.identity });
            }

            if (!capped)
            {
                verticalFaces = new List<Mesh>();

                for (int i = 0; i < editableMesh.outterEdges.Count; i++)
                {
                    var edge = editableMesh.outterEdges[i];
                    Vector3 offsetVector = Vector3.Cross((mesh.vertices[edge.v2] - mesh.vertices[edge.v1]), Vector3.up);
                    if (clockWise)
                    {
                        offsetVector *= -1;
                    }
                    Mesh verticalFace = new Mesh();
                    Vector3[] verts = new Vector3[]
                    {
                    mesh.vertices[edge.v1] + offsetVector.normalized*reversedOffset,
                    mesh.vertices[edge.v2] + offsetVector.normalized*reversedOffset,
                    mesh.vertices[edge.v2] + new Vector3(0,totalHeight,0)+ offsetVector.normalized*reversedOffset,
                    mesh.vertices[edge.v1] + new Vector3(0,totalHeight,0)+ offsetVector.normalized*reversedOffset,
                    };
                    verticalFace = GeometryHelper.GetMeshFrom4Points(verts, true, Color.white);
                    verticalFaces.Add(verticalFace);
                    m_instances.Add(new CombineInstance() { mesh = verticalFace, transform = Matrix4x4.identity });
                }
            }

            overallMesh = new Mesh();
            overallMesh.CombineMeshes(m_instances.ToArray(), true, true);
            Vector2[] m_uvs = new Vector2[overallMesh.vertexCount];
            for (int i = 0; i < m_uvs.Length; i++)
            {
                m_uvs[i] = new Vector2(overallMesh.vertices[i].x, overallMesh.vertices[i].y);
            }
            overallMesh.uv = m_uvs;
            overallMesh.uv2 = m_uvs;
            overallMesh.uv3 = m_uvs;
            overallMesh.uv4 = m_uvs;
            UpdateMeshComponents();
        }
        private void GenerateMeshes()
        {
            List<CombineInstance> m_instances = new List<CombineInstance>();

            //------Creating the lists of bottom and top vertices----//
            List<Vector3> topVertices = new List<Vector3>();
            List<Vector3> bottomVertices = new List<Vector3>();
            for (int i = 0; i < polygon.mesh.vertices.Length; i++)
            {
                bottomVertices.Add(polygon.mesh.vertices[i] - new Vector3(0, 0.1f, 0));
                topVertices.Add(bottomVertices[i] + new Vector3(0, totalHeight, 0));
            }

            bool clockWise = Polygon.IsClockWise(bottomVertices);

            if (!clockWise && capped)
            {
                flip = true;
            }

            if (capped)
            {
                //-----------Generating the mesh of the top face of the extrusion----//
                topFace = new Mesh();
                topFace.vertices = topVertices.ToArray();
                int[] tris = new int[polygon.mesh.triangles.Length];
                for (int i = 0; i < tris.Length; i += 3)
                {
                    tris[i] = polygon.mesh.triangles[i + 2];
                    tris[i + 1] = polygon.mesh.triangles[i + 1];
                    tris[i + 2] = polygon.mesh.triangles[i];
                }
                topFace.triangles = tris;
                topFace.colors = polygon.mesh.colors;
                m_instances.Add(new CombineInstance() { mesh = topFace, transform = Matrix4x4.identity });
            }

            verticalFaces = new List<Mesh>();
            for (int i = 0; i < bottomVertices.Count; i++)
            {
                Mesh verticalFace = new Mesh();
                int nextIndex = (i + 1) % bottomVertices.Count;
                Vector3[] verts = new Vector3[]
                {
                    bottomVertices[i],
                    bottomVertices[nextIndex],
                    topVertices[nextIndex],
                    topVertices[i]
                };
                verticalFace = GeometryHelper.GetMeshFrom4Points(verts, flip, Color.white);
                verticalFaces.Add(verticalFace);
                m_instances.Add(new CombineInstance() { mesh = verticalFace, transform = Matrix4x4.identity });
            }

            if (!capped)
            {
                verticalFaces = new List<Mesh>();
                var offsetBottom = OffsetPolyline(bottomVertices.ToArray(), reversedOffset);
                var offsetTop = OffsetPolyline(topVertices.ToArray(), reversedOffset);

                for (int i = 0; i < bottomVertices.Count; i++)
                {
                    Mesh verticalFace = new Mesh();
                    int nextIndex = (i + 1) % bottomVertices.Count;
                    Vector3[] verts = new Vector3[]
                    {
                        offsetBottom[i],
                        offsetBottom[nextIndex],
                        offsetTop[nextIndex],
                        offsetTop[i]
                    };
                    verticalFace = GeometryHelper.GetMeshFrom4Points(verts, true, Color.white);
                    verticalFaces.Add(verticalFace);
                    m_instances.Add(new CombineInstance() { mesh = verticalFace, transform = Matrix4x4.identity });
                }
            }

            overallMesh = new Mesh();
            overallMesh.CombineMeshes(m_instances.ToArray(), true, true);
            Vector2[] m_uvs = new Vector2[overallMesh.vertexCount];
            for (int i = 0; i < m_uvs.Length; i++)
            {
                m_uvs[i] = new Vector2(overallMesh.vertices[i].x, overallMesh.vertices[i].y);
            }
            overallMesh.uv = m_uvs;
            overallMesh.uv2 = m_uvs;
            overallMesh.uv3 = m_uvs;
            overallMesh.uv4 = m_uvs;
            UpdateMeshComponents();

        }

        private Vector3[] OffsetPolyline(Vector3[] vertices, float distance)
        {
            Vector3[] offsetVerts = new Vector3[vertices.Length];

            for (int i = 0; i < vertices.Length; i++)
            {
                int prevIndex = i - 1;
                if (prevIndex < 0)
                {
                    prevIndex = vertices.Length - 1;
                }
                int nextIndex = (i + 1) % vertices.Length;

                Vector3 a = vertices[nextIndex] - vertices[i];
                Vector3 b = vertices[prevIndex] - vertices[i];
                Vector3 c = a.normalized + b.normalized;
                c.Normalize();

                offsetVerts[i] = vertices[i] + c * distance;
            }

            return offsetVerts;
        }

        private void OnPolygonUpdated(Polygon sender)
        {
            GenerateMeshes();
        }

        private void OnEditableMeshUpdated(EditableMesh sender)
        {
            GenerateExtrusionMeshes();
        }

        private void UpdateMeshComponents()
        {
            overallMesh.RecalculateNormals();
            overallMesh.RecalculateBounds();
            meshFilter.mesh = overallMesh;
            GetComponent<MeshCollider>().sharedMesh = overallMesh;
        }
        #endregion
    }
}
