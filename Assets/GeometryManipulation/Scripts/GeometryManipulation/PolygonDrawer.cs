﻿using BrydenWoodUnity.UIElements;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using BrydenWoodUnity.Navigation;

using BrydenWood.Interop;

namespace BrydenWoodUnity.GeometryManipulation
{
    public enum DrawingPlane
    {
        XY,
        XZ,
        ZY
    }
    public class PolygonDrawer : MonoBehaviour, IVertexManager
    {

        [Header("Main References")]
        public GameObject polygonPrefab;
        public GameObject polygonVertexPrefab;
        public GameObject controlPointUIPrefab;
        public DrawingPlane drawingPlane = DrawingPlane.XZ;
        public Button addSiteButton;

        [Header("Optional References:")]
        public Camera raycastingCamera;
        public Transform polygonParent;
        public Transform polygonVertexParent;
        public Transform controlPointUiParent;

        [Header("Behaviour Settings:")]
        public float lastVertexSnap = 0.1f;

        [Header("UI dependencies:")]
        public Transform buttonParent;

        protected List<PolygonVertex> temp_Vertices;
        public bool creatingPolygon = false;
        protected Plane plane;
        protected Polygon temp_polygon;
        protected LineRenderer temp_lineRenderer;
        protected List<Vector3> temp_points;

        public ResourcesManager resourcesMan;
        public Button setGrid;

        public float offsetValue;
        public SiteZoom sz;

        bool fileIsNull = true;

        public delegate void OnPolygonEnded(Polygon polygon, Polygon offset = null);
        public static event OnPolygonEnded polygonEnded;
        public static event OnPolygonEnded polygonLoaded;
        // Use this for initialization
        void Start()
        {
            if (raycastingCamera == null)
            {
                raycastingCamera = Camera.main;
            }
            if (polygonParent == null)
            {
                polygonParent = transform;
            }
            if (controlPointUiParent == null)
            {
                controlPointUiParent = new GameObject("ControlPointUIParent").transform;
                controlPointUiParent.SetParent(GameObject.Find("Canvas").transform);
            }
            plane = SetPlane(Vector3.zero, drawingPlane);
        }


        public void OnDestroySite()
        {
            if (creatingPolygon)
                StartCoroutine(AbortPolygon());
            else
            {

                GameObject deleteParent = new GameObject();
                List<Transform> temp_delete = new List<Transform>();
                foreach (Transform cp in controlPointUiParent)
                {
                    temp_delete.Add(cp);
                }
                foreach (Transform p in transform)
                {
                    temp_delete.Add(p);
                }
                foreach (Transform a in temp_delete)
                {
                    a.SetParent(deleteParent.transform);
                }
                temp_polygon = null;
                temp_points = null;
                DestroyImmediate(deleteParent);
            }
               
                addSiteButton.GetComponent<CloseSite>().siteStarted = false;
                addSiteButton.interactable = true;
                addSiteButton.GetComponentInChildren<Text>().text = "Set Site";
                raycastingCamera.GetComponent<DrawGLines>().polygonPoints = null;
        }
        void OnDestroy()
        {
            if (polygonEnded != null)
            {
                foreach (System.Delegate del in polygonEnded.GetInvocationList())
                {
                    polygonEnded -= (OnPolygonEnded)del;
                }

            }

            if (polygonLoaded != null)
            {
                foreach (System.Delegate del in polygonLoaded.GetInvocationList())
                {
                    polygonLoaded -= (OnPolygonEnded)del;
                }
            }

        }

        // Update is called once per frame
        void Update()
        {

            if (creatingPolygon)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    AddPoint();
                }

                if (Input.GetKeyDown(KeyCode.Escape) || CustomInput.GetAxis("GUIEscape") == 3)
                {
                    StartCoroutine(AbortPolygon());

                }

                if (Input.GetKeyDown(KeyCode.Return))
                {
                    EndPolygon();
                }
            }

            if (Input.GetKeyDown(KeyCode.Delete) || CustomInput.GetAxis("GUIDelete") == 3)
            {
                if (creatingPolygon)
                {
                    UndoPointAddition();
                }
            }

        }

       
        public void ClosePolygonUI()
        {
            EndPolygon();
        }
        public void OnLoadingPolygon()
        {
            if (resourcesMan.lastSavedState != null)
            {
                LoadPolygon(resourcesMan.lastSavedState.site);


            }
        }


        public virtual void StartPolygon()
        {
            temp_Vertices = new List<PolygonVertex>();
            temp_points = new List<Vector3>();
            creatingPolygon = true;
            temp_polygon = Instantiate(polygonPrefab, polygonParent).GetComponent<Polygon>();
            temp_polygon.updated += OnPolygonUpdated;
            temp_polygon.deleted += OnPolygonDeleted;
        }

        public virtual void AddPoint()
        {
            bool eventCheck = Input.touchCount > 0 ? !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId) : !EventSystem.current.IsPointerOverGameObject(-1);
            if (eventCheck)
            {
                Ray r = raycastingCamera.ScreenPointToRay(Input.mousePosition);
                float dist = 0;
                if (plane.Raycast(r, out dist))
                {
                    Vector3 point = r.GetPoint(dist);
                    if (polygonVertexParent == null)
                    {
                        polygonVertexParent = temp_polygon.transform;
                    }
                    PolygonVertex pVertex = Instantiate(polygonVertexPrefab).GetComponent<PolygonVertex>();
                    pVertex.transform.position = point;
                    pVertex.Initialize(temp_Vertices.Count, this);
                    ControlPointUI cpui = Instantiate(controlPointUIPrefab, controlPointUiParent).GetComponent<ControlPointUI>();
                    cpui.Initialize(pVertex.transform);
                    pVertex.SetGUIElement(cpui);
                    pVertex.SetPosition(point);
                    if (temp_Vertices.Count == 0)
                    {
                        cpui.onSelected += OnFisrtCPClicked;
                    }
                    temp_Vertices.Add(pVertex);
                    temp_points.Add(pVertex.currentPosition);
                    raycastingCamera.GetComponent<DrawGLines>().polygonPoints = temp_points.ToArray();
                }
            }
        }

        public virtual void AddPoint(Vector3 point)
        {
            if (polygonVertexParent == null)
            {
                polygonVertexParent = temp_polygon.transform;
            }

            PolygonVertex pVertex = Instantiate(polygonVertexPrefab).GetComponent<PolygonVertex>();
            pVertex.transform.position = point;
            pVertex.Initialize(temp_Vertices.Count, this);
            ControlPointUI cpui = Instantiate(controlPointUIPrefab, controlPointUiParent).GetComponent<ControlPointUI>();
            cpui.Initialize(pVertex.transform);
            pVertex.SetGUIElement(cpui);
            pVertex.SetPosition(point);
            if (temp_Vertices.Count == 0)
            {
                cpui.onSelected += OnFisrtCPClicked;
            }
            temp_Vertices.Add(pVertex);
            temp_points.Add(pVertex.currentPosition);
            raycastingCamera.GetComponent<DrawGLines>().polygonPoints = temp_points.ToArray();
        }

        public virtual void EndPolygon(bool loaded = false)
        {
            List<PolygonVertex> m_vertices = new List<PolygonVertex>();
            for (int i = 0; i < temp_Vertices.Count; i++)
            {
                m_vertices.Add(temp_Vertices[i]);
            }
            if (temp_lineRenderer != null && temp_lineRenderer.gameObject != null)
            {
                Destroy(temp_lineRenderer.gameObject);
            }
            temp_polygon.Initialize(m_vertices);
            for (int i = 0; i < m_vertices.Count; i++)
            {
                m_vertices[i].transform.SetParent(polygonVertexParent);
            }
            temp_points.Add(temp_points[0]);
            raycastingCamera.GetComponent<DrawGLines>().polygonPoints = temp_points.ToArray();
            temp_Vertices.Clear();
            creatingPolygon = false;

            var offset = temp_polygon.Offset(offsetValue);
            offset.gameObject.name += "offset";
            offset.transform.SetParent(transform);
            offset.layer = 29;
            offset.GetComponent<Polygon>().basePolygon = temp_polygon;
            if (!loaded)
            {
                if (polygonEnded != null)
                {
                    polygonEnded(temp_polygon, offset.GetComponent<Polygon>());
                    setGrid.interactable = true;
                   
                }
            }
            else
            {
                if (polygonLoaded != null)
                {
                    polygonLoaded(temp_polygon, offset.GetComponent<Polygon>());
                    setGrid.interactable = true;
                    

                }
            }

            addSiteButton.interactable = false;

        }

        public virtual IEnumerator AbortPolygon()
        {
            for (int i = 0; i < temp_Vertices.Count; i++)
            {
                Destroy(temp_Vertices[i].cpui.gameObject);
                Destroy(temp_Vertices[i].gameObject);
            }
            yield return new WaitForEndOfFrame();

            temp_polygon = null;
            temp_points = null;
            raycastingCamera.GetComponent<DrawGLines>().polygonPoints = null;
            temp_Vertices.Clear();
            creatingPolygon = false;
            addSiteButton.interactable = true;
        }

        public void LoadPolygon(Site site)
        {
            Site thisSite = site;
            StartPolygon();
            for (int i = 0; i < thisSite.controlPoints.Count; i++)
            {
                AddPoint(new Vector3(thisSite.controlPoints[i][0], thisSite.controlPoints[i][1], thisSite.controlPoints[i][2]));
            }
            EndPolygon(true);
        }

        public void OnPolygonUpdated(Polygon sender)
        {
            var verts = sender.GetVectors();
            var corners = sender.GetBoundingBox(transform);
            raycastingCamera.GetComponent<DrawGLines>().polygonPoints = verts.ToArray();
        }

        public void OnPolygonDeleted(Polygon sender)
        {
            sender.updated -= OnPolygonUpdated;
            sender.updated -= OnPolygonDeleted;
        }

        public void UndoPointAddition()
        {
            Destroy(temp_Vertices[temp_Vertices.Count - 1].gameObject);
            Destroy(temp_Vertices[temp_Vertices.Count - 1].cpui.gameObject);
            temp_Vertices.RemoveAt(temp_Vertices.Count - 1);
            temp_points.RemoveAt(temp_points.Count - 1);
            raycastingCamera.GetComponent<DrawGLines>().polygonPoints = temp_points.ToArray();
        }

        public void OnFisrtCPClicked(ControlPointUI sender)
        {
            if (temp_Vertices != null && temp_Vertices.Count > 2)
            {
                if (sender == temp_Vertices[0].cpui)
                {
                    if (creatingPolygon)
                    {
                        EndPolygon();
                    }
                }
            }
        }

        public Plane SetPlane(Vector3 origin, DrawingPlane orientation)
        {
            switch (orientation)
            {
                case DrawingPlane.XY:
                    return new Plane(Vector3.forward, origin);
                case DrawingPlane.XZ:
                    return new Plane(Vector3.up, origin);
                case DrawingPlane.ZY:
                    return new Plane(Vector3.right, origin);
                default:
                    return new Plane();
            }
        }

        public void GenerateTempLineRenderer()
        {
            GameObject gameObject = new GameObject("TempLineRenderer");
            temp_lineRenderer = gameObject.AddComponent<LineRenderer>();
            Material mat = new Material(Shader.Find("Standard"));
            mat.color = Color.red;
            temp_lineRenderer.material = mat;
            temp_lineRenderer.startWidth = 0.5f;
            temp_lineRenderer.endWidth = 0.5f;
        }

        public List<PolygonVertex> GetPolygonVertices()
        {
            throw new System.NotImplementedException();
        }
    }
}
