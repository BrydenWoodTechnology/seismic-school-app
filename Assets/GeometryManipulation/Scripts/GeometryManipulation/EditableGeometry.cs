﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


namespace BrydenWoodUnity.GeometryManipulation
{
    public class EditableGeometry : SelectableGeometry
    {
        [HideInInspector]
        public Extrusion extrusion;
        public Material selectMaterial;
        public bool moveable = false;
        public bool editEdges = true;

        protected Material originalMaterial;

        public override void SelectHighlight(bool select)
        {
            if (select)
            {
                GetComponent<MeshRenderer>().material = selectMaterial;
                if (extrusion != null)
                {
                    extrusion.GetComponent<MeshRenderer>().material = selectMaterial;
                }
                if (moveable)
                {
                    DisplayMoveUI();
                }
                if (editEdges)
                {
                    DisplayEdgesUI();
                }
            }
            else
            {
                GetComponent<MeshRenderer>().material = originalMaterial;
                if (extrusion != null)
                {
                    extrusion.GetComponent<MeshRenderer>().material = originalMaterial;
                }
                if (moveable)
                {
                    HideMoveUI();
                }
                if (editEdges)
                {
                    HideEdgesUI();
                }
            }
        }

        protected virtual void DisplayMoveUI()
        {

        }

        protected virtual void DisplayEdgesUI()
        {

        }

        protected virtual void HideMoveUI()
        {

        }

        protected virtual void HideEdgesUI()
        {

        }
    }
}
