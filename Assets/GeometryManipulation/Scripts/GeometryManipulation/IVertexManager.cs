﻿using BrydenWoodUnity.GeometryManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrydenWoodUnity.GeometryManipulation
{
    public interface IVertexManager
    {

        List<PolygonVertex> GetPolygonVertices();
    }
}
