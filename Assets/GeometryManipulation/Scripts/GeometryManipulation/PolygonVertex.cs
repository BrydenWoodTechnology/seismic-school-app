﻿using BrydenWoodUnity.UIElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BrydenWoodUnity.GeometryManipulation
{

    /// <summary>
    /// A Polygon Vertex Unity component
    /// </summary>
    public class PolygonVertex : MonoBehaviour
    {
        public float snapThreshold = 1.0f;
        /// <summary>
        /// The initial position of the vertex component
        /// </summary>
        public Vector3 initialPosition { get; set; }

        /// <summary>
        /// The current position of the polygon Vertex
        /// </summary>
        public Vector3 currentPosition { get; private set; }
        public Vector3 currentLocalPosition { get; private set; }

        /// <summary>
        /// The index of this vertex
        /// </summary>
        public int index { get; private set; }

        public IVertexManager vertexManager { get; private set; }
        public List<Polygon> polygons { get; set; }
        public Polygon polygon { get; set; }
        public ControlPointUI cpui { get; private set; }

        private List<int> polygonVerts;

        #region Events
        /// <summary>
        /// Triggered when the vertex has been moved
        /// </summary>
        public event VertexMoved vertexMoved;
        /// <summary>
        /// Called when the vertex is being moved in order to trigger the corresponding event
        /// </summary>
        /// <param name="sender">The sender of the event</param>
        public void OnMoved(object sender)
        {
            currentPosition = transform.position;
            currentLocalPosition = transform.localPosition;
            //VertexSnap();
            if (vertexMoved != null)
            {
                vertexMoved(this);
            }
            if (moved != null)
            {
                moved(this);
            }
        }

        public void OnMoved()
        {
            OnMoved(this);
        }

        public static event VertexMoved moved;
        #endregion



        private void Start()
        {
            currentPosition = transform.localPosition;
            currentLocalPosition = transform.localPosition;
        }

        private void Update()
        {

        }

        private void OnDisable()
        {
            if (currentPosition != transform.position)
            {
                currentPosition = transform.position;
            }
            if (currentLocalPosition != transform.localPosition)
            {
                currentLocalPosition = transform.localPosition;
            }
        }

        #region Public Methods
        public void Initialize(int index, IVertexManager vertexManager)
        {
            this.index = index;
            this.vertexManager = vertexManager;
            initialPosition = transform.position;
            currentPosition = transform.position;
            currentLocalPosition = transform.localPosition;
            polygons = new List<Polygon>();
            polygonVerts = new List<int>();
        }

        public void Initialize(int index)
        {
            this.index = index;

            initialPosition = transform.position;
            currentPosition = transform.position;
            currentLocalPosition = transform.localPosition;
            polygons = new List<Polygon>();
            polygonVerts = new List<int>();
        }

        public void UpdatePosition()
        {
            currentPosition = transform.position;
            currentLocalPosition = transform.localPosition;
        }

        public void UpdatePosition(Vector3 position)
        {
            currentPosition = position;
            currentLocalPosition = transform.localPosition;
        }

        public void SetPolygon(Polygon polygon, bool polygonChild = false)
        {
            this.polygon = polygon;
            if (polygonChild)
            {
                transform.SetParent(polygon.transform);
                currentLocalPosition = transform.localPosition;
            }
            for (int i = 0; i < polygon.vertices.Count; i++)
            {
                polygonVerts.Add(polygon.vertices[i].index);
            }
        }

        public void SetPosition(Vector3 pos)
        {
            transform.position = pos;
            currentPosition = pos;
            currentLocalPosition = transform.localPosition;
            if (cpui != null)
                cpui.UpdatePositions(transform);
            OnMoved(this);
        }

        public void SetLocalPosition(Vector3 pos)
        {
            transform.localPosition = pos;
            currentLocalPosition = pos;
            currentPosition = transform.position;
            cpui.UpdatePositions(transform);
            OnMoved(this);
        }

        public void UpdateLocalPosition()
        {
            currentLocalPosition = transform.localPosition;
            currentPosition = transform.position;
        }
        public void UpdateLocalPosition(Vector3 pos)
        {
            transform.localPosition = pos;
            currentLocalPosition = pos;
            currentPosition = transform.position;
        }

        public void AddPolygon(Polygon polygon)
        {
            polygons.Add(polygon);
            for (int i = 0; i < polygon.vertices.Count; i++)
            {
                polygonVerts.Add(polygon.vertices[i].index);
            }
        }
        /// <summary>
        /// Sets the GUI element to be used for moving the vertex
        /// </summary>
        /// <param name="cpui">The Control Point UI element for moving the vertex</param>
        public void SetGUIElement(ControlPointUI cpui)
        {
            this.cpui = cpui;
            this.cpui.moved += OnMoved;
        }

        public void VertexSnap()
        {
            PolygonVertex secondClosest;
            PolygonVertex thirdClosest;
            PolygonVertex closest = FindClosestVertex(vertexManager.GetPolygonVertices(), out secondClosest, out thirdClosest);


            if (secondClosest != null && thirdClosest != null && closest != null)
            {
                Vector3 lineA = secondClosest.currentPosition - closest.currentPosition;
                Vector3 m_vecA = closest.currentPosition - currentPosition;
                Vector3 lineB = thirdClosest.currentPosition - closest.currentPosition;
                Vector3 m_vecB = closest.currentPosition - currentPosition;
                Vector3 line;
                Vector3 endPoint;

                Vector3 projectionA = BrydenWoodUtils.ProjectOnCurve(closest.currentPosition, secondClosest.currentPosition, currentPosition);
                Vector3 projectionB = BrydenWoodUtils.ProjectOnCurve(closest.currentPosition, thirdClosest.currentPosition, currentPosition);
                Vector3 projection;

                float lineDistA = Vector3.Distance(projectionA, currentPosition);
                float lineDistB = Vector3.Distance(projectionB, currentPosition);
                float lineDist;

                if (lineDistA < lineDistB)
                {
                    lineDist = lineDistA;
                    projection = projectionA;
                    line = lineA;
                    endPoint = secondClosest.currentPosition;
                }
                else
                {
                    lineDist = lineDistB;
                    projection = projectionB;
                    line = lineB;
                    endPoint = thirdClosest.currentPosition;
                }


                float dist = Vector3.Distance(closest.currentPosition, currentPosition);
                bool inside = (Vector3.Distance(projection, closest.currentPosition) < Vector3.Distance(closest.currentPosition, endPoint)) && (Vector3.Distance(projection, endPoint) < Vector3.Distance(closest.currentPosition, endPoint));

                if (lineDist < snapThreshold && inside)
                {
                    transform.position = projection;
                    currentPosition = transform.position;
                    if (cpui != null)
                    {
                        cpui.UpdateUIPosition();
                    }
                }

                if (dist < snapThreshold)
                {
                    transform.position = closest.currentPosition;
                    currentPosition = transform.position;
                    if (cpui != null)
                    {
                        cpui.UpdateUIPosition();
                    }
                }
            }
        }

        public PolygonVertex FindClosestVertex(List<PolygonVertex> vertices, out PolygonVertex secondClose, out PolygonVertex thirdClose)
        {
            PolygonVertex closest = null;
            secondClose = null;
            thirdClose = null;
            Dictionary<PolygonVertex, float> distances = new Dictionary<PolygonVertex, float>();
            for (int i = 0; i < vertices.Count; i++)
            {
                if (!polygonVerts.Contains(vertices[i].index) && (vertices[i].currentPosition.y == currentPosition.y))
                {
                    float d = Vector3.Distance(currentPosition, vertices[i].currentPosition);
                    distances.Add(vertices[i], d);
                }
            }
            if (distances.Count > 0)
            {
                var sortClose = distances.OrderBy(x => x.Value).ToList();
                closest = sortClose[0].Key;
            }

            if (closest != null)
            {
                Polygon snapPolygon = closest.polygon;
                if (snapPolygon != null)
                {
                    for (int i = 0; i < snapPolygon.vertices.Count; i++)
                    {
                        if (snapPolygon.vertices[i].index == closest.index)
                        {
                            int nextIndex = (i + 1) % snapPolygon.vertices.Count;
                            int prevIndex = i - 1;
                            if (i == 0)
                            {
                                prevIndex = snapPolygon.vertices.Count - 1;
                            }
                            secondClose = snapPolygon.vertices[nextIndex];
                            thirdClose = snapPolygon.vertices[prevIndex];
                        }
                    }
                }
            }

            return closest;
        }

        public PolygonVertex FindClosestVertex(List<PolygonVertex> vertices, out List<PolygonVertex> secondary)
        {
            PolygonVertex closest = null;
            Dictionary<PolygonVertex, float> distances = new Dictionary<PolygonVertex, float>();
            for (int i = 0; i < vertices.Count; i++)
            {
                if (!polygonVerts.Contains(vertices[i].index))
                {
                    float d = Vector3.Distance(currentPosition, vertices[i].currentPosition);
                    distances.Add(vertices[i], d);
                }
            }
            var sortClose = distances.OrderBy(x => x.Value).ToList();
            closest = sortClose[0].Key;

            List<Polygon> snapPolygon = closest.polygons;
            secondary = new List<PolygonVertex>();

            for (int j = 0; j < snapPolygon.Count; j++)
            {
                for (int i = 0; i < snapPolygon[j].vertices.Count; i++)
                {
                    if (snapPolygon[j].vertices[i].index == closest.index)
                    {
                        int nextIndex = (i + 1) % snapPolygon[j].vertices.Count;
                        int prevIndex = i - 1;
                        if (i == 0)
                        {
                            prevIndex = snapPolygon[j].vertices.Count - 1;
                        }
                        secondary.Add(snapPolygon[j].vertices[nextIndex]);
                        secondary.Add(snapPolygon[j].vertices[prevIndex]);
                    }
                }
            }

            return closest;
        }
        #endregion

    }
}
