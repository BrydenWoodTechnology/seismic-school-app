﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using BrydenWoodUnity.Navigation;

namespace BrydenWoodUnity.GeometryManipulation
{
    public class SelectableGeometry: MonoBehaviour
    {
        public delegate void OnSelect(SelectableGeometry sender);
        public static event OnSelect selected;

        public bool isFirst;     

        protected virtual void OnSelected()
        {
            if (selected != null)
            {
                selected(this);
            }
        }

        public virtual void Select()
        {
           
            OnSelected();
            SelectHighlight(true);
        }

        public virtual void DeSelect()
        {
            SelectHighlight(false);
        }

        public void OnMirror()
        {
            SelectHighlight(true); 
        }

        public virtual void SelectHighlight(bool select)
        {
            var drawLines = Camera.main.GetComponent<DrawGLines>();
            if (drawLines != null)
            {
                if(isFirst)
                {
                    drawLines.SwitchMaterial(true);
                }
                if (select)
                {
                    List<Vector3> corners = new List<Vector3>();
                    var collider = GetComponent<BoxCollider>();
                    if (collider != null)
                    {
                        var size = collider.bounds.size;
                        var myX = Mathf.Abs(size.x + (size.x * transform.localScale.x));
                        var myY = Mathf.Abs(size.y);
                        var myZ = Mathf.Abs(size.z + (size.z * transform.localScale.z));

                        corners.Add(transform.TransformPoint(new Vector3(0, 0, 0)));
                        corners.Add(transform.TransformPoint(new Vector3(0, 0, myZ)));
                        corners.Add(transform.TransformPoint(new Vector3(myX, 0, myZ)));
                        corners.Add(transform.TransformPoint(new Vector3(myX, 0, 0)));

                        corners.Add(transform.TransformPoint(new Vector3(0, myY, 0)));
                        corners.Add(transform.TransformPoint(new Vector3(0, myY, myZ)));
                        corners.Add(transform.TransformPoint(new Vector3(myX, myY, myZ)));
                        corners.Add(transform.TransformPoint(new Vector3(myX, myY, 0)));

                        drawLines.selectedPrefab = new List<Vector3[]>();
                        drawLines.selectedPrefab.Add(corners.ToArray());
                    }
                }
                else
                {
                    drawLines.selectedPrefab = null;
                }
            }
        }
    }
}
