﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using BWG = BrydenWood.Geometry;
using System;
using BrydenWoodUnity.UIElements;
using System.Linq;

namespace BrydenWoodUnity.GeometryManipulation
{
    public delegate void VertexMoved(PolygonVertex polygonVertex);
    public delegate void MeshVertexMoved(MeshVertex meshVertex);

    public struct EdgeRelation
    {
        public List<PolygonVertex> vertices;
        public List<Vector3> originalDifs;
    }

    public struct MeshEdgeRelation
    {
        public List<MeshVertex> vertices;
        public List<Vector3> originalDifs;
    }

    /// <summary>
    /// A polygon Unity component
    /// </summary>
    [RequireComponent(typeof(MeshFilter))]
    public class Polygon : EditableGeometry
    {
        #region Public Fields
        /// <summary>
        /// The mesh filter which will be used for the mesh
        /// </summary>
        [Header("Unity specific fields:")]
        public MeshFilter meshFilter;
        public GameObject moveUiElement;
        public GameObject edgeUiElement;
        public GameObject controlPoint;
        public bool isOffseted;
        public Vector3 Center { get {


                Vector3 myCenter = Vector3.zero;
                for(int i=0;i<vertices.Count;i++)
                {
                    myCenter += vertices[i].currentPosition;
                }
                return myCenter / vertices.Count;
            } }


        /// <summary>
        /// The area of the polygon
        /// </summary>
        [Space(10)]
        [Header("Geometry specific fields:")]
        public bool generateMesh = true;
        public bool visibleMesh = false;
        [Tooltip("The area of the polygon (read-only)")]
        public float area;

        /// <summary>
        /// The perimeter of the polygon 
        /// </summary>
        [Tooltip("The perimeter of the polygon (read-only)")]
        public float perimeter;

        [Tooltip("The offset from base to avoid Z-fighting")]
        public float offsetFromBase = 0.1f;
        /// <summary>
        /// Whether the resulting mesh should be flipped
        /// </summary>
        public bool flip = false;
        public bool capped = false;
        public bool isClockWise = true;

        /// <summary>
        /// The mesh of the polygon
        /// </summary>
        public Mesh mesh { get; set; }
        /// <summary>
        /// The vertices of the polygon as a list of PolygonVertex
        /// </summary>
        public List<PolygonVertex> vertices { get; set; }
        public ControlPointUI movePoint { get; set; }

        public bool hasListeners { get; private set; }
        public Polygon basePolygon;
        public float offsetDist;
        #endregion

        #region Private Fields
        protected int[] triangles { get; set; }
        private List<GameObject> edgeControlPoints { get; set; }
        private Dictionary<ControlPointUI, EdgeRelation> edges { get; set; }
        private ControlPointUI edgeControlPointSelected { get; set; }
        #endregion

        #region Events
        public delegate void PolygonUpdated(Polygon sender);
        public event PolygonUpdated updated;
        public event PolygonUpdated stoppedMoving;
        public event PolygonUpdated deleted;

        protected virtual void OnUpdated()
        {
            if (updated != null)
            {
                updated(this);
            }
        }
        #endregion

        // Use this for initialization
        void Start()
        {
            //vertices = new List<PolygonVertex>();
            if (meshFilter == null)
            {
                meshFilter = GetComponent<MeshFilter>();
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnDestroy()
        {
            if (vertices != null && vertices.Count != 0)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    vertices[i].vertexMoved -= UpdateGeometry;
                    if (vertices[i].gameObject != null)
                    {
                        Destroy(vertices[i].gameObject);
                    }
                   // Destroy(vertices[i].cpui.gameObject);
                }
            }
            if (deleted != null)
            {
                deleted(this);
            }
        }

        void OnEnable()
        {
            if (vertices != null)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    vertices[i].gameObject.SetActive(true);
                }
            }
        }

        void OnDisable()
        {
            if (vertices != null && vertices.Count > 0)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    if (vertices[i].gameObject != null)
                    {
                        vertices[i].gameObject.SetActive(false);
                    }
                }
            }
            if (movePoint != null)
            {
                HideMoveUI();
            }
            if (edges != null)
            {
                HideEdgesUI();
            }
        }

        public List<Vector3> GetVectors()
        {
            List<Vector3> positions = new List<Vector3>();
            for (int i = 0; i < vertices.Count; i++)
            {
                positions.Add(vertices[i].transform.position);
            }
            positions.Add(vertices[0].transform.position);
            return positions;
        }

        public Vector3[] GetBoundingBox(Transform transform)
        {
            Vector3[] corners = new Vector3[4];

            List<Vector3> relativePoints = new List<Vector3>();
            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i].UpdateLocalPosition();
                relativePoints.Add(transform.InverseTransformPoint(vertices[i].currentPosition));
            }

            var xValues = relativePoints.Select(x => x.x).ToList();
            //xValues.Sort();
            var zValues = relativePoints.Select(x => x.z).ToList();
            //zValues.Sort();

            var xMax = xValues.Max();
            var xMin = xValues.Min();
            var zMax = zValues.Max();
            var zMin = zValues.Min();
            var y = transform.InverseTransformPoint(vertices[0].currentPosition).y;
            corners[0] = transform.TransformPoint(new Vector3(xMin, y, zMin));
            corners[1] = transform.TransformPoint(new Vector3(xMin, y, zMax));
            corners[2] = transform.TransformPoint(new Vector3(xMax, y, zMax));
            corners[3] = transform.TransformPoint(new Vector3(xMax, y, zMin));

            return corners;
        }

        public bool IsIncluded(Vector3 point)
        {
            Vector3 infinity = point + new Ray(point, Vector3.right).GetPoint(100000);
            int counter = 0;
            for (int i=0; i<vertices.Count; i++)
            {
                int next = (i + 1) % vertices.Count;
                Vector p1 = new Vector(point.x, point.z);
                Vector p2 = new Vector(infinity.x, infinity.z);
                Vector p3 = new Vector(vertices[i].currentPosition.x,vertices[i].currentPosition.z);
                Vector p4 = new Vector(vertices[next].currentPosition.x, vertices[next].currentPosition.z);
                Vector intersection;
                if (BrydenWoodUtils.LineSegementsIntersect(p1,p2,p3,p4,out intersection, false))
                {
                    counter++;
                }
            }

            if (counter % 2 == 1)
            {
                return true;
            }
            else 
            {
                return false;
            }
        }

        #region Public Methods
        /// <summary>
        /// Initiates the polygon component
        /// </summary>
        /// <param name="vertices">The list of PolygonVertex to be used as vertices by the Polygon</param>
        public virtual void Initialize(List<PolygonVertex> vertices, bool verticesChildren = false)
        {
            Vector3 centre = new Vector3();
            if (vertices.Count != 0)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    centre += vertices[i].currentPosition;
                }
                centre /= vertices.Count;
                try
                {
                    transform.localPosition = centre;
                }
                catch (Exception e)
                {
                    Debug.Log("Invalid polygon centre");
                }
            }
            else
            {
                Debug.Log("Zero list vertices!");
            }
            GetComponent<MeshRenderer>().enabled = visibleMesh;
            SetPolyline(vertices);
            GenerateMesh();

            UpdateOutline();
            originalMaterial = GetComponent<MeshRenderer>().material;
        }

        /// <summary>
        /// Sets the polyline of the vertices of the polygon
        /// </summary>
        /// <param name="vertices">The list of PolygonVertex to be used as vertices by the Polygon</param>
        public void SetPolyline(List<PolygonVertex> vertices, bool verticesChildren = false)
        {
            if (this.vertices != null && this.vertices.Count != 0)
            {
                for (int i = 0; i < this.vertices.Count; i++)
                {
                    this.vertices[i].vertexMoved -= UpdateGeometry;
                }
            }
            this.vertices = new List<PolygonVertex>();
            this.vertices = vertices;
            List<Vector3> verts = new List<Vector3>();
            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i].SetPolygon(this);
                verts.Add(vertices[i].currentPosition);
                vertices[i].vertexMoved += UpdateGeometry;
            }
            isClockWise = IsClockWise(verts);
            hasListeners = true;
        }

        public void RemoveListeners()
        {
            if (hasListeners)
            {
                if (this.vertices != null && this.vertices.Count != 0)
                {
                    for (int i = 0; i < this.vertices.Count; i++)
                    {
                        this.vertices[i].vertexMoved -= UpdateGeometry;
                    }
                }
            }
            hasListeners = false;
        }

        public void AddListeners()
        {
            if (!hasListeners)
            {
                if (this.vertices != null && this.vertices.Count != 0)
                {
                    for (int i = 0; i < this.vertices.Count; i++)
                    {
                        this.vertices[i].vertexMoved += UpdateGeometry;
                    }
                }
            }
            hasListeners = true;
        }

        public void OnMoved(object sender)
        {
            HideEdgesUI();
            if (vertices != null)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    vertices[i].cpui.gameObject.SetActive(false);
                }
            }
            if (updated != null)
            {
                updated(this);
            }
        }

        public void OnStoppedMoving(object sender)
        {
            if (stoppedMoving != null)
            {
                stoppedMoving(this);
            }

            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i].cpui.gameObject.SetActive(true);
                var transVec = transform.localPosition + mesh.vertices[i] - new Vector3(0, offsetFromBase, 0);
                vertices[i].UpdateLocalPosition(transVec);
                vertices[i].cpui.UpdateUIPosition();
            }
            HideMoveUI();
            DisplayMoveUI();
            DisplayEdgesUI();
        }

        public void SetOriginalMaterial(Material material)
        {
            originalMaterial = material;
        }

        public void DisplayStandardMode()
        {
            GetComponent<MeshRenderer>().material = originalMaterial;
            extrusion.GetComponent<MeshRenderer>().material = originalMaterial;
            extrusion.DisplayStandardMode();
        }

        public void UpdateGeometry()
        {
            GenerateMesh();

            UpdateOutline();
        }

        public void ShowControlPoints(bool show)
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i].cpui.gameObject.SetActive(show);
                vertices[i].gameObject.SetActive(show);
            }
        }
        /// <summary>
        /// Offsets the polygon based on the given distance
        /// </summary>
        /// <param name="distance">The offset distance</param>
        /// <returns>GameObject of the polygon</returns>
        public GameObject Offset(float distance)
        {

            offsetDist = distance;
            List<Vector3> linePoints = new List<Vector3>();
            for (int i = 0; i < vertices.Count; i++)
            {
                int next = (i + 1) % vertices.Count;
                int prev = i - 1;
                if (i - 1 == -1)
                {
                    prev = vertices.Count - 1;
                }
                Vector3 v1 = vertices[prev].currentPosition - vertices[i].currentPosition;
                Vector3 v2 = vertices[next].currentPosition - vertices[i].currentPosition;
                v1.Normalize();
                v2.Normalize();
                var cross = Vector3.Cross(v1, v2);
                float angle = Vector3.Angle(v1, v2);
                float vectorLength;
                if (Mathf.Sin(Mathf.Deg2Rad * angle * 0.5f) != 0)
                {
                    vectorLength = (distance) / Mathf.Sin(Mathf.Deg2Rad * angle * 0.5f);
                }
                else
                {
                    vectorLength = (distance);
                }
                var addition = (v1 + v2);
                addition.Normalize();
                addition *= vectorLength;
                if (Vector3.Cross(v1, v2).y < 0)
                {
                    addition *= -1;
                }
                if (isClockWise)
                {
                    addition *= -1;
                }

                if (addition.magnitude == 0)
                {
                    addition = Vector3.Cross(v1, Vector3.up);
                    addition.Normalize();
                    addition *= -vectorLength;
                }

                linePoints.Add(vertices[i].currentPosition + addition);
            }

            Polygon newPolygon = Instantiate(Resources.Load("Polygon") as GameObject).GetComponent<Polygon>();
            newPolygon.transform.position = transform.position;
            newPolygon.isOffseted = true;
            newPolygon.offsetDist = distance;
            this.updated += newPolygon.UpdateOffset;

            List<PolygonVertex> cpts = new List<PolygonVertex>();

            for (int i = 0; i < linePoints.Count; i++)
            {
                PolygonVertex pt = Instantiate(Resources.Load("PolygonVertex") as GameObject, newPolygon.transform).GetComponent<PolygonVertex>();
                pt.transform.position = linePoints[i];
                pt.Initialize(i);
                pt.UpdatePosition();
                cpts.Add(pt);
            }

            newPolygon.Initialize(cpts);
            newPolygon.gameObject.layer = 29;
            return newPolygon.gameObject;
        }

        public void UpdateOffset(Polygon externalPoly)
        {
            for (int i = 0; i < externalPoly.vertices.Count; i++)
            {
                int next = (i + 1) % externalPoly.vertices.Count;
                int prev = i - 1;
                if (i - 1 == -1)
                {
                    prev = externalPoly.vertices.Count - 1;
                }
                Vector3 v1 = externalPoly.vertices[prev].currentPosition - externalPoly.vertices[i].currentPosition;
                Vector3 v2 = externalPoly.vertices[next].currentPosition - externalPoly.vertices[i].currentPosition;
                v1.Normalize();
                v2.Normalize();
                var cross = Vector3.Cross(v1, v2);
                float angle = Vector3.Angle(v1, v2);
                float vectorLength;
                if (Mathf.Sin(Mathf.Deg2Rad * angle * 0.5f) != 0)
                {
                    vectorLength = (offsetDist) / Mathf.Sin(Mathf.Deg2Rad * angle * 0.5f);
                }
                else
                {
                    vectorLength = (offsetDist);
                }
                var addition = (v1 + v2);
                addition.Normalize();
                addition *= vectorLength;
                if (Vector3.Cross(v1, v2).y < 0)
                {
                    addition *= -1;
                }
                if (isClockWise)
                {
                    addition *= -1;
                }

                if (addition.magnitude == 0)
                {
                    addition = Vector3.Cross(v1, Vector3.up);
                    addition.Normalize();
                    addition *= -vectorLength;
                }
                vertices[i].SetPosition(externalPoly.vertices[i].currentPosition + addition);
                //linePoints.Add(vertices[i].currentPosition + addition);
            }

            if(updated!=null)
            {
                updated(this);

            }


        }
        #endregion

        #region Private Methods
        protected override void DisplayMoveUI()
        {
            GameObject m_parent = GameObject.Find("Canvas");
            movePoint = Instantiate(moveUiElement, m_parent.transform).GetComponent<ControlPointUI>();
            movePoint.Initialize(transform);
            movePoint.moved += OnMoved;
            movePoint.stoppedMoving += OnStoppedMoving;
        }

        protected override void HideMoveUI()
        {
            if (movePoint != null)
            {
                movePoint.moved -= OnMoved;
                movePoint.stoppedMoving -= OnStoppedMoving;
                Destroy(movePoint.gameObject);
                movePoint = null;
            }
        }

        protected override void DisplayEdgesUI()
        {
            if (edges == null)
            {
                edges = new Dictionary<ControlPointUI, EdgeRelation>();
                edgeControlPoints = new List<GameObject>();
                for (int i = 0; i < vertices.Count; i++)
                {
                    int next = (i + 1) % vertices.Count;
                    GameObject m_parent = GameObject.Find("EdgeControlPointsUI");
                    if (m_parent == null)
                    {
                        m_parent = new GameObject("EdgeControlPointsUI");
                        m_parent.transform.SetParent(GameObject.Find("Canvas").transform);
                    }
                    var m_edgePoint = Instantiate(edgeUiElement, m_parent.transform).GetComponent<ControlPointUI>();
                    edgeControlPoints.Add(Instantiate(controlPoint));
                    edgeControlPoints[i].GetComponent<PolygonVertex>().SetGUIElement(m_edgePoint);
                    edgeControlPoints[i].transform.position = vertices[i].currentPosition + (vertices[next].currentPosition - vertices[i].currentPosition) / 2.0f;
                    m_edgePoint.Initialize(edgeControlPoints[i].transform);
                    m_edgePoint.axisConstrain = true;
                    m_edgePoint.axis = Vector3.Cross(Vector3.up, vertices[next].currentPosition - vertices[i].currentPosition).normalized;
                    m_edgePoint.moved += OnEdgeMoved;
                    m_edgePoint.onSelected += OnEdgeCPUISelected;
                    if (!edges.ContainsKey(m_edgePoint))
                    {
                        EdgeRelation m_relation = new EdgeRelation();
                        m_relation.vertices = new List<PolygonVertex>() { vertices[i], vertices[next] };
                        m_relation.originalDifs = new List<Vector3>();
                        m_relation.originalDifs.Add(vertices[i].currentPosition - edgeControlPoints[i].transform.position);
                        m_relation.originalDifs.Add(vertices[next].currentPosition - edgeControlPoints[i].transform.position);

                        edges.Add(m_edgePoint, m_relation);
                    }
                }
            }
        }

        private void OnEdgeCPUISelected(ControlPointUI cpui)
        {
            edgeControlPointSelected = cpui;
            EdgeRelation m_relation;
            if (edges.TryGetValue(cpui, out m_relation))
            {
                cpui.axis = Vector3.Cross(Vector3.up, m_relation.vertices[1].currentPosition - m_relation.vertices[0].currentPosition).normalized;
                m_relation.originalDifs = new List<Vector3>();
                for (int i = 0; i < 2; i++)
                {
                    m_relation.originalDifs.Add(m_relation.vertices[i].transform.position - cpui.sceneObject[0].transform.position);
                }
                edges[cpui] = m_relation;
            }
        }

        protected override void HideEdgesUI()
        {
            if (edges != null)
            {
                foreach (var item in edges)
                {
                    item.Key.moved -= OnEdgeMoved;
                    item.Key.onSelected -= OnEdgeCPUISelected;
                    if (item.Key.gameObject != null)
                    {
                        Destroy(item.Key.gameObject);
                    }
                }
            }
            if (edgeControlPoints != null)
            {
                foreach (var item in edgeControlPoints)
                {
                    if (item != null)
                    {
                        Destroy(item);
                    }
                }
            }
            edges = null;
            edgeControlPoints = null;
        }

        private void OnEdgeMoved(object sender)
        {
            ControlPointUI cpui = (ControlPointUI)sender;
            if (edgeControlPointSelected == cpui)
            {
                EdgeRelation relation;
                if (edges.TryGetValue(cpui, out relation))
                {
                    for (int i = 0; i < relation.originalDifs.Count; i++)
                    {
                        relation.vertices[i].SetPosition(cpui.sceneObject[0].transform.position + relation.originalDifs[i]);
                        relation.vertices[i].cpui.UpdateUIPosition();
                    }
                }
            }
        }

        private void UpdateGeometry(PolygonVertex vertex)
        {
            GenerateMesh();
            UpdateOutline();
        }

        protected void UpdateOutline()
        {
            //var drawLInes = Camera.main.GetComponent<DrawGLines>();
            //if (drawLInes!=null)
            //{
            //    Vector3[] pts = new Vector3[vertices.Count];
            //    for (int i=0; i<pts.Length; i++)
            //    {
            //        pts[i] = vertices[i].currentPosition;
            //    }
            //    if (isOffseted)
            //    {
            //        drawLInes.offsetPolyPoints = pts;
            //    }
            //    //else
            //    //{
            //    //    drawLInes.
            //    //}
            //}
        }

        protected void UpdateGeometryData()
        {
            area = CalculateArea();
            perimeter = CalculatePerimeter();
        }

        protected virtual void GenerateMesh()
        {
            if (generateMesh)
            {
                Vector2[] verts = new Vector2[vertices.Count];
                Vector3[] vert3D = new Vector3[vertices.Count];
                for (int i = 0; i < verts.Length; i++)
                {
                    vert3D[i] = vertices[i].currentPosition + new Vector3(0, offsetFromBase, 0);
                    vert3D[i] = transform.InverseTransformPoint(vert3D[i]);
                    verts[i] = new Vector2(vert3D[i].x, vert3D[i].z);
                }

                /*
                if (!isClockWise)
                {
                    verts = verts.Reverse().ToArray();
                }
                */
        /*
        var sebPol = new Sebastian.Geometry.Polygon(verts);
        var triang = new Sebastian.Geometry.Triangulator(sebPol);
        var tris = triang.Triangulate();
        */
        var triang = new TriangulatorSimple(verts);
                var tris = triang.Triangulate();


                mesh = new Mesh();
                mesh.vertices = vert3D;

                if (!capped)
                {
                    mesh.triangles = tris;
                    triangles = tris;
                }
                else
                {
                    triangles = new int[tris.Length];
                    for (int i = 0; i < tris.Length; i += 3)
                    {
                        triangles[i] = tris[i + 2];
                        triangles[i + 1] = tris[i + 1];
                        triangles[i + 2] = tris[i];
                    }
                    mesh.triangles = triangles;
                }


                Vector2[] m_uvs = new Vector2[mesh.vertexCount];
                for (int i = 0; i < m_uvs.Length; i++)
                {
                    m_uvs[i] = new Vector2(mesh.vertices[i].x, mesh.vertices[i].y);
                }
                mesh.uv = m_uvs;
                mesh.uv2 = m_uvs;
                mesh.uv3 = m_uvs;
                mesh.uv4 = m_uvs;

                UpdateMeshComponents();
                UpdateGeometryData();
            }
            OnUpdated();
        }

        protected void UpdateMeshComponents()
        {
            try
            {
                mesh.RecalculateNormals();
                mesh.RecalculateBounds();
                meshFilter.mesh = mesh;
                GetComponent<MeshCollider>().sharedMesh = mesh;
            }
            catch (Exception e)
            {
                Debug.Log(gameObject.name + ": " + e);
            }
        }

        public static bool IsClockWise(List<Vector3> points)
        {
            float sum = 0;
            for (int i = 0; i < points.Count; i++)
            {
                int next = (i + 1) % points.Count;
                sum += ((points[next].x - points[i].x) * (points[next].z + points[i].z));
            }
            return sum >= 0;
        }

        private float CalculateArea()
        {
            float m_area = 0;
            for (int i = 0; i < triangles.Length / 3; i++)
            {
                Vector3 a = vertices[triangles[i * 3]].transform.position;
                Vector3 b = vertices[triangles[i * 3 + 1]].transform.position;
                Vector3 c = vertices[triangles[i * 3 + 2]].transform.position;
                float partA = a.x * (b.z - c.z);
                float partB = b.x * (c.z - a.z);
                float partC = c.x * (a.z - b.z);
                m_area += Mathf.Abs((partA + partB + partC)) / 2.0f;
            }
            return m_area;
        }

        private float CalculatePerimeter()
        {
            float m_perimeter = 0;
            for (int i = 1; i < vertices.Count; i++)
            {
                m_perimeter += Vector3.Distance(vertices[i].currentPosition, vertices[i - 1].currentPosition);
            }
            return m_perimeter;
        }
        #endregion
    }
}
