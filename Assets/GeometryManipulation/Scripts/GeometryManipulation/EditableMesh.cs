﻿using BrydenWoodUnity.UIElements;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BrydenWoodUnity.GeometryManipulation
{
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    public class EditableMesh : EditableGeometry
    {
        /// <summary>
        /// The area of the polygon
        /// </summary>
        [Space(10)]
        [Header("Geometry specific fields:")]
        public bool initializeOnStart = false;
        [Tooltip("The area of the polygon (read-only)")]
        public float area;
        /// <summary>
        /// The perimeter of the polygon 
        /// </summary>
        [Tooltip("The perimeter of the polygon (read-only)")]
        public float perimeter;
        [Tooltip("The offset from base to avoid Z-fighting")]
        public float offsetFromBase = 0.1f;

        public bool flip = false;
        public bool capped = false;
        public bool isClockWise = true;
        public bool isBuildingVertex = true;
        public bool moveColinear = false;

        [HideInInspector]
        public Color typeColor;
        [HideInInspector]
        public ControlPointUI movePoint;
        public Transform verticesParent { get; set; }
        public Transform cpuiParent { get; set; }
        public GameObject vertexPrefab { get; set; }
        public GameObject cpuiPrefab { get; set; }
        public GameObject edgeUiElement { get; set; }
        public GameObject moveUiElement { get; set; }
        public bool hasListeners { get; private set; }
        public List<MeshVertex> vertices { get; set; }
        public Mesh mesh { get; set; }
        public List<EdgeHelpers.Edge> outterEdges { get; set; }

        private MeshFilter meshFilter { get; set; }
        private MeshRenderer meshRenderer { get; set; }
        private Material typeMaterial { get; set; }
        private List<GameObject> edgeControlPoints { get; set; }
        private Dictionary<ControlPointUI, MeshEdgeRelation> edges { get; set; }
        private ControlPointUI edgeControlPointSelected { get; set; }


        #region Events
        public delegate void MeshUpdated(EditableMesh sender);
        public event MeshUpdated updated;
        public event MeshUpdated stoppedMoving;

        protected virtual void OnUpdated()
        {
            if (updated != null)
            {
                updated(this);
            }
        }
        #endregion

        // Use this for initialization
        void Awake()
        {
            if (initializeOnStart)
            {
                Initialize();
            }
        }

        // Update is called once per frame
        void Update()
        {

            if (outterEdges != null)
            {
                for (int i=0; i< outterEdges.Count; i++)
                {
                    Debug.DrawLine(mesh.vertices[outterEdges[i].v1], mesh.vertices[outterEdges[i].v2],Color.red);
                }
            }
        }

        private void OnDestroy()
        {
            if (this.vertices != null && this.vertices.Count != 0)
            {
                for (int i = 0; i < this.vertices.Count; i++)
                {
                    this.vertices[i].vertexMoved -= OnVertexMoved;
                }
            }
        }

        void OnEnable()
        {
            if (vertices != null)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    vertices[i].gameObject.SetActive(true);
                }
            }
        }

        void OnDisable()
        {
            if (vertices != null)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    vertices[i].gameObject.SetActive(false);
                }
            }
            if (movePoint != null)
            {
                HideMoveUI();
            }
            if (edges != null)
            {
                HideEdgesUI();
            }
        }

        #region Public Methods
        /// <summary>
        /// Initiates the polygon component
        /// </summary>
        /// <param name="vertices">The list of PolygonVertex to be used as vertices by the Polygon</param>
        public void Initialize(Mesh mesh = null,Transform verticesParent= null, Transform cpuiParent = null)
        {
            meshFilter = GetComponent<MeshFilter>();
            meshRenderer = GetComponent<MeshRenderer>();
            if (mesh != null)
            {
                this.mesh = mesh;
                meshFilter.sharedMesh = this.mesh;

            }
            else
            {
                if (meshFilter.sharedMesh == null)
                {
                   this.mesh = new Mesh();
                }
                else
                {
                    this.mesh = meshFilter.sharedMesh;
                }
            }

            if (verticesParent != null)
            {
                this.verticesParent = verticesParent;
            }
            else
            {
                this.verticesParent = transform;
            }

            if (cpuiParent != null)
            {
                this.cpuiParent = cpuiParent;
            }
            if (vertexPrefab == null)
            {
                vertexPrefab = Resources.Load("Geometry/MeshVertex") as GameObject;
            }
            if (cpuiPrefab == null)
            {
                cpuiPrefab = Resources.Load("GUI/ControlPointUI") as GameObject;
            }
            if (edgeUiElement == null)
            {
                edgeUiElement = Resources.Load("GUI/MovePointUI") as GameObject;
            }
            if (moveUiElement == null)
            {
                moveUiElement = Resources.Load("GUI/MovePointUI") as GameObject;
            }
            if (typeMaterial == null)
            {
                typeMaterial = Resources.Load("Materials/TypeMaterial") as Material;
            }
            if (originalMaterial == null)
            {
                originalMaterial = GetComponent<MeshRenderer>().material;
            }
            if (selectMaterial == null)
            {
                selectMaterial = Resources.Load("Materials/SelectMaterial") as Material;
            }

            vertices = new List<MeshVertex>();
            outterEdges = EdgeHelpers.GetEdges(this.mesh.triangles).FindBoundary().SortEdges();
            PopulateVertexList();
        }

        public void AddListeners()
        {
            if (!hasListeners)
            {
                if (this.vertices != null && this.vertices.Count != 0)
                {
                    for (int i = 0; i < this.vertices.Count; i++)
                    {
                        this.vertices[i].vertexMoved += OnVertexMoved;
                    }
                }
            }
            hasListeners = true;
        }
        public void RemoveListeners()
        {
            if (hasListeners)
            {
                if (this.vertices != null && this.vertices.Count != 0)
                {
                    for (int i = 0; i < this.vertices.Count; i++)
                    {
                        this.vertices[i].vertexMoved -= OnVertexMoved;
                    }
                }
            }
            hasListeners = false;
        }

        public void UpdateGeometry()
        {
            OnVertexMoved(vertices[0]);
            OnUpdated();
        }

        public Extrusion GetExtrusion()
        {
            extrusion = Instantiate(Resources.Load("Geometry/Extrusion") as GameObject, transform).GetComponent<Extrusion>();
            extrusion.totalHeight = 3.0f;
            extrusion.Initialize(this);
            return extrusion;
        }

        public Extrusion GetExtrusion(float totalHeight, bool capped = false)
        {
            extrusion = Instantiate(Resources.Load("Geometry/Extrusion") as GameObject, transform).GetComponent<Extrusion>();
            extrusion.capped = capped;
            this.capped = capped;
            if (capped)
            {
                flip = true;
                FlipMesh();
            }
            extrusion.totalHeight = totalHeight;
            extrusion.Initialize(this);
            return extrusion;
        }

        public void OnMoved(object sender)
        {
            HideEdgesUI();
            if (vertices != null)
            {
                for (int i = 0; i < vertices.Count; i++)
                {
                    vertices[i].cpui.gameObject.SetActive(false);
                }
            }
            if (updated != null)
            {
                updated(this);
            }
        }

        public void OnStoppedMoving(object sender)
        {
            if (stoppedMoving != null)
            {
                stoppedMoving(this);
            }

            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i].cpui.gameObject.SetActive(true);
                var transVec = transform.localPosition + mesh.vertices[i] - new Vector3(0, offsetFromBase, 0);
                vertices[i].UpdatePosition(transVec);
                vertices[i].cpui.UpdateUIPosition();
            }
            HideMoveUI();
            DisplayMoveUI();
            DisplayNakedEdgesUI();
        }

        public void ShowControlPoints(bool show)
        {
            for (int i = 0; i < vertices.Count; i++)
            {
                vertices[i].cpui.gameObject.SetActive(show);
                vertices[i].gameObject.SetActive(show);
            }
        }

        public void DisplayStandardMode()
        {
            GetComponent<MeshRenderer>().material = originalMaterial;
            extrusion.GetComponent<MeshRenderer>().material = originalMaterial;
            extrusion.DisplayStandardMode();
        }
        public void FlipMesh()
        {
            int[] flippedTris = new int[mesh.triangles.Length];
            for (int i = 0; i < mesh.triangles.Length; i += 3)
            {
                flippedTris[i] = mesh.triangles[i + 2];
                flippedTris[i + 1] = mesh.triangles[i + 1];
                flippedTris[i + 2] = mesh.triangles[i];
            }
            mesh.triangles = flippedTris;
            UpdateMeshGeometry();
        }
        #endregion

        #region Private Methods
        private void OnColinearChanged(bool colinear)
        {
            moveColinear = colinear;
        }
        private void PopulateVertexList()
        {
            if (mesh.vertices != null && mesh.vertices.Length > 0)
            {
                for (int i = 0; i < mesh.vertices.Length; i++)
                {
                    MeshVertex meshVertex = Instantiate(vertexPrefab, verticesParent).GetComponent<MeshVertex>();
                    meshVertex.gameObject.name = name + "_vertex" + i;
                    meshVertex.transform.localPosition = mesh.vertices[i];
                    meshVertex.Initialize(i);
                    meshVertex.vertexMoved += OnVertexMoved;
                    vertices.Add(meshVertex);
                }
            }
        }

        private float CalculateArea()
        {
            float m_area = 0;
            for (int i = 0; i < mesh.triangles.Length / 3; i++)
            {
                Vector3 a = vertices[mesh.triangles[i * 3]].transform.position;
                Vector3 b = vertices[mesh.triangles[i * 3 + 1]].transform.position;
                Vector3 c = vertices[mesh.triangles[i * 3 + 2]].transform.position;
                float partA = a.x * (b.z - c.z);
                float partB = b.x * (c.z - a.z);
                float partC = c.x * (a.z - b.z);
                m_area += Mathf.Abs((partA + partB + partC)) / 2.0f;
            }
            return m_area;
        }

        private void OnVertexMoved(MeshVertex sender)
        {
            var verts = mesh.vertices;
            verts[sender.index] = sender.currentPosition;
            mesh.vertices = verts;
            UpdateMeshGeometry();
            OnUpdated();
        }

        private void UpdateMeshGeometry()
        {
            CalculateArea();
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
            meshFilter.sharedMesh = mesh;
            GetComponent<MeshCollider>().sharedMesh = mesh;
        }

        protected override void HideMoveUI()
        {
            if (movePoint != null)
            {
                movePoint.moved -= OnMoved;
                movePoint.stoppedMoving -= OnStoppedMoving;
                Destroy(movePoint.gameObject);
                movePoint = null;
            }
        }

        protected override void DisplayMoveUI()
        {
            GameObject m_parent = GameObject.Find("Canvas");
            movePoint = Instantiate(moveUiElement, m_parent.transform).GetComponent<ControlPointUI>();
            movePoint.Initialize(transform);
            movePoint.moved += OnMoved;
            movePoint.stoppedMoving += OnStoppedMoving;
        }

        private void DisplayNakedEdgesUI()
        {
            if (edges == null)
            {
                edges = new Dictionary<ControlPointUI, MeshEdgeRelation>();
                edgeControlPoints = new List<GameObject>();
                for (int i=0; i<outterEdges.Count; i++)
                {
                    GameObject m_parent = GameObject.Find("EdgeControlPoints");
                    var m_edgePoint = Instantiate(edgeUiElement, m_parent.transform).GetComponent<ControlPointUI>();
                    edgeControlPoints.Add(Instantiate(vertexPrefab));
                    edgeControlPoints[i].GetComponent<MeshVertex>().SetGUIElement(m_edgePoint);
                    edgeControlPoints[i].transform.position = vertices[outterEdges[i].v1].currentPosition + (vertices[outterEdges[i].v2].currentPosition - vertices[outterEdges[i].v1].currentPosition) / 2.0f;
                    m_edgePoint.Initialize(edgeControlPoints[i].transform);
                    m_edgePoint.axisConstrain = true;
                    m_edgePoint.axis = Vector3.Cross(Vector3.up, vertices[outterEdges[i].v2].currentPosition - vertices[outterEdges[i].v1].currentPosition).normalized;
                    m_edgePoint.moved += OnEdgeMoved;
                    m_edgePoint.onSelected += OnEdgeCPUISelected;
                    if (!edges.ContainsKey(m_edgePoint))
                    {
                        MeshEdgeRelation m_relation = new MeshEdgeRelation();
                        m_relation.vertices = new List<MeshVertex>() { vertices[outterEdges[i].v1], vertices[outterEdges[i].v2] };
                        m_relation.originalDifs = new List<Vector3>();
                        m_relation.originalDifs.Add(vertices[outterEdges[i].v1].currentPosition - edgeControlPoints[i].transform.position);
                        m_relation.originalDifs.Add(vertices[outterEdges[i].v2].currentPosition - edgeControlPoints[i].transform.position);
                        

                        edges.Add(m_edgePoint, m_relation);
                    }
                }
            }
        }

        protected override void DisplayEdgesUI()
        {
            DisplayNakedEdgesUI();
            /*
            if (edges == null)
            {
                edges = new Dictionary<ControlPointUI, MeshEdgeRelation>();
                edgeControlPoints = new List<GameObject>();
                for (int i = 0; i < vertices.Count; i++)
                {
                    int next = (i + 1) % vertices.Count;
                    GameObject m_parent = GameObject.Find("EdgeControlPoints");
                    var m_edgePoint = Instantiate(edgeUiElement, m_parent.transform).GetComponent<ControlPointUI>();
                    edgeControlPoints.Add(Instantiate(vertexPrefab));
                    edgeControlPoints[i].GetComponent<MeshVertex>().SetGUIElement(m_edgePoint);
                    edgeControlPoints[i].transform.position = vertices[i].currentPosition + (vertices[next].currentPosition - vertices[i].currentPosition) / 2.0f;
                    m_edgePoint.Initialize(edgeControlPoints[i].transform);
                    m_edgePoint.axisConstrain = true;
                    m_edgePoint.axis = Vector3.Cross(Vector3.up, vertices[next].currentPosition - vertices[i].currentPosition).normalized;
                    m_edgePoint.moved += OnEdgeMoved;
                    m_edgePoint.onSelected += OnEdgeCPUISelected;
                    if (!edges.ContainsKey(m_edgePoint))
                    {
                        MeshEdgeRelation m_relation = new MeshEdgeRelation();
                        m_relation.vertices = new List<MeshVertex>() { vertices[i], vertices[next] };
                        m_relation.originalDifs = new List<Vector3>();
                        m_relation.originalDifs.Add(vertices[i].currentPosition - edgeControlPoints[i].transform.position);
                        m_relation.originalDifs.Add(vertices[next].currentPosition - edgeControlPoints[i].transform.position);
                        

                        edges.Add(m_edgePoint, m_relation);
                    }
                }
            }
            */
        }

        protected override void HideEdgesUI()
        {
            if (edges != null)
            {
                foreach (var item in edges)
                {
                    item.Key.moved -= OnEdgeMoved;
                    item.Key.onSelected -= OnEdgeCPUISelected;
                    if (item.Key.gameObject != null)
                    {
                        Destroy(item.Key.gameObject);
                    }
                }
            }
            if (edgeControlPoints != null)
            {
                foreach (var item in edgeControlPoints)
                {
                    if (item != null)
                    {
                        Destroy(item);
                    }
                }
            }
            edges = null;
            edgeControlPoints = null;
        }

        private void OnEdgeMoved(object sender)
        {
            ControlPointUI cpui = (ControlPointUI)sender;
            if (edgeControlPointSelected == cpui)
            {
                MeshEdgeRelation relation;
                if (edges.TryGetValue(cpui, out relation))
                {
                    for (int i = 0; i < relation.originalDifs.Count; i++)
                    {
                        relation.vertices[i].SetPosition(cpui.sceneObject[0].transform.position + relation.originalDifs[i]);
                        relation.vertices[i].cpui.UpdateUIPosition();
                    }
                }
            }
        }

        private void OnEdgeCPUISelected(ControlPointUI cpui)
        {
            edgeControlPointSelected = cpui;
            MeshEdgeRelation m_relation;
            if (edges.TryGetValue(cpui, out m_relation))
            {
                cpui.axis = Vector3.Cross(Vector3.up, m_relation.vertices[1].currentPosition - m_relation.vertices[0].currentPosition).normalized;
                m_relation.originalDifs = new List<Vector3>();
                if (moveColinear)
                {
                    for (int i = 0; i < m_relation.vertices.Count; i++)
                    {
                        m_relation.originalDifs.Add(m_relation.vertices[i].transform.position - cpui.sceneObject[0].transform.position);
                    }
                }
                else
                {
                    for (int i = 0; i < 2; i++)
                    {
                        m_relation.originalDifs.Add(m_relation.vertices[i].transform.position - cpui.sceneObject[0].transform.position);
                    }
                }
                edges[cpui] = m_relation;
            }
        }
        #endregion
    }
}
