﻿using BrydenWoodUnity.GeometryManipulation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BrydenWoodUnity
{

    /// <summary>
    /// A static class with helper functions regarding the translation and IO of geometry
    /// </summary>
    public static class GeometryHelper
    {
        /*
        #region Mesh Methods
        /// <summary>
        /// Converts a Unity mesh into a Bryden Wood Geometry Mesh
        /// </summary>
        /// <param name="m">The Unity mesh to be converted</param>
        /// <returns>The resulting Bryden Wood Geometry mesh</returns>
        public static BWG.Mesh MeshToBWG(Mesh m)
        {
            BWG.Mesh mesh = new BWG.Mesh();
            BWG.Vector3d[] verts = new BWG.Vector3d[m.vertexCount];
            for (int i = 0; i < m.vertices.Length; i++)
            {
                verts[i] = VectorToBWG(m.vertices[i]);
            }
            mesh.vertices = verts;
            System.Drawing.Color[] vertexColours = new System.Drawing.Color[m.colors.Length];
            for (int i = 0; i < m.colors.Length; i++)
            {
                vertexColours[i] = ColorFromUnity(m.colors[i]);
            }
            mesh.vertexColours = vertexColours;
            mesh.triangles = new int[m.triangles.Length];
            for (int i = 0; i < m.triangles.Length; i++)
            {
                mesh.triangles[i] = m.triangles[i];
            }
            return mesh;
        }

        /// <summary>
        /// Converts a Bryden Wood Geometry mesh into a Unity mesh
        /// </summary>
        /// <param name="m">The Bryden Wood Geometry mesh</param>
        /// <returns>The resulting Unity mesh</returns>
        public static Mesh[] BWGToMesh(BWG.Mesh m)
        {
            List<Mesh> meshes = new List<Mesh>();
            Mesh mesh = new Mesh();

            Vector3[] verts = new Vector3[m.vertices.Length];
            for (int i = 0; i < verts.Length; i++)
            {
                verts[i] = BWGToVector(m.vertices[i]);
            }
            mesh.vertices = verts;

            Color[] vertexColours = new Color[m.vertexColours.Length];
            for (int i = 0; i < vertexColours.Length; i++)
            {
                vertexColours[i] = ColorToUnity(m.vertexColours[i]);
            }
            mesh.colors = vertexColours;

            int[] triangles = new int[m.triangles.Length];
            for (int i = 0; i < triangles.Length; i++)
            {
                triangles[i] = m.triangles[i];
            }
            mesh.triangles = triangles;
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
            meshes.Add(mesh);

            return meshes.ToArray();
        }

        /// <summary>
        /// Creates a GameObject with meshes from a BWG Mesh
        /// </summary>
        /// <param name="mesh">The BWG mesh</param>
        /// <param name="gameObject">The GameObject to hold the meshes</param>
        /// <param name="material">The material to be used for the meshes</param>
        public static void BWGToMeshObject(BWG.Mesh mesh, GameObject gameObject, Material material)
        {
            var meshes = BWGToMesh(mesh);
            for (int i = 0; i < meshes.Length; i++)
            {
                GameObject mObj = new GameObject("mesh_" + i);
                mObj.transform.SetParent(gameObject.transform);
                mObj.transform.localPosition = Vector3.zero;
                mObj.transform.localEulerAngles = Vector3.zero;
                mObj.transform.localScale = Vector3.zero;

                mObj.AddComponent<MeshRenderer>().material = material;
                mObj.AddComponent<MeshFilter>().mesh = meshes[i];
            }
            //gameObject.transform.localPosition = BWGToVector(mesh.translation);
            //gameObject.transform.localEulerAngles = BWGToVector(mesh.eulerAngles);
            //gameObject.transform.localScale = BWGToVector(mesh.scale);
        }
        #endregion

        #region Vector methods
        /// <summary>
        /// Translates a Unity Vector into a BrydenWoodGeometry Vector
        /// </summary>
        /// <param name="vec">The Unity vector to be translated</param>
        /// <returns>The resulting BrydenWood Geometry vector</returns>
        public static BWG.Vector3d VectorToBWG(Vector3 vec)
        {
            BWG.Vector3d vector = new BWG.Vector3d(vec.x, vec.z, vec.y);
            return vector;
        }

        /// <summary>
        /// Translates a BrydenWoodGeometry Vector into a Unity Vector
        /// </summary>
        /// <param name="vec">The BrydenWood Geometry Vector to be translated</param>
        /// <returns>The resulting Unity Vector</returns>
        public static Vector3 BWGToVector(BWG.Vector3d vec)
        {
            return new Vector3((float)vec.X, (float)vec.Z, (float)vec.Y);
        }
        #endregion
        */
        #region Color Methods
        /// <summary>
        /// Translates a Unity Color into a System.Drawing.Color 
        /// </summary>
        /// <param name="col">The Unity Color to be translated</param>
        /// <returns>The resulting System.Drawing.Color</returns>
        public static System.Drawing.Color ColorFromUnity(Color col)
        {
            return System.Drawing.Color.FromArgb((int)(col.r * 255), (int)(col.g * 255), (int)(col.b * 255), (int)(col.a * 255));
        }

        /// <summary>
        /// Translates a System.Drawing.Color into a Unity Color
        /// </summary>
        /// <param name="col">The System.Drawing.Color to be translated</param>
        /// <returns>The resulting Unity Color</returns>
        public static Color ColorToUnity(System.Drawing.Color col)
        {
            return new Color(col.R / 255.0f, col.G / 255.0f, col.B / 255.0f, col.A / 255.0f);
        }

        /// <summary>
        /// Generates a surface mesh from 4 points
        /// </summary>
        /// <param name="vertices">The list of vertices</param>
        /// <param name="flip">Whether the mesh should face one way or the other</param>
        /// <param name="color">A single color to be used for vertex colours</param>
        /// <returns>The generated mesh</returns>
        public static Mesh GetMeshFrom4Points(Vector3[] vertices, bool flip, Color color)
        {
            Mesh surfaceMesh = new Mesh();

            int[] triangles;
            if (flip)
            {
                triangles = new int[] { 2, 1, 0, 0, 3, 2 };
            }
            else
            {
                triangles = new int[] { 0, 1, 2, 2, 3, 0 };
            }
            surfaceMesh.vertices = vertices;
            surfaceMesh.triangles = triangles;
            surfaceMesh.colors = new Color[] { color, color, color, color };

            return surfaceMesh;
        }

        public static List<Mesh> GetPanelsFrom4PointSurf(Vector3[] points, float size, bool flip, Color color)
        {
            List<Mesh> panels = new List<Mesh>();

            int uNum = Mathf.FloorToInt(Vector3.Distance(points[0], points[1]) / size);
            int vNum = Mathf.FloorToInt(Vector3.Distance(points[0], points[3]) / size);

            for (int i=0; i<uNum; i++)
            {

                for (int j=0; j< vNum; j++)
                {
                    Mesh panel = new Mesh();

                    Vector3 segU = (points[1] - points[0]) * (i / ((float)uNum - 1));
                    Vector3 segV = (points[3] - points[0]) * (j / ((float)vNum - 1));

                    Vector3[] verts = new Vector3[4];
                    verts[0] = points[0];
                    verts[1] = points[0] + segU;
                    verts[3] = points[0] + segV;
                    verts[2] = points[0] + segU + segV;

                    panel = GetMeshFrom4Points(verts, flip, color);

                    panels.Add(panel);
                }

            }

            return panels;
        }

        public static List<Mesh> GetPanelsFrom4PointSurf(Vector3[] points, float size, bool flip, Color color, out List<Ray> ray)
        {
            List<Mesh> panels = new List<Mesh>();
            ray = new List<Ray>();

            Vector3 centre = new Vector3();
            Vector3 normal = new Vector3();
            int uNum = Mathf.FloorToInt(Vector3.Distance(points[0], points[1]) / size);
            int vNum = Mathf.FloorToInt(Vector3.Distance(points[0], points[3]) / size);
            float stepU = 1.0f / uNum;
            float stepV = 1.0f / vNum;
            Vector3 segU = (points[1] - points[0]) * stepU;
            Vector3 segV = (points[3] - points[0]) * stepV;

            for (int i = 0; i < uNum; i++)
            {
                for (int j = 0; j < vNum; j++)
                {
                    Mesh panel = new Mesh();

                    Vector3[] verts = new Vector3[4];
                    verts[0] = points[0] + segU * i + segV * j;
                    verts[1] = verts[0] + segU;
                    verts[3] = verts[0] + segV;
                    verts[2] = verts[0] + segU + segV;

                    centre = (verts[0] + verts[1] + verts[2] + verts[3]) / 4.0f;

                    panel = GetMeshFrom4Points(verts, flip, color);
                    panel.RecalculateNormals();
                    panel.RecalculateBounds();
                    normal = panel.normals[0];

                    Ray test = new Ray(centre + (normal * -0.1f), normal);
                    RaycastHit hit;
                    if (!Physics.Raycast(test, out hit, 0.15f))
                    {
                        ray.Add(new Ray(centre, normal));
                        panels.Add(panel);
                    }
                }

            }
            if (ray.Count != 0)
            {
                return panels;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Generates a surface mesh from 4 points
        /// </summary>
        /// <param name="vertices">The list of vertices</param>
        /// <param name="flip">Whether the mesh should face one way or the other</param>
        /// <param name="colors">A list of colors to be used for vertex colours</param>
        /// <returns>The generated mesh</returns>
        public static Mesh GetMeshFrom4Points(Vector3[] vertices, bool flip, Color[] colors)
        {
            Mesh surfaceMesh = new Mesh();

            int[] triangles;
            if (!flip)
            {
                triangles = new int[] { 0, 1, 2, 2, 3, 0 };
            }
            else
            {
                triangles = new int[] { 2, 1, 0, 0, 3, 2 };
            }
            surfaceMesh.vertices = vertices;
            surfaceMesh.triangles = triangles;
            surfaceMesh.colors = colors;

            return surfaceMesh;
        }


        #endregion

    }
}
