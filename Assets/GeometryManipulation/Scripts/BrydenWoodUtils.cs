﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BrydenWoodUnity
{
    /// <summary>
    /// A class with Unity utilities
    /// </summary>
    public static class BrydenWoodUtils
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="listHeldPoints"></param>
        /// <param name="listPlacedPoints"></param>
        /// <returns></returns>
        public static List<Vector3> KDTreeVertices(List<Vector3> listHeldPoints, List<Vector3> listPlacedPoints)
        {

            /*
             * For every held cluster a user holds, run through the for each loop. Essentially this code looks at the currently held cluster and finds
             * the nearest placed cluster. The for each loop runs through both clusters box collider to find all 8 verticies, the following for loops 
             * finds the nearest two vertices, the closest held cluster vertex and the closest placed cluster vertex. 
             * The debug draw line shows that the method works as it should
             */
            List<Vector3> closest = new List<Vector3>();

            Vector3 nearestHeld = Vector3.zero; //nearest vertex into a vector
            float closestHeldVertex = Mathf.Infinity; //this allows all gameobjects to be considered within the scene. e.g all placed/held

            Vector3 nearestPlaced = Vector3.zero;
            float closestPlacedVertex = Mathf.Infinity;

            //foreach (var heldcluster in listHeldClusters) //for each held cluster object within the held cluster list..
            //{
            Vector3 heldCorners = new Vector3(); //outputs list of vertiices to singular point
            Vector3 placedCorners = new Vector3(); //outputs list of vertices to singular point

            for (int i = 0; i < listHeldPoints.Count; i++) //count all the points within the held list
            {
                for (int x = 0; x < listPlacedPoints.Count; x++) //count all the points within the placed list
                {
                    heldCorners = listHeldPoints[i];

                    var heldClusterDistance = Vector3.Distance(heldCorners, placedCorners); //distance between held vertex and placed

                    if (heldClusterDistance < closestHeldVertex) //if distance is less than all game objects in scene
                    {
                        closestHeldVertex = heldClusterDistance;
                        nearestHeld = heldCorners;
                        //closest held corner is stored into nearest held, which stores the closet held vertex point to another object
                    }

                    placedCorners = listPlacedPoints[x];

                    var placedClusterDistance = Vector3.Distance(heldCorners, placedCorners);

                    if (placedClusterDistance < closestPlacedVertex)
                    {
                        closestPlacedVertex = placedClusterDistance;
                        nearestPlaced = placedCorners;
                    }

                }//list placed points end of loop

            }//list held points end of loop

            //}//placedcluster
            closest.Add(nearestPlaced);
            closest.Add(nearestHeld);
            // Debug.DrawLine(nearestHeld, nearestPlaced, Color.red); //draw red line
            return closest;
        }//heldcluster
        /// <summary>
        /// Checks whether an object is a List
        /// </summary>
        /// <param name="o">The object to be checked</param>
        /// <returns>Whether the object is a List</returns>
        public static bool IsList(object o)
        {
            if (o == null) return false;
            return o is IList &&
                   o.GetType().IsGenericType &&
                   o.GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(List<>));
        }

        /// <summary>
        /// Checks whether an object is a Dictionary
        /// </summary>
        /// <param name="o">The object to be checked</param>
        /// <returns>Whether the object is a Dictionary</returns>
        public static bool IsDictionary(object o)
        {
            if (o == null) return false;
            return o is IDictionary &&
                   o.GetType().IsGenericType &&
                   o.GetType().GetGenericTypeDefinition().IsAssignableFrom(typeof(Dictionary<,>));
        }

        /// <summary>
        /// Projects a point on the curve between 2 points
        /// </summary>
        /// <param name="start">First point of curve</param>
        /// <param name="end">Second point of curve</param>
        /// <param name="myPoint">The point to project</param>
        /// <returns>The projected point</returns>
        public static Vector3 ProjectOnCurve(Vector3 start, Vector3 end, Vector3 myPoint)
        {
            Vector3 projectedPoint = Vector3.Project(myPoint - start, end - start) + start;

            return projectedPoint;
        }

        public static IEnumerator GetFileContent(string filePath, Action<string> result)
        {
            string content = "";
            //As Taken from https://stackoverflow.com/questions/43693213/application-streamingassetspath-and-webgl-build

            //-----For vertexData------//
            if (filePath.Contains("://") || filePath.Contains(":///"))
            {
                WWW www = new WWW(filePath);
                yield return www;
                content = www.text;
                if (!String.IsNullOrEmpty(content))
                {
                    yield return null;
                }
            }
            else
            {
                content = File.ReadAllText(filePath);
                yield return null;
            }
            result(content);
        }

        //As taken from: https://www.codeproject.com/Tips/862988/Find-the-Intersection-Point-of-Two-Line-Segments
        /// <summary>
        /// Test whether two line segments intersect. If so, calculate the intersection point.
        /// <see cref="http://stackoverflow.com/a/14143738/292237"/>
        /// </summary>
        /// <param name="p">Vector to the start point of p.</param>
        /// <param name="p2">Vector to the end point of p.</param>
        /// <param name="q">Vector to the start point of q.</param>
        /// <param name="q2">Vector to the end point of q.</param>
        /// <param name="intersection">The point of intersection, if any.</param>
        /// <param name="considerOverlapAsIntersect">Do we consider overlapping lines as intersecting?
        /// </param>
        /// <returns>True if an intersection point was found.</returns>
        public static bool LineSegementsIntersect(Vector p, Vector p2, Vector q, Vector q2,
            out Vector intersection, bool considerCollinearOverlapAsIntersect = false)
        {
            intersection = new Vector();

            var r = p2 - p;
            var s = q2 - q;
            var rxs = r.Cross(s);
            var qpxr = (q - p).Cross(r);

            // If r x s = 0 and (q - p) x r = 0, then the two lines are collinear.
            if (rxs.IsZero() && qpxr.IsZero())
            {
                // 1. If either  0 <= (q - p) * r <= r * r or 0 <= (p - q) * s <= * s
                // then the two lines are overlapping,
                if (considerCollinearOverlapAsIntersect)
                    if ((0 <= (q - p) * r && (q - p) * r <= r * r) || (0 <= (p - q) * s && (p - q) * s <= s * s))
                        return true;

                // 2. If neither 0 <= (q - p) * r = r * r nor 0 <= (p - q) * s <= s * s
                // then the two lines are collinear but disjoint.
                // No need to implement this expression, as it follows from the expression above.
                return false;
            }

            // 3. If r x s = 0 and (q - p) x r != 0, then the two lines are parallel and non-intersecting.
            if (rxs.IsZero() && !qpxr.IsZero())
                return false;

            // t = (q - p) x s / (r x s)
            var t = (q - p).Cross(s) / rxs;

            // u = (q - p) x r / (r x s)

            var u = (q - p).Cross(r) / rxs;

            // 4. If r x s != 0 and 0 <= t <= 1 and 0 <= u <= 1
            // the two line segments meet at the point p + t r = q + u s.
            if (!rxs.IsZero() && (0 <= t && t <= 1) && (0 <= u && u <= 1))
            {
                // We can calculate the intersection point using either t or u.
                intersection = p + t * r;

                // An intersection was found.
                return true;
            }

            // 5. Otherwise, the two line segments are not parallel but do not intersect.
            return false;
        }

        //Based on http://mathforum.org/library/drmath/view/51833.html
        public static Vector3 XZFromLatLon(double lat1, double lon1, double lat2, double lon2)
        {
            float R = 6367000.0f; //Earth's Radius
            float x = (float)((lon2 - lon1) * Math.Cos(lat1) * Math.PI * R / 180);
            float z = (float)((lat2 - lat1) * Math.PI * R / 180);
            return new Vector3(x, 0, z);
        }
    }

    public class Vector
    {
        public double X;
        public double Y;

        // Constructors.
        public Vector(double x, double y) { X = x; Y = y; }
        public Vector() : this(double.NaN, double.NaN) { }

        public static Vector operator -(Vector v, Vector w)
        {
            return new Vector(v.X - w.X, v.Y - w.Y);
        }

        public static Vector operator +(Vector v, Vector w)
        {
            return new Vector(v.X + w.X, v.Y + w.Y);
        }

        public static double operator *(Vector v, Vector w)
        {
            return v.X * w.X + v.Y * w.Y;
        }

        public static Vector operator *(Vector v, double mult)
        {
            return new Vector(v.X * mult, v.Y * mult);
        }

        public static Vector operator *(double mult, Vector v)
        {
            return new Vector(v.X * mult, v.Y * mult);
        }

        public double Cross(Vector v)
        {
            return X * v.Y - Y * v.X;
        }

        public override bool Equals(object obj)
        {
            var v = (Vector)obj;
            return (X - v.X).IsZero() && (Y - v.Y).IsZero();
        }
    }
    public static class Extensions
    {
        private const double Epsilon = 1e-10;

        public static bool IsZero(this double d)
        {
            return Math.Abs(d) < Epsilon;
        }
    }
}
