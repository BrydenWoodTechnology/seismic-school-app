﻿using BrydenWoodUnity.GeometryManipulation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using BrydenWoodUnity;
using BrydenWoodUnity.Grid;

namespace BrydenWoodUnity.Navigation
{

    public class Selector : Tagged<Selector>
    {

        [Tooltip("The layer mask to be used for Raycasting")]
        public LayerMask selectablesLayermask;

        public SelectableGeometry selected;
        public List<SelectableGeometry> SelectAll;
        private Camera cam;
        public MeshlessGridManager gridManager;
        Toggle circOn;
        public Transform exteriorParent;

        public delegate void OnDeselect();
        public static event OnDeselect deselect;

        // Use this for initialization
        void Start()
        {
            SelectableGeometry.selected += SetSelected;
            SelectAll = new List<SelectableGeometry>();
            circOn = GameObject.Find("TranslationPanel").transform.GetChild(2).GetComponentInChildren<Toggle>();
            circOn.onValueChanged.AddListener(SelectedCirculation);
        }

        new void OnDestroy()
        {
            if (deselect != null)
            {
                foreach (System.Delegate del in deselect.GetInvocationList())
                {
                    deselect -= (OnDeselect)del;
                }
            }
            SelectableGeometry.selected -= SetSelected;

        }

        // Update is called once per frame
        void Update()
        {
            bool eventCheck = Input.touchCount > 0 ? !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId) : !EventSystem.current.IsPointerOverGameObject(-1);
            if (eventCheck)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    cam = Camera.main;
                    Ray r = cam.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(r, out hit, 500000, selectablesLayermask) && !gridManager.currentGrid.placingAdditional)
                    {

                        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                        {
                            var obj = hit.collider.gameObject;
                            if (!SelectAll.Contains(obj.GetComponent<SelectableGeometry>()))
                                SelectAll.Add(obj.GetComponent<SelectableGeometry>());
                            SelectOnebyOne();
                        }
                        else
                        {
                            var obj = hit.collider.gameObject;
                            if (selected == null)
                            {
                                obj.SendMessage("Select", SendMessageOptions.DontRequireReceiver);
                                obj.SendMessageUpwards("OnClusterSelected", obj, SendMessageOptions.DontRequireReceiver);
                                if (!SelectAll.Contains(obj.GetComponent<SelectableGeometry>()))
                                    SelectAll.Add(obj.GetComponent<SelectableGeometry>());
                            }
                            else
                            {

                                if (obj.GetComponent<SelectableGeometry>() != selected)
                                {
                                    if (SelectAll.Count > 1 && SelectAll.Contains(obj.GetComponent<SelectableGeometry>()))
                                    {
                                        SelectOnebyOne(obj.transform);
                                    }
                                    else
                                    {
                                        selected.DeSelect();
                                        selected.SendMessage("DeSelect", SendMessageOptions.DontRequireReceiver);
                                        selected.SendMessageUpwards("OnClusterDeselected", SendMessageOptions.DontRequireReceiver);
                                        SelectAll.Remove(selected);
                                        DeselectAll();
                                        obj.SendMessage("Select", SendMessageOptions.DontRequireReceiver);
                                        obj.SendMessageUpwards("OnClusterSelected", obj/*.GetComponent<PlaceablePrefab>()*/, SendMessageOptions.DontRequireReceiver);
                                        if (!SelectAll.Contains(obj.GetComponent<SelectableGeometry>()))
                                            SelectAll.Add(obj.GetComponent<SelectableGeometry>());
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (selected != null)
                        {
                            DeselectAll();
                            selected.SendMessage("DeSelect", SendMessageOptions.DontRequireReceiver);
                            selected.SendMessageUpwards("OnClusterDeselected", SendMessageOptions.DontRequireReceiver);
                            selected = null;

                            if (deselect != null)
                            {
                                deselect();
                                DeselectAll();
                            }
                        }
                    }
                }
                //if(Input.GetMouseButtonDown(0))
                //{
                //    if(selected!=null)
                //    {
                //        selected.SendMessage("Move");
                //    }
                //}
            }
            //else
            //{
            //    if (selected != null)
            //    {
            //        DeselectAll();
            //        selected.SendMessage("DeSelect", SendMessageOptions.DontRequireReceiver);
            //        selected.SendMessageUpwards("OnClusterDeselected", SendMessageOptions.DontRequireReceiver);
            //        selected = null;

            //        if (deselect != null)
            //        {
            //            deselect();
            //            DeselectAll();
            //        }
            //    }
            //}

            if (Input.GetKeyDown(KeyCode.Delete) || Input.GetKeyDown(KeyCode.Backspace) || CustomInput.GetAxis("GUIDelete") == 3)
            {
                DeleteSelected();
            }
        }


        public void SelectedCirculation(bool iscircOn)
        {
            foreach (SelectableGeometry sg in SelectAll)
            {
                if (!sg.name.Contains("Hall") && !sg.name.Contains("Kitchen") && !sg.name.Contains("Storage"))
                {
                    sg.GetComponent<ProceduralCluster>().OnToggleCirculation(!iscircOn);
                }
                Camera.main.GetComponent<DrawGLines>().DrawConnectingLines();
            }
        }
        public void SelectOnebyOne(Transform moveParent = null)
        {
            if (SelectAll.Count > 1)
            {
                if (moveParent != null)
                {
                    for (int i = 0; i < SelectAll.Count; i++)
                    {
                        if (SelectAll[i] != null)
                        {
                            if (SelectAll[i].transform.GetComponent<PlaceablePrefab>() != null)
                                SelectAll[i].transform.SetParent(SelectAll[i].GetComponent<PlaceablePrefab>().originalParent);
                            if (SelectAll[i].transform.GetComponent<AdditionalPrefab>() != null)
                                SelectAll[i].transform.SetParent(SelectAll[i].GetComponent<AdditionalPrefab>().originalParent);
                        }

                    }
                    for (int i = 0; i < SelectAll.Count; i++)
                    {
                        if (SelectAll[i] != null && SelectAll[i].transform != moveParent)
                        {
                            SelectAll[i].transform.SendMessage("Select");
                            SelectAll[i].transform.SetParent(moveParent);
                            SelectAll[i].SendMessage("SelectHighlight", true, SendMessageOptions.DontRequireReceiver);
                        }

                    }
                    if (moveParent.gameObject.GetComponent<PlaceablePrefab>() != null)
                        gridManager.currentGrid.currentObject = moveParent.gameObject;
                    else if (moveParent.gameObject.GetComponent<AdditionalPrefab>() != null)
                        gridManager.currentGrid.currentAdditional = moveParent.gameObject;
                }
                else
                {
                    moveParent = SelectAll[0].transform;
                    moveParent.SendMessage("Select", SendMessageOptions.DontRequireReceiver);
                    for (int i = 0; i < SelectAll.Count; i++)
                    {
                        if (SelectAll[i] != null)
                        {
                            if (SelectAll[i].transform.GetComponent<PlaceablePrefab>() != null)
                                SelectAll[i].transform.SetParent(SelectAll[i].GetComponent<PlaceablePrefab>().originalParent);
                            if (SelectAll[i].transform.GetComponent<AdditionalPrefab>() != null)
                                SelectAll[i].transform.SetParent(SelectAll[i].GetComponent<AdditionalPrefab>().originalParent);
                        }
                    }
                    for (int i = 1; i < SelectAll.Count; i++)
                    {
                        SelectAll[i].transform.SetParent(moveParent);
                    }
                    if (moveParent.gameObject.GetComponent<PlaceablePrefab>() != null)
                        gridManager.currentGrid.currentObject = moveParent.gameObject;
                    else if (moveParent.gameObject.GetComponent<AdditionalPrefab>() != null)
                        gridManager.currentGrid.currentAdditional = moveParent.gameObject;
                }
            }
            else if (SelectAll.Count == 1)
            {
                SelectAll[0].transform.SendMessage("Select");
                SelectAll[0].SendMessageUpwards("OnClusterSelected", SelectAll[0].gameObject, SendMessageOptions.DontRequireReceiver);
            }

        }

        public void DeselectAll()
        {
            foreach (SelectableGeometry sg in SelectAll)
            {
                if (sg != null)
                {
                    sg.DeSelect();
                    if (sg.transform.GetComponent<PlaceablePrefab>() != null)
                        sg.transform.SetParent(sg.GetComponent<PlaceablePrefab>().originalParent);
                    if (sg.transform.GetComponent<AdditionalPrefab>() != null)
                        sg.transform.SetParent(sg.GetComponent<AdditionalPrefab>().originalParent);
                    //sg.transform.SetParent(sg.transform.root);

                    sg.SelectHighlight(false);

                    sg.gameObject.SendMessageUpwards("OnClusterDeselected", SendMessageOptions.DontRequireReceiver);
                }
                // sg.GetComponent<PlaceablePrefab>().isSelected = false;
                // sg.GetComponent<PlaceablePrefab>().SelectHighlight(false);
                //Destroy(sg.transform.GetChild(sg.transform.childCount-1).gameObject);
            }

            SelectAll.Clear();
        }
        public void Deselect()
        {

            if (selected != null)
            {
                selected.DeSelect();
                selected.gameObject.SendMessageUpwards("OnClusterDeselected", SendMessageOptions.DontRequireReceiver);
                SelectAll.Clear();
                selected = null;
                if (deselect != null)
                {
                    deselect();
                }
            }

            //if (SelectAll.Count > 0)
            //{
            //    foreach (SelectableGeometry s in SelectAll)
            //    {
            //        s.DeSelect();

            //    }
            //}
            //SelectAll.Clear();
        }

        public void DeleteSelected()
        {
            if (SelectAll.Count > 0)
            {

                foreach (SelectableGeometry sg in SelectAll)
                {
                    if (sg != null)
                    {
                        sg.SelectHighlight(false);

                        if (sg.gameObject.name.Contains("Tile"))
                        {
                            sg.GetComponent<AdditionalPrefab>().originalParent.GetComponent<ClusterManager>().RemoveTile(sg.name.Replace("(Clone)", string.Empty));
                        }
                        else if (sg.GetComponent<ProceduralCluster>() == null)
                        {
                            if (sg.GetComponent<BatchPlacement>() != null)
                            {
                                for (int i = 0; i < sg.transform.childCount - 1; i++)
                                {
                                    sg.GetComponent<AdditionalPrefab>().originalParent.GetComponent<ClusterManager>().RemoveElement(sg.name.Replace("(Clone)", string.Empty));
                                }
                            }
                            else

                                sg.GetComponent<AdditionalPrefab>().originalParent.GetComponent<ClusterManager>().RemoveElement(sg.name.Replace("(Clone)", string.Empty));
                        }
                        Destroy(sg.gameObject);
                    }
                }
                SelectAll.Clear();
                selected = null;
                if (deselect != null)
                {
                    deselect();
                }
            }
            else if (selected != null)
            {
                selected.GetComponent<SelectableGeometry>().SelectHighlight(false);

                if (selected.gameObject.name.Contains("Tile"))
                {
                    selected.transform.root.GetComponent<ClusterManager>().RemoveTile(selected.name.Replace("(Clone)", string.Empty));
                }
                else if (selected.GetComponent<ProceduralCluster>() == null)
                {
                    if (selected.GetComponent<BatchPlacement>() != null)
                    {
                        for (int i = 0; i < selected.transform.childCount - 1; i++)
                        {
                            selected.GetComponent<ClusterManager>().RemoveElement(selected.name.Replace("(Clone)", string.Empty));
                        }
                    }
                    else
                        selected.transform.root.GetComponent<ClusterManager>().RemoveElement(selected.name.Replace("(Clone)", string.Empty));
                }
                Destroy(selected.gameObject);
                selected = null;
                if (deselect != null)
                {
                    deselect();
                }
            }

        }

        private void SetSelected(SelectableGeometry sender)
        {
            //if (selected != null && selected != sender)
            //{
            //    selected.DeSelect();
            //}

            selected = sender;

            //SelectAll.Add(selected);
        }
    }
}
