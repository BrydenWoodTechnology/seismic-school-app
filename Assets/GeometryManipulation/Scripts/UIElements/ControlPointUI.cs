﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// Delegate to be used when the UI element is moved
    /// </summary>
    public delegate void Moved(object sender);

    public delegate void Selected(ControlPointUI sender);

    /// <summary>
    /// An enum with the constrain planes for the movement of scene objects by UI elements
    /// </summary>
    public enum MovePlanes
    {
        XY,
        XZ,
        YZ
    }

    /// <summary>
    /// A Control Point UI Unity component. It can be used for moving scene objects
    /// </summary>
    public class ControlPointUI : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler, IEndDragHandler
    {
        /// <summary>
        /// The gameObject in scene to be moved by this control point
        /// </summary>
        [Tooltip("The gameObject in scene to be moved by this control point")]
        public List<Transform> sceneObject;
        /// <summary>
        /// The type of plane to contrain the movement on (the actual plane will be parallel to this one but with its centre at the initial position of the scene object
        /// </summary>
        [Tooltip("The type of plane to contrain the movement on (the actual plane will be parallel to this one but with its centre at the initial position of the scene object")]
        public MovePlanes movePlane = MovePlanes.XZ;
        public Vector3 axis;
        public bool axisConstrain = false;

        /// <summary>
        /// The Image component of the GUI object
        /// </summary>
        [Tooltip("The Image component of the GUI object")]
        public Image image;
        /// <summary>
        /// The original color of the Image
        /// </summary>
        [Tooltip("The original color of the Image")]
        public Color original;
        /// <summary>
        /// The color to be used when the mouse is over the element
        /// </summary>
        [Tooltip("The color to be used when the mouse is over the element")]
        public Color mouseOver;
        /// <summary>
        /// The color to be used when the element is clicked
        /// </summary>
        [Tooltip("The color to be used when the element is clicked")]
        public Color selected;

        [HideInInspector]
        public List<Vector3> initialDiffs;
        public bool isSelected { get; set; }

        #region Private Fields
        private Plane plane;
        private Vector3 initPosition;
        private Vector3 initMousePosition;
        private Vector3 prevCameraPosition;
        private float cameraPrevFoV;
        private List<Vector3> initObjectPos;
        #endregion

        #region Events
        /// <summary>
        /// Triggered when the element is moved
        /// </summary>
        public event Moved moved;
        public event Moved stoppedMoving;
        public event Selected onSelected;
        #endregion

        private void Start()
        {
            if (sceneObject != null)
            {
                Initialize(sceneObject);
            }
        }

        void Update()
        {
            if (prevCameraPosition != Camera.main.transform.position || cameraPrevFoV!= Camera.main.orthographicSize)
            {
                UpdateUIPosition();
            }
            prevCameraPosition = Camera.main.transform.position;
            cameraPrevFoV = Camera.main.orthographicSize;
        }

        private void OnDestroy()
        {
            if (sceneObject != null && sceneObject.Count > 0)
            {
                for (int i = 0; i < sceneObject.Count; i++)
                {
                    if (sceneObject[i] != null && sceneObject[i].GetComponent<PolygonVertex>() != null)
                    {
                        moved -= sceneObject[i].GetComponent<PolygonVertex>().OnMoved;
                    }
                }
            }
        }

        public void UpdateUIPosition()
        {
            var camPos = Camera.main.WorldToScreenPoint(sceneObject[0].position);
            camPos = new Vector3(camPos.x, camPos.y, 0);
            GetComponent<RectTransform>().position = camPos;
            
        }
        /// <summary>
        /// Initiates the component
        /// </summary>
        /// <param name="sceneObject">The gameObject in scene to be moved by this control point</param>
        public void Initialize(List<Transform> sceneObject)
        {
            this.sceneObject = sceneObject;
            initialDiffs = new List<Vector3>();
            for (int i = 0; i < sceneObject.Count; i++)
            {
                initialDiffs.Add(sceneObject[i].position - sceneObject[0].position);
            }
            UpdateUIPosition();
            switch (movePlane)
            {
                case MovePlanes.XY:
                    plane = new Plane(new Vector3(0, 0, 1), sceneObject[0].position);
                    break;
                case MovePlanes.XZ:
                    plane = new Plane(new Vector3(0, 1, 0), sceneObject[0].position);
                    break;
                case MovePlanes.YZ:
                    plane = new Plane(new Vector3(1, 0, 0), sceneObject[0].position);
                    break;
            }
        }

        public void Initialize(Transform singleObject)
        {
            if (sceneObject == null || (sceneObject.Count > 0 && sceneObject[0] == null))
            {
                sceneObject = new List<Transform>();
            }
            if (initialDiffs == null)
            {
                initialDiffs = new List<Vector3>();
            }
            sceneObject.Add(singleObject);
            initialDiffs.Add(singleObject.position - sceneObject[0].position);
            UpdateUIPosition();
            switch (movePlane)
            {
                case MovePlanes.XY:
                    plane = new Plane(new Vector3(0, 0, 1), sceneObject[0].position);
                    break;
                case MovePlanes.XZ:
                    plane = new Plane(new Vector3(0, 1, 0), sceneObject[0].position);
                    break;
                case MovePlanes.YZ:
                    plane = new Plane(new Vector3(1, 0, 0), sceneObject[0].position);
                    break;
            }
        }

        public void UpdatePositions(Transform sender)
        {
            for (int i=0; i<sceneObject.Count; i++)
            {
                if (sceneObject[i] != sender && i!=0)
                {
                    sceneObject[i].position = sender.position + initialDiffs[i];
                    if (sceneObject[i].GetComponent<PolygonVertex>()!=null)
                    {
                        sceneObject[i].GetComponent<PolygonVertex>().OnMoved();
                    }
                    if (sceneObject[i].GetComponent<MeshVertex>() != null)
                    {
                        sceneObject[i].GetComponent<MeshVertex>().OnMoved();
                    }
                }
                else if (sceneObject[i] != sender && i == 0)
                {
                    int senderIndex = -1;
                    for (int j=0; j<sceneObject.Count; j++)
                    {
                        if (sender == sceneObject[j])
                        {
                            senderIndex = j;
                        }
                    }

                    if (senderIndex != -1)
                    {
                        sceneObject[i].position = sender.position - initialDiffs[senderIndex];
                        if (sceneObject[i].GetComponent<PolygonVertex>() != null)
                        {
                            sceneObject[i].GetComponent<PolygonVertex>().OnMoved();
                        }
                        if (sceneObject[i].GetComponent<MeshVertex>() != null)
                        {
                            sceneObject[i].GetComponent<MeshVertex>().OnMoved();
                        }
                    }

                }
            }
        }

        /// <summary>
        /// Called when the element is being dragged
        /// </summary>
        /// <param name="eventData"></param>
        public void OnDrag(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                var dif = Input.mousePosition - initMousePosition;
                GetComponent<RectTransform>().position = initPosition + dif;
                Ray r = Camera.main.ScreenPointToRay(initPosition + dif);
                float dist = 0;
                if (plane.Raycast(r, out dist))
                {
                    if (axisConstrain)
                    {
                        for (int i = 0; i < sceneObject.Count; i++)
                        {
                            var moveVec = r.GetPoint(dist) - initObjectPos[i];
                            sceneObject[i].position = initObjectPos[i] + Vector3.Project(moveVec, axis);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < sceneObject.Count; i++)
                        {
                            sceneObject[i].position = r.GetPoint(dist) + initialDiffs[i];
                        }
                    }
                }


                if (moved != null)
                {
                    moved(this);
                }

                image.color = selected;

                Camera.main.GetComponent<OrbitCamera>().enabled = false;
            }
        }
/// <summary>
/// Called when the dragginf stops
/// </summary>
/// <param name="eventData"></param>
        public void OnEndDrag(PointerEventData eventData)
        {
            Camera.main.GetComponent<OrbitCamera>().enabled = true;
        }

        public void ToggleAxisConstraint()
        {
            axisConstrain = !axisConstrain;
        }

        /// <summary>
        /// Called when the pointer is down on top of the element
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerDown(PointerEventData eventData)
        {
            initObjectPos = new List<Vector3>();
            initMousePosition = Input.mousePosition;
            initPosition = GetComponent<RectTransform>().position;
            transform.SetAsLastSibling();
            for (int i = 0; i < sceneObject.Count; i++)
            {
                initObjectPos.Add(sceneObject[i].transform.position);
            }
            if (onSelected != null)
            {
                onSelected(this);
            }
        }

        /// <summary>
        /// Called when the pointer is up on top of the element
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerUp(PointerEventData eventData)
        {
            if (stoppedMoving != null)
            {
                stoppedMoving(this);
            }
        }

        /// <summary>
        /// Called when the pointer enters the element
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!isSelected)
            {
                image.color = mouseOver;
            }
        }

        /// <summary>
        /// Called when the pointer exits the element
        /// </summary>
        /// <param name="eventData"></param>
        public void OnPointerExit(PointerEventData eventData)
        {
            if (!isSelected)
            {
                image.color = original;
            }
        }

        
    }
}
