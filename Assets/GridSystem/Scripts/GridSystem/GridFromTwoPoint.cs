﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BrydenWoodUnity.GeometryManipulation;

public class GridFromTwoPoint : MonoBehaviour {

    [Header("Main References")]
    public GameObject gridPrefab;
    public DrawingPlane drawingPlane = DrawingPlane.XZ;

    [Header("Optional References:")]
    public Camera raycastingCamera;


    private bool creatingGrid = false;
    private LineRenderer temp_lineRenderer { get; set; }
    private GameObject temp_gridObject { get; set; }
    private Vector3 diagonal { get; set; }
    private Vector3 startVector { get; set; }
    private Plane plane { get; set; }
    private Vector3[] temp_points { get; set; }
    private Vector3 side1 { get; set; }
    private Vector3 side2 { get; set; }

    #region Events
    public delegate void OnGridCreationEnded();
    public static event OnGridCreationEnded gridCreated;
    private void GridCreated()
    {
        if (gridCreated != null)
        {
            gridCreated();
        }
    }
    #endregion

    // Use this for initialization
    void Start () {
        if (raycastingCamera == null)
        {
            raycastingCamera = Camera.main;
        }
        plane = SetPlane(Vector3.zero, drawingPlane);
    }
	
	// Update is called once per frame
	void Update () {
		
        if (Input.GetKeyDown(KeyCode.T))
        {
            StartGrid();
        }

        if (creatingGrid)
        {
            Ray r = raycastingCamera.ScreenPointToRay(Input.mousePosition);
            float dist = 0;
            Vector3 point = Vector3.zero;
            if (plane.Raycast(r, out dist))
            { 
                point = r.GetPoint(dist);

                if (Input.GetMouseButtonDown(0))
                {
                    startVector = point;
                    temp_gridObject.transform.position = startVector;
                    temp_gridObject.transform.eulerAngles = new Vector3(0, raycastingCamera.transform.eulerAngles.y, 0);
                    temp_points[1] = startVector;
                    temp_points[5] = startVector;
                }

                if (Input.GetMouseButton(0))
                {
                    Vector3 endVector = point;
                    diagonal = endVector - startVector;
                    side1 = Vector3.Project(diagonal, temp_gridObject.transform.right);
                    side2 = endVector - (startVector + side1);
                    temp_points[0] = endVector;
                    temp_points[2] = startVector + side1;
                    temp_points[3] = endVector;
                    temp_points[4] = endVector - side1;
                    //raycastingCamera.GetComponent<DrawGLines>().gridPoints = temp_points;
                }

                if (Input.GetMouseButtonUp(0))
                {
                    EndGrid();
                }
            }
        }
	}

    public void StartGrid()
    {
        creatingGrid = true;
        temp_gridObject = Instantiate(gridPrefab);
        //GenerateTempLineRenderer();
        temp_points = new Vector3[6];
    }

    public void PlaceGrid(Vector3 centre, Vector3 side1, Vector3 side2)
    {
        temp_gridObject.transform.position = centre;
        var grid = temp_gridObject.GetComponent<Grid>();
        grid.xNum = Mathf.CeilToInt(side1.magnitude / grid.tileSizeX);
        grid.yNum = Mathf.CeilToInt(side2.magnitude / grid.tileSizeY);
        grid.Initialize();
    }

    public void EndGrid()
    {
        creatingGrid = false;
        PlaceGrid(startVector+(diagonal/2.0f),side1,side2);
        GridCreated();
    }
    public Plane SetPlane(Vector3 origin, DrawingPlane orientation)
    {
        switch (orientation)
        {
            case DrawingPlane.XY:
                return new Plane(Vector3.forward, origin);
            case DrawingPlane.XZ:
                return new Plane(Vector3.up, origin);
            case DrawingPlane.ZY:
                return new Plane(Vector3.right, origin);
            default:
                return new Plane();
        }
    }

    private void GenerateTempLineRenderer()
    {
        GameObject gameObject = new GameObject("Temp_LineRenderer");
        temp_lineRenderer = gameObject.AddComponent<LineRenderer>();
        Material mat = new Material(Shader.Find("Standard"));
        mat.color = Color.red;
        temp_lineRenderer.material = mat;
        temp_lineRenderer.endWidth = 0.1f;
        temp_lineRenderer.startWidth = 0.1f;
    }
}
