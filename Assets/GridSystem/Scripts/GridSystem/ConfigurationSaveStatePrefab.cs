﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class ConfigurationSaveStatePrefab
{

    public List<GridState> grids { get; set; }


    public ConfigurationSaveStatePrefab()
    {

    }
}



public struct ClusterAsPrefab
{
    public float[] position { get; set; }
    public float[] rotation { get; set; }
    public float[] scale { get; set; }
    public string name { get; set; }
    public float[] startPoint { get; set; }
    public float builtWidth { get; set; }
    public float builtDepth { get; set; }
    public float builtHeight { get; set; }
    public float circulationDepth { get; set; }
    public float topOffset { get; set; }
    public float bottomOffset { get; set; }
    public int facilitiesIndex { get; set; }
    public float moduleWidth { get; set; }
    public float moduleDepth { get; set; }
    public float moduleHeight { get; set; }
    public bool hasCirculation { get; set; }
    public bool halfRight { get; set; }
    public bool halfLeft { get; set; }
    public int level { get; set; }
    /*
    public float[] position { get; set; }
    public float[] rotation { get; set; }
    public float[] size { get; set; }
    public bool[] hasExternalWalls { get; set; }
    public bool hasCirculation { get; set; }
    public int gridIndex { get; set; }
    public int level { get; set; }
    public string type { get; set; }
    */
}

