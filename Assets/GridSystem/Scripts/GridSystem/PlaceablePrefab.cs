﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.Three;
using System.Linq;
using UnityEngine.UI;
using System;
using BrydenWoodUnity.Grid;
using BrydenWoodUnity.Navigation;

namespace BrydenWoodUnity.GeometryManipulation
{
    public class PlaceablePrefab : SelectableGeometry
    {

        public string type = "";
        public Vector3 size = new Vector3(46.8f, 2.995f, 18.0f);


        public List<GameObject> right;
        public List<GameObject> front;
        public List<GameObject> left;
        public List<GameObject> back;

        public bool hasCirculation = true;
        public List<GameObject> circulation;
        public List<bool> hasAdjacent;

        [HideInInspector]
        public bool allowed = true;
        [HideInInspector]
        public bool placed = false;
        [HideInInspector]
        public bool isSelected = false;
        [HideInInspector]
        public int gridIndex = -1;
        public int level;
        public Vector3 originalSize { get; set; }
        public ProceduralCluster proceduralCluster { get; set; }
        public GameObject[] adjacent;
        public GameObject incompatibilityButtonPRefab;
        public GameObject incompatibilityUnresolveButtonPRefab;
        public MeshlessGrid grid { get; set; }

        public float startModuleWidth;
        public Vector3 startLocalPos;

        List<GameObject> allAdjacent;

        public bool isIntersecting;
        public bool isPlaceable = true;
        public GameObject other;
        [HideInInspector]
        public Transform buttonPositionRight, buttonPositionLeft;

        public Vector3 prefabRotation;
        public Ray[] SideRays { get { return proceduralCluster.SideRays; } }



        private IncompatibilityResolveButton rightButton;
        private IncompatibilityResolveButton leftButton;
        public Transform originalParent;

        public delegate void OnDelete(PlaceablePrefab sender);
        public static event OnDelete deleted;

        //---------Unresolve-----
        public bool resolvedLeft = false;
        public bool resolvedRight = false;
        public IncompatibilityResolveButton rightUnButton;
        public IncompatibilityResolveButton leftUnButton;

        public delegate void OnResolved(ProceduralCluster cluster);
        public static event OnResolved resolvedRoom;

        // Use this for initialization
        void Start()
        {
            allAdjacent = new List<GameObject>();
            isPlaceable = true;
        }

        // Update is called once per frame
        void Update()
        {
            if (isSelected)
            {
                Camera.main.GetComponent<DrawGLines>().selectedPrefab = new List<Vector3[]>();
                Camera.main.GetComponent<DrawGLines>().selectedPrefab.Add(GetBoundingCorners());
                for (int i = 0; i < transform.childCount; i++)
                {
                    if (transform.GetChild(i).GetComponent<PlaceablePrefab>() != null)
                    {
                        Camera.main.GetComponent<DrawGLines>().selectedPrefab.Add(transform.GetChild(i).GetComponent<PlaceablePrefab>().GetBoundingCorners());
                    }
                }
            }
        }

        private void OnDrawGizmosSelected()
        {
            var rays = SideRays;

            for (int i = 0; i < rays.Length; i++)
            {
                Gizmos.color = Color.yellow;
                Gizmos.DrawCube(rays[i].origin, Vector3.one * 0.5f);
                Gizmos.color = Color.green;
                Gizmos.DrawLine(rays[i].origin, rays[i].origin + rays[i].direction * 1.5f);
            }
        }
        public void Initialize()
        {
            originalSize = size;

            hasAdjacent = new List<bool>();
            hasAdjacent.Add(false);
            hasAdjacent.Add(false);
            hasAdjacent.Add(false);
            hasAdjacent.Add(false);
            hasAdjacent.Add(false);
            hasAdjacent.Add(false);

            var box = GetComponent<BoxCollider>();
            if (box != null)
            {
                box.size = size;
                box.center = (size / 2.0f) + proceduralCluster.startPoint;
            }
            adjacent = new GameObject[6];
            if (!name.Contains("Kitchen") && !name.Contains("SmallHall") && !name.Contains("Storage"))
                originalParent = transform.parent;

        }
        /// <summary>
        /// When a clusters changes shape update the box collider
        /// </summary>
        public void UpdateColider()
        {
            var box = GetComponent<BoxCollider>();
            if (box != null)
            {
                box.size = size;
                box.center = (box.size / 2.0f) + proceduralCluster.startPoint;
            }

        }


        private void OnDestroy()
        {
            Delete();
            if (deleted != null)
            {
                foreach (System.Delegate del in deleted.GetInvocationList())
                {
                    deleted -= (OnDelete)del;
                }

            }
            if (resolvedRoom != null)
            {
                foreach (System.Delegate del in resolvedRoom.GetInvocationList())
                {
                    resolvedRoom -= (OnResolved)del;
                }

            }

        }
        /// <summary>
        /// Called when a cluster is deleted
        /// </summary>
        public void Delete()
        {
            if (deleted != null)
            {
                deleted(this);
                if (Camera.main != null)
                    if (Camera.main.GetComponent<DrawGLines>().selectedPrefab != null)
                        Camera.main.GetComponent<DrawGLines>().selectedPrefab = null;
            }

            if (rightButton != null)
            {
                Destroy(rightButton.gameObject);
                Destroy(buttonPositionRight.gameObject);
            }
            if (leftButton != null)
            {
                Destroy(leftButton.gameObject);
                Destroy(buttonPositionLeft.gameObject);
            }
            if (rightUnButton != null)
            {
                Destroy(rightUnButton.gameObject);
                Destroy(buttonPositionRight.gameObject);
            }
            if (leftUnButton != null)
            {
                Destroy(leftUnButton.gameObject);
                Destroy(buttonPositionLeft.gameObject);
            }
            if (ProceduralClusterManager.TaggedObject != null)
                ProceduralClusterManager.TaggedObject.RemoveCluster(GetComponent<ProceduralCluster>());


        }
        /// <summary>
        /// Select all adjacent objects to this
        /// </summary>
        public void SelectGroup()
        {
            if (!isSelected)
            {
                gameObject.layer = 0;
                if (placed)
                {
                    base.Select();
                    isSelected = true;

                }
                allAdjacent.Clear();
                ClearAdjacencies();
                CheckAdjacencies(1.8f, false);
                SetCombinedParent(transform, allAdjacent);
            }
        }
        /// <summary>
        /// select only this
        /// </summary>
        public override void Select()
        {
            allAdjacent.Clear();
            ClearAdjacencies();
            if (!isSelected)
            {
                gameObject.layer = 0;
                if (placed)
                {
                    base.Select();
                    isSelected = true;
                }
            }
        }

        /// <summary>
        /// Deselect this
        /// </summary>
        public override void DeSelect()
        {
            base.DeSelect();
            isSelected = false;
            gameObject.layer = 13;
            List<PlaceablePrefab> m_children = new List<PlaceablePrefab>();
            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).GetComponent<PlaceablePrefab>() != null && !transform.GetChild(i).name.Contains("Kitchen") && !transform.GetChild(i).name.Contains("Storage") && !transform.GetChild(i).name.Contains("Small"))
                {
                    transform.GetChild(i).gameObject.layer = 13;
                    m_children.Add(transform.GetChild(i).GetComponent<PlaceablePrefab>());
                }
            }
            m_children.TrimExcess();
            for (int i = 0; i < m_children.Count; i++)
            {
                if (m_children[i].GetComponent<PlaceablePrefab>().enabled)
                {
                    m_children[i].RestoreParent();
                    m_children[i].CheckAdjacencies(1.8f, false);
                    m_children[i].allAdjacent.Clear();
                }

            }
            if (GetComponent<PlaceablePrefab>().enabled == true)
                allAdjacent.Clear();

        }
        /// <summary>
        /// Updates position on grid
        /// </summary>
        public void UpdateGridPosition()
        {
            grid.initDiff = Vector3.zero;
            grid.UpdateCentre(gameObject);
        }
        /// <summary>
        /// Creates outline if this is selected
        /// </summary>
        /// <param name="select"></param>returns true if this is selected
        public override void SelectHighlight(bool select)
        {
            if (select)
            {
                Camera.main.GetComponent<DrawGLines>().selectedPrefab = new List<Vector3[]>();
                Camera.main.GetComponent<DrawGLines>().selectedPrefab.Add(GetBoundingCorners());

                for (int i = 0; i < transform.childCount; i++)
                {
                    if (transform.GetChild(i).GetComponent<PlaceablePrefab>() != null)
                    {
                        Camera.main.GetComponent<DrawGLines>().selectedPrefab.Add(transform.GetChild(i).GetComponent<PlaceablePrefab>().GetBoundingCorners());
                    }
                }

            }
            else
            {
                Camera.main.GetComponent<DrawGLines>().selectedPrefab = null;
            }
        }
        /// <summary>
        /// If group selected adds outline to all selected recursively
        /// </summary>
        /// <param name="previous"></param> To not check this gameobject
        public void AddBoundingRender(GameObject previous = null)
        {
            Camera.main.GetComponent<DrawGLines>().selectedPrefab.Add(GetBoundingCorners());
            for (int i = 0; i < adjacent.Length; i++)
            {
                if (adjacent[i] != previous && adjacent[i] != null)
                {
                    adjacent[i].GetComponent<PlaceablePrefab>().AddBoundingRender(gameObject);
                }
            }
        }
        /// <summary>
        /// Clears the array of adjacent objects 
        /// </summary>
        public void ClearAdjacencies()
        {
            for (int i = 0; i < adjacent.Length; i++)
            {
                adjacent[i] = null;
            }
        }
        /// <summary>
        /// Set one parent for all selected
        /// </summary>
        /// <param name="parent"></param> the parent to set
        /// <param name="myAdjacencies"></param> all adjacent gameobjects
        /// <param name="previous"></param>To not check this gameobject
        public void SetCombinedParent(Transform parent, List<GameObject> myAdjacencies, GameObject previous = null)
        {

            if (previous != null)
                myAdjacencies.Add(previous);
            for (int i = 0; i < adjacent.Length; i++)
            {
                if (i != 1 && i != 3 && i != 4 && i != 5 && adjacent[i] != previous && adjacent[i] != null && !myAdjacencies.Contains(adjacent[i]))
                {
                    adjacent[i].transform.SetParent(parent);
                    adjacent[i].gameObject.layer = 0;
                    adjacent[i].GetComponent<PlaceablePrefab>().SetCombinedParent(parent, myAdjacencies, gameObject);
                }
            }
        }
        /// <summary>
        /// Adds a box collider to this
        /// </summary>
        public void GenerateBoxCollider()
        {
            BoxCollider box = gameObject.AddComponent<BoxCollider>();
            box.size = size;
            box.center = size / 2.0f;
            box.isTrigger = true;
        }




#if UNITY_EDITOR

        public void PopulateLists()
        {
            circulation = new List<GameObject>();
            right = new List<GameObject>();
            front = new List<GameObject>();
            back = new List<GameObject>();
            left = new List<GameObject>();
            PopulateLists(right, front, left, back, circulation, transform);
        }

        public void ClearLists()
        {
            circulation = new List<GameObject>();
            right = new List<GameObject>();
            front = new List<GameObject>();
            back = new List<GameObject>();
            left = new List<GameObject>();
        }
#endif
        /// <summary>
        /// Updates the collider of the cluster
        /// </summary>
        public void UpdateBoxCollider()
        {
            var box = GetComponent<BoxCollider>();
            var bounds = GetAllBounds();
            box.size = bounds.size;
            box.center = (bounds.size / 2.0f);
            size = bounds.size;
        }
        /// <summary>
        /// Updates the size of the cluster
        /// </summary>
        public void UpdateSize()
        {
            if (hasCirculation)
            {
                size += new Vector3(0, 0, 2.2f);
            }
            else
            {
                size += new Vector3(0, 0, -2.2f);
            }
            var box = GetComponent<BoxCollider>();
            if (box != null)
            {
                box.size = size;
                box.center = (size / 2.0f) + proceduralCluster.startPoint;
            }
        }

#if UNITY_EDITOR
        /// <summary>
        /// Turn the cluster's circulation on/off
        /// </summary>
        public void ToggleCirculation()
        {
            if (!name.Contains("Hall") && !name.Contains("Corner"))
            {
                hasCirculation = !hasCirculation;
                proceduralCluster.hasCirculation = hasCirculation;
                foreach (var item in circulation)
                {
                    item.SetActive(hasCirculation);
                }
                UpdateSize();
            }
        }
#endif
        /// <summary>
        /// Turn the cluster's circulation on/off
        /// </summary>
        /// <param name="show"></param>
        public void ToggleCirculation(bool show)
        {
            if (!name.Contains("Hall") && !name.Contains("Corner"))
            {
                if (hasCirculation != show)
                {
                    hasCirculation = show;
                    proceduralCluster.hasCirculation = hasCirculation;
                    foreach (var item in circulation)
                    {
                        item.SetActive(hasCirculation);
                    }
                    UpdateSize();
                }
            }
        }

        public void UpdateConditionals(bool[] walls, bool circulation)
        {
            ToggleCirculation(circulation);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="right"></param>
        /// <param name="front"></param>
        /// <param name="left"></param>
        /// <param name="back"></param>
        /// <param name="circulation"></param>
        /// <param name="parent"></param>
        private void PopulateLists(List<GameObject> right, List<GameObject> front, List<GameObject> left, List<GameObject> back, List<GameObject> circulation, Transform parent)
        {
            var obj = parent.gameObject.GetComponent<ThreeObject>();
            if (obj != null && obj.userDataKeys != null)
            {
                if (obj.userDataKeys.Contains("conditional"))
                {
                    int conditionalIndex = obj.userDataKeys.IndexOf("conditional");
                    if (obj.userDataValues[conditionalIndex].Contains("externalwall"))
                    {
                        if (obj.userDataKeys.Contains("side"))
                        {
                            int index = obj.userDataKeys.IndexOf("side");
                            switch (obj.userDataValues[index])
                            {
                                case "right":
                                    right.Add(parent.gameObject);
                                    break;
                                case "front":
                                    front.Add(parent.gameObject);
                                    break;
                                case "left":
                                    left.Add(parent.gameObject);
                                    break;
                                case "back":
                                    back.Add(parent.gameObject);
                                    break;
                            }
                        }
                        if (obj.userDataValues[conditionalIndex].Contains("circulation"))
                        {
                            circulation.Add(parent.gameObject);
                        }
                    }
                    else if (obj.userDataValues[conditionalIndex].Contains("circulation"))
                    {
                        circulation.Add(parent.gameObject);
                    }
                    else
                    {
                        if (parent.childCount != 0)
                        {
                            for (int i = 0; i < parent.childCount; i++)
                            {
                                PopulateLists(right, front, left, back, circulation, parent.GetChild(i));
                            }
                        }
                    }
                }
                else
                {
                    if (parent.childCount != 0)
                    {
                        for (int i = 0; i < parent.childCount; i++)
                        {
                            PopulateLists(right, front, left, back, circulation, parent.GetChild(i));
                        }
                    }
                }
            }
            else
            {
                if (parent.childCount != 0)
                {
                    for (int i = 0; i < parent.childCount; i++)
                    {
                        PopulateLists(right, front, left, back, circulation, parent.GetChild(i));
                    }
                }
            }
        }
        /// <summary>
        /// In case of a Group Selection this is called to restore the original parents
        /// </summary>
        public void RestoreParent()
        {
            UpdateGridPosition();
            transform.SetParent(originalParent);
            gameObject.layer = 13;
        }
        /// <summary>
        /// Finds all points to create box outline
        /// </summary>
        /// <returns></returns>
        private Vector3[] GetBoundingCorners()
        {
            List<Vector3> corners = new List<Vector3>();

            Vector3 point = new Vector3();

            point = new Vector3(0, 0, 0) + new Vector3(-1 * transform.localScale.x, 0, -1 * transform.localScale.z);
            point.x *= transform.localScale.x;
            point.z *= transform.localScale.z;
            corners.Add(transform.TransformPoint(point));
            point = new Vector3(0, 0, size.z * transform.localScale.z) + new Vector3(-1 * transform.localScale.x, 0, 1 * transform.localScale.z);
            point.x *= transform.localScale.x;
            point.z *= transform.localScale.z;
            corners.Add(transform.TransformPoint(point));
            point = new Vector3(size.x * transform.localScale.x, 0, size.z * transform.localScale.z) + new Vector3(1 * transform.localScale.x, 0, 1 * transform.localScale.z);
            point.x *= transform.localScale.x;
            point.z *= transform.localScale.z;
            corners.Add(transform.TransformPoint(point));
            point = new Vector3(size.x * transform.localScale.x, 0, 0) + new Vector3(1 * transform.localScale.x, 0, -1 * transform.localScale.z);
            point.x *= transform.localScale.x;
            point.z *= transform.localScale.z;
            corners.Add(transform.TransformPoint(point));

            point = new Vector3(0, size.y, 0) + new Vector3(-1 * transform.localScale.x, 1, -1 * transform.localScale.z);
            point.x *= transform.localScale.x;
            point.z *= transform.localScale.z;
            corners.Add(transform.TransformPoint(point));
            point = new Vector3(0, size.y, size.z * transform.localScale.z) + new Vector3(-1 * transform.localScale.x, 1, 1 * transform.localScale.z);
            point.x *= transform.localScale.x;
            point.z *= transform.localScale.z;
            corners.Add(transform.TransformPoint(point));
            point = new Vector3(size.x * transform.localScale.x, size.y, size.z * transform.localScale.z) + new Vector3(1 * transform.localScale.x, 1, 1 * transform.localScale.z);
            point.x *= transform.localScale.x;
            point.z *= transform.localScale.z;
            corners.Add(transform.TransformPoint(point));
            point = new Vector3(size.x * transform.localScale.x, size.y, 0) + new Vector3(1 * transform.localScale.x, 1, -1 * transform.localScale.z);
            point.x *= transform.localScale.x;
            point.z *= transform.localScale.z;
            corners.Add(transform.TransformPoint(point));

            return corners.ToArray();
        }
        /// <summary>
        /// Fonds all bounds of given gameobjects
        /// </summary>
        /// <returns></returns> bound
        private Bounds GetAllBounds()
        {
            Bounds bounds = new Bounds();

            var renderers = GetComponentsInChildren<MeshRenderer>(false);
            foreach (var item in renderers)
            {
                bounds.Encapsulate(item.bounds);
            }

            return bounds;
        }

        /// <summary>
        /// Checks all adjacenty objects
        /// </summary>
        /// <param name="dist"></param>distanfce to check
        /// <param name="checkOthers"></param> to check recursively
        public void CheckAdjacencies(float dist, bool checkOthers = false)
        {

            Vector3 centre = GetComponent<BoxCollider>().bounds.center;
            Vector3 extend = GetComponent<BoxCollider>().size;
            Ray[] rays = new Ray[6];
            if (tag == "Untagged")
            {
                rays[0] = new Ray(centre + transform.right * (extend.x / 2 - 0.5f), transform.right);
                rays[1] = new Ray(centre + transform.forward * (extend.z / 2 - 0.5f), transform.forward);
                rays[2] = new Ray(centre - transform.right * (extend.x / 2 - 0.5f), -transform.right);
                rays[3] = new Ray(centre - transform.forward * (extend.z / 2 - 0.5f), -transform.forward);
                rays[4] = new Ray(centre + transform.up * (extend.y / 2 - 0.5f), transform.up);
                rays[5] = new Ray(centre - transform.up * (extend.y / 2 - 0.5f), -transform.up);

            }
            else if (tag == "MirroredX")
            {
                rays[0] = new Ray(centre - transform.right * (extend.x / 2 - 0.5f), -transform.right);
                rays[1] = new Ray(centre + transform.forward * (extend.z / 2 - 0.5f), transform.forward);
                rays[2] = new Ray(centre + transform.right * (extend.x / 2 - 0.5f), transform.right);
                rays[3] = new Ray(centre - transform.forward * (extend.z / 2 - 0.5f), -transform.forward);
                rays[4] = new Ray(centre + transform.up * (extend.y / 2 - 0.5f), transform.up);
                rays[5] = new Ray(centre - transform.up * (extend.y / 2 - 0.5f), -transform.up);

            }
            else if (tag == "MirroredZ")
            {
                rays[0] = new Ray(centre + transform.right * (extend.x / 2 - 0.5f), transform.right);
                rays[1] = new Ray(centre - transform.forward * (extend.z / 2 - 0.5f), -transform.forward);
                rays[2] = new Ray(centre - transform.right * (extend.x / 2 - 0.5f), -transform.right);
                rays[3] = new Ray(centre + transform.forward * (extend.z / 2 - 0.5f), transform.forward);
                rays[4] = new Ray(centre + transform.up * (extend.y / 2 - 0.5f), transform.up);
                rays[5] = new Ray(centre - transform.up * (extend.y / 2 - 0.5f), -transform.up);

            }
            else if (tag == "Mirrored")
            {
                rays[0] = new Ray(centre - transform.right * (extend.x / 2 - 0.5f), -transform.right);
                rays[1] = new Ray(centre - transform.forward * (extend.z / 2 - 0.5f), -transform.forward);
                rays[2] = new Ray(centre - transform.right * (extend.x / 2 - 0.5f), -transform.right);
                rays[3] = new Ray(centre - transform.forward * (extend.z / 2 - 0.5f), -transform.forward);
                rays[4] = new Ray(centre + transform.up * (extend.y / 2 - 0.5f), transform.up);
                rays[5] = new Ray(centre - transform.up * (extend.y / 2 - 0.5f), -transform.up);

            }


            for (int i = 0; i < rays.Length; i++)
            {
                RaycastHit hit;
                if (Physics.Raycast(rays[i], out hit))
                {
                    var other = hit.collider.gameObject.GetComponent<PlaceablePrefab>();
                    if (other != null && other.GetComponent<PlaceablePrefab>().enabled == true)
                    {
                        if (hit.distance < 1.8f)
                        {
                            hasAdjacent[i] = true;
                            adjacent[i] = hit.collider.gameObject;

                            if (checkOthers)
                            {

                                other.CheckAdjacencies(dist);
                            }
                        }
                        else
                        {
                            hasAdjacent[i] = false;
                            if (adjacent[i] != null)
                            {
                                for (int j = 0; j < adjacent[i].GetComponent<PlaceablePrefab>().adjacent.Length; j++)
                                {
                                    if (adjacent[i].GetComponent<PlaceablePrefab>().adjacent[j] == gameObject)
                                    {
                                        adjacent[i].GetComponent<PlaceablePrefab>().adjacent[j] = null;
                                    }
                                }
                            }
                            adjacent[i] = null;
                        }
                    }
                }
                else
                {
                    hasAdjacent[i] = false;
                    if (adjacent[i] != null)
                    {
                        for (int j = 0; j < adjacent[i].GetComponent<PlaceablePrefab>().adjacent.Length; j++)
                        {
                            if (adjacent[i].GetComponent<PlaceablePrefab>().adjacent[j] == gameObject)
                            {
                                adjacent[i].GetComponent<PlaceablePrefab>().adjacent[j] = null;
                            }
                        }
                    }
                    adjacent[i] = null;
                }
            }
            CheckCompatibility();
        }
        /// <summary>
        /// Check if there are half modules
        /// </summary>
        public void CheckCompatibility()
        {

            if (tag == "MirroredX" || tag == "Mirrored")
            {
                if (adjacent[0] == null && resolvedLeft)
                {
                    if (leftUnButton == null)
                    {
                        Vector3 worldPos = GetComponent<Collider>().bounds.center - transform.right * size.x / 2.0f;
                        buttonPositionLeft = new GameObject().transform;
                        buttonPositionLeft.SetParent(transform);
                        buttonPositionLeft.position = worldPos;
                        InstantiateUnresolveButton(ref leftUnButton, "left", worldPos, buttonPositionLeft);
                    }

                }
                else
                {
                    if (leftUnButton != null)
                    {
                        Destroy(leftUnButton.gameObject);
                        Destroy(buttonPositionLeft.gameObject);
                        leftUnButton = null;
                    }
                }
                if (adjacent[2] == null && resolvedRight)
                {
                    if (rightUnButton == null)
                    {
                        Vector3 worldPos = GetComponent<Collider>().bounds.center + transform.right * size.x / 2.0f;
                        buttonPositionRight = new GameObject().transform;
                        buttonPositionRight.SetParent(transform);
                        buttonPositionRight.position = worldPos;
                        InstantiateUnresolveButton(ref rightUnButton, "right", worldPos, buttonPositionRight);
                    }


                }
                else
                {
                    if (rightUnButton != null)
                    {
                        Destroy(rightUnButton.gameObject);
                        Destroy(buttonPositionLeft.gameObject);
                        rightUnButton = null;
                    }
                }
                if (proceduralCluster.halfRight)
                {
                    if (adjacent[2] != null && !adjacent[2].GetComponent<ProceduralCluster>().halfLeft)
                    {
                        if (rightButton == null)
                        {

                            Vector3 worldPos = GetComponent<Collider>().bounds.center + transform.right * size.x / 2.0f;

                            buttonPositionRight = new GameObject().transform;
                            buttonPositionRight.SetParent(transform);
                            buttonPositionRight.position = worldPos;
                            InstantiateResolveButton(ref rightButton, "right", worldPos, buttonPositionRight);
                        }
                    }

                    else
                    {
                        if (rightButton != null)
                        {
                            Destroy(rightButton.gameObject);
                            Destroy(buttonPositionRight.gameObject);
                            rightButton = null;
                        }

                    }
                }

                if (proceduralCluster.halfLeft)
                {

                    if (adjacent[0] != null && !adjacent[0].GetComponent<ProceduralCluster>().halfRight)
                    {
                        if (leftButton == null)
                        {
                            Vector3 worldPos = GetComponent<Collider>().bounds.center - transform.right * size.x / 2.0f;

                            buttonPositionLeft = new GameObject().transform;
                            buttonPositionLeft.SetParent(transform);
                            buttonPositionLeft.position = worldPos;
                            InstantiateResolveButton(ref leftButton, "left", worldPos, buttonPositionLeft);
                        }
                    }
                    else
                    {
                        if (leftButton != null)
                        {
                            Destroy(leftButton.gameObject);
                            Destroy(buttonPositionLeft.gameObject);
                            leftButton = null;
                        }

                    }
                }
            }
            else
            {
                if (adjacent[0] == null && resolvedRight)
                {
                    if (rightUnButton == null)
                    {
                        Vector3 worldPos = GetComponent<Collider>().bounds.center + transform.right * size.x / 2.0f;

                        buttonPositionRight = new GameObject().transform;
                        buttonPositionRight.SetParent(transform);
                        buttonPositionRight.position = worldPos;
                        InstantiateUnresolveButton(ref rightUnButton, "right", worldPos, buttonPositionRight);
                    }


                }
                else
                {
                    if (rightUnButton != null)
                    {
                        Destroy(rightUnButton.gameObject);
                        Destroy(buttonPositionRight.gameObject);
                        rightUnButton = null;
                    }
                }
                if (adjacent[2] == null && resolvedLeft)
                {
                    if (leftUnButton == null)
                    {
                        Vector3 worldPos = GetComponent<Collider>().bounds.center - transform.right * size.x / 2.0f;

                        buttonPositionLeft = new GameObject().transform;
                        buttonPositionLeft.SetParent(transform);
                        buttonPositionLeft.position = worldPos;
                        InstantiateUnresolveButton(ref leftUnButton, "left", worldPos, buttonPositionLeft);
                    }


                }
                else
                {
                    if (leftUnButton != null)
                    {
                        Destroy(leftUnButton.gameObject);
                        Destroy(buttonPositionLeft.gameObject);
                        leftUnButton = null;
                    }
                }

                if (proceduralCluster.halfRight)
                {
                    if (adjacent[0] != null && !adjacent[0].GetComponent<ProceduralCluster>().halfLeft)
                    {
                        if (rightButton == null)
                        {

                            Vector3 worldPos = GetComponent<Collider>().bounds.center + transform.right * size.x / 2.0f;

                            buttonPositionRight = new GameObject().transform;
                            buttonPositionRight.SetParent(transform);
                            buttonPositionRight.position = worldPos;
                            InstantiateResolveButton(ref rightButton, "right", worldPos, buttonPositionRight);
                        }
                    }
                    else
                    {
                        if (rightButton != null)
                        {
                            Destroy(rightButton.gameObject);
                            Destroy(buttonPositionRight.gameObject);
                            rightButton = null;
                        }

                    }
                }
                if (proceduralCluster.halfLeft)
                {

                    if (adjacent[2] != null && !adjacent[2].GetComponent<ProceduralCluster>().halfRight)
                    {
                        if (leftButton == null)
                        {
                            Vector3 worldPos = GetComponent<Collider>().bounds.center - transform.right * size.x / 2.0f;

                            buttonPositionLeft = new GameObject().transform;
                            buttonPositionLeft.SetParent(transform);
                            buttonPositionLeft.position = worldPos;
                            InstantiateResolveButton(ref leftButton, "left", worldPos, buttonPositionLeft);
                        }
                    }
                    else
                    {
                        if (leftButton != null)
                        {
                            Destroy(leftButton.gameObject);
                            Destroy(buttonPositionLeft.gameObject);
                            leftButton = null;
                        }

                    }
                }
            }
        }
        /// <summary>
        /// Called if a half module is resolved
        /// </summary>
        /// <param name="side"></param> the resolved side
        public void Expand(string side)
        {

            if (side == "right")
            {
                if ((tag == "Untagged" || tag == "MirroredZ"))
                {
                    if (adjacent[0] != null && adjacent[2] == null)
                    {
                        Move(1.8f, "right");

                    }

                    else if (adjacent[0] != null && adjacent[2] != null)
                    {

                        var temp = adjacent[2];
                        Move(1.8f, "right");
                        temp.GetComponent<PlaceablePrefab>().SelectGroup();
                        // RestoreParent();
                        temp.GetComponent<PlaceablePrefab>().Move(1.8f, "right");
                        temp.GetComponent<PlaceablePrefab>().DeSelect();


                    }
                }
                else
                {
                    if (adjacent[2] != null && adjacent[0] == null)
                    {
                        Move(1.8f, "right");
                    }
                    else if (adjacent[2] != null && adjacent[0] != null)
                    {
                        var temp = adjacent[0];
                        Move(1.8f, "right");
                        temp.GetComponent<PlaceablePrefab>().SelectGroup();
                        // RestoreParent();
                        temp.GetComponent<PlaceablePrefab>().Move(1.8f, "right");
                        temp.GetComponent<PlaceablePrefab>().DeSelect();

                    }

                }
                if (rightButton != null)
                {
                    Destroy(rightButton.gameObject);
                    rightButton = null;
                    resolvedRight = true;
                    Destroy(buttonPositionRight.gameObject);
                }
            }
            else if (side == "left")
            {
                if (tag == "MirroredX" || tag == "Mirrored")
                {
                    if (adjacent[0] != null && adjacent[2] == null)
                    {
                        Move(-1.8f, "left");

                    }
                    else if (adjacent[0] != null && adjacent[2] != null)
                    {
                        var temp = adjacent[2];
                        Move(-1.8f, "left"); ;
                        temp.GetComponent<PlaceablePrefab>().SelectGroup();
                        //RestoreParent();
                        temp.GetComponent<PlaceablePrefab>().Move(-1.8f, "left");
                        temp.GetComponent<PlaceablePrefab>().DeSelect();



                    }
                }
                else
                {
                    if (adjacent[2] != null && adjacent[0] == null)
                    {
                        Move(-1.8f, "left");
                    }
                    else if (adjacent[2] != null && adjacent[0] != null)
                    {
                        var temp = adjacent[0];
                        Move(-1.8f, "left"); ;
                        temp.GetComponent<PlaceablePrefab>().SelectGroup();
                        // RestoreParent();
                        temp.GetComponent<PlaceablePrefab>().Move(-1.8f, "left");
                        temp.GetComponent<PlaceablePrefab>().DeSelect();


                    }
                }
                if (leftButton != null)
                {
                    Destroy(leftButton.gameObject);
                    leftButton = null;
                    resolvedLeft = true;
                    Destroy(buttonPositionLeft.gameObject);
                }
            }

            proceduralCluster.Expand(side);
            if (resolvedRoom != null)
            {
                resolvedRoom(proceduralCluster);
            }
        }
        /// <summary>
        /// Moves cluster, called in case of resolve/unresolve
        /// </summary>
        /// <param name="dist"></param> The distance to move
        /// <param name="side"></param> Which side to move to
        public void Move(float dist, string side)
        {

            transform.position += transform.right * -dist;
        }
        /// <summary>
        /// Called when cluster is unresolved
        /// </summary>
        /// <param name="side"></param>
        public void ExpandBack(string side)
        {

            if (side == "right")
            {
                if ((tag == "Untagged" || tag == "MirroredZ"))
                {
                    if (adjacent[0] != null)
                        Move(-1.8f, "right");
                }
                else
                {
                    if (adjacent[2] != null)
                        Move(1.8f, "right");
                }
                if (rightUnButton != null)
                {
                    Destroy(rightUnButton.gameObject);
                    rightUnButton = null;
                    resolvedRight = false;
                    Destroy(buttonPositionRight.gameObject);
                }
            }
            else if (side == "left")
            {
                if (tag == "MirroredX" || tag == "Mirrored")
                {
                    if (adjacent[0] != null)
                        Move(-1.8f, "left");
                }
                else
                {
                    if (adjacent[2] != null)
                        Move(1.8f, "left");
                }
                if (leftUnButton != null)
                {
                    Destroy(leftUnButton.gameObject);
                    leftUnButton = null;
                    resolvedLeft = false;
                    Destroy(buttonPositionLeft.gameObject);
                }
            }



            proceduralCluster.ExpandBack(side);
            if (resolvedRoom != null)
            {
                resolvedRoom(proceduralCluster);
            }
        }
        /// <summary>
        /// Creates the button to start resolve process
        /// </summary>
        /// <param name="button"></param> the button with IncompatibilityResolveButton script on
        /// <param name="side"></param> which side to instantiate
        /// <param name="worldPos"></param> The position to instantiate
        /// <param name="buttonPosition"></param> A gameobject to help with transformations of button
        private void InstantiateResolveButton(ref IncompatibilityResolveButton button, string side, Vector3 worldPos, Transform buttonPosition)
        {
            GameObject obj = Instantiate(incompatibilityButtonPRefab, GameObject.Find("Incompatibilities").transform);
            button = obj.GetComponent<IncompatibilityResolveButton>();
            button.worldPos = worldPos;
            button.cluster = this;
            button.side = side;
            button.myPosition = buttonPosition;
        }
        /// <summary>
        /// Creates the button to start unresolve process
        /// </summary>
        /// <param name="button"></param>the button with IncompatibilityResolveButton script on
        /// <param name="side"></param>which side to instantiate
        /// <param name="worldPos"></param>The position to instantiate
        /// <param name="buttonPosition"></param>A gameobject to help with transformations of button
        private void InstantiateUnresolveButton(ref IncompatibilityResolveButton button, string side, Vector3 worldPos, Transform buttonPosition)
        {
            GameObject obj = Instantiate(incompatibilityUnresolveButtonPRefab, GameObject.Find("Incompatibilities").transform);
            button = obj.GetComponent<IncompatibilityResolveButton>();
            button.worldPos = worldPos;
            button.cluster = this;
            button.side = side;
            button.myPosition = buttonPosition;
        }
        /// <summary>
        /// Moves gameobject up. down a level
        /// </summary>
        /// <param name="tilePosition"></param> the position of the gameobject on grid
        /// <param name="other"></param>the 
        public void OnChangeLevel(Vector3 tilePosition, GameObject other = null)
        {
            if (!name.Contains("Entrance") && !name.Contains("Plant") && !name.Contains("Hall"))
            {
                transform.position = new Vector3(tilePosition.x, level * size.y, tilePosition.z);
            }
        }
    }
}
