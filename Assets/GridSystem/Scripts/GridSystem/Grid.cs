﻿using BrydenWoodUnity.GeometryManipulation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]
public class Grid : MonoBehaviour
{

    public float tileSizeX = 3.6f;
    public float tileSizeY = 18.0f;
    public int xNum = 40;
    public int yNum = 40;
    public bool initializeOnStart = false;
    public bool snapOnObjects = false;
    public float objectSnapDistance = 36.0f;
    public List<GameObject> placePrefabs;

    public Transform placedItemParent;
    public LayerMask raycastingMask;

    [HideInInspector]
    public Mesh gridMesh;
    [HideInInspector]
    public MeshFilter meshFilter;
    [HideInInspector]
    public MeshRenderer meshRenderer;
    [HideInInspector]
    public MeshCollider meshCollider;
    [HideInInspector]
    public Material material;
    [HideInInspector]
    public Camera camera;

    private List<Vector3> debugPoints;

    private GameObject currentObject;
    private int prefabIndex = 0;
    private Vector3 currentEuler = Vector3.zero;
    private bool initialized = false;
    private bool placing = false;
    private List<GameObject> placedPrefabs;

    private string loadingMessage = "";

    public delegate void OnGrideCalculated(Grid grid);
    public static event OnGrideCalculated gridCalculated;

    private List<Vector3> connections;
    private List<Vector3> projection;

    // Use this for initialization
    void Awake()
    {
        if (initializeOnStart)
        {
            Initialize();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (initialized)
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                StartPlacing();
            }

            if (placing)
            {
                CastRay(camera);

                /*
                if (Input.GetAxis("Mouse ScrollWheel") != 0)
                {
                    float angle = 90.0f;
                    if (Input.GetAxis("Mouse ScrollWheel") > 0)
                    {
                        angle *= -1;
                    }
                    RotatePrefab(angle);
                }

                if (Input.GetKeyDown("o"))
                {
                    prefabIndex = (prefabIndex + 1) % (placePrefabs.Count);
                    SetCurrentPrefab(prefabIndex);
                }
                */

                if (Input.GetMouseButtonDown(0))
                {
                    StartCoroutine(PlacePrefab());
                }

                /*
                if (debugPoints != null)
                {
                    Debug.DrawLine(debugPoints[0], debugPoints[1]);
                    Debug.DrawLine(debugPoints[1], debugPoints[2]);
                    Debug.DrawLine(debugPoints[2], debugPoints[0]);
                    Debug.DrawLine(debugPoints[3], debugPoints[4]);
                    Debug.DrawLine(debugPoints[4], debugPoints[5]);
                    Debug.DrawLine(debugPoints[5], debugPoints[3]);
                }
                */

                if (connections != null && connections.Count > 1)
                {
                    Debug.DrawLine(connections[0], connections[1]);
                }

                if (projection != null && projection.Count > 1)
                {
                    Debug.DrawLine(projection[0], projection[1]);
                }
            }
        }
    }

    private void OnGUI()
    {
        if (!String.IsNullOrEmpty(loadingMessage))
        {
            GUI.color = Color.black;
            GUI.Label(new Rect(220, 50, 500, 20), loadingMessage);
            GUI.color = Color.white;
        }

        if (GUI.Button(new Rect(10, 110, 200, 20), "Place Item"))
        {
            StartPlacing();
        }

        snapOnObjects = GUI.Toggle(new Rect(10, 140, 200, 20), snapOnObjects, "Snap to Objects");
    }

    #region Public Methods
    public void Initialize()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        meshFilter = GetComponent<MeshFilter>();
        meshCollider = GetComponent<MeshCollider>();
        if (meshRenderer.material == null)
        {
            material = new Material(Shader.Find("Standard"));
            material.color = Color.grey;
            meshRenderer.material = material;
        }
        else
        {
            material = meshRenderer.material;
        }

        camera = Camera.main;
        gridMesh = GenerateSingleMesh(tileSizeX, tileSizeY, xNum, yNum);

        meshFilter.sharedMesh = gridMesh;
        meshCollider.sharedMesh = gridMesh;

        if (placedItemParent == null)
        {
            placedItemParent = transform;
        }
        placedPrefabs = new List<GameObject>();
        SetCurrentPrefab(prefabIndex);
        initialized = true;
    }

    public void Initialize(Polygon boundary)
    {
        meshRenderer = GetComponent<MeshRenderer>();
        meshFilter = GetComponent<MeshFilter>();
        meshCollider = GetComponent<MeshCollider>();
        if (meshRenderer.material == null)
        {
            material = new Material(Shader.Find("Standard"));
            material.color = Color.grey;
            meshRenderer.material = material;
        }
        else
        {
            material = meshRenderer.material;
        }

        camera = Camera.main;
        gridMesh = GenerateSingleMesh(tileSizeX, tileSizeY, xNum, yNum);
        gridMesh = CheckForBoundaryInclusion(gridMesh, boundary);
        meshFilter.sharedMesh = gridMesh;
        meshCollider.sharedMesh = gridMesh;

        if (placedItemParent == null)
        {
            placedItemParent = transform;
        }

        placedPrefabs = new List<GameObject>();
        //SetCurrentPrefab(prefabIndex);
        initialized = true;
    }

    public IEnumerator InitializeCoroutine(Polygon boundary)
    {
        meshRenderer = GetComponent<MeshRenderer>();
        meshFilter = GetComponent<MeshFilter>();
        meshCollider = GetComponent<MeshCollider>();
        if (meshRenderer.material == null)
        {
            material = new Material(Shader.Find("Standard"));
            material.color = Color.grey;
            meshRenderer.material = material;
        }
        else
        {
            material = meshRenderer.material;
        }

        camera = Camera.main;
        gridMesh = GenerateSingleMesh(tileSizeX, tileSizeY, xNum, yNum);
        yield return CheckForBoundaryInclusion(boundary);
    }

    public void OnFinishedInitializing()
    {
        meshFilter.sharedMesh = gridMesh;
        meshCollider.sharedMesh = gridMesh;

        if (placedItemParent == null)
        {
            placedItemParent = transform;
        }

        placedPrefabs = new List<GameObject>();
        //SetCurrentPrefab(prefabIndex);
        initialized = true;
        if (gridCalculated != null)
        {
            gridCalculated(this);
        }
    }

    public void StartPlacing()
    {
        placing = true;
        SetCurrentPrefab(prefabIndex);
    }

    public void StopPlacing()
    {
        placing = false;
    }

    public void SetCurrentPrefab(int i)
    {
        if (currentObject != null)
        {
            Destroy(currentObject);
        }
        currentObject = Instantiate(placePrefabs[i], placedItemParent);
        currentObject.transform.eulerAngles = transform.eulerAngles;
        currentObject.transform.localPosition = Vector3.zero;
        currentObject.transform.localScale = Vector3.one;
        currentEuler = Vector3.zero;
    }

    public void RotatePrefab(float angle)
    {
        currentEuler = currentObject.transform.eulerAngles + new Vector3(0, angle, 0);
        currentObject.transform.eulerAngles = currentEuler;
    }

    public List<Vector3> GetGridLines()
    {
        List<Vector3> gridPoints = new List<Vector3>();

        for (int i = 0; i < gridMesh.triangles.Length - 5; i += 6)
        {
            List<Vector3> faceVertices = new List<Vector3>();

            faceVertices.Add(transform.TransformPoint(gridMesh.vertices[gridMesh.triangles[i]]));
            faceVertices.Add(transform.TransformPoint(gridMesh.vertices[gridMesh.triangles[i + 1]]));
            faceVertices.Add(transform.TransformPoint(gridMesh.vertices[gridMesh.triangles[i + 2]]));
            faceVertices.Add(transform.TransformPoint(gridMesh.vertices[gridMesh.triangles[i + 3]]));
            faceVertices.Add(transform.TransformPoint(gridMesh.vertices[gridMesh.triangles[i + 4]]));
            faceVertices.Add(transform.TransformPoint(gridMesh.vertices[gridMesh.triangles[i + 5]]));

            var cleanVertices = faceVertices.Distinct().ToList();
            gridPoints.AddRange(cleanVertices);
        }

        return gridPoints;
    }

    #endregion

    #region Private Methods

    private Mesh GenerateSingleMesh(float sizeX, float sizeY, int xNum, int yNum)
    {
        Mesh mesh = new Mesh();
        float startX = -(sizeX * xNum) / 2;
        float startY = -(sizeY * yNum) / 2;
        Vector3 startVector = new Vector3(startX, 0, startY);
        List<Vector3> vertices = new List<Vector3>();
        List<int> triangles = new List<int>();

        for (int i=0; i<xNum*yNum; i++)
        {
            var index0 = i%xNum;
            var index1 = index0 + 1;
            var index2 = ((i - index0) / xNum);
            var index3 = index2+1;

            triangles.AddRange(new List<int>() { vertices.Count + 2, vertices.Count + 1, vertices.Count, vertices.Count + 3, vertices.Count + 2, vertices.Count });
            Vector3 vecA = startVector + new Vector3(sizeX * index0, 0, sizeY * index2);
            vertices.Add(vecA);
            Vector3 vecB = startVector + new Vector3(sizeX * index1, 0, sizeY * index2);
            vertices.Add(vecB);
            Vector3 vecC = startVector + new Vector3(sizeX * index1, 0, sizeY * index3);
            vertices.Add(vecC);
            Vector3 vecD = startVector + new Vector3(sizeX * index0, 0, sizeY * index3);
            vertices.Add(vecD);
        }

        /*
        for (int i = 0; i < xNum; i++)
        {
            for (int j = 0; j < yNum; j++)
            {
                Vector3 vecA = startVector + new Vector3(sizeX * i, 0, sizeY * j);
                Vector3 vecB = startVector + new Vector3(sizeX * (i + 1), 0, sizeY * j);
                Vector3 vecC = startVector + new Vector3(sizeX * (i + 1), 0, sizeY * (j + 1));
                Vector3 vecD = startVector + new Vector3(sizeX * i, 0, sizeY * (j + 1));

                Vector3 centre = (vecA + vecB + vecC + vecD) / 4;

                Vector3[] verts = new Vector3[] { vecA, vecB, vecC, vecD };
                int[] tris = new int[] { 2, 1, 0, 3, 2, 0 };
            }
        }
        */
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();

        return mesh;
    }

    private IEnumerator CheckForBoundaryInclusion(Polygon boundary)
    {
        var tris = gridMesh.triangles.ToList();
        var verts = gridMesh.vertices.ToList();

        for (int i = 0; i < gridMesh.triangles.Length - 5; i += 6)
        {
            bool inside = false;
            int index = gridMesh.triangles[i];
            int index2 = gridMesh.triangles[i + 1];
            int index3 = gridMesh.triangles[i + 2];
            int index4 = gridMesh.triangles[i + 3];
            int index5 = gridMesh.triangles[i + 4];
            int index6 = gridMesh.triangles[i + 5];

            Vector3 centre = 
                gridMesh.vertices[index] +
                gridMesh.vertices[index2] +
                gridMesh.vertices[index3] +
                gridMesh.vertices[index4] +
                gridMesh.vertices[index5] +
                gridMesh.vertices[index6];

            centre /= 6.0f;
            centre = transform.TransformPoint(centre);

            Ray r = new Ray(centre + new Vector3(0, 0.5f, 0), Vector3.down);
            RaycastHit hit;
            if (Physics.Raycast(r, out hit))
            {
                if (hit.collider.gameObject.GetComponent<Polygon>() != null)
                {
                    if (hit.collider == boundary.GetComponent<MeshCollider>())
                    {
                        inside = true;
                    }
                }
            }

            if (!inside)
            {
                tris.Remove(index);
                tris.Remove(index2);
                tris.Remove(index3);
                tris.Remove(index4);
                tris.Remove(index5);
                tris.Remove(index6);
            }
            if (i % 200 == 0)
            {
                loadingMessage = string.Format("Grid Mesh Calculation: {0}%", (int)(((float)i / gridMesh.triangles.Length) * 100));
                yield return new WaitForEndOfFrame();
            }
        }
        loadingMessage = "";
        gridMesh.vertices = verts.ToArray();
        gridMesh.triangles = tris.ToArray();
        gridMesh.RecalculateBounds();
        gridMesh.RecalculateNormals();
        OnFinishedInitializing();
    }

    private Mesh CheckForBoundaryInclusion(Mesh mesh, Polygon boundary)
    {
        Mesh m_mesh = new Mesh();
        var tris = mesh.triangles.ToList();
        var verts = mesh.vertices.ToList();

        for (int i = 0; i < mesh.triangles.Length - 5; i += 6)
        {
            bool inside = false;
            int index = mesh.triangles[i];
            int index2 = mesh.triangles[i + 1];
            int index3 = mesh.triangles[i + 2];
            int index4 = mesh.triangles[i + 3];
            int index5 = mesh.triangles[i + 4];
            int index6 = mesh.triangles[i + 5];

            List<Vector3> facePoints = new List<Vector3>();
            facePoints.Add(mesh.vertices[index]);
            facePoints.Add(mesh.vertices[index2]);
            facePoints.Add(mesh.vertices[index3]);
            facePoints.Add(mesh.vertices[index4]);
            facePoints.Add(mesh.vertices[index5]);
            facePoints.Add(mesh.vertices[index6]);
            Vector3 centre = Vector3.zero;
            for (int j = 0; j < facePoints.Count; j++)
            {
                centre += facePoints[j];
            }
            centre /= (float)facePoints.Count;
            centre = transform.TransformPoint(centre);
            Ray r = new Ray(centre + new Vector3(0, 0.5f, 0), Vector3.down);
            RaycastHit hit;
            if (Physics.Raycast(r, out hit))
            {
                if (hit.collider == boundary.GetComponent<MeshCollider>())
                {
                    inside = true;
                }
            }

            if (!inside)
            {
                tris.Remove(index);
                tris.Remove(index2);
                tris.Remove(index3);
                tris.Remove(index4);
                tris.Remove(index5);
                tris.Remove(index6);
            }
        }
        m_mesh.vertices = verts.ToArray();
        m_mesh.triangles = tris.ToArray();
        m_mesh.RecalculateBounds();
        m_mesh.RecalculateNormals();
        return m_mesh;
    }

    private void CastRay(Camera camera)
    {
        connections = new List<Vector3>();
        projection = new List<Vector3>();
        if (!EventSystem.current.IsPointerOverGameObject(-1))
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 5000, raycastingMask))
            {
                if (hit.collider == meshCollider && currentObject != null)
                {
                    var tri = hit.triangleIndex;
                    var corresponding = tri + 1;

                    if (tri % 2 == 1)
                    {
                        corresponding = tri - 1;
                    }

                    debugPoints = new List<Vector3>();
                    debugPoints.Add(transform.TransformPoint(gridMesh.vertices[gridMesh.triangles[tri * 3 + 0]]));
                    debugPoints.Add(transform.TransformPoint(gridMesh.vertices[gridMesh.triangles[tri * 3 + 1]]));
                    debugPoints.Add(transform.TransformPoint(gridMesh.vertices[gridMesh.triangles[tri * 3 + 2]]));
                    debugPoints.Add(transform.TransformPoint(gridMesh.vertices[gridMesh.triangles[corresponding * 3 + 0]]));
                    debugPoints.Add(transform.TransformPoint(gridMesh.vertices[gridMesh.triangles[corresponding * 3 + 1]]));
                    debugPoints.Add(transform.TransformPoint(gridMesh.vertices[gridMesh.triangles[corresponding * 3 + 2]]));

                    Vector3 centre = Vector3.zero;
                    for (int i = 0; i < debugPoints.Count; i++)
                    {
                        centre += debugPoints[i];
                    }
                    centre /= debugPoints.Count;
                    Bounds bounds = currentObject.GetComponent<Collider>().bounds;
                    if (tri % 2 == 1)
                    {
                        currentObject.transform.position = centre;//debugPoints[5] + currentObject.transform.right.normalized * bounds.extents.x + currentObject.transform.forward.normalized * bounds.extents.z;//new Vector3(bounds.extents.x, 0, bounds.extents.z);
                    }
                    else
                    {
                        currentObject.transform.position = centre;// debugPoints[2] + currentObject.transform.right.normalized* bounds.extents.x + currentObject.transform.forward.normalized * bounds.extents.z;
                    }
                    if (snapOnObjects)
                    {
                        float minDIst = float.MaxValue;
                        GameObject closer = null;
                        if (placedPrefabs != null && placedPrefabs.Count > 0)
                        {
                            for (int i = 0; i < placedPrefabs.Count; i++)
                            {
                                if (placedPrefabs[i] != currentObject)
                                {
                                    var dist = Vector3.Distance(placedPrefabs[i].GetComponent<Collider>().bounds.center, currentObject.GetComponent<Collider>().bounds.center);
                                    if (dist < minDIst)
                                    {
                                        minDIst = dist;
                                        closer = placedPrefabs[i];
                                    }
                                }
                            }

                            if (minDIst < objectSnapDistance)
                            {
                                /*
                                var closeCentre = closer.GetComponent<Collider>().bounds.center;
                                Vector3 closerPos = new Vector3(closeCentre.x, closer.transform.position.y, closeCentre.z);
                                var thisCentre = currentObject.GetComponent<Collider>().bounds.center;
                                Vector3 thisPos = new Vector3(thisCentre.x, currentObject.transform.position.y, thisCentre.z);

                                connections.Add(thisPos);
                                connections.Add(closerPos);

                                Vector3 connection = connections[1] - connections[0];
                                var projRight = Vector3.Project(connection, closer.transform.right);
                                var projForward = Vector3.Project(connection, closer.transform.forward);

                                var otherPrefab = closer.GetComponent<PlaceablePrefab>();
                                var thisPrefab = closer.GetComponent<PlaceablePrefab>();

                                float angle1 = Vector3.Angle(connection, projRight);
                                float angle2 = Vector3.Angle(connection, projForward);

                                var cross1 = Vector3.Cross(connection, projRight);
                                var cross2 = Vector3.Cross(connection, projForward);
                                var addVector = Vector3.zero;

                                if (Mathf.Abs(angle1) < Mathf.Abs(angle2))
                                {
                                    addVector = closer.transform.right.normalized * (otherPrefab.size.x / 2.0f + thisPrefab.size.x / 2.0f);
                                    if (cross1.y < 0)
                                    {
                                        addVector *= -1;
                                    }
                                    projection.Add(closerPos);
                                    projection.Add(closerPos + (-projRight));
                                }
                                else if (angle2 < angle1)
                                {
                                    addVector = closer.transform.forward.normalized * (otherPrefab.size.z / 2.0f + thisPrefab.size.z / 2.0f);
                                    if (cross2.y < 0)
                                    {
                                        addVector *= -1;
                                    }
                                    projection.Add(closerPos);
                                    projection.Add(closerPos + (-projForward));
                                }

                                currentObject.transform.position = closer.transform.position + addVector;
                                */
                                var otherPrefab = closer.GetComponent<PlaceablePrefab>();
                                var thisPrefab = closer.GetComponent<PlaceablePrefab>();

                                List<Vector3> closerSnaps = new List<Vector3>();
                                closerSnaps.Add(closer.transform.position + closer.transform.right * (otherPrefab.size.x / 2.0f));
                                closerSnaps.Add(closer.transform.position + closer.transform.forward * (otherPrefab.size.z / 2.0f));
                                closerSnaps.Add(closer.transform.position - closer.transform.right * (otherPrefab.size.x / 2.0f));
                                closerSnaps.Add(closer.transform.position - closer.transform.forward * (otherPrefab.size.z / 2.0f));

                                List<Vector3> currentSnaps = new List<Vector3>();
                                currentSnaps.Add(currentObject.transform.right * thisPrefab.size.x / 2.0f);
                                currentSnaps.Add(currentObject.transform.forward * thisPrefab.size.z / 2.0f);
                                currentSnaps.Add(-currentObject.transform.right * thisPrefab.size.x / 2.0f);
                                currentSnaps.Add(-currentObject.transform.forward * thisPrefab.size.z / 2.0f);

                                float snapdist = float.MaxValue;
                                int snapIndex = -1;
                                for (int i = 0; i < closerSnaps.Count; i++)
                                {
                                    if (Vector3.Distance(currentObject.transform.position, closerSnaps[i]) < snapdist)
                                    {
                                        snapdist = Vector3.Distance(currentObject.transform.position, closerSnaps[i]);
                                        snapIndex = i;
                                    }
                                }

                                if (snapIndex != -1)
                                {
                                    currentObject.transform.position = closerSnaps[snapIndex] + currentSnaps[snapIndex];
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private Bounds TransformBounds(Transform _transform, Bounds _localBounds)
    {
        var center = _transform.TransformPoint(_localBounds.center);

        // transform the local extents' axes
        var extents = _localBounds.extents;
        var axisX = _transform.TransformVector(extents.x, 0, 0);
        var axisY = _transform.TransformVector(0, extents.y, 0);
        var axisZ = _transform.TransformVector(0, 0, extents.z);

        // sum their absolute value to get the world extents
        extents.x = Mathf.Abs(axisX.x) + Mathf.Abs(axisY.x) + Mathf.Abs(axisZ.x);
        extents.y = Mathf.Abs(axisX.y) + Mathf.Abs(axisY.y) + Mathf.Abs(axisZ.y);
        extents.z = Mathf.Abs(axisX.z) + Mathf.Abs(axisY.z) + Mathf.Abs(axisZ.z);

        return new Bounds { center = center, extents = extents };
    }

    private IEnumerator PlacePrefab()
    {
        if (!EventSystem.current.IsPointerOverGameObject(-1) && currentObject != null)
        {
            if (currentObject.GetComponent<PlaceablePrefab>().allowed)
            {
                placedPrefabs.Add(currentObject);
                currentObject = null;
                StopPlacing();
                /*
                currentObject = Instantiate(placePrefabs[prefabIndex], placedItemParent);
                currentObject.transform.eulerAngles = transform.eulerAngles;
                currentObject.transform.localPosition = Vector3.zero;
                currentObject.transform.localScale = Vector3.one;
                currentEuler = Vector3.zero;
                */
                //currentObject.transform.eulerAngles = currentEuler;
                yield return new WaitForEndOfFrame();
                currentObject.GetComponent<PlaceablePrefab>().placed = true;
            }
        }
    }
    #endregion
}
