﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using BrydenWoodUnity.Grid;
using BrydenWood.Interop;

namespace BrydenWoodUnity.GeometryManipulation
{
    public class ClusterManager : MonoBehaviour
    {
        //For debug

        public PrefabButton[] clusterButtons;
        public float[] clusterNumbers;
        public string[] clusterNames;
        public MeshlessGridManager gridManager;
        public Text infoText;
        public Dictionary<PrefabButton, float> clusterToPlace { get; set; }

        private string title = "" /*"Number of Placed Components:\r\n"*/;
        private Dictionary<string, string> requiredPlaced;
        Dictionary<string, int> tileNumbers;
        Dictionary<string, int> elementNumbers;
        public TextManager textManager;
        string information;
        // Use this for initialization
        void Awake()
        {
            GeneratePrefabsList();
            requiredPlaced = new Dictionary<string, string>();
            tileNumbers = new Dictionary<string, int>();
            elementNumbers = new Dictionary<string, int>();
        }

        // Update is called once per frame
        void Update()
        {


        }

        public void AddTile(string tile)
        {
            if (tileNumbers.ContainsKey(tile))
            {
                tileNumbers[tile]++;
            }
            else
            {
                tileNumbers.Add(tile, 1);
            }
            UpdateInfoText();
        }

        public void RemoveTile(string tile)
        {
            if (tileNumbers.ContainsKey(tile))
            {
                if (tileNumbers[tile] > 0)
                    tileNumbers[tile]--;
            }
            UpdateInfoText();
        }

        public void AddElement(string element)
        {
            if (elementNumbers.ContainsKey(element))
            {
                elementNumbers[element]++;
            }
            else
            {
                elementNumbers.Add(element, 1);
            }
            UpdateInfoText();
        }

        public void RemoveElement(string element)
        {
            if (elementNumbers.ContainsKey(element))
            {
                if (elementNumbers[element] > 0)
                    elementNumbers[element]--;
            }
            UpdateInfoText();
        }

        public void GenerateRequired(string entry)
        {
            if (entry == "1FE")
            {

                clusterNumbers[5] = 1125;
                clusterNumbers[6] = 105;
                clusterNumbers[7] = 5220;


            }
            else if (entry == "2FE")
            {

                clusterNumbers[5] = 1650;
                clusterNumbers[6] = 210;
                clusterNumbers[7] = 9840;


            }
            else if (entry == "3FE")
            {

                clusterNumbers[5] = 2175;
                clusterNumbers[6] = 315;
                clusterNumbers[7] = 14460;


            }
            else
            {
                for (int i = 0; i < clusterNumbers.Length; i++)
                {
                    clusterNumbers[i] = 0;
                }
            }
            for (int i = 0; i < clusterNames.Length; i++)
            {
                if (clusterNames[i].Contains("Tile"))
                {
                    if (!tileNumbers.ContainsKey(clusterNames[i]))
                        tileNumbers.Add(clusterNames[i], 0);
                }
                else
                {
                    if (!elementNumbers.ContainsKey(clusterNames[i]))
                        elementNumbers.Add(clusterNames[i], 0);
                }

            }
        }

        public void GeneratePrefabsList()
        {
            requiredPlaced = new Dictionary<string, string>();
            clusterToPlace = new Dictionary<PrefabButton, float>();
            for (int i = 0; i < clusterButtons.Length; i++)
            {
                if (!float.IsInfinity(clusterNumbers[i]))
                {
                    
                    clusterToPlace.Add(clusterButtons[i], 0);
                }
            }


           
        }

        public void UpdateInfoText()
        {

            textManager.exterior = title;
            GenerateRequired(textManager.program);
            var tileKeys = tileNumbers.Keys.ToList();
            var elementKeys = elementNumbers.Keys.ToList();

            if (textManager.program != "Free")
            {
                for (int i = 0; i < clusterNames.Length; i++)
                {
                    if (clusterNames[i].Contains("Tile") && !clusterNames[i].Contains("Restrictions"))
                    {
                        if (clusterNumbers[i] > tileNumbers[clusterNames[i]] * 12.96f)
                        {
                            textManager.exterior += "<mark=#ffffff35><color=#ff0000ff>" + clusterNames[i] + ": p: " + tileNumbers[clusterNames[i]] * 12.96f + ", r: " + clusterNumbers[i] + "\r\n" + "</color></mark>";
                        }
                        else if (clusterNumbers[i] < tileNumbers[clusterNames[i]] * 12.96f)
                        {
                            textManager.exterior += "<mark=#ffffff35><color=#ffa500ff>" + clusterNames[i] + ": p: " + tileNumbers[clusterNames[i]] * 12.96f + ", r: " + clusterNumbers[i] + "\r\n" + "</color></mark>";
                        }
                        else
                        {
                            textManager.exterior += string.Format("<mark=#ffffff35>{0}: p {2} sq.m, r {1} sq.m</mark>\r\n", clusterNames[i], clusterNumbers[i], tileNumbers[clusterNames[i]] * 12.96f);
                        }
                    }
                   
                }

            }
            else
            {

                for (int i = 0; i < tileKeys.Count; i++)
                {
                    if (tileKeys[i].Contains("Tile") || tileKeys[i].Contains("Restrictions"))
                        textManager.exterior += string.Format("<mark=#ffffff35>{0}:  placed {1}sq.m \r\n</mark>", tileKeys[i], tileNumbers[tileKeys[i]] * 12.96f);
                }
               
            }
            textManager.UpdateText();
            textManager.UpdateUIText();
        }
    }
}
