﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.Navigation;


    namespace BrydenWoodUnity.GeometryManipulation
{
    public class ProceduralCluster : MonoBehaviour
    {
        public string name = "Cluster";

        public Vector3 startPoint = Vector3.zero;

        public float builtWidth = 21.6f;
        public float builtDepth = 7.8f;
        public float builtHeight = 2.995f;
        public float circulationDepth = 2.09f;
        public float topOffset = 0.2f;
        public float bottomOffset = 0.2f;

        public int facilitiesIndex = -1;

        public string doorIndex;

        public float moduleWidth = 3.6f;
        public float moduleDepth = 10.2f;
        public float moduleHeight = 2.995f;

        public bool hasCirculation = true;
        public bool halfRight = false;
        public bool halfLeft = false;

        public bool initializeOnStart = false;
        public Material standardBuitMaterial;
        public Material facilitiesMaterial;
        public Material modulesMaterial;

        public List<GameObject> builtParts { get; private set; }
        public List<GameObject> modules { get; private set; }
        public List<int> moduleRenderIndices { get; private set; }

        public int renderIndex { get; private set; }

        public DrawGLines renderCamera;


        private List<Vector3[]> cornerPoints { get; set; }
        private PlaceablePrefab placeablePrefab { get; set; }
        private Vector3 prevPos { get; set; }
        private Vector3 prevRot { get; set; }

        public bool isResolved = false;
        public string detailedName;
        public string type;
        public GameObject label;
        GameObject mTag;

        public GameObject doorArrow;
        public GameObject door;
        public GameObject window;
        public Transform roomIcon;

        public string description;

        public Ray[] SideRays { get { return GetSideRays(); } }
        private Ray[] sideRays;

        // Use this for initialization
        void Start()
        {
            if (initializeOnStart)
            {
                Initialize();
            }


        }

        // Update is called once per frame
        void Update()
        {
            if (prevPos != transform.position || prevRot != transform.eulerAngles)
            {
                ToggleCornerPoints(true);
                prevPos = transform.position;
                prevRot = transform.eulerAngles;
            }


        }

        private void OnDestroy()
        {
            ToggleCornerPoints(false);
        }

        private void OnDisable()
        {
            ToggleCornerPoints(false);
        }

        private void OnEnable()
        {
            ToggleCornerPoints(true);
        }
        /// <summary>
        /// Raycasts from side of gameobject to detect collision
        /// </summary>
        /// <returns></returns>
        public Ray[] GetSideRays()
        {
            if (sideRays == null)
            {
                sideRays = new Ray[2];
            }

            for (int i = 0; i < builtParts.Count; i++)
            {
                if (builtParts[i].transform.localScale.x >= 3.6f)
                {
                    sideRays[0] = new Ray(builtParts[i].transform.position + Vector3.up * moduleHeight * 1.5f, Vector3.down);
                    break;
                }
            }

            for (int i = builtParts.Count - 1; i > 0; i--)
            {
                if (builtParts[i].transform.localScale.x >= 3.6f)
                {
                    sideRays[1] = new Ray(builtParts[i].transform.position + Vector3.up * moduleHeight * 1.5f, Vector3.down);
                    break;
                }
            }

            return sideRays;
        }
        /// <summary>
        /// Creates a procedural cluster
        /// </summary>
        public void Initialize()
        {
            renderCamera = Camera.main.GetComponent<DrawGLines>();
            GenerateGeometries();


            if (renderCamera != null && renderCamera.modulesCorners != null)
            {
                renderIndex = renderCamera.modulesCorners.Count;
                renderCamera.modulesCorners.Add(cornerPoints);

            }

            placeablePrefab = GetComponent<PlaceablePrefab>();
            placeablePrefab.proceduralCluster = this;
            placeablePrefab.size = new Vector3(builtWidth, moduleHeight, builtDepth + circulationDepth);
            if (!name.Contains("Hall") && !name.Contains("Corner"))
                placeablePrefab.hasCirculation = hasCirculation;

            else
            {
                placeablePrefab.hasCirculation = false;
                hasCirculation = false;
            }

            placeablePrefab.Initialize();
        }
        /// <summary>
        /// Creates a procedural cluster with loaded info
        /// </summary>
        /// <param name="info"></param>
        public void Initialize(Cluster info)
        {
            name = info.name;
            detailedName = info.detailedname;
            var coords = info.startPoint;
            startPoint = new Vector3(coords[0], coords[1], coords[2]);
            builtWidth = info.builtWidth;
            builtDepth = info.builtDepth;
            builtHeight = info.builtHeight;
            circulationDepth = info.circulationDepth;
            topOffset = info.topOffset;
            bottomOffset = info.bottomOffset;
            facilitiesIndex = info.facilitiesIndex;
            moduleWidth = info.moduleWidth;
            moduleDepth = info.moduleDepth;
            moduleHeight = info.moduleHeight;
            doorIndex = info.doorIndex;
            type = info.type;

            if (!name.Contains("Hall") && !name.Contains("Corner"))
            {
                halfRight = false;
                halfLeft = false;
                hasCirculation = info.hasCirculation;
            }

            else
            {
                hasCirculation = false;
                halfRight = info.halfRight;
                halfLeft = info.halfLeft;
            }
            Initialize();
            halfRight = info.halfRight;
            halfLeft = info.halfLeft;
        }
        /// <summary>
        /// Creates a procedural cluster with info from a file
        /// </summary>
        /// <param name="row"></param>
        /// <param name="mat"></param>
        public void Initialize(DataRow row, Material mat)
        {

            name = gameObject.name;

            var coords = row["startpoint"].ToString().Split(';').Select(x => float.Parse(x)).ToList();
            coords.TrimExcess();
            startPoint = new Vector3(coords[0], coords[1], coords[2]);
            builtWidth = float.Parse(row["builtwidth"].ToString());
            builtDepth = float.Parse(row["builtdepth"].ToString());
            builtHeight = float.Parse(row["builtheight"].ToString());
            circulationDepth = float.Parse(row["circulationdepth"].ToString());
            topOffset = float.Parse(row["topoffset"].ToString());
            bottomOffset = float.Parse(row["bottomoffset"].ToString());
            facilitiesIndex = int.Parse(row["facilitiesindex"].ToString());
            moduleWidth = float.Parse(row["modulewidth"].ToString());
            moduleDepth = float.Parse(row["moduledepth"].ToString());
            moduleHeight = float.Parse(row["moduleheight"].ToString());
            doorIndex = row["doorindex"].ToString();
            standardBuitMaterial = mat;
            detailedName = row["DetailedClusterName"].ToString();
            type = row["Type"].ToString();
            description = row["Description"].ToString();
            Initialize();
        }
        /// <summary>
        /// Create all geometries comprising a cluster
        /// </summary>
        public void GenerateGeometries()
        {
            builtParts = new List<GameObject>();
            modules = new List<GameObject>();
            moduleRenderIndices = new List<int>();
            int modulesNumber = Mathf.FloorToInt((builtWidth + startPoint.x) / moduleWidth);

            for (int i = 0; i < modulesNumber; i++)
            {
                GameObject module = GameObject.CreatePrimitive(PrimitiveType.Cube);
                module.layer = 13;
                module.name = "Module_" + i;
                module.GetComponent<Collider>().enabled = false;
                module.GetComponent<MeshRenderer>().material = modulesMaterial;
                module.GetComponent<MeshRenderer>().enabled = false;
                module.transform.SetParent(transform);
                module.transform.localScale = new Vector3(moduleWidth, moduleHeight, moduleDepth);
                module.transform.localPosition = module.transform.localScale / 2.0f + new Vector3(moduleWidth * i, 0, 0);
                module.transform.localEulerAngles = Vector3.zero;
                modules.Add(module);
            }

            int builtNum = modulesNumber;
            builtNum += Mathf.CeilToInt(Mathf.Abs(startPoint.x) / moduleWidth);
            float rest = builtWidth + startPoint.x - (modulesNumber * moduleWidth);
            if (rest >= 0.05f)
            {
                builtNum += Mathf.CeilToInt((builtWidth + startPoint.x - modulesNumber * moduleWidth) / moduleWidth);
            }
            Vector3 start = startPoint;


            for (int i = 0; i < builtNum; i++)
            {
                GameObject builtPart = GameObject.CreatePrimitive(PrimitiveType.Cube);

                builtPart.name = "Built Part";
                builtPart.layer = 13;
                builtPart.GetComponent<Collider>().enabled = false;
                if (i == facilitiesIndex)
                {
                    builtPart.GetComponent<MeshRenderer>().material = facilitiesMaterial;

                }
                else
                {
                    builtPart.GetComponent<MeshRenderer>().material = standardBuitMaterial;
                }

                builtPart.transform.SetParent(transform);
                builtHeight = moduleHeight - topOffset - bottomOffset;

                if (i == 0 && startPoint.x != 0)
                {
                    builtPart.transform.localScale = new Vector3(Mathf.Abs(startPoint.x), builtHeight, builtDepth);
                    builtPart.transform.localPosition = start + builtPart.transform.localScale / 2.0f + new Vector3(0, bottomOffset, 0);
                    if (startPoint.x % moduleWidth == 0)
                    {
                        GameObject module = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        module.name = "Module_" + modules.Count;
                        module.layer = 13;
                        module.GetComponent<Collider>().enabled = false;
                        module.GetComponent<MeshRenderer>().material = modulesMaterial;
                        module.GetComponent<MeshRenderer>().enabled = false;
                        module.transform.SetParent(transform);
                        module.transform.localScale = new Vector3(moduleWidth, moduleHeight, moduleDepth);
                        module.transform.localPosition = start + module.transform.localScale / 2.0f;
                        module.transform.localEulerAngles = Vector3.zero;
                        modules.Add(module);
                        halfLeft = false;
                    }
                    else
                    {
                        if (!name.Contains("Hall") && !name.Contains("Corner"))
                            halfLeft = true;
                    }
                }
                else if (i == builtNum - 1 && rest >= 0.05f)
                {
                    builtPart.transform.localScale = new Vector3(rest, builtHeight, builtDepth);
                    builtPart.transform.localPosition = start + builtPart.transform.localScale / 2.0f + new Vector3(0, bottomOffset, 0);
                    builtPart.transform.localEulerAngles = Vector3.zero;
                    if (!name.Contains("Hall") && !name.Contains("Corner"))
                        halfRight = true;
                }
                else
                {
                    builtPart.transform.localScale = new Vector3(moduleWidth, builtHeight, builtDepth);
                    builtPart.transform.localPosition = start + builtPart.transform.localScale / 2.0f + new Vector3(0, bottomOffset, 0);
                    builtPart.transform.localEulerAngles = Vector3.zero * 0.01f;
                }

                start += new Vector3(builtPart.transform.localScale.x, 0, 0);
                if (doorIndex != "-1")
                {
                    if (!name.Contains("Hall") && !name.Contains("Kitchen") && !name.Contains("Storage") && !name.Contains("Corner"))
                    {
                        var myDoor = doorIndex.Split('*');
                        for (int d = 0; d < myDoor.Length; d++)
                        {
                            if (int.Parse(myDoor[d]) == i)
                            {
                                GameObject arrow = Instantiate(doorArrow);
                                Vector3 pos = new Vector3(builtPart.GetComponent<MeshRenderer>().bounds.center.x, 0.01f, builtPart.GetComponent<MeshRenderer>().bounds.center.z + builtPart.GetComponent<MeshRenderer>().bounds.extents.z);

                                arrow.transform.SetParent(builtPart.transform);
                                arrow.transform.position = pos;
                                arrow.transform.localScale = Vector3.one * 0.05f;
                                arrow.transform.localEulerAngles = new Vector3(0, 180, 0);
                            }

                        }
                    }
                }


                builtParts.Add(builtPart);
            }
            //----------------External Wall||Window Creation---------
            if (!name.Contains("Hall") && !name.Contains("Kitchen") && !name.Contains("Storage") && !name.Contains("Corner"))
            {
                Transform win = Instantiate(window).transform;
                Vector3 posi = new Vector3();

                if (name.Contains("Infant"))
                    posi = new Vector3(9, 0, 0);
                else if (name.Contains("2FE Library"))
                    posi = new Vector3(2.7f, 0, 0);
                else if (name.Contains("2FE Staff"))
                    posi = new Vector3(4.5f, 0, 0);
                else if (name.Contains("Stair"))
                    posi = new Vector3(builtWidth / 2, 0, -2.0905f);
                else
                    posi = new Vector3(builtWidth / 2, 0, 0);
                win.SetParent(transform);
                win.localPosition = posi;
                win.localScale = new Vector3(builtWidth / 3.6f, 1f, 0.3f);
            }

            //----------------------MAIN DOOR OF BUILDING IN ENTRANCE CLUSTER--------------------------
            if (name.Contains("Entrance"))
            {
                GameObject arrow = Instantiate(doorArrow);

                Vector3 post = new Vector3(3.6f, 0.1f, 0);
                arrow.transform.SetParent(transform);
                arrow.transform.localPosition = post;

                arrow.transform.localScale = Vector3.one * 0.5f;
                arrow.transform.localEulerAngles = new Vector3(0, 0, 0);
                Material doorMat = new Material(arrow.transform.GetChild(0).GetComponent<MeshRenderer>().material);
                doorMat.color = Color.red;
                arrow.transform.GetChild(0).GetComponent<MeshRenderer>().material = doorMat;

            }

            if (name.Contains("Hall") && !name.Contains("Small"))
            {
                GameObject arrow = Instantiate(doorArrow);

                Vector3 pos = new Vector3(0, 0.05f, 9);
                arrow.transform.SetParent(transform);
                arrow.transform.localPosition = pos;
                arrow.transform.localScale = Vector3.one * 0.25f;
                arrow.transform.localEulerAngles = new Vector3(0, 90, 0);


            }


            //----------------Icon above cluster---------

            Texture myIcon = Resources.Load<Texture>("Clusters/RoomIcons/" + detailedName);
            if (myIcon != null)
            {
                Transform icon = Instantiate(roomIcon);
                icon.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", myIcon);
                Vector3 pos = new Vector3(builtWidth / 2, moduleHeight + 0.2f, builtDepth / 2);
                icon.SetParent(transform);
                icon.localPosition = pos;
                icon.localScale = new Vector3(0.3f, 1, 0.3f);
            }
            else if (name.Contains("Hygiene"))
            {
                Texture thisIcon = Resources.Load<Texture>("Clusters/RoomIcons/Reception&Hygiene");
                Transform icon = Instantiate(roomIcon);
                icon.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", thisIcon);

                Vector3 pos = new Vector3(builtWidth / 2, moduleHeight + 0.2f, builtDepth / 2);
                icon.SetParent(transform);
                icon.localPosition = pos;
                icon.localScale = new Vector3(0.3f, 1, 0.3f);
            }

            //----------------Corridor creation---------
            GameObject corridor = GameObject.Instantiate(Resources.Load("Floorplans/Corridor") as GameObject);
            corridor.transform.SetParent(transform);
            corridor.transform.localScale = new Vector3((builtWidth) * 0.1f, 1, circulationDepth * 0.1f);
            Vector3 po = new Vector3();
            if (name.Contains("Infant") || name.Contains("2FE Library") || name.Contains("2FE Staff"))
                po = new Vector3(9, 0.1f, moduleDepth - (circulationDepth / 2));
            else
                po = new Vector3(builtWidth / 2, 0.1f, moduleDepth - (circulationDepth / 2));
            corridor.transform.localPosition = po;

            //----------------Add Module lines---------

            UpdateCornerPoints();

        }
        /// <summary>
        /// Turn cluster circuation on/off
        /// </summary>
        /// <param name="circOn"></param> boolean from UI toggle
        public void OnToggleCirculation(bool circOn)
        {
            GameObject cor = transform.Find("Corridor(Clone)").gameObject;
            if (!name.Contains("Hall") && !name.Contains("Corner"))
            {
                hasCirculation = circOn;
                if (circOn)
                {
                    cor.SetActive(true);
                    moduleDepth = 10.2f;
                    GetComponent<BoxCollider>().size = new Vector3(GetComponent<BoxCollider>().size.x, GetComponent<BoxCollider>().size.y, moduleDepth);
                    GetComponent<BoxCollider>().center = new Vector3(GetComponent<BoxCollider>().center.x, GetComponent<BoxCollider>().center.y, moduleDepth / 2.0f);
                    float zScale = moduleDepth;
                    float zPosition = 5.1f;
                    for (int i = 0; i < modules.Count; i++)
                    {
                        Transform tr = modules[i].transform;
                        tr.localScale = new Vector3(tr.localScale.x, tr.localScale.y, zScale);
                        tr.localPosition = new Vector3(tr.localPosition.x, tr.localPosition.y, zPosition);
                    }
                }
                else
                {
                    cor.SetActive(false);
                    moduleDepth = builtDepth;
                    GetComponent<BoxCollider>().size = new Vector3(GetComponent<BoxCollider>().size.x, GetComponent<BoxCollider>().size.y, moduleDepth);
                    GetComponent<BoxCollider>().center = new Vector3(GetComponent<BoxCollider>().center.x, GetComponent<BoxCollider>().center.y, moduleDepth / 2.0f);
                    float zScale = moduleDepth;
                    float zPosition = 3.9f;
                    for (int i = 0; i < modules.Count; i++)
                    {
                        Transform tr = modules[i].transform;
                        tr.localScale = new Vector3(tr.localScale.x, tr.localScale.y, zScale);
                        tr.localPosition = new Vector3(tr.localPosition.x, tr.localPosition.y, zPosition);
                    }
                }
                UpdateCornerPoints();
                ToggleCornerPoints(true);
            }
            else
            {
                hasCirculation = false;
            }

        }
        /// <summary>
        /// Update the size of the corridor when cluster is resolved
        /// </summary>
        public void UpdateFloorplanResolved()
        {

            GameObject cor = transform.Find("Corridor(Clone)").gameObject;
            GameObject wall = transform.Find("Wall(Clone)").gameObject;

            cor.transform.localScale = new Vector3(cor.transform.localScale.x + 0.2f, cor.transform.localScale.y, cor.transform.localScale.z);
            cor.transform.localPosition = new Vector3(GetComponent<BoxCollider>().center.x, cor.transform.localPosition.y, cor.transform.localPosition.z);
            wall.transform.localScale = new Vector3(GetComponent<BoxCollider>().size.x / 3.6f, wall.transform.localScale.y, wall.transform.localScale.z);
            wall.transform.localPosition = new Vector3(GetComponent<BoxCollider>().center.x, wall.transform.localPosition.y, wall.transform.localPosition.z);
        }

        /// <summary>
        /// Update the size of the corridor when cluster is resolved 
        /// </summary>
        public void UpdateFloorplanUnResolved()
        {

            GameObject cor = transform.Find("Corridor(Clone)").gameObject;
            GameObject wall = transform.Find("Wall(Clone)").gameObject;

            cor.transform.localScale = new Vector3(cor.transform.localScale.x - 0.2f, cor.transform.localScale.y, cor.transform.localScale.z);
            cor.transform.localPosition = new Vector3(GetComponent<BoxCollider>().center.x, cor.transform.localPosition.y, cor.transform.localPosition.z);
            wall.transform.localScale = new Vector3(GetComponent<BoxCollider>().size.x / 3.6f, wall.transform.localScale.y, wall.transform.localScale.z);
            wall.transform.localPosition = new Vector3(GetComponent<BoxCollider>().center.x, wall.transform.localPosition.y, wall.transform.localPosition.z);

        }

        /// <summary>
        /// Get accumulative Bounds of all Children of object
        /// </summary>
        /// <param name="g"></param>
        /// <returns></returns>
        public Bounds GetMaxBounds(List<GameObject> g)
        {
            int counter = 0;
            Bounds bnds = new Bounds();
            foreach (GameObject t in g)
            {

                if (t.GetComponent<MeshRenderer>() != null)
                {
                    if (counter == 0)
                    {

                        bnds = t.GetComponent<BoxCollider>().bounds;
                    }
                    else
                    {

                        Bounds _b = t.GetComponent<BoxCollider>().bounds;
                        bnds.Encapsulate(_b);
                    }
                    counter++;
                }


            }

            return bnds;
        }
        /// <summary>
        /// When a cluster is resolved, expand half module to full
        /// </summary>
        /// <param name="side"></param> which sied to expand
        public void Expand(string side)
        {
            bool isMirrored = false;
            isResolved = true;

            if (side == "right")
            {
                if (tag == "MirroredX" || tag == "Mirrored")
                {
                    isMirrored = true;
                    halfRight = false;
                    builtParts[0].transform.localScale = new Vector3(moduleWidth, builtHeight, builtDepth);
                    builtParts[0].transform.localPosition += new Vector3(-0.9f, 0, 0);

                    GameObject module = Instantiate(modules[0], transform);
                    module.transform.localEulerAngles = Vector3.zero;
                    module.layer = 13;
                    module.name = "Module_" + modules.Count;
                    module.GetComponent<Collider>().enabled = false;
                    module.GetComponent<MeshRenderer>().material = modulesMaterial;
                    module.GetComponent<MeshRenderer>().enabled = false;
                    module.transform.localScale = new Vector3(moduleWidth, moduleHeight, moduleDepth);
                    module.transform.localPosition = modules[0].transform.localPosition - new Vector3(moduleWidth, 0, 0);
                    modules.Insert(0, module);
                    startPoint += new Vector3(-moduleWidth / 2.0f, 0, 0);
                    builtWidth += moduleWidth / 2.0f;
                }
                else
                {
                    isMirrored = false;
                    halfRight = false;
                    builtParts.Last().transform.localScale = new Vector3(moduleWidth, builtHeight, builtDepth);
                    builtParts.Last().transform.localPosition += new Vector3(0.9f, 0, 0);

                    GameObject module = Instantiate(modules.Last(), transform);
                    module.transform.localEulerAngles = Vector3.zero;
                    module.layer = 13;
                    module.name = "Module_" + modules.Count;
                    module.GetComponent<Collider>().enabled = false;
                    module.GetComponent<MeshRenderer>().material = modulesMaterial;
                    module.GetComponent<MeshRenderer>().enabled = false;
                    module.transform.localScale = new Vector3(moduleWidth, moduleHeight, moduleDepth);
                    module.transform.localPosition = modules.Last().transform.localPosition + new Vector3(moduleWidth, 0, 0);
                    modules.Add(module);
                    builtWidth += moduleWidth / 2.0f;
                }

            }
            else
            {
                if (tag == "MirroredX" || tag == "Mirrored")
                {
                    isMirrored = true;
                    halfLeft = false;
                    builtParts.Last().transform.localScale = new Vector3(moduleWidth, builtHeight, builtDepth);
                    builtParts.Last().transform.localPosition += new Vector3(0.9f, 0, 0);

                    GameObject module = Instantiate(modules.Last(), transform);
                    module.transform.localEulerAngles = Vector3.zero;
                    module.layer = 13;
                    module.name = "Module_" + modules.Count;
                    module.GetComponent<Collider>().enabled = false;
                    module.GetComponent<MeshRenderer>().material = modulesMaterial;
                    module.GetComponent<MeshRenderer>().enabled = false;
                    module.transform.localScale = new Vector3(moduleWidth, moduleHeight, moduleDepth);
                    module.transform.localPosition = modules.Last().transform.localPosition + new Vector3(moduleWidth, 0, 0);
                    modules.Add(module);
                    builtWidth += moduleWidth / 2.0f;
                }
                else
                {
                    isMirrored = false;
                    halfLeft = false;
                    builtParts[0].transform.localScale = new Vector3(moduleWidth, builtHeight, builtDepth);
                    builtParts[0].transform.localPosition += new Vector3(-0.9f, 0, 0);

                    GameObject module = Instantiate(modules[0], transform);
                    module.transform.localEulerAngles = Vector3.zero;
                    module.layer = 13;
                    module.name = "Module_" + modules.Count;
                    module.GetComponent<Collider>().enabled = false;
                    module.GetComponent<MeshRenderer>().material = modulesMaterial;
                    module.GetComponent<MeshRenderer>().enabled = false;
                    module.transform.localScale = new Vector3(moduleWidth, moduleHeight, moduleDepth);
                    module.transform.localPosition = modules[0].transform.localPosition - new Vector3(moduleWidth, 0, 0);
                    modules.Insert(0, module);
                    startPoint += new Vector3(-moduleWidth / 2.0f, 0, 0);
                    builtWidth += moduleWidth / 2.0f;
                }
            }

            placeablePrefab.size = new Vector3(builtWidth, moduleHeight, builtDepth + circulationDepth);

            placeablePrefab.UpdateColider();
            UpdateCornerPoints();
            ToggleCornerPoints(true);

        }
        /// <summary>
        /// Removes half module in case of unresolve
        /// </summary>
        /// <param name="side"></param> from which side to remove
        public void ExpandBack(string side)
        {
            bool isMirrored = false;
            isResolved = false;
            if (side == "right")
            {
                if (tag == "MirroredX" || tag == "Mirrored")
                {
                    halfRight = true;
                    Destroy(modules[0]);
                    modules.RemoveAt(0);
                    builtParts[0].transform.localScale = new Vector3(moduleWidth / 2, builtHeight, builtDepth);
                    builtParts[0].transform.localPosition -= new Vector3(-0.9f, 0, 0);
                    builtWidth -= moduleWidth / 2.0f;

                }
                else
                {
                    halfRight = true;
                    Destroy(modules.Last());
                    modules.Remove(modules.Last());
                    builtParts.Last().transform.localScale = new Vector3(moduleWidth / 2, builtHeight, builtDepth);
                    builtParts.Last().transform.localPosition -= new Vector3(0.9f, 0, 0);
                    builtWidth -= moduleWidth / 2.0f;
                }
            }
            else
            {
                if (tag == "MirroredX" || tag == "Mirrored")
                {
                    halfLeft = true;
                    Destroy(modules.Last());
                    modules.Remove(modules.Last());
                    builtParts.Last().transform.localScale = new Vector3(moduleWidth / 2, builtHeight, builtDepth);
                    builtParts.Last().transform.localPosition -= new Vector3(0.9f, 0, 0);
                    builtWidth -= moduleWidth / 2.0f;
                }
                else
                {
                    halfLeft = true;
                    Destroy(modules[0]);
                    modules.RemoveAt(0);
                    builtParts[0].transform.localScale = new Vector3(moduleWidth / 2, builtHeight, builtDepth);
                    builtParts[0].transform.localPosition -= new Vector3(-0.9f, 0, 0);
                    builtWidth -= moduleWidth / 2.0f;
                    startPoint -= new Vector3(-moduleWidth / 2.0f, 0, 0);

                }

            }


            placeablePrefab.size = new Vector3(builtWidth, moduleHeight, builtDepth + circulationDepth);
            if (name.Contains("Infant"))
            {
                placeablePrefab.UpdateColider();
            }
            else
                placeablePrefab.UpdateColider();
            UpdateCornerPoints();
            ToggleCornerPoints(true);

        }
        /// <summary>
        /// Updates the module lines
        /// </summary>
        public void UpdateCornerPoints()
        {

            cornerPoints = new List<Vector3[]>();
            if (name.Contains("Hall") || name.Contains("Storage") || name.Contains("Kitchen") || name.Contains("Corner"))
            {
                List<Vector3> lines = new List<Vector3>();


                Vector3 centre = transform.localPosition;

                BoxCollider b = GetComponent<BoxCollider>();
                lines.Add(transform.TransformPoint(b.center + new Vector3(b.size.x, b.size.y, b.size.z) * 0.5f));
                lines.Add(transform.TransformPoint(b.center + new Vector3(-b.size.x, b.size.y, b.size.z) * 0.5f));
                lines.Add(transform.TransformPoint(b.center + new Vector3(-b.size.x, b.size.y, -b.size.z) * 0.5f));
                lines.Add(transform.TransformPoint(b.center + new Vector3(b.size.x, b.size.y, -b.size.z) * 0.5f));

                lines.Add(transform.TransformPoint(b.center + new Vector3(b.size.x, -b.size.y, b.size.z) * 0.5f));
                lines.Add(transform.TransformPoint(b.center + new Vector3(-b.size.x, -b.size.y, b.size.z) * 0.5f));
                lines.Add(transform.TransformPoint(b.center + new Vector3(-b.size.x, -b.size.y, -b.size.z) * 0.5f));
                lines.Add(transform.TransformPoint(b.center + new Vector3(b.size.x, -b.size.y, -b.size.z) * 0.5f));

                lines.TrimExcess();
                cornerPoints.Add(lines.ToArray());


            }
            else
            {


                for (int i = 0; i < modules.Count; i++)
                {
                    List<Vector3> lines = new List<Vector3>();

                    Vector3 centre = modules[i].transform.localPosition;

                    lines.Add(transform.TransformPoint(centre + new Vector3(-moduleWidth / 2, -moduleHeight / 2, -moduleDepth / 2)));
                    lines.Add(transform.TransformPoint(centre + new Vector3(moduleWidth / 2, -moduleHeight / 2, -moduleDepth / 2)));
                    lines.Add(transform.TransformPoint(centre + new Vector3(moduleWidth / 2, -moduleHeight / 2, moduleDepth / 2)));
                    lines.Add(transform.TransformPoint(centre + new Vector3(-moduleWidth / 2, -moduleHeight / 2, moduleDepth / 2)));

                    lines.Add(transform.TransformPoint(centre + new Vector3(-moduleWidth / 2, moduleHeight / 2, -moduleDepth / 2)));
                    lines.Add(transform.TransformPoint(centre + new Vector3(moduleWidth / 2, moduleHeight / 2, -moduleDepth / 2)));
                    lines.Add(transform.TransformPoint(centre + new Vector3(moduleWidth / 2, moduleHeight / 2, moduleDepth / 2)));
                    lines.Add(transform.TransformPoint(centre + new Vector3(-moduleWidth / 2, moduleHeight / 2, moduleDepth / 2)));

                    cornerPoints.Add(lines.ToArray());
                }
            }

            cornerPoints.TrimExcess();
        }
        /// <summary>
        /// Turn module lines on/off
        /// </summary>
        /// <param name="show"></param>
        public void ToggleCornerPoints(bool show)
        {

            if (show)
            {
                if (cornerPoints != null)
                {
                    UpdateCornerPoints();
                    if (renderCamera != null && renderCamera.modulesCorners != null)
                    {
                        renderCamera.modulesCorners[renderIndex] = cornerPoints;
                    }
                }
            }
            else
            {
                if (renderCamera != null && renderCamera.modulesCorners != null)
                {
                    renderCamera.modulesCorners[renderIndex] = null;
                }
            }
        }
    }
}
