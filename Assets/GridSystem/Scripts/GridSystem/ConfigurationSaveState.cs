﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class ConfigurationSaveState
{
    public string latitude { get; set; }
    public string longitude { get; set; }
    public List<GridState> grids { get; set; }
    public Site site { get; set; }
    public List<AdditionalCluster> additionalClusters { get; set; }
    public string program { get; set; }
    public bool loadedMap;
    public bool existingDeleted;
    public ConfigurationSaveState()
    {

    }
}



public struct GridState
{
    public float[] position { get; set; }
    public float[] rotation { get; set; }
    public float sizeX { get; set; }
    public float sizeY { get; set; }
    public int xNum { get; set; }
    public int yNum { get; set; }
    public float snapDistance { get; set; }
    public List<Cluster> clusters { get; set; }
   
}

public struct Cluster
{
    public float[] position { get; set; }
    public float[] rotation { get; set; }
    public float[] localposition { get; set; }
    public float[] localrotation { get; set; }
    public float[] scale { get; set; }
    public string name { get; set; }
    public string detailedname { get; set; }
    public float[] startPoint { get; set; }
    public float builtWidth { get; set; }
    public float builtDepth { get; set; }
    public float builtHeight { get; set; }
    public float circulationDepth { get; set; }
    public float topOffset { get; set; }
    public float bottomOffset { get; set; }
    public int facilitiesIndex { get; set; }
    public float moduleWidth { get; set; }
    public float moduleDepth { get; set; }
    public float moduleHeight { get; set; }
    public bool hasCirculation { get; set; }
    public bool halfRight { get; set; }
    public bool halfLeft { get; set; }
    public int level { get; set; }
    public string doorIndex { get; set; }
    public string type { get; set; }

    public List<Cluster> clustersWithin { get; set; }
   
}

public struct AdditionalCluster
{
    public float[] position { get; set; }
    public float[] rotation { get; set; }
    public string name { get; set; }


}
public struct Site
{
    public List<float[]> controlPoints { get; set; }
}