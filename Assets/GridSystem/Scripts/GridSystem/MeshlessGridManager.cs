﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.UI;
using BrydenWoodUnity.UIElements;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using System.IO;
using Mapbox.Unity.Map;
using System.Data;
using BrydenWoodUnity.Navigation;
using BrydenWood.Interop;

namespace BrydenWoodUnity.Grid
{
    public class MeshlessGridManager : MonoBehaviour
    {
        /// <summary>
        /// The prefab of the grid
        /// </summary>
        [Header("Main References")]
        [Tooltip("The prefab of the grid")]
        public GameObject gridPrefab;
        /// <summary>
        /// The object to hold the prefabs as children
        /// </summary>
        [Tooltip("The object to hold the prefabs as children")]
        public Transform configurationParent;
        /// <summary>
        /// The prefab for the grid GUI element
        /// </summary>
        [Tooltip("The prefab for the grid GUI element")]
        public GameObject guiGridSelectPrefab;
        /// <summary>
        /// The object to hold the grid GUI elements as children
        /// </summary>
        [Tooltip("The object to hold the grid GUI elements as children")]
        public Transform guiGridParent;
        /// <summary>
        /// The list of available colors for the different grids
        /// </summary>
        [Tooltip("The list of available colors for the different grids")]
        public List<Color> gridColors;
        /// <summary>
        /// The floor to floor height
        /// </summary>
        [Tooltip("The floor to floor height")]
        public float floor2floor = 3.75f;
        [Tooltip("The cluster manager for the scene")]
        public ClusterManager clusterManager;
        [Tooltip("The map box component")]
        public GetGeometries map;

        Polygon myPolygon;

        public GameObject proceduralClusterPrefab;

        [Space(10)]
        [Header("GUI references")]
        public Slider levelSlider;
        public Button addGridButton;
        public Text addGridText;
        public GameObject transformationTab;
        /// <summary>
        /// The current prefab to be placed
        /// </summary>
        public GameObject currentPrefab { get; set; }
        /// <summary>
        /// The polygon of the site
        /// </summary>
        public Polygon sitePolygon; /*{ get; set; }*/

        public MeshlessGrid currentGrid { get; set; }
        private GuiSelect currentGridGUIElement { get; set; }
        private int currentGridIndex { get; set; }
        public List<MeshlessGrid> grids { get; set; }
        public List<GuiSelect> gridGUIElements { get; set; }
        private bool settingOrientation { get; set; }
        private Vector3 orientation { get; set; }
        private Vector3 start { get; set; }
        private Vector3 end { get; set; }
        private ControlPointUI startPoint { get; set; }

        private GameObject selectedPrefab { get; set; }
        GameObject rotationParent;
        int lastSliderValue;
        bool moving = false;

        public bool existingDeleted = false;
        public bool mapLoaded = false;


        public ResourcesManager resManager;
        public Toggle clusterPanel, exteriorPanel;
        public Slider rotateGrid;
        public Button deleteSite;
        public EntrySelect entry;
        public Transform additionalParent;
        List<GameObject> currentGridObjects;

        public PrefabButton senderButton { get; set; }

        public delegate void OnPrefabAdded(MeshlessGridManager sender);
        public static event OnPrefabAdded prefabAdded;
        public void PrefabAdded()
        {
            if (prefabAdded != null)
            {
                prefabAdded(this);
            }
        }

        // Use this for initialization
        void Awake()
        {
            grids = new List<MeshlessGrid>();
            currentGridObjects = new List<GameObject>();
            gridGUIElements = new List<GuiSelect>();
            PolygonDrawer.polygonEnded += OnSitePlaced;
            PolygonDrawer.polygonLoaded += OnSiteLoaded;
            PlaceablePrefab.deleted += OnObjectDeleted;
            SelectableGeometry.selected += OnPlacedSelected;
            MeshlessGrid.clusterAdded += PrefabAdded;
            Selector.deselect += OnDeselect;

            lastSliderValue = 360;
        }

        void OnDestroy()
        {
            if (prefabAdded != null)
            {
                foreach (System.Delegate del in prefabAdded.GetInvocationList())
                {
                    prefabAdded -= (OnPrefabAdded)del;
                }
            }

            PolygonDrawer.polygonEnded -= OnSitePlaced;
            PolygonDrawer.polygonLoaded -= OnSiteLoaded;
            PlaceablePrefab.deleted -= OnObjectDeleted;
            SelectableGeometry.selected -= OnPlacedSelected;
            MeshlessGrid.clusterAdded -= PrefabAdded;
            Selector.deselect -= OnDeselect;


        }

        // Update is called once per frame
        void Update()
        {


            if (settingOrientation)
            {
                if (Input.GetKeyDown(KeyCode.Escape) || CustomInput.GetAxis("GUIEscape") == 3)
                {
                    AbortOrientation();
                }
            }

        }

        public void OnCirculation(bool on)
        {
            foreach (Transform cluster in configurationParent)
            {
                cluster.GetComponent<PlaceablePrefab>().hasCirculation = on;
                cluster.GetComponent<PlaceablePrefab>().UpdateSize();
            }
        }
        /// <summary>
        /// Change grid of selected clusters
        /// </summary>
        public void OnGridChanged()
        {
            foreach (Transform obj in configurationParent)
            {
                if (obj.GetComponent<PlaceablePrefab>().isSelected)
                {
                    obj.GetComponent<PlaceablePrefab>().grid = currentGrid;
                    obj.GetComponent<PlaceablePrefab>().gridIndex = currentGrid.index;
                    obj.eulerAngles = currentGrid.transform.eulerAngles;
                    currentGrid.PlaceCluster(obj.transform.position, obj.gameObject);
                }
            }
        }
        public void MapIsLoaded()
        {
            existingDeleted = true;

        }
        public void ExistingIsDeleted()
        {
            mapLoaded = true;
        }

        public void OnAddGrid()
        {
            addGridButton.interactable = false;
            settingOrientation = true;
            addGridText.text = "Select two control points to define the axis of the grid";
        }

        public void OnSaveState()
        {
            resManager.WriteSaveState(SetSaveState(), "testProject", GetComponent<ProceduralClusterManager>().clusterTable, "testexport");
        }


        #region Public Methods

        /// <summary>
        /// Called when the grid has been calculated
        /// </summary>
        /// <param name="grid">The grid which was calculated</param>
        public void OnGridCalculated(MeshlessGrid grid)
        {
            if (currentGrid == grid)
            {
                grids.Add(currentGrid);
                var gridLines = currentGrid.GetPanelPoints();
                var drawLines = Camera.main.GetComponent<DrawGLines>();
                drawLines.currentGridIndex = grids.Count - 1;
                var mat = new Material(drawLines.gridMat);
                mat.color = gridColors[grids.Count - 1];
                currentGridIndex = grids.Count - 1;
            }
        }

        public void RotateSelectedCluster(float r)
        {
            currentGrid.RotateCluster(r);
        }

        public void MirrorClusterHorizontally()
        {
            currentGrid.MirrorObject("x");
        }

        public void MirrorClusterVertically()
        {
            currentGrid.MirrorObject("z");
        }

        /// <summary>
        /// Called when the site polygon has been placed (either drawn or loaded)
        /// </summary>
        /// <param name="sitePolygon"></param>
        public void OnSitePlaced(Polygon polygon, Polygon offset)
        {
            this.sitePolygon = offset;
            myPolygon = sitePolygon;
            for (int i = 0; i < polygon.vertices.Count; i++)
            {
                polygon.vertices[i].cpui.onSelected += OnCPSelected;
            }
            polygon.updated += UpdateCurrentGrid;

        }
        /// <summary>
        /// Called when the site polygon has been placed (either drawn or loaded)
        /// </summary>
        /// <param name="sitePolygon"></param>
        public void OnSiteLoaded(Polygon polygon, Polygon offset)
        {

            this.sitePolygon = polygon;
            myPolygon = offset;
            for (int i = 0; i < polygon.vertices.Count; i++)
            {

                this.sitePolygon.vertices[i].cpui.onSelected += OnCPSelected;
                Camera.main.GetComponent<DrawGLines>().DrawConnectingLines();
            }
            this.sitePolygon.updated += UpdateCurrentGrid;
            OnClustersLoaded();
        }


        /// <summary>
        /// Called when grids and clusters are loaded
        /// </summary>
        public void OnClustersLoaded()
        {

            var grids = resManager.lastSavedState.grids;
            entry.currentEntry = resManager.lastSavedState.program;
            entry.OnContinue();
            map.latitude.text = resManager.lastSavedState.latitude.ToString();
            map.longitude.text = resManager.lastSavedState.longitude.ToString();
            if (resManager.lastSavedState.loadedMap)
            {
                map.LoadMap();

            }
            sitePolygon = myPolygon;
            for (int i = 0; i < grids.Count; i++)
            {
                GenerateGrid(new Vector3(grids[i].rotation[0], grids[i].rotation[1], grids[i].rotation[2]));


                List<Cluster> clusters = grids[i].clusters;
                clusters.TrimExcess();
                for (int j = 0; j < clusters.Count; j++)
                {
                    Vector3 position = new Vector3(clusters[j].position[0], clusters[j].position[1], clusters[j].position[2]);
                    Vector3 euler = new Vector3(clusters[j].rotation[0], clusters[j].rotation[1], clusters[j].rotation[2]);
                    Vector3 scale = new Vector3(clusters[j].scale[0], clusters[j].scale[1], clusters[j].scale[2]);
                    string type = clusters[j].name;
                    var col = gameObject.GetComponent<ProceduralClusterManager>().clusterMaterials[type];
                    proceduralClusterPrefab.GetComponent<ProceduralCluster>().standardBuitMaterial.color = gameObject.GetComponent<ProceduralClusterManager>().clusterMaterials[type].color;
                    OnPlaceCluster(proceduralClusterPrefab, position, euler, clusters[j].level, clusters[j], scale);
                    GetComponent<ProceduralClusterManager>().placedClusters[clusters[j].name]++;

                    GetComponent<ProceduralClusterManager>().UpdateText();

                }
            }

            List<AdditionalCluster> additionalClusters = resManager.lastSavedState.additionalClusters;
            additionalClusters.TrimExcess();
            if (additionalClusters != null && additionalClusters.Count > 0)
            {
                for (int d = 0; d < additionalClusters.Count; d++)
                {
                    Vector3 position = new Vector3(additionalClusters[d].position[0], additionalClusters[d].position[1], additionalClusters[d].position[2]);
                    Vector3 euler = new Vector3(additionalClusters[d].rotation[0], additionalClusters[d].rotation[1], additionalClusters[d].rotation[2]);
                    string name = additionalClusters[d].name;
                    var currentAdditional = Instantiate(Resources.Load<GameObject>("Exterior/" + name), clusterManager.transform);

                    currentAdditional.layer = 12;
                    currentAdditional.transform.eulerAngles = euler;
                    currentAdditional.transform.position = position;

                }
            }
            UpdateCurrentGrid(sitePolygon);
            clusterPanel.interactable = true;
            exteriorPanel.interactable = true;
            rotateGrid.interactable = true;
        }

        /// <summary>
        /// Called when a control point of the site polygon is selected
        /// </summary>
        /// <param name="sender"></param>
        private void OnCPSelected(ControlPointUI sender)
        {
            if (settingOrientation)
            {
                if (start == Vector3.zero)
                {
                    start = sender.sceneObject[0].position;
                    startPoint = sender;
                    startPoint.gameObject.GetComponent<Image>().color = startPoint.selected;
                    startPoint.isSelected = true;
                }
                else
                {
                    end = sender.sceneObject[0].position;
                    EndSettingOrientation();
                }
            }
        }

        public void AbortOrientation()
        {
            start = Vector3.zero;
            end = Vector3.zero;
            orientation = Vector3.right;
            if (startPoint != null)
            {
                startPoint.isSelected = false;
                startPoint.gameObject.GetComponent<Image>().color = startPoint.original;
            }

            addGridButton.interactable = true;
            settingOrientation = false;
        }

        /// <summary>
        /// Closes the orientation process
        /// </summary>
        public void EndSettingOrientation()
        {
            settingOrientation = false;
            orientation = end - start;
            GenerateGrid(); ;
            start = Vector3.zero;
            end = Vector3.zero;
            orientation = Vector3.right;
            startPoint.isSelected = false;
            startPoint.gameObject.GetComponent<Image>().color = startPoint.original;
            addGridButton.interactable = true;
            addGridText.text = "";
            clusterPanel.interactable = true;
            exteriorPanel.interactable = true;
            rotateGrid.interactable = true;
            deleteSite.interactable = false;
        }

        /// <summary>
        /// Creates a grid
        /// </summary>
        public void GenerateGrid()
        {
            if (currentGrid != null)
            {
                currentGrid.gameObject.SetActive(false);
            }
            if (currentGridGUIElement != null)
            {
                currentGridGUIElement.Deselect();
            }
            currentGrid = Instantiate(gridPrefab, transform).GetComponent<MeshlessGrid>();
            currentGrid.transform.right = orientation;
            currentGrid.gameObject.layer = 9;
            currentGrid.placedItemParent = configurationParent;
            var corners = sitePolygon.GetBoundingBox(currentGrid.transform);
            var side1 = corners[3] - corners[0];
            var side2 = corners[1] - corners[0];
            Vector3 bounds = ((corners[0] + corners[1] + corners[2] + corners[3]) / 4);
            currentGrid.transform.position = ((corners[0] + corners[1] + corners[2] + corners[3]) / 4);
            currentGrid.xNum = Mathf.CeilToInt(side1.magnitude / currentGrid.tileSizeX);
            currentGrid.yNum = Mathf.CeilToInt(side2.magnitude / currentGrid.tileSizeY);
            currentGrid.Initialize(sitePolygon, grids.Count);
            grids.Add(currentGrid);
            var gridLines = currentGrid.GetPanelPoints();
            var drawLines = Camera.main.GetComponent<DrawGLines>();
            drawLines.currentGridIndex = grids.Count - 1;
            var mat = new Material(drawLines.gridMat);
            mat.color = gridColors[grids.Count - 1];
            drawLines.gridColors.Add(mat);
            drawLines.gridPoints.Add(gridLines);
            currentGridIndex = grids.Count - 1;
            drawLines.siteBoundaryPoints = corners;
            GenerateGUISelect(mat.color);
        }

        public void GenerateGrid(Vector3 euler)
        {
            if (currentGrid != null)
            {
                currentGrid.gameObject.SetActive(false);
            }
            if (currentGridGUIElement != null)
            {
                currentGridGUIElement.Deselect();
            }
            currentGrid = Instantiate(gridPrefab, transform).GetComponent<MeshlessGrid>();
            currentGrid.transform.eulerAngles = euler;
            currentGrid.gameObject.layer = 9;
            currentGrid.placedItemParent = configurationParent;
            var corners = sitePolygon.GetBoundingBox(currentGrid.transform);
            var side1 = corners[3] - corners[0];
            var side2 = corners[1] - corners[0];
            Vector3 bounds = ((corners[0] + corners[1] + corners[2] + corners[3]) / 4);
            currentGrid.transform.position = new Vector3(bounds.x, currentGrid.transform.position.y, bounds.z);
            currentGrid.xNum = Mathf.CeilToInt(side1.magnitude / currentGrid.tileSizeX);
            currentGrid.yNum = Mathf.CeilToInt(side2.magnitude / currentGrid.tileSizeY);
            currentGrid.Initialize(sitePolygon, grids.Count);
            grids.Add(currentGrid);
            var gridLines = currentGrid.GetPanelPoints();
            var drawLines = Camera.main.GetComponent<DrawGLines>();
            drawLines.currentGridIndex = grids.Count - 1;
            var mat = new Material(drawLines.gridMat);
            mat.color = gridColors[grids.Count - 1];
            drawLines.gridColors.Add(mat);
            drawLines.gridPoints.Add(gridLines);
            currentGridIndex = grids.Count - 1;
            drawLines.siteBoundaryPoints = corners;
            GenerateGUISelect(mat.color);
        }

        /// <summary>
        /// Creates a GUI element for the grid
        /// </summary>
        /// <param name="color">The color of the GUI element</param>
        public void GenerateGUISelect(Color color)
        {
            currentGridGUIElement = Instantiate(guiGridSelectPrefab, guiGridParent).GetComponent<GuiSelect>();
            currentGridGUIElement.transform.localPosition = new Vector3(25 + 42 * gridGUIElements.Count, 0);
            currentGridGUIElement.color = color;
            currentGridGUIElement.Initialize();
            gridGUIElements.Add(currentGridGUIElement);
            currentGridGUIElement.selected += OnGuiSelected;
        }



        /// <summary>
        /// Called when the GUI element of a grid is selected
        /// </summary>
        /// <param name="sender">The GUI element that was selected</param>
        public void OnGuiSelected(GuiSelect sender)
        {
            for (int i = 0; i < gridGUIElements.Count; i++)
            {
                if (sender == gridGUIElements[i])
                {
                    if (currentGridGUIElement != null)
                    {
                        currentGridGUIElement.Deselect();
                    }
                    currentGridGUIElement = sender;
                    currentGridIndex = i;
                    if (selectedPrefab != null)
                    {
                        currentGrid.RemovePrefab(selectedPrefab);
                    }
                    SetCurrentGrid(currentGridIndex);
                    if (selectedPrefab != null)
                    {
                        currentGrid.AddPrefab(selectedPrefab);
                        selectedPrefab.GetComponent<PlaceablePrefab>().gridIndex = currentGridIndex;
                    }
                }
            }

        }

        /// <summary>
        /// Sets the current grid
        /// </summary>
        /// <param name="index">The index of the grid to be set</param>
        public void SetCurrentGrid(int index)
        {
            for (int i = 0; i < grids.Count; i++)
            {
                if (i == index)
                {
                    currentGrid = grids[i];
                    currentGrid.gameObject.SetActive(true);
                    var drawLines = Camera.main.GetComponent<DrawGLines>();
                    drawLines.currentGridIndex = i;
                    var corners = sitePolygon.GetBoundingBox(currentGrid.transform);
                    var side1 = corners[3] - corners[0];
                    var side2 = corners[1] - corners[0];
                    drawLines.siteBoundaryPoints = corners;
                }
                else
                {
                    grids[i].gameObject.SetActive(false);
                }
            }
        }

        /// <summary>
        /// Called when the current grid is to be rotated
        /// </summary>
        /// <param name="angle">The angle to rotate the current grid</param>
        public void RotateGrid(float angle)
        {
            currentGrid.transform.eulerAngles += new Vector3(0, angle, 0);
            var corners = sitePolygon.GetBoundingBox(currentGrid.transform);
            var side1 = corners[3] - corners[0];
            var side2 = corners[1] - corners[0];
            Vector3 bounds = ((corners[0] + corners[1] + corners[2] + corners[3]) / 4);
            currentGrid.transform.position = new Vector3(bounds.x, currentGrid.level * floor2floor, bounds.z);
            currentGrid.xNum = Mathf.CeilToInt(side1.magnitude / currentGrid.tileSizeX);
            currentGrid.yNum = Mathf.CeilToInt(side2.magnitude / currentGrid.tileSizeY);
            currentGrid.ReorientGrid(sitePolygon, angle);
            var drawLines = Camera.main.GetComponent<DrawGLines>();
            drawLines.currentGridIndex = currentGridIndex;
            drawLines.gridPoints[currentGridIndex] = currentGrid.GetPanelPoints();
            drawLines.siteBoundaryPoints = corners;
        }

        /// <summary>
        /// Called when site polygon is changed
        /// </summary>
        /// <param name="polygon"></param>
        public void UpdateCurrentGrid(Polygon polygon)
        {
            if (currentGrid != null)
            {
                var corners = polygon.GetBoundingBox(currentGrid.transform);
                var side1 = corners[3] - corners[0];
                var side2 = corners[1] - corners[0];
                Vector3 bounds = ((corners[0] + corners[1] + corners[2] + corners[3]) / 4);
                currentGrid.transform.position = new Vector3(bounds.x, currentGrid.transform.position.y, bounds.z);
                currentGrid.xNum = Mathf.CeilToInt(side1.magnitude / currentGrid.tileSizeX);
                currentGrid.yNum = Mathf.CeilToInt(side2.magnitude / currentGrid.tileSizeY);
                currentGrid.UpdateGrid(sitePolygon);
                var drawLines = Camera.main.GetComponent<DrawGLines>();
                drawLines.currentGridIndex = currentGridIndex;
                drawLines.gridPoints[currentGridIndex] = currentGrid.GetPanelPoints();
                drawLines.siteBoundaryPoints = corners;
            }
        }

        /// <summary>
        /// Rotate grid and placed clusters based on slider value
        /// </summary>
        /// <param name="sliderValue"></param>he slider value
        public void RotateGridWithSlider(float sliderValue)
        {
            print(currentGrid.transform.position.y);
            if (configurationParent.childCount > 0)
            {
                currentGridObjects.Clear();
                if (rotationParent == null)
                {
                    rotationParent = new GameObject("RotationParent");
                }
                foreach (Transform cluster in configurationParent)
                {

                    if (cluster.GetComponent<PlaceablePrefab>().grid == currentGrid)
                        currentGridObjects.Add(cluster.gameObject);
                }
                foreach (Transform additional in additionalParent)
                {
                    currentGridObjects.Add(additional.gameObject);
                }
                var bnds = GetMaxBounds(currentGridObjects);
                rotationParent.transform.position = bnds.center;
                for (int i = 0; i < currentGridObjects.Count; i++)
                {
                    currentGridObjects[i].transform.SetParent(rotationParent.transform);
                }
                rotationParent.transform.Rotate(Vector3.up, sliderValue - lastSliderValue);
                for (int i = 0; i < currentGridObjects.Count; i++)
                {
                    if (currentGridObjects[i].GetComponent<PlaceablePrefab>() != null)
                        currentGridObjects[i].transform.SetParent(configurationParent);
                    else
                        currentGridObjects[i].transform.SetParent(additionalParent);
                }
                Vector3 centre = GetMaxBounds(configurationParent.gameObject).center;
                currentGrid.transform.RotateAround(centre, Vector3.up, sliderValue - lastSliderValue);
                lastSliderValue = (int)sliderValue;

                RotateGrid(sliderValue - lastSliderValue);
            }
            else
            {
                RotateGrid(sliderValue - lastSliderValue);
                lastSliderValue = (int)sliderValue;
            }

        }
        /// <summary>
        /// Returns the collider bounds of all children of given gameobject
        /// </summary>
        /// <param name="g"></param> the parent gameobject
        /// <returns></returns>
        Bounds GetMaxBounds(GameObject g)
        {
            var b = new Bounds(g.transform.position, Vector3.zero);
            foreach (BoxCollider r in g.GetComponentsInChildren<BoxCollider>())
            {
                b.Encapsulate(r.bounds);
            }
            return b;
        }

        /// <summary>
        /// Returns the collider bounds of all the gameObjects in a list
        /// </summary>
        /// <param name="objs">The list of gameObjects</param>
        /// <returns>UnityEngine Bounds</returns>
        Bounds GetMaxBounds(List<GameObject> objs)
        {
            var b = new Bounds(objs[0].transform.position, Vector3.zero);
            for (int i = 0; i < objs.Count; i++)
            {
                var collider = objs[i].GetComponent<BoxCollider>();
                if (collider != null)
                {
                    b.Encapsulate(collider.bounds);
                }
            }
            return b;
        }
        public void OnPlaceFromPrefabButton()
        {
            OnStartPlacing(senderButton);

        }
        /// <summary>
        /// Called when a prefab button is pressed to start the placing process
        /// </summary>
        /// <param name="sender">The prefab button that was pressed</param>
        public void OnStartPlacing(PrefabButton sender)
        {
            Selector.TaggedObject.Deselect();
            currentGrid.initDiff = Vector3.zero;
            currentGrid.currentObject = null;
            currentGrid.currentAdditional = null;
            if (!clusterManager.clusterButtons.Contains(sender))
            {

                GetComponent<ProceduralClusterManager>().UpdateText();
                currentGrid.clustersToPlace = new List<GameObject>() { sender.cluster };
                currentGrid.clusterIndex = 0;
                currentGrid.StartPlacing();
            }
            else
            {
                if (clusterManager.clusterToPlace.ContainsKey(sender))
                {
                    clusterManager.clusterToPlace[sender]++;
                }
                clusterManager.UpdateInfoText();
                currentGrid.additionalElementPrefab = sender.cluster;
                currentGrid.StartPlacingAdditional();
            }
        }



        /// <summary>
        /// Snapping between clusters
        /// </summary>
        /// <param name="s"></param> Snap or not
        public void OnSnap(bool s)
        {
            currentGrid.GetComponent<MeshlessGrid>().snapOnObjects = s;
        }

        public ProceduralCluster OnStartPlacing(GameObject cluster, List<DataRow> row, Material mat)
        {
            Selector.TaggedObject.Deselect();
            currentGrid.initDiff = Vector3.zero;
            currentGrid.currentObject = null;
            currentGrid.clustersToPlace = new List<GameObject>() { cluster };
            if (row.Count > 1)
            {
                currentGrid.clusterIndex = 0;
            }
            else
            {
                currentGrid.clusterIndex = 0;
            }

            return currentGrid.StartPlacing(row, mat);
        }


        public void OnPlaceCluster(GameObject cluster, Vector3 position, Vector3 euler, int level, Cluster info, Vector3 scale)
        {
            currentGrid.clustersToPlace = new List<GameObject>() { cluster };

            currentGrid.clusterIndex = 0;
            currentGrid.StartPlacing(info);
            currentGrid.currentObject.transform.eulerAngles = euler;
            currentGrid.currentObject.transform.position = position;
            currentGrid.currentObject.transform.localScale = scale;
            currentGrid.currentObject.GetComponent<PlaceablePrefab>().level = level;
            currentGrid.PlaceCluster(position);

        }

        public void OnPlaceAdditional(GameObject additional, Vector3 position, Vector3 euler, string name)
        {



        }

        /// <summary>
        /// Called when a placed prefab is deleted
        /// </summary>
        /// <param name="sender">The deleted placed prefab</param>
        public void OnObjectDeleted(PlaceablePrefab sender)
        {
            currentGrid.RemovePrefab(sender.gameObject);
            clusterManager.UpdateInfoText();
            PrefabAdded();

        }
        /// <summary>
        /// When the level of a grid changes
        /// </summary>
        /// <param name="level"></param> Slider value to give height
        public void OnLevelChanged(float level)
        {
            currentGrid.SetGridHeight(floor2floor, (int)level);
            Camera.main.GetComponent<DrawGLines>().gridPoints[currentGridIndex] = currentGrid.GetPanelPoints();
        }
        /// <summary>
        /// Total site covered area
        /// </summary>
        /// <returns></returns>
        public float GetPrefabFootprint()
        {
            float footPrint = 0.0f;

            for (int i = 0; i < currentGrid.placedItemParent.childCount; i++)
            {
                var prefab = currentGrid.placedItemParent.GetChild(i).GetComponent<PlaceablePrefab>();
                if (prefab.level == 0)
                {
                    footPrint += prefab.size.x * prefab.size.z;
                }
            }
            return footPrint;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Called when a placed prefab is being selected
        /// </summary>
        /// <param name="sender">The placed prefab that was selected</param>
        private void OnPlacedSelected(SelectableGeometry sender)
        {

            if (sender as PlaceablePrefab != null)
            {

                var placeable = sender as PlaceablePrefab;
                currentGridIndex = placeable.gridIndex;
                SetCurrentGrid(currentGridIndex);
                currentGrid.currentObject = placeable.gameObject;
                currentGrid.hasSelected = true;
                currentGrid.GetInitDiff();
                currentGridGUIElement.Deselect();
                currentGridGUIElement = gridGUIElements[currentGridIndex];
                currentGridGUIElement.SetAsCurrent();

                transformationTab.SendMessage("OnSelect", SendMessageOptions.DontRequireReceiver);
            }
            else if (sender as AdditionalPrefab != null)
            {

                currentGrid.currentObject = null;
                currentGrid.hasSelected = true;
                currentGrid.currentAdditional = sender.gameObject;
                transformationTab.SendMessage("OnSelect", SendMessageOptions.DontRequireReceiver);
            }
        }

        private void OnDeselect()
        {

            if (currentGrid != null)
            {
                currentGrid.raycasting = false;

                selectedPrefab = null;
                currentGrid.hasSelected = false;
                currentGrid.currentObject = null;
                currentGrid.currentAdditional = null;
            }

        }


        /// <summary>
        /// Saves the info on grids and clusters
        /// </summary>
        /// <returns></returns>
        public ConfigurationSaveState SetSaveState()
        {
            ConfigurationSaveState saveState = new ConfigurationSaveState();
            saveState.program = entry.currentEntry;
            saveState.latitude = map.latitude.text;
            saveState.longitude = map.longitude.text;
            saveState.loadedMap = map.mapLoaded;
            saveState.existingDeleted = map.existingDeleted;
            Site site = new Site();
            site.controlPoints = new List<float[]>();
            if (sitePolygon != null)
            {
                for (int i = 0; i < sitePolygon.basePolygon.vertices.Count; i++)
                {
                    site.controlPoints.Add(new float[] { sitePolygon.basePolygon.vertices[i].currentPosition.x, sitePolygon.basePolygon.vertices[i].currentPosition.y, sitePolygon.basePolygon.vertices[i].currentPosition.z });
                }
            }
            saveState.grids = new List<GridState>();
            saveState.grids.TrimExcess();
            if (grids != null)
            {
                for (int i = 0; i < grids.Count; i++)
                {
                    GridState grid = new GridState()
                    {
                        sizeX = grids[i].tileSizeX,
                        sizeY = grids[i].tileSizeY,
                        xNum = grids[i].xNum,
                        yNum = grids[i].yNum,
                        snapDistance = grids[i].objectSnapDistance,
                        position = new float[] { grids[i].transform.position.x, grids[i].transform.position.y, grids[i].transform.position.z },
                        rotation = new float[] { grids[i].transform.eulerAngles.x, grids[i].transform.eulerAngles.y, grids[i].transform.eulerAngles.z }
                    };
                    List<Cluster> clusters = new List<Cluster>();

                    var prefabs = grids[i].placedClusters.Keys.ToList();
                    for (int j = 0; j < prefabs.Count; j++)
                    {
                        if (prefabs[j].GetComponent<ProceduralCluster>() != null)
                        {
                            var proc = prefabs[j].GetComponent<ProceduralCluster>();
                            List<Cluster> children = new List<Cluster>();
                            if (proc.transform.childCount > 0)
                            {
                                for (int c = 0; c < proc.transform.childCount; c++)
                                {
                                    if (proc.transform.GetChild(c).GetComponent<ProceduralCluster>() != null)
                                    {
                                        var temp = proc.transform.GetChild(c).GetComponent<ProceduralCluster>();
                                        Cluster cl = new Cluster()
                                        {
                                            name = temp.name,
                                            detailedname = temp.detailedName,
                                            startPoint = new float[] { temp.startPoint.x, proc.startPoint.y, proc.startPoint.z },
                                            builtWidth = temp.builtWidth,
                                            builtHeight = temp.builtHeight,
                                            builtDepth = temp.builtDepth,
                                            circulationDepth = temp.circulationDepth,
                                            topOffset = temp.topOffset,
                                            bottomOffset = temp.bottomOffset,
                                            facilitiesIndex = temp.facilitiesIndex,
                                            moduleDepth = temp.moduleDepth,
                                            moduleWidth = temp.moduleWidth,
                                            moduleHeight = temp.moduleHeight,
                                            hasCirculation = temp.hasCirculation,
                                            halfLeft = temp.halfLeft,
                                            halfRight = temp.halfRight,
                                            position = new float[] { temp.transform.position.x, temp.transform.position.y, temp.transform.position.z },
                                            rotation = new float[] { temp.transform.eulerAngles.x, temp.transform.eulerAngles.y, temp.transform.eulerAngles.z },
                                            localposition = new float[] { temp.transform.localPosition.x, temp.transform.localPosition.y, temp.transform.localPosition.z },
                                            localrotation = new float[] { temp.transform.localEulerAngles.x, temp.transform.localEulerAngles.y, temp.transform.localEulerAngles.z },
                                            scale = new float[] { prefabs[j].transform.localScale.x, temp.transform.localScale.y, temp.transform.localScale.z },
                                            level = temp.GetComponent<PlaceablePrefab>().level,
                                            doorIndex = temp.doorIndex,
                                        };
                                        children.Add(cl);
                                    }

                                }
                            }
                            clusters.Add(new Cluster()
                            {
                                name = proc.name,
                                detailedname = proc.detailedName,
                                startPoint = new float[] { proc.startPoint.x, proc.startPoint.y, proc.startPoint.z },
                                builtWidth = proc.builtWidth,
                                builtHeight = proc.builtHeight,
                                builtDepth = proc.builtDepth,
                                circulationDepth = proc.circulationDepth,
                                topOffset = proc.topOffset,
                                bottomOffset = proc.bottomOffset,
                                facilitiesIndex = proc.facilitiesIndex,
                                moduleDepth = proc.moduleDepth,
                                moduleWidth = proc.moduleWidth,
                                moduleHeight = proc.moduleHeight,
                                hasCirculation = proc.hasCirculation,
                                halfLeft = proc.halfLeft,
                                halfRight = proc.halfRight,
                                position = new float[] { prefabs[j].transform.position.x, prefabs[j].transform.position.y, prefabs[j].transform.position.z },
                                rotation = new float[] { prefabs[j].transform.eulerAngles.x, prefabs[j].transform.eulerAngles.y, prefabs[j].transform.eulerAngles.z },
                                scale = new float[] { prefabs[j].transform.localScale.x, prefabs[j].transform.localScale.y, prefabs[j].transform.localScale.z },
                                level = prefabs[j].GetComponent<PlaceablePrefab>().level,
                                doorIndex = proc.doorIndex,
                                clustersWithin = children,
                            });

                        }
                    }


                    clusters.TrimExcess();
                    grid.clusters = clusters;
                    saveState.grids.Add(grid);
                }
            }
            List<AdditionalCluster> additionalClusters = new List<AdditionalCluster>();
            var ext = clusterManager.transform;
            for (int d = 0; d < ext.childCount; d++)
            {
                var ad = ext.GetChild(d);
                additionalClusters.Add(new AdditionalCluster()
                {

                    position = new float[] { ad.position.x, ad.position.y, ad.position.z },
                    rotation = new float[] { ad.eulerAngles.x, ad.eulerAngles.y, ad.eulerAngles.z },
                    name = ad.name.Split('(')[0]

                });


            }
            additionalClusters.TrimExcess();
            saveState.additionalClusters = additionalClusters;
            saveState.site = site;
            return saveState;
        }

        #endregion
    }
}
