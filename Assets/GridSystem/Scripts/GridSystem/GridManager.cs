﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.UIElements;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BrydenWoodUnity.Navigation;

public class GridManager : MonoBehaviour {

    [Header("Main References")]
    public GameObject gridPrefab;
    public Transform configurationParent;
    public List<Color> gridColors;

    [HideInInspector]
    public Polygon sitePolygon;

    private Grid currentGrid;
    private int currentGridIndex = 0;
    private List<Grid> allGrids;
    private bool settingOrientation=false;
    private Vector3 orientation = Vector3.zero;
    private Vector3 start = Vector3.zero;
    private Vector3 end = Vector3.zero;

    // Use this for initialization
    void Start () {
        allGrids = new List<Grid>();
        PolygonDrawer.polygonEnded += OnSitePlaced;
        Grid.gridCalculated += OnGridMeshCalculated;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.T))
        {
            settingOrientation = true;
            //GenerateGrid();
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            currentGridIndex = (currentGridIndex + 1) % allGrids.Count;
            SetCurrentGrid(currentGridIndex);
        }
	}

    private void OnGUI()
    {
        if (!settingOrientation)
        {
            if (GUI.Button(new Rect(10, 50, 200, 20), "Set Orientation"))
            {
                settingOrientation = true;
            }
        }

        if (allGrids.Count > 1)
        {
            if (GUI.Button(new Rect(10, 80, 200, 20), "Toggle Grid"))
            {
                currentGridIndex = (currentGridIndex + 1) % allGrids.Count;
                SetCurrentGrid(currentGridIndex);
            }
        }
    }

    public void OnSitePlaced(Polygon sitePolygon, Polygon offset=null)
    {
        this.sitePolygon = sitePolygon;
        for (int i=0; i<sitePolygon.vertices.Count; i++)
        {
            sitePolygon.vertices[i].cpui.onSelected += OnCPSelected;
        }
    }

    public void GenerateGrid()
    {
        if (currentGrid != null)
        {
            currentGrid.gameObject.SetActive(false);
        }
        currentGrid = Instantiate(gridPrefab,transform).GetComponent<Grid>();
        currentGrid.transform.right = orientation;
        currentGrid.gameObject.layer = 9;
        currentGrid.placedItemParent = configurationParent;
        var corners = sitePolygon.GetBoundingBox(currentGrid.transform);
        var side1 = corners[3] - corners[0];
        var side2 = corners[1] - corners[0];
        currentGrid.transform.position = ((corners[0]+corners[1]+corners[2]+corners[3])/4);
        currentGrid.xNum = Mathf.CeilToInt(side1.magnitude / currentGrid.tileSizeX);
        currentGrid.yNum = Mathf.CeilToInt(side2.magnitude / currentGrid.tileSizeY);
        currentGrid.Initialize(sitePolygon);
        allGrids.Add(currentGrid);
        var gridLines = currentGrid.GetGridLines();
        var drawLines = Camera.main.GetComponent<DrawGLines>();
        drawLines.currentGridIndex = allGrids.Count - 1;
        var mat = new Material(drawLines.gridMat);
        mat.color = gridColors[allGrids.Count - 1];
        drawLines.gridColors.Add(mat);
        //drawLines.gridPoints.Add(gridLines.ToArray());
        currentGridIndex = allGrids.Count - 1;
        drawLines.siteBoundaryPoints = corners;
    }

    public IEnumerator GenerateGridCoroutine()
    {
        if (currentGrid != null)
        {
            currentGrid.gameObject.SetActive(false);
        }
        currentGrid = Instantiate(gridPrefab, transform).GetComponent<Grid>();
        currentGrid.transform.right = orientation;
        currentGrid.gameObject.layer = 9;
        currentGrid.placedItemParent = configurationParent;
        var corners = sitePolygon.GetBoundingBox(currentGrid.transform);
        var side1 = corners[3] - corners[0];
        var side2 = corners[1] - corners[0];
        currentGrid.transform.position = ((corners[0] + corners[1] + corners[2] + corners[3]) / 4);
        currentGrid.xNum = Mathf.CeilToInt(side1.magnitude / currentGrid.tileSizeX);
        currentGrid.yNum = Mathf.CeilToInt(side2.magnitude / currentGrid.tileSizeY);
        Camera.main.GetComponent<DrawGLines>().siteBoundaryPoints = corners;
        yield return currentGrid.InitializeCoroutine(sitePolygon);
    }

    public void OnGridMeshCalculated(Grid grid)
    {
        if (currentGrid == grid)
        {
            allGrids.Add(currentGrid);
            var gridLines = currentGrid.GetGridLines();
            var drawLines = Camera.main.GetComponent<DrawGLines>();
            drawLines.currentGridIndex = allGrids.Count - 1;
            var mat = new Material(drawLines.gridMat);
            mat.color = gridColors[allGrids.Count - 1];
            drawLines.gridColors.Add(mat);
            //drawLines.gridPoints.Add(gridLines.ToArray());
            currentGridIndex = allGrids.Count - 1;
        }
    }

    public void SetCurrentGrid(int index)
    {
        for (int i=0; i<allGrids.Count; i++)
        {
            if (i == index)
            {
                currentGrid = allGrids[i];
                currentGrid.gameObject.SetActive(true);
                Camera.main.GetComponent<DrawGLines>().currentGridIndex = index;
            }
            else
            {
                allGrids[i].gameObject.SetActive(false);
            }
        }
    }

    public void SetGridOrientation()
    {

    }

    public void EndSettingOrientation()
    {
        settingOrientation = false;
        orientation = end - start;
        StartCoroutine(GenerateGridCoroutine());
        start = Vector3.zero;
        end = Vector3.zero;
        orientation = Vector3.right;
    }

    private void OnCPSelected(ControlPointUI sender)
    {
        if (settingOrientation)
        {
            if (start == Vector3.zero)
            {
                start = sender.sceneObject[0].position;
            }
            else
            {
                end = sender.sceneObject[0].position;
                EndSettingOrientation();
            }
        }
    }
}
