﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using BrydenWoodUnity.GeometryManipulation;

[CustomEditor(typeof(PlaceablePrefab))]
public class PlaceablePrefabEditor : Editor
{
    void OnEnable()
    {
    }

    public override void OnInspectorGUI()
    {
        
        var myTarget = ((PlaceablePrefab)target);
        DrawDefaultInspector();
        if (GUILayout.Button("Populate Lists"))
        {
            myTarget.PopulateLists();
        }
        if (GUILayout.Button("Clear Lists"))
        {
            myTarget.ClearLists();
        }

        if (GUILayout.Button("Toggle Circulation"))
        {
            myTarget.ToggleCirculation();
        }

        if (myTarget.GetComponent<BoxCollider>() == null)
        {
            if (GUILayout.Button("Generate Box Collider"))
            {
                myTarget.GenerateBoxCollider();
            }
        }
    }

}
