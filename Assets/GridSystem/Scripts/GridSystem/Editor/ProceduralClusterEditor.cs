﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using BrydenWoodUnity.GeometryManipulation;

[CustomEditor(typeof(ProceduralCluster))]
public class ProceduralClusterEditor : Editor
{

    private void OnEnable()
    {
        
    }

    public override void OnInspectorGUI()
    {
        var myTarget = (ProceduralCluster)target;
        DrawDefaultInspector();

        if (GUILayout.Button("Generate Geometries"))
        {
            myTarget.GenerateGeometries();
        }
    }
}
