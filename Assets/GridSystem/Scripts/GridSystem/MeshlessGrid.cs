﻿using BrydenWoodUnity;
using BrydenWoodUnity.GeometryManipulation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using BrydenWoodUnity.Navigation;

namespace BrydenWoodUnity.Grid
{
    public class MeshlessGrid : MonoBehaviour
    {
        #region Public Properties
        [Header("Grid Information:")]
        public float tileSizeX = 3.6f;
        public float tileSizeY = 3.6f;
        public int xNum = 40;
        public int yNum = 40;
        public bool initializeOnStart = false;
        public bool snapOnObjects = true;
        public float objectSnapDistance = 1.8f;
        [Space(10)]
        [Header("Scene related information:")]
        public List<GameObject> clustersToPlace;
        public Transform placedItemParent;
        public LayerMask snapMask;
        [Space(10)]
        [Header("UI Elements:")]
        public RectTransform transformationTab;
        public Button moveButton;

        public GameObject additionalElementPrefab { get; set; }

        public Camera raycastingCamera { get; set; }
        public int clusterIndex { get; set; }
        public bool hasSelected { get; set; }
        public GameObject currentObject { get; set; }
        public int index { get; set; }
        public int level { get; set; }
        public Dictionary<GameObject, int[]> placedClusters { get; set; }
        public Vector3 initDiff { get; set; }

        public bool translationTabSet = false;
        GameObject transformPanel;
        public bool raycasting = false;
        bool moving = false;
        private Vector3 prevCameraPosition;
        private float cameraPrevFoV;

        bool isInter;
        GameObject other;

        Transform additionalParent;
        #endregion

        #region Private Properties
        private Vector3 gridStart { get; set; }
        private Vector3[][] centres { get; set; }
        private bool[][] siteInclusion { get; set; }
        private Plane gridPlane { get; set; }
        private Vector3[] minMax { get; set; }
        private Polygon sitePolygon { get; set; }

        private string loadingMessage { get; set; }


        private bool initialized { get; set; }
        public bool placing { get; set; }
        public bool placingAdditional { get; set; }

        private Vector3 currentEuler { get; set; }
        private int[] currentTile { get; set; }
        public GameObject currentAdditional { get; set; }

        private float minTreeScale = 0.8f;
        private float maxTreeScale = 1.1f;


        Vector3[] allCorners;
        Vector3 initialPosition;
        Vector3 clusterRotation;
        Text notification;
        bool isAdditionalPlaceable = true;

        public LayerMask myMask;
        int levelCount = 0;

        public List<Material> allMats;
        public Material builtMaterial;

        bool touchPlaceAdditional;

        #endregion

        #region Events
        public delegate void OnGridCalculated(MeshlessGrid grid);
        public static event OnGridCalculated gridCalculated;

        public delegate void OnClusterAdded();
        public static event OnClusterAdded clusterAdded;
        #endregion



        // Use this for initialization
        private void Awake()
        {
            if (initializeOnStart)
            {
                Initialize();
            }
            transformPanel = GameObject.Find("TranslationPanel");
            additionalParent = GameObject.Find("ClusterManager").transform;


            moveButton = transformPanel.transform.GetChild(0).GetChild(0).GetComponent<Button>();

            snapOnObjects = true;
            notification = GameObject.Find("Notifications").GetComponent<Text>();

            allMats = new List<Material>();

        }



        private void LateUpdate()
        {
            bool eventCheck = Input.touchCount > 0 ? !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId) : !EventSystem.current.IsPointerOverGameObject(-1);
            if (eventCheck)
            {

                if (initialized)
                {
                    if (placing)
                    {
                        if (Input.GetKeyDown(KeyCode.Escape) || CustomInput.GetAxis("GUIEscape") == 3)
                        {
                            AbortPlacing();
                        }


                        if (Input.GetMouseButtonDown(0))
                        {

                            Raycast();


                            if (currentObject != null && currentObject.GetComponent<PlaceablePrefab>().isPlaceable)
                            {

                                StartCoroutine(PlaceCluster());

                                notification.text = "";
                            }
                            else
                            {
                                notification.text = "Cannot place this cluster there";
                                StartCoroutine(ErrorText());

                            }


                        }
                    }

                    if (placingAdditional)
                    {

                        if (Input.GetKeyDown(KeyCode.Escape) || CustomInput.GetAxis("GUIEscape") == 3)
                        {
                            AbortPlacing();
                        }

                        if (Input.GetMouseButton(0))
                        {

                            Raycast();
                            BrushPlaceAdditional();
                        }
                        if (Input.GetMouseButtonUp(0) && touchPlaceAdditional)
                        {
                            StartCoroutine(StopBrush());
                        }

                    }

                    prevCameraPosition = Camera.main.transform.position;
                    cameraPrevFoV = Camera.main.orthographicSize;

                    if (hasSelected)
                    {
                        if (Input.touchCount > 0)
                        {
                            if (Input.GetMouseButtonDown(0) && Input.GetTouch(0).phase != TouchPhase.Moved)
                            {
                                notification.text = "";
                                GetInitDiff();

                            }
                            if (Input.GetMouseButton(0) && Input.GetTouch(0).phase == TouchPhase.Moved)
                            {
                                Raycast();
                                notification.text = "";
                            }
                            if (Input.GetMouseButtonUp(0))
                            {
                                if (currentObject != null)
                                {
                                    if (currentObject.GetComponent<PlaceablePrefab>().isPlaceable)
                                    {
                                        currentObject.layer = 13;
                                        StartCoroutine(PlaceCluster(false));
                                        notification.text = "";
                                    }

                                    else
                                    {

                                        currentObject.transform.position = initialPosition;
                                        currentObject.GetComponent<PlaceablePrefab>().isPlaceable = true;
                                        notification.text = "Cannot place this cluster there";
                                        StartCoroutine(ErrorText());
                                        currentObject.layer = 13;
                                    }
                                }
                                else if (currentAdditional != null)
                                {
                                    StartCoroutine(StopBrush());
                                }
                            }
                        }
                        else
                        {
                            if (Input.GetMouseButtonDown(0))
                            {
                                notification.text = "";
                                GetInitDiff();

                            }
                            if (Input.GetMouseButton(0) && !Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift))
                            {
                                Raycast();
                                notification.text = "";
                            }
                            if (Input.GetMouseButtonUp(0))
                            {
                                if (currentObject != null)
                                {
                                    if (currentObject.GetComponent<PlaceablePrefab>().isPlaceable)
                                    {
                                        currentObject.layer = 13;
                                        StartCoroutine(PlaceCluster(false));
                                        notification.text = "";
                                    }

                                    else
                                    {

                                        currentObject.transform.position = initialPosition;
                                        currentObject.GetComponent<PlaceablePrefab>().isPlaceable = true;
                                        notification.text = "Cannot place this cluster there";
                                        StartCoroutine(ErrorText());
                                        currentObject.layer = 13;
                                    }
                                }
                                else if (currentAdditional != null)
                                {
                                    StartCoroutine(StopBrush());
                                }
                            }
                        }

                    }
                }
            }
            else
            {
                if (initialized)
                {
                    if (placing)
                    {
                        if (Input.GetMouseButton(0))
                        {
                            AbortPlacing();

                        }

                    }
                }
                if (placingAdditional)
                {
                    if (Input.GetMouseButton(0))
                    {
                        AbortPlacing();

                    }
                }
                //if(currentObject!=null&&Input.GetMouseButton(0))
                //{
                //    Selector.TaggedObject.DeselectAll();
                //    Selector.TaggedObject.Deselect();
                //}

            }
        }
        /// <summary>
        /// Place additional clusters based on type. Brush for tiles and single placement for others
        /// </summary>
        void BrushPlaceAdditional()
        {
            //print(currentAdditional.name);
            if (currentAdditional.name.Contains("Tile"))
            {
                placingAdditional = true;
                touchPlaceAdditional = true;
                if (isAdditionalPlaceable)
                {

                    currentAdditional.layer = 12;
                    currentAdditional = Instantiate(currentAdditional, currentAdditional.transform.parent);
                    currentAdditional.layer = 2;
                    additionalParent.GetComponent<ClusterManager>().AddTile(currentAdditional.name.Split('(')[0]);
                }
            }
            else
            {

                placingAdditional = true;
                touchPlaceAdditional = true;

            }
        }
        /// <summary>
        /// Make the notification text disappear after 2 secs
        /// </summary>
        /// <returns></returns>null
        IEnumerator ErrorText()
        {
            yield return new WaitForSeconds(2f);
            notification.text = "";

        }
        /// <summary>
        /// Stops placement of additional objects and records them
        /// </summary>
        /// <returns></returns>
        IEnumerator StopBrush()
        {
            placingAdditional = false;
            yield return new WaitForEndOfFrame();

            if (!currentAdditional.name.Contains("Tile"))
            {
                if (currentAdditional.GetComponent<BatchPlacement>() != null)
                {
                    for (int i = 0; i < currentAdditional.transform.childCount - 1; i++)
                    {
                        additionalParent.GetComponent<ClusterManager>().AddElement(currentAdditional.name.Replace("(Clone)", string.Empty));
                    }
                }
                else
                {
                    additionalParent.GetComponent<ClusterManager>().AddElement(currentAdditional.name.Replace("(Clone)", string.Empty));
                }
            }

            currentAdditional.layer = 12;
        }
        /// <summary>
        /// Mirrors the selected gameobject
        /// </summary>
        /// <param name="or"></param> mirror axis as letter(x or z)
        public void MirrorObject(string or)
        {
            ProceduralCluster pc = currentObject.GetComponent<ProceduralCluster>();
            PlaceablePrefab pl = currentObject.GetComponent<PlaceablePrefab>();
            if (or == "x")
            {
                bool temp = pc.halfLeft;
                //if (!currentObject.name.Contains("Infant"))
                //{
                pc.halfLeft = pc.halfRight;
                pc.halfRight = temp;
                bool temp2 = pl.resolvedLeft;
                pl.resolvedLeft = pl.resolvedRight;
                pl.resolvedRight = temp2;


                if (currentObject.tag == "MirroredX")
                {
                    currentObject.tag = "Untagged";
                }

                else if (currentObject.tag == "Untagged")
                {
                    currentObject.tag = "MirroredX";

                }
                else if (currentObject.tag == "MirroredZ")
                {
                    currentObject.tag = "Mirrored";

                }
                else if (currentObject.tag == "Mirrored")
                {
                    currentObject.tag = "MirroredZ";
                }



                currentObject.transform.localScale = new Vector3(-currentObject.transform.localScale.x, currentObject.transform.localScale.y, currentObject.transform.localScale.z);
                int wholePanel = Mathf.CeilToInt(currentObject.GetComponent<ProceduralCluster>().builtWidth / 3.6f);
                currentObject.transform.position += ((currentObject.transform.right * wholePanel * 3.6f) * (-currentObject.transform.localScale.x));

                Vector3 newPos = currentObject.transform.position + currentObject.transform.forward * (tileSizeX / 2) * currentObject.transform.localScale.z + currentObject.transform.right * (tileSizeY / 2) * Mathf.Abs(currentObject.transform.localScale.x) + new Vector3(0, 0.5f, 0);

                initDiff = Vector3.zero;
                var newCoords = Raycast(new Ray(newPos, Vector3.down));
                placedClusters[currentObject] = newCoords;

                if (pl.rightUnButton != null)
                {
                    DestroyImmediate(pl.rightUnButton.gameObject);
                    DestroyImmediate(pl.buttonPositionRight.gameObject);
                    pl.CheckAdjacencies(1.8f, false);
                }
                else if (pl.leftUnButton != null)
                {
                    DestroyImmediate(pl.leftUnButton.gameObject);
                    DestroyImmediate(pl.buttonPositionLeft.gameObject);
                    pl.CheckAdjacencies(1.8f, false);
                }
            }
            else if (or == "z")
            {
                if (currentObject.tag == "MirroredZ")
                {
                    currentObject.tag = "Untagged";
                }
                else if (currentObject.tag == "Untagged")
                {
                    currentObject.tag = "MirroredZ";
                }
                else if (currentObject.tag == "MirroredX")
                {
                    currentObject.tag = "Mirrored";

                }
                else if (currentObject.tag == "Mirrored")
                {
                    currentObject.tag = "MirroredX";
                }
                currentObject.transform.localScale = new Vector3(currentObject.transform.localScale.x, currentObject.transform.localScale.y, -currentObject.transform.localScale.z);
                currentObject.transform.position += ((currentObject.transform.forward * (3.6f * 3)) * (-currentObject.transform.localScale.z));
                Vector3 newPos = currentObject.transform.position + currentObject.transform.forward * (tileSizeX / 2) * currentObject.transform.localScale.z + currentObject.transform.right * (tileSizeY / 2) * currentObject.transform.localScale.x + new Vector3(0, 0.5f, 0);

                initDiff = Vector3.zero;
                var newCoords = Raycast(new Ray(newPos, Vector3.down));
                placedClusters[currentObject] = newCoords;
            }
            if (!currentObject.name.Contains("Hall") && !currentObject.name.Contains("Corner"))
            {
                currentObject.GetComponent<ProceduralCluster>().UpdateCornerPoints();
                currentObject.GetComponent<ProceduralCluster>().ToggleCornerPoints(true);
            }

            currentObject.GetComponent<SelectableGeometry>().OnMirror();
            Camera.main.GetComponent<DrawGLines>().DrawConnectingLines();
        }


        #region Public Methods
        /// <summary>
        /// Creates the grid
        /// </summary>
        public void Initialize()
        {
            GenerateGrid(tileSizeX, tileSizeY, xNum, yNum);
            if (raycastingCamera == null)
                raycastingCamera = Camera.main;
            initialized = true;
            placedClusters = new Dictionary<GameObject, int[]>();
        }
        /// <summary>
        /// Creates the grid on a polygon
        /// </summary>
        /// <param name="polygon"></param>The polygon to place the grid on
        /// <param name="index"></param> The index of the grid

        public void Initialize(Polygon polygon, int index)
        {
            this.index = index;
            sitePolygon = polygon;

            GenerateGrid(tileSizeX, tileSizeY, xNum, yNum, polygon);
            if (raycastingCamera == null)
                raycastingCamera = Camera.main;
            initialized = true;
            placedClusters = new Dictionary<GameObject, int[]>();
        }
        /// <summary>
        /// Updates the grid when polygon changes
        /// </summary>
        /// <param name="polygon"></param>
        public void UpdateGrid(Polygon polygon)
        {
            sitePolygon = polygon;
            GenerateGrid(tileSizeX, tileSizeY, xNum, yNum, polygon);
        }
        /// <summary>
        /// Rotates grid based on polygon
        /// </summary>
        /// <param name="polygon"></param>The polygon affecting rotation
        /// <param name="angle"></param>the angle of rotation
        public void ReorientGrid(Polygon polygon, float angle)
        {
            sitePolygon = polygon;
            GenerateGrid(tileSizeX, tileSizeY, xNum, yNum, polygon);

        }
        /// <summary>
        /// Positions oblect on grid
        /// </summary>
        /// <param name="prefab"></param>the object to position
        /// <param name="coords"></param>the coordinates to give as point
        /// <param name="angle"></param>the rotation
        public void SetPrefabNewPosition(GameObject prefab, int[] coords, float angle)
        {
            prefab.transform.position = transform.TransformPoint(centres[coords[0]][coords[1]]);
        }
        /// <summary>
        /// Returns an array of coordinates with raycast
        /// </summary>
        /// <param name="r"></param> The ray of the raycast
        /// <returns></returns>
        public int[] Raycast(Ray r)
        {
            int[] coords = new int[2];
            float dist = 0;
            if (gridPlane.Raycast(r, out dist))
            {
                Vector3 point = r.GetPoint(dist) + initDiff;
                Vector3 localPoint;
                if (Included(point, out localPoint))
                {
                    float xCoord = localPoint.x - gridStart.x;
                    float yCoord = localPoint.z - gridStart.z;
                    coords[0] = Mathf.FloorToInt(xCoord / tileSizeX);
                    coords[1] = Mathf.FloorToInt(yCoord / tileSizeY);
                }
            }

            return coords;
        }

        public void FindTile()
        {

        }
        /// <summary>
        /// Finds the difference in position between gameobject's position and mouse raycast
        /// </summary>
        public void GetInitDiff()
        {
            if (currentObject != null && currentObject.GetComponent<PlaceablePrefab>() != null)
            {
                Ray ray = new Ray();
                currentObject.GetComponent<PlaceablePrefab>().CheckAdjacencies(1.8f, false);
                initialPosition = currentObject.transform.position;

                ray = raycastingCamera.ScreenPointToRay(Input.mousePosition);


                float dist = 0;
                if (gridPlane.Raycast(ray, out dist))
                {
                    int[] gridCoords;
                    if (placedClusters.TryGetValue(currentObject, out gridCoords))
                    {
                        initDiff = transform.TransformPoint(centres[gridCoords[0]][gridCoords[1]]) - ray.GetPoint(dist);
                    }
                    else
                    {
                        initDiff = currentObject.transform.position - ray.GetPoint(dist);
                    }
                }
            }
            else if (currentAdditional != null)
            {
                initialPosition = currentAdditional.transform.position;
                Ray ray = raycastingCamera.ScreenPointToRay(Input.mousePosition);
                float dist = 0;
                if (gridPlane.Raycast(ray, out dist))
                {
                    initDiff = currentAdditional.transform.position - ray.GetPoint(dist);
                }
            }
        }

        /// <summary>
        /// Moves object on grid based on raycast & checks rules of moving/placing
        /// </summary>
        public void Raycast()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            currentTile = Raycast(ray);
            if (currentObject != null)
            {
                currentObject.layer = 0;
                PlaceablePrefab myPrefab = currentObject.GetComponent<PlaceablePrefab>();
                if (Physics.Raycast(ray, out hit, 500000, LayerMask.GetMask("selectable")) || Physics.Raycast(myPrefab.SideRays[0], out hit, 5000, LayerMask.GetMask("selectable")) || Physics.Raycast(myPrefab.SideRays[1], out hit, 5000, LayerMask.GetMask("selectable")))
                {

                    if (!hit.collider.name.Contains("Hall") && !hit.collider.name.Contains("Storage") && !hit.collider.name.Contains("Kitchen") && !currentObject.name.Contains("Hall") && !currentObject.name.Contains("Stair") && !currentObject.name.Contains("Entrance") && !currentObject.name.Contains("Plant") && !currentObject.name.Contains("Lift") && FindClusterWithinPArent(myPrefab).Count == 0)
                    {
                        try
                        {
                            myPrefab.level = hit.collider.GetComponent<PlaceablePrefab>().level + 1;

                        }
                        catch (Exception e)
                        {
                            print(hit.collider.name);
                        }

                        myPrefab.OnChangeLevel(transform.TransformPoint(centres[currentTile[0]][currentTile[1]] - new Vector3(tileSizeX / 2.0f, 0, tileSizeY / 2.0f)), hit.collider.gameObject);

                        myPrefab.isPlaceable = true;
                        if (myPrefab.transform.rotation != hit.collider.transform.rotation)
                        {
                            notification.text = "The cluster you are placing should match the orientation of the cluster below";
                            StartCoroutine(ErrorText());
                        }
                        else
                        {
                            notification.text = "";
                        }
                    }
                    else if (hit.collider.name.Contains("Stair") && currentObject.name.Contains("Stair") || hit.collider.name.Contains("Lift") && currentObject.name.Contains("Lift"))
                    {
                        myPrefab.level = hit.collider.GetComponent<PlaceablePrefab>().level + 1;
                        myPrefab.OnChangeLevel(transform.TransformPoint(centres[currentTile[0]][currentTile[1]] - new Vector3(tileSizeX / 2.0f, 0, tileSizeY / 2.0f)), hit.collider.gameObject);

                        myPrefab.isPlaceable = true;
                        if (myPrefab.transform.rotation != hit.collider.transform.rotation)
                        {
                            notification.text = "The cluster you are placing should match the orientation of the cluster below";
                            StartCoroutine(ErrorText());
                        }
                        else
                        {
                            notification.text = "";
                        }
                    }

                    else
                    {
                        myPrefab.isPlaceable = false;
                    }
                }
                else if (!siteInclusion[currentTile[0]][currentTile[1]])
                {
                    myPrefab.isPlaceable = false;
                }
                else
                {
                    if (myPrefab.level != 0 && myPrefab.adjacent[5] == null)
                    {
                        myPrefab.level = 0;
                        myPrefab.OnChangeLevel(transform.TransformPoint(centres[currentTile[0]][currentTile[1]] - new Vector3(tileSizeX / 2.0f, 0, tileSizeY / 2.0f)));
                        myPrefab.isPlaceable = true;

                    }
                    if (currentObject.name.Contains("Stair") || currentObject.name.Contains("Lift"))
                    {
                        myPrefab.isPlaceable = true;
                        if (currentObject.transform.parent.childCount > 1)
                        {
                            for (int i = 0; i < currentObject.transform.parent.childCount; i++)
                            {
                                var s = currentObject.transform.parent.GetChild(i);
                                if (s.name.Contains(("Stair")) && s != currentObject.transform)
                                {
                                    if (Vector3.Distance(s.position, currentObject.transform.position) < 30)
                                    {
                                        myPrefab.isPlaceable = false;

                                    }
                                    else
                                    {
                                        myPrefab.isPlaceable = true;

                                    }
                                }

                            }
                        }
                        else
                        {
                            myPrefab.isPlaceable = true;

                        }
                    }
                    else
                    {
                        myPrefab.isPlaceable = true;
                    }


                    if (hasSelected)
                        transformPanel.SendMessage("UpdateUIPosition");

                }
                Vector3 projectPosition = transform.TransformPoint(centres[currentTile[0]][currentTile[1]] - new Vector3(tileSizeX / 2.0f, 0, tileSizeY / 2.0f));
                currentObject.transform.position = new Vector3(projectPosition.x, projectPosition.y + (myPrefab.level * myPrefab.size.y), projectPosition.z);
                Snap(currentObject);
            }
            else if (currentAdditional != null)
            {
                if (siteInclusion[currentTile[0]][currentTile[1]])
                {
                    if (currentAdditional.name.Contains("Tile"))
                        snapOnObjects = false;
                    else
                        snapOnObjects = true;
                    if (Physics.Raycast(ray, out hit, 5000000, myMask) && hit.collider.gameObject.layer == 31)
                    {
                        isAdditionalPlaceable = true;
                    }
                    else
                    {
                        isAdditionalPlaceable = false;
                    }

                    currentAdditional.transform.position = transform.TransformPoint(centres[currentTile[0]][currentTile[1]]);
                }
                if (hasSelected)
                    transformPanel.SendMessage("UpdateUIPosition");

            }
        }
        /// <summary>
        /// Finds all placeable prefabs within a parent
        /// </summary>
        /// <param name="parent"></param> The parent to check
        /// <returns></returns>
        List<Transform> FindClusterWithinPArent(PlaceablePrefab parent)
        {
            List<Transform> temp = new List<Transform>();
            foreach (Transform c in parent.transform)
            {
                if (c.GetComponent<PlaceablePrefab>() != null)
                {
                    if (c.name.Contains("Plant") || c.name.Contains("Hall") || c.name.Contains("Lift") || c.name.Contains("Entrance"))
                    {
                        temp.Add(c);

                    }
                }

            }
            return temp;

        }
        /// <summary>
        /// Snaps objects between them
        /// </summary>
        /// <param name="current"></param> The object that is to snap 
        /// <param name="other"></param> The other object to check
        void Snap(GameObject current, Transform other = null)
        {
            var tempCorners = new List<Vector3>();
            if (current.transform.root.childCount > 1)
            {
                if (other != null)
                {
                    if (other.GetComponent<ProceduralCluster>().facilitiesIndex != -1 && currentObject.GetComponent<ProceduralCluster>().facilitiesIndex != -1)
                    {
                        other.GetComponent<ProceduralCluster>().builtParts[other.GetComponent<ProceduralCluster>().facilitiesIndex].GetComponent<BoxCollider>().enabled = true;

                        BoxCollider b = other.GetComponent<ProceduralCluster>().builtParts[other.GetComponent<ProceduralCluster>().facilitiesIndex].GetComponent<BoxCollider>();
                        tempCorners.Add(other.TransformPoint(b.center + new Vector3(b.size.x, b.size.y, b.size.z) * 0.5f));
                        tempCorners.Add(other.TransformPoint(b.center + new Vector3(-b.size.x, b.size.y, b.size.z) * 0.5f));
                        tempCorners.Add(other.TransformPoint(b.center + new Vector3(-b.size.x, b.size.y, -b.size.z) * 0.5f));
                        tempCorners.Add(other.TransformPoint(b.center + new Vector3(b.size.x, b.size.y, -b.size.z) * 0.5f));
                        Debug.DrawLine(other.TransformPoint(b.center + new Vector3(b.size.x, b.size.y, b.size.z) * 0.5f), other.TransformPoint(b.center + new Vector3(-b.size.x, b.size.y, b.size.z) * 0.5f), Color.red, 60);
                        Debug.DrawLine(other.TransformPoint(b.center + new Vector3(-b.size.x, b.size.y, -b.size.z) * 0.5f), other.TransformPoint(b.center + new Vector3(b.size.x, b.size.y, -b.size.z) * 0.5f), Color.green, 60);
                    }
                    else
                    {

                        BoxCollider b = other.GetComponent<BoxCollider>();
                        tempCorners.Add(other.TransformPoint(b.center + new Vector3(b.size.x, b.size.y, b.size.z) * 0.5f));
                        tempCorners.Add(other.TransformPoint(b.center + new Vector3(-b.size.x, b.size.y, b.size.z) * 0.5f));
                        tempCorners.Add(other.TransformPoint(b.center + new Vector3(-b.size.x, b.size.y, -b.size.z) * 0.5f));
                        tempCorners.Add(other.TransformPoint(b.center + new Vector3(b.size.x, b.size.y, -b.size.z) * 0.5f));
                        Debug.DrawLine(other.TransformPoint(b.center + new Vector3(b.size.x, b.size.y, b.size.z) * 0.5f), other.TransformPoint(b.center + new Vector3(-b.size.x, b.size.y, b.size.z) * 0.5f), Color.blue, 60);
                        Debug.DrawLine(other.TransformPoint(b.center + new Vector3(-b.size.x, b.size.y, -b.size.z) * 0.5f), other.TransformPoint(b.center + new Vector3(b.size.x, b.size.y, -b.size.z) * 0.5f), Color.cyan, 60);
                    }
                }
                else
                {
                    foreach (Transform child in current.transform.parent)
                    {
                        if (child.GetComponent<PlaceablePrefab>() != null && child.gameObject != current && child.GetComponent<PlaceablePrefab>().level == current.GetComponent<PlaceablePrefab>().level)
                        {
                            if (!child.name.Contains("Stair"))
                            {
                                BoxCollider b = child.GetComponent<BoxCollider>();
                                tempCorners.Add(child.TransformPoint(b.center + new Vector3(b.size.x, -b.size.y, b.size.z) * 0.5f));
                                tempCorners.Add(child.TransformPoint(b.center + new Vector3(-b.size.x, -b.size.y, b.size.z) * 0.5f));
                                tempCorners.Add(child.TransformPoint(b.center + new Vector3(-b.size.x, -b.size.y, -b.size.z) * 0.5f));
                                tempCorners.Add(child.TransformPoint(b.center + new Vector3(b.size.x, -b.size.y, -b.size.z) * 0.5f));
                            }
                            else
                            {
                                BoxCollider b = child.GetComponent<BoxCollider>();

                                tempCorners.Add(child.TransformPoint(b.center + new Vector3(b.size.x * 0.5f, -b.size.y * 0.5f, b.size.z * 0.5f)));
                                tempCorners.Add(child.TransformPoint(b.center + new Vector3(-b.size.x * 0.5f, -b.size.y * 0.5f, b.size.z * 0.5f)));
                                tempCorners.Add(child.TransformPoint(b.center + new Vector3(-b.size.x * 0.5f, -b.size.y * 0.5f, -b.size.z * 0.5f + 2.0905f)));
                                tempCorners.Add(child.TransformPoint(b.center + new Vector3(b.size.x * 0.5f, -b.size.y * 0.5f, -b.size.z * 0.5f + 2.0905f)));
                                //GameObject v = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                                //v.transform.position = child.TransformPoint(b.center + new Vector3(b.size.x * 0.5f, -b.size.y * 0.5f, b.size.z * 0.5f));
                                //GameObject v2 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                                //v2.transform.position = child.TransformPoint(b.center + new Vector3(-b.size.x * 0.5f, -b.size.y * 0.5f, b.size.z * 0.5f));

                                //GameObject v3 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                                //v3.transform.position = child.TransformPoint(b.center + new Vector3(-b.size.x * 0.5f, -b.size.y * 0.5f, -b.size.z * 0.5f + 2.39f));
                                //GameObject v4 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                                //v4.transform.position = child.TransformPoint(b.center + new Vector3(b.size.x * 0.5f, -b.size.y * 0.5f, -b.size.z * 0.5f + 2.39f));
                            }

                        }
                    }
                }
                tempCorners.TrimExcess();
                allCorners = new Vector3[tempCorners.Count];
                for (int i = 0; i < tempCorners.Count; i++)
                {
                    allCorners[i] = tempCorners[i];
                }

                var currenttempCorners = new List<Vector3>();
                if (!current.name.Contains("Stair"))
                {
                    BoxCollider my_b = current.GetComponent<BoxCollider>();
                    currenttempCorners.Add(current.transform.TransformPoint(my_b.center + new Vector3(my_b.size.x, -my_b.size.y, my_b.size.z) * 0.5f));
                    currenttempCorners.Add(current.transform.TransformPoint(my_b.center + new Vector3(-my_b.size.x, -my_b.size.y, my_b.size.z) * 0.5f));
                    currenttempCorners.Add(current.transform.TransformPoint(my_b.center + new Vector3(-my_b.size.x, -my_b.size.y, -my_b.size.z) * 0.5f));
                    currenttempCorners.Add(current.transform.TransformPoint(my_b.center + new Vector3(my_b.size.x, -my_b.size.y, -my_b.size.z) * 0.5f));
                }
                else
                {
                    BoxCollider my_b = current.GetComponent<BoxCollider>();
                    currenttempCorners.Add(current.transform.TransformPoint(my_b.center + new Vector3(my_b.size.x * 0.5f, -my_b.size.y * 0.5f, my_b.size.z * 0.5f)));
                    currenttempCorners.Add(current.transform.TransformPoint(my_b.center + new Vector3(-my_b.size.x * 0.5f, -my_b.size.y * 0.5f, my_b.size.z * 0.5f)));
                    currenttempCorners.Add(current.transform.TransformPoint(my_b.center + new Vector3(-my_b.size.x * 0.5f, -my_b.size.y * 0.5f, -my_b.size.z * 0.5f + 2.0905f)));
                    currenttempCorners.Add(current.transform.TransformPoint(my_b.center + new Vector3(my_b.size.x * 0.5f, -my_b.size.y * 0.5f, -my_b.size.z * 0.5f + 2.0905f)));
                }


                currenttempCorners.TrimExcess();
                var closest = BrydenWoodUtils.KDTreeVertices(currenttempCorners, tempCorners);
                if (Vector3.Distance(closest[0], closest[1]) < tileSizeX)
                {
                    current.transform.position = current.transform.position - closest[1] + closest[0];
                }
            }
        }
        /// <summary>
        /// Called when a grid id initialized
        /// </summary>
        public void OnFinishedInitializing()
        {

            if (placedItemParent == null)
            {
                placedItemParent = transform;
            }

            placedClusters = new Dictionary<GameObject, int[]>();
            initialized = true;
            if (gridCalculated != null)
            {
                gridCalculated(this);
            }
        }
        /// <summary>
        /// Sets the cluster to be placed and starts placing process
        /// </summary>
        public void StartPlacing()
        {
            placing = true;
            Selector.TaggedObject.enabled = false;
            SetCurrentCluster(clusterIndex);
        }
        /// <summary>
        /// Creates cluster to be placed and starts placing process
        /// </summary>
        /// <param name="row"></param> The data from which the cluster is created
        /// <param name="material"></param> the material of the cluster
        /// <returns></returns>
        public ProceduralCluster StartPlacing(List<DataRow> row, Material material)
        {
            placing = true;
            Selector.TaggedObject.enabled = false;
            if (row.Count > 1)
            {
                if (currentObject != null)
                {
                    Destroy(currentObject);
                }
                //---------------------------CREATE HALL------------------
                currentObject = Instantiate(clustersToPlace[clusterIndex], placedItemParent);
                currentObject.name = row[0]["Entry"].ToString() + " " + row[0]["name"].ToString();
                currentObject.GetComponent<ProceduralCluster>().Initialize(row[0], material);
                currentObject.name = currentObject.name + "_" + placedItemParent.childCount.ToString();
                currentObject.transform.eulerAngles = transform.eulerAngles;
                currentObject.transform.localPosition = new Vector3(800, 800, 800);
                currentObject.transform.localScale = Vector3.one;
                currentEuler = Vector3.zero;
                currentObject.GetComponent<PlaceablePrefab>().gridIndex = index;
                currentObject.GetComponent<PlaceablePrefab>().level = 0;
                currentObject.GetComponent<PlaceablePrefab>().grid = this;

                //--------------------------CREATE KITCHEN--------------------
                var kitchen = Instantiate(clustersToPlace[clusterIndex], currentObject.transform);
                kitchen.name = row[1]["Entry"].ToString() + " " + row[1]["name"].ToString();
                kitchen.GetComponent<ProceduralCluster>().Initialize(row[1], material);
                kitchen.name = kitchen.name + "_" + currentObject.transform.childCount.ToString();
                kitchen.transform.localPosition = new Vector3(currentObject.GetComponent<BoxCollider>().size.x, 0, 0);
                kitchen.transform.localScale = Vector3.one;
                kitchen.transform.localEulerAngles = Vector3.zero;
                kitchen.GetComponent<PlaceablePrefab>().gridIndex = index;
                kitchen.GetComponent<PlaceablePrefab>().level = 0;
                kitchen.GetComponent<PlaceablePrefab>().grid = this;
                kitchen.GetComponent<PlaceablePrefab>().originalParent = currentObject.transform;
                kitchen.GetComponent<PlaceablePrefab>().enabled = false;




                if (row.Count > 2)
                {

                    //---------------------CREATE SMALL HALL(3FE)--------------------
                    var smallHall = Instantiate(clustersToPlace[clusterIndex], currentObject.transform);
                    smallHall.name = row[2]["Entry"].ToString() + " " + row[2]["name"].ToString();
                    smallHall.GetComponent<ProceduralCluster>().Initialize(row[2], material);
                    smallHall.name = smallHall.name + "_" + currentObject.transform.childCount.ToString();
                    smallHall.transform.localPosition = new Vector3(0, 0, currentObject.GetComponent<BoxCollider>().size.z /*-smallHall.GetComponent<BoxCollider>().size.z*/); //Vector3.zero;
                    smallHall.transform.localScale = Vector3.one;
                    smallHall.transform.localEulerAngles = Vector3.zero;
                    smallHall.GetComponent<PlaceablePrefab>().gridIndex = index;
                    smallHall.GetComponent<PlaceablePrefab>().level = 0;
                    smallHall.GetComponent<PlaceablePrefab>().grid = this;
                    smallHall.GetComponent<PlaceablePrefab>().originalParent = currentObject.transform;
                    smallHall.GetComponent<PlaceablePrefab>().enabled = false;
                    //--------------------------CREATE STORAGE(3FE)--------------------
                    var storage = Instantiate(clustersToPlace[clusterIndex], currentObject.transform);
                    storage.name = row[3]["Entry"].ToString() + " " + row[3]["name"].ToString();
                    storage.GetComponent<ProceduralCluster>().Initialize(row[3], material);
                    storage.name = storage.name + "_" + currentObject.transform.childCount.ToString();
                    storage.transform.localPosition = new Vector3(smallHall.GetComponent<BoxCollider>().size.x, 0, currentObject.GetComponent<BoxCollider>().size.z); //Vector3.zero;
                    storage.transform.localScale = Vector3.one;
                    storage.transform.localEulerAngles = Vector3.zero;
                    storage.GetComponent<PlaceablePrefab>().gridIndex = index;
                    storage.GetComponent<PlaceablePrefab>().level = 0;
                    storage.GetComponent<PlaceablePrefab>().grid = this;
                    storage.GetComponent<PlaceablePrefab>().originalParent = currentObject.transform;
                    storage.GetComponent<PlaceablePrefab>().enabled = false;

                    smallHall.transform.localPosition = new Vector3(0, 0, currentObject.GetComponent<BoxCollider>().size.z);
                    storage.transform.localPosition = new Vector3(smallHall.GetComponent<BoxCollider>().size.x, 0, currentObject.GetComponent<BoxCollider>().size.z); //Vector3.zero;
                }
                return currentObject.GetComponent<ProceduralCluster>();

            }
            else
            {
                return SetCurrentCluster(clusterIndex, row[0], material);
            }

        }
        /// <summary>
        /// Loads cluster to be placed and starts placing process
        /// </summary>
        /// <param name="info"></param>
        public void StartPlacing(Cluster info)
        {
            placing = true;
            Selector.TaggedObject.enabled = false;
            SetCurrentCluster(clusterIndex, info);
        }
        /// <summary>
        /// Sets additional cluster to be placed and starts placing process 
        /// </summary>
        public void StartPlacingAdditional()
        {
            placingAdditional = true;
            touchPlaceAdditional = false;
            SetCurrentAdditional();
        }
        /// <summary>
        /// Ends placing process
        /// </summary>
        public void StopPlacing()
        {
            placing = false;
            Selector.TaggedObject.enabled = true;
            currentObject.GetComponent<PlaceablePrefab>().CheckAdjacencies(objectSnapDistance, true);

            initDiff = Vector3.zero;
            currentObject.layer = 13;
        }
        /// <summary>
        /// Quits placing process
        /// </summary>
        public void AbortPlacing()
        {
            if (currentAdditional != null)
            {
                placingAdditional = false;
                Destroy(currentAdditional);
                currentAdditional = null;

            }
            else if (currentObject != null)
            {
                placing = false;
                Destroy(currentObject);
                ProceduralClusterManager.TaggedObject.RemoveCluster(currentObject.GetComponent<ProceduralCluster>());
                currentObject = null;

            }

            Selector.TaggedObject.enabled = true;

        }
        /// <summary>
        /// Create cluster
        /// </summary>
        /// <param name="i"></param>
        public void SetCurrentCluster(int i)
        {
            if (currentObject != null)
            {
                Destroy(currentObject);
            }
            currentObject = Instantiate(clustersToPlace[i], placedItemParent);
            currentObject.GetComponent<PlaceablePrefab>().Initialize();
            currentObject.name = currentObject.name + "_" + placedItemParent.childCount.ToString();
            currentObject.transform.eulerAngles = transform.eulerAngles;
            currentObject.transform.localPosition = new Vector3(1800, 1800, 1800);
            currentObject.transform.localScale = Vector3.one;
            currentEuler = Vector3.zero;
            currentObject.GetComponent<PlaceablePrefab>().gridIndex = index;
            currentObject.GetComponent<PlaceablePrefab>().level = 0;
            currentObject.GetComponent<PlaceablePrefab>().grid = this;
        }
        /// <summary>
        /// Creates cluster based on data
        /// </summary>
        /// <param name="i"></param> the index of the cluster
        /// <param name="row"></param>the data
        /// <param name="mat"></param>the material to use for the cluster
        /// <returns></returns>
        public ProceduralCluster SetCurrentCluster(int i, DataRow row, Material mat)
        {
            if (currentObject != null)
            {
                Destroy(currentObject);
            }
            currentObject = Instantiate(clustersToPlace[i], placedItemParent);
            currentObject.name = row["Entry"].ToString() + " " + row["name"].ToString();
            currentObject.GetComponent<ProceduralCluster>().Initialize(row, mat);
            currentObject.name = currentObject.name + "_" + placedItemParent.childCount.ToString();
            currentObject.transform.eulerAngles = transform.eulerAngles;
            currentObject.transform.localPosition = new Vector3(1800, 1800, 1800);
            currentObject.transform.localScale = Vector3.one;
            currentEuler = Vector3.zero;
            currentObject.GetComponent<PlaceablePrefab>().gridIndex = index;
            currentObject.GetComponent<PlaceablePrefab>().level = 0;
            currentObject.GetComponent<PlaceablePrefab>().grid = this;
            return currentObject.GetComponent<ProceduralCluster>();
        }
        /// <summary>
        /// Loads cluster
        /// </summary>
        /// <param name="i"></param> the index of the cluster
        /// <param name="info"></param> the loaded info
        public void SetCurrentCluster(int i, Cluster info)
        {
            if (currentObject != null)
            {
                Destroy(currentObject);
            }

            currentObject = Instantiate(clustersToPlace[i], placedItemParent);
            Material mat = new Material(currentObject.GetComponent<ProceduralCluster>().standardBuitMaterial);
            mat.color = transform.parent.GetComponent<ProceduralClusterManager>().clusterMaterials[info.name].color;
            currentObject.GetComponent<ProceduralCluster>().standardBuitMaterial = mat;
            currentObject.name = info.name;
            currentObject.GetComponent<ProceduralCluster>().Initialize(info);
            currentObject.name = currentObject.name + "_" + placedItemParent.childCount.ToString();
            currentObject.transform.eulerAngles = transform.eulerAngles;
            currentObject.transform.localPosition = new Vector3(1800, 1800, 1800);
            currentObject.transform.localScale = Vector3.one;
            currentEuler = Vector3.zero;
            currentObject.GetComponent<PlaceablePrefab>().gridIndex = index;
            currentObject.GetComponent<PlaceablePrefab>().level = 0;
            currentObject.GetComponent<PlaceablePrefab>().grid = this;
            transform.parent.GetComponent<ProceduralClusterManager>().allClusters.Add(currentObject.GetComponent<ProceduralCluster>());
            transform.parent.GetComponent<ProceduralClusterManager>().UpdateListOfRooms();
            if (info.clustersWithin != null && info.clustersWithin.Count > 0)
            {
                for (int j = 0; j < info.clustersWithin.Count; j++)
                {
                    var tempInfo = info.clustersWithin[j];
                    var extraObject = Instantiate(clustersToPlace[i], currentObject.transform);
                    mat = new Material(extraObject.GetComponent<ProceduralCluster>().standardBuitMaterial);
                    mat.color = transform.parent.GetComponent<ProceduralClusterManager>().clusterMaterials[tempInfo.name].color;
                    extraObject.GetComponent<ProceduralCluster>().standardBuitMaterial = mat;
                    extraObject.name = tempInfo.name;
                    extraObject.GetComponent<ProceduralCluster>().Initialize(tempInfo);

                    extraObject.name = extraObject.name + "_" + currentObject.transform.childCount.ToString();
                    if (tempInfo.localposition != null)
                        extraObject.transform.localEulerAngles = new Vector3(tempInfo.localrotation[0], tempInfo.localrotation[1], tempInfo.localrotation[2]);

                    if (tempInfo.localrotation != null)
                        extraObject.transform.localPosition = new Vector3(tempInfo.localposition[0], tempInfo.localposition[1], tempInfo.localposition[2]);
                    extraObject.transform.localScale = Vector3.one;
                    extraObject.GetComponent<PlaceablePrefab>().gridIndex = index;
                    extraObject.GetComponent<PlaceablePrefab>().level = 0;
                    extraObject.GetComponent<PlaceablePrefab>().grid = this;
                }

            }

        }
        /// <summary>
        /// Creates Additional cluster
        /// </summary>
        public void SetCurrentAdditional()
        {
            if (currentAdditional != null)
            {
                Destroy(currentAdditional);
            }
            currentAdditional = Instantiate(additionalElementPrefab, additionalParent);
            currentAdditional.layer = 2;
            currentAdditional.transform.eulerAngles = new Vector3(currentAdditional.transform.eulerAngles.x, transform.localEulerAngles.y, currentAdditional.transform.eulerAngles.z); ;
            currentAdditional.transform.localPosition = new Vector3(1800, 1800, 1800);
            currentAdditional.transform.position = new Vector3(currentAdditional.transform.position.x, currentAdditional.transform.position.y + 0.1f, currentAdditional.transform.position.z);

        }
        /// <summary>
        /// Creates cluster based on prefab
        /// </summary>
        /// <param name="prefab"></param> the prefab
        public void SetCurrentCluster(GameObject prefab)
        {
            if (currentObject != null)
            {
                Destroy(currentObject);
            }
            currentObject = Instantiate(prefab, placedItemParent);
            currentObject.name = placedItemParent.childCount.ToString();
            currentObject.transform.eulerAngles = transform.eulerAngles;
            currentObject.transform.localPosition = Vector3.zero;
            currentObject.transform.localScale = Vector3.one;
            currentEuler = Vector3.zero;
            currentObject.GetComponent<PlaceablePrefab>().gridIndex = index;
            currentObject.GetComponent<PlaceablePrefab>().grid = this;
        }
        /// <summary>
        /// Rotates a cluster around itself
        /// </summary>
        /// <param name="angle"></param> The rotation angle
        public void RotateCluster(float angle)
        {

            if (currentObject != null)
            {
                currentEuler = currentObject.transform.localEulerAngles;
                currentObject.transform.localEulerAngles = new Vector3(currentEuler.x, currentEuler.y + angle, currentEuler.z);
                clusterRotation = currentObject.transform.localEulerAngles;

            }
            else if (currentAdditional != null)
            {
                currentEuler = currentAdditional.transform.localEulerAngles;
                currentAdditional.transform.localEulerAngles = new Vector3(currentEuler.x, currentEuler.y + angle, currentEuler.z);
                clusterRotation = currentAdditional.transform.localEulerAngles;

            }

        }
        /// <summary>
        /// Recalculates if this grid is within the site polygon
        /// </summary>
        public void RecalculateInclusion()
        {
            for (int i = 0; i < centres.Length; i++)
            {
                for (int j = 0; j < centres[i].Length; j++)
                {
                    siteInclusion[i][j] = sitePolygon.IsIncluded(transform.TransformPoint(centres[i][j]));
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Vector3[] GetPanelPoints()
        {
            List<Vector3> corners = new List<Vector3>();
            Vector3 panelExtends = new Vector3(tileSizeX, 0, tileSizeY);
            for (int i = 0; i < centres.Length; i++)
            {
                for (int j = 0; j < centres[i].Length; j++)
                {
                    if (siteInclusion[i][j])
                    {
                        corners.Add(transform.TransformPoint(centres[i][j] + new Vector3(-tileSizeX / 2.0f, 0, -tileSizeY / 2.0f)));
                        corners.Add(transform.TransformPoint(centres[i][j] + new Vector3(-tileSizeX / 2.0f, 0, tileSizeY / 2.0f)));
                        corners.Add(transform.TransformPoint(centres[i][j] + new Vector3(tileSizeX / 2.0f, 0, tileSizeY / 2.0f)));
                        corners.Add(transform.TransformPoint(centres[i][j] + new Vector3(tileSizeX / 2.0f, 0, -tileSizeY / 2.0f)));
                    }
                }
            }
            corners.TrimExcess();
            return corners.ToArray();
        }
        /// <summary>
        /// Changes the height of the grid
        /// </summary>
        /// <param name="f2f"></param> floor to floor height, the actual height to move
        /// <param name="level"></param> the amount of times to move the grid on the given height
        public void SetGridHeight(float f2f, int level)
        {
            this.level = level;
            float height = f2f * level;
            transform.position = new Vector3(transform.position.x, height, transform.position.z);
            gridPlane = new Plane(Vector3.up, transform.position);
            if (placedClusters.Count > 0)
            {
                foreach (var item in placedClusters)
                {
                    if (item.Key.GetComponent<PlaceablePrefab>().level > level)
                    {
                        item.Key.SetActive(false);
                    }
                    else
                    {
                        if (!item.Key.activeSelf)
                            item.Key.SetActive(true);
                    }
                }
            }

        }
        /// <summary>
        /// Updates the position of a given cluster
        /// </summary>
        /// <param name="cluster"></param> the cluster to move
        public void UpdateCentre(GameObject cluster)
        {
            var coords = FindCenter(cluster.transform.position, true);
            placedClusters[cluster] = new int[] { coords[0], coords[1] };
        }
        /// <summary>
        /// Removes a deleted cluster from the recorded list of placed
        /// </summary>
        /// <param name="prefab"></param> the cluster to remove
        public void RemovePrefab(GameObject prefab)
        {
            var keys = placedClusters.Keys.ToArray();
            for (int i = 0; i < keys.Length; i++)
            {
                if (keys[i] == prefab)
                {
                    placedClusters.Remove(keys[i]);
                }
            }
        }
        /// <summary>
        /// Adds a cluster to a position on grid
        /// </summary>
        /// <param name="prefab"></param> the cluster
        public void AddPrefab(GameObject prefab)
        {
            if (!placedClusters.ContainsKey(prefab))
            {
                Vector3 pos = prefab.transform.position;
                prefab.transform.eulerAngles = transform.eulerAngles;
                var coords = FindCenter(pos);
                prefab.transform.position = transform.TransformPoint(centres[coords[0]][coords[1]] + new Vector3(-tileSizeX / 2.0f, 0, -tileSizeY / 2.0f));
                placedClusters.Add(prefab, coords);
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Checks if a point is included in a grid tile
        /// </summary>
        /// <param name="point"></param> the point to check
        /// <param name="localPoint"></param> the returns included local point
        /// <param name="corner"></param> if the point is a corner point
        /// <returns></returns>
        private bool Included(Vector3 point, out Vector3 localPoint, bool corner = false)
        {
            if (!corner)
            {
                localPoint = transform.InverseTransformPoint(point);
            }
            else
            {
                localPoint = transform.InverseTransformPoint(point) + new Vector3(tileSizeX / 2.0f, 0, tileSizeY / 2.0f);
            }
            return (localPoint.x < minMax[1].x &&
                localPoint.x > minMax[0].x &&
                localPoint.z < minMax[1].z &&
                localPoint.z > minMax[0].z);
        }
        /// <summary>
        /// Creates a grid
        /// </summary>
        /// <param name="sizeX"></param> grid tile size on x axis
        /// <param name="sizeY"></param>grid tile size on y axis
        /// <param name="xNum"></param>number of tiles on x axis
        /// <param name="yNum"></param>number of tiles on y axis

        private void GenerateGrid(float sizeX, float sizeY, int xNum, int yNum)
        {
            gridPlane = new Plane(Vector3.up, transform.position);
            centres = new Vector3[xNum][];
            float startX = -(sizeX * xNum) / 2;
            float startY = -(sizeY * yNum) / 2;
            gridStart = new Vector3(startX, transform.position.y, startY);
            for (int i = 0; i < xNum; i++)
            {
                centres[i] = new Vector3[yNum];
                for (int j = 0; j < yNum; j++)
                {
                    centres[i][j] = (gridStart + new Vector3(sizeX * i, 0, sizeY * j) + new Vector3(sizeX / 2.0f, 0, sizeY / 2.0f));
                }
            }
            minMax = new Vector3[2];
            minMax[0] = new Vector3(startX, transform.position.y, startY);
            minMax[1] = new Vector3((sizeX * xNum) / 2, transform.position.y, (sizeY * yNum) / 2);
        }
        /// <summary>
        /// Creates a grid based on a polygon
        /// </summary>
        /// <param name="sizeX"></param>grid tile size on x axis
        /// <param name="sizeY"></param>grid tile size on y axis
        /// <param name="xNum"></param>number of tiles on x axis
        /// <param name="yNum"></param>number of tiles on y axis
        /// <param name="site"></param> the polygon

        private void GenerateGrid(float sizeX, float sizeY, int xNum, int yNum, Polygon site)
        {
            gridPlane = new Plane(Vector3.up, transform.position);
            centres = new Vector3[xNum][];
            siteInclusion = new bool[xNum][];
            float startX = -(sizeX * xNum) / 2;
            float startY = -(sizeY * yNum) / 2;
            gridStart = new Vector3(startX, 0.0f, startY);
            for (int i = 0; i < xNum; i++)
            {
                centres[i] = new Vector3[yNum];
                siteInclusion[i] = new bool[yNum];
                for (int j = 0; j < yNum; j++)
                {
                    centres[i][j] = (gridStart + new Vector3(sizeX * i, 0, sizeY * j) + new Vector3(sizeX / 2.0f, 0, sizeY / 2.0f));
                    siteInclusion[i][j] = sitePolygon.IsIncluded(transform.TransformPoint(centres[i][j]));

                }
            }
            minMax = new Vector3[2];
            minMax[0] = new Vector3(startX, 0.0f, startY);
            minMax[1] = new Vector3((sizeX * xNum) / 2, 0.0f, (sizeY * yNum) / 2);
        }
        /// <summary>
        /// Places selected cluster on the grid
        /// </summary>
        /// <param name="position"></param> the position to place on
        public void PlaceCluster(Vector3 position)
        {
            currentTile = FindCenter(position + new Vector3(tileSizeX / 2.0f, 0, tileSizeY / 2.0f));
            if (!placedClusters.ContainsKey(currentObject))
            {
                placedClusters.Add(currentObject, currentTile);
            }
            else
            {
                placedClusters[currentObject] = currentTile;
            }
            StopPlacing();
            currentObject.GetComponent<PlaceablePrefab>().placed = true;
            currentObject.layer = 13;
            currentObject = null;
            if (clusterAdded != null)
            {
                clusterAdded();
            }

        }
        /// <summary>
        /// places an additional cluster on the grid
        /// </summary>
        /// <param name="position"></param>the position to place on
        /// <param name="cluster"></param>the cluster to place
        public void PlaceAdditional(Vector3 position, GameObject cluster)
        {
            currentTile = FindCenter(position + new Vector3(tileSizeX / 2.0f, 0, tileSizeY / 2.0f));
            cluster.transform.position = transform.TransformPoint(centres[currentTile[0]][currentTile[1]]);
        }
        /// <summary>
        /// Places a cluster on the grid
        /// </summary>
        /// <param name="position"></param>the position to place on
        /// <param name="cluster"></param>the cluster to place

        public void PlaceCluster(Vector3 position, GameObject cluster)
        {
            currentTile = FindCenter(position + new Vector3(tileSizeX / 2.0f, 0, tileSizeY / 2.0f));
            currentObject = cluster;
            currentObject.transform.position = transform.TransformPoint(centres[currentTile[0]][currentTile[1]] - new Vector3(tileSizeX / 2.0f, 0, tileSizeY / 2.0f));
            if (!placedClusters.ContainsKey(currentObject))
            {
                placedClusters.Add(currentObject, currentTile);
            }
            else
            {
                placedClusters[currentObject] = currentTile;
            }
            StopPlacing();
            currentObject.GetComponent<PlaceablePrefab>().placed = true;
            currentObject.layer = 13;
            currentObject = null;
            if (clusterAdded != null)
            {
                clusterAdded();
            }

        }
        /// <summary>
        /// Places a cluster in a coroutine
        /// </summary>
        /// <param name="newCluster"></param> the cluster to place
        /// <returns></returns>
        private IEnumerator PlaceCluster(bool newCluster = true)
        {
            if (currentObject != null)
            {
                if (!placedClusters.ContainsKey(currentObject))
                {
                    placedClusters.Add(currentObject, currentTile);
                }
                else
                {
                    placedClusters[currentObject] = currentTile;
                }

                StopPlacing();
                yield return new WaitForEndOfFrame();
                currentObject.GetComponent<PlaceablePrefab>().placed = true;

                currentObject.GetComponent<PlaceablePrefab>().ClearAdjacencies();
                currentObject.GetComponent<PlaceablePrefab>().CheckAdjacencies(1.8f, false);
                currentObject.layer = 13;
                if (newCluster)
                {
                    currentObject = null;
                    if (clusterAdded != null)
                    {
                        clusterAdded();
                    }
                }
            }
            initDiff = Vector3.zero;

        }
        /// <summary>
        /// places additional in a coroutine
        /// </summary>
        /// <returns></returns>
        public IEnumerator PlaceAdditional()
        {
            placingAdditional = false;
            yield return new WaitForEndOfFrame();
            currentAdditional = null;
        }
        /// <summary>
        /// Finds position on a tile
        /// </summary>
        /// <param name="pos"></param> the  position to alter
        /// <param name="corner"></param>if its a cornet point
        /// <returns></returns>
        public int[] FindCenter(Vector3 pos, bool corner = false)
        {
            int[] coords = new int[2];

            pos = new Vector3(pos.x, transform.position.y, pos.z);

            Vector3 localPoint;
            if (Included(pos, out localPoint, corner))
            {
                float xCoord = localPoint.x - gridStart.x;
                float yCoord = localPoint.z - gridStart.z;
                coords[0] = Mathf.FloorToInt(xCoord / tileSizeX);
                coords[1] = Mathf.FloorToInt(yCoord / tileSizeY);
            }

            return coords;
        }

        #endregion
    }
}
