﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using BrydenWood.Interop;
using System;
using System.Linq;
using UnityEngine.UI;
using BrydenWoodUnity;
using TMPro;
using System.IO;
using BrydenWoodUnity.Grid;
using BrydenWoodUnity.GeometryManipulation;

public class ProceduralClusterManager : Tagged<ProceduralClusterManager>
{

    
   
    public DataTable clusterTable { get; set; }
    public GameObject clusterPrefab;
    public List<ProceduralCluster> clusters;
    public MeshlessGridManager gridManager;
    public string entry = "Free";
    public TextMeshProUGUI clusterInfo;
    public Dictionary<string, Material> clusterMaterials;

    private List<DataRow> currentRows;
    string clusterList;
    string roomList;

    public Dictionary<string, int> requiredClusters;
    public Dictionary<string, int> placedClusters;

    public Text error;
    public TextManager txtMan;
    public Transform configurationParent;
    public ClusterManager externalManager;
    bool hasFloors;
    string verticalCommunication = "";
    public string activeCluster { get; set; }

    Dictionary<string, List<string>> roomAreas;
    List<string> roomTypes;
    List<float> totalAreas;
    public List<ProceduralCluster> allClusters;
    // Use this for initialization
    void Awake()
    {

        totalAreas = new List<float>();
        roomTypes = new List<string>();
        roomAreas = new Dictionary<string, List<string>>();
        allClusters = new List<ProceduralCluster>();


        PlaceablePrefab.resolvedRoom += OnResolveUpdateRoom;
        StartCoroutine(ReadDataPath());


    }
    /// <summary>
    /// Reads necessary
    /// </summary>
    /// <returns></returns>
    IEnumerator ReadDataPath()
    {
        string filePath_clusters = Application.streamingAssetsPath + "/ProceduralClusters.csv";
        if (filePath_clusters.Contains("://") || filePath_clusters.Contains(":///"))
        {
            WWW www = new WWW(filePath_clusters);
            yield return www;
            clusterList = www.text;
          
        }
        else
        {
            clusterList = File.ReadAllText(filePath_clusters);
        }

        string filepath_rooms = Application.streamingAssetsPath + "/RoomAreas.csv";
        if (filepath_rooms.Contains("://") || filepath_rooms.Contains(":///"))
        {
            WWW www = new WWW(filepath_rooms);
            yield return www;
            roomList = www.text;
        }
        else
        {
            roomList = File.ReadAllText(filepath_rooms);
        }

        clusterTable = CsvIO.CsvContentToTable(clusterList, "List of Clusters");
        clusterMaterials = new Dictionary<string, Material>();
        requiredClusters = new Dictionary<string, int>();
        placedClusters = new Dictionary<string, int>();


        for (int i = 0; i < clusterTable.Rows.Count; i++)
        {
            var row = clusterTable.Rows[i];
            Material mat = new Material(clusterPrefab.GetComponent<ProceduralCluster>().standardBuitMaterial);
            var vals = row["color"].ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Select(x => (float.Parse(x) / 255.0f)).ToList();
            mat.color = new Color(vals[0], vals[1], vals[2]);
            if (!clusterMaterials.ContainsKey(row["Entry"].ToString() + " " + row["name"].ToString()))
            {
                clusterMaterials.Add(row["Entry"].ToString() + " " + row["name"].ToString(), mat);
            }
        }


        currentRows = clusterTable.AsEnumerable().ToList();
        for (int i = 0; i < currentRows.Count; i++)
        {
            string name = currentRows[i]["entry"].ToString() + " " + currentRows[i]["name"].ToString();
            int index = -1;
            var number = currentRows[i]["number"].ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            int[] m_numbers = number.Select(x => int.Parse(x)).ToArray();
            int mNumber = int.Parse(currentRows[i]["number"].ToString());
            requiredClusters.Add(name, mNumber);
            placedClusters.Add(name, 0);
        }

        var roomFile = roomList.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
        roomTypes = roomFile[0].Split(',').Skip(2).ToList();
        for (int i = 1; i < roomFile.Length - 1; i++)
        {
            var temp = roomFile[i].Split(',');
            string clusterName = temp[1] + " " + temp[0];
            if (roomAreas.ContainsKey(clusterName))
            {
                roomAreas[clusterName].AddRange(temp.Skip(2));
            }
            else
            {
                roomAreas.Add(clusterName, new List<string>());
                roomAreas[clusterName].AddRange(temp.Skip(2));
            }
        }
        for (int i = 0; i < roomTypes.Count; i++)
        {
            totalAreas.Add(0);
        }

    }
    void OnResolveUpdateRoom(ProceduralCluster cluster)
    {
        UpdateListOfRooms();
        UpdateText();
    }
    public void UpdateListOfRooms()
    {
        txtMan.roomAreas = "";
        for (int j = 0; j < totalAreas.Count; j++)
        {
            totalAreas[j] = 0;
        }
        for (int i = 0; i < allClusters.Count; i++)
        {
            string key = MyCluster(allClusters[i]);

            for (int j = 0; j < roomAreas[key].Count; j++)
            {
                totalAreas[j] += float.Parse(roomAreas[key][j]);
            }
        }
        for (int i = 0; i < roomTypes.Count; i++)
        {
            if (totalAreas[i] != 0)
                txtMan.roomAreas += "<mark=#ffffff35>" + roomTypes[i] + ":" + totalAreas[i] + "</mark>" + "\n";
        }
    }
    private void Start()
    {
        UpdateText();
    }
    // Update is called once per frame
    void Update()
    {

    }

    public void OnActiveCluster()
    {
        if (activeCluster.Split(' ').Length > 1)
        {
            CreateCluster(activeCluster);
        }
        else
        {
            CreateHall(activeCluster);
        }

    }

    public void UpdateProgram()
    {
        if (requiredClusters != null)
            requiredClusters.Clear();

        currentRows = clusterTable.AsEnumerable().ToList();
        //Debug.Log(currentRows.Count);
        for (int i = 0; i < currentRows.Count; i++)
        {
            string name = currentRows[i]["entry"].ToString() + " " + currentRows[i]["name"].ToString();

            var number = currentRows[i]["number"].ToString().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            int[] m_numbers = number.Select(x => int.Parse(x)).ToArray();
            int mNumber = int.Parse(currentRows[i]["number"].ToString());

            requiredClusters.Add(name, mNumber);
        }
        UpdateText();
    }

    public void CreateCluster(string name)
    {
        var rows = currentRows.Where(x => x["entry"].ToString() + " " + x["name"].ToString() == name).ToList();
        rows.TrimExcess();
        allClusters.Add(gridManager.OnStartPlacing(clusterPrefab, rows, clusterMaterials[name]));
        placedClusters[name]++;
        UpdateListOfRooms();
        UpdateText();

    }

    public void CreateHall(string myEntry)
    {
        var rows = currentRows.Where(x => x["entry"].ToString() + " " + x["name"].ToString() == myEntry + " Hall Cluster").ToList();
        rows.AddRange(currentRows.Where(x => x["entry"].ToString() + " " + x["name"].ToString() == myEntry + " Kitchen Cluster").ToList());
        if (myEntry == "3FE")
        {
            rows.AddRange(currentRows.Where(x => x["entry"].ToString() + " " + x["name"].ToString() == myEntry + " Small Hall Cluster").ToList());
            rows.AddRange(currentRows.Where(x => x["entry"].ToString() + " " + x["name"].ToString() == myEntry + " Storage").ToList());
        }

        allClusters.Add(gridManager.OnStartPlacing(clusterPrefab, rows, clusterMaterials[myEntry + " Hall Cluster"]));

        rows.TrimExcess();
        placedClusters[myEntry + " Hall Cluster"]++;
        UpdateListOfRooms();
        UpdateText();

    }


    public void RemoveCluster(ProceduralCluster cluster)
    {

        string clName = cluster.name.Split('_')[0];
        if (placedClusters[clName] > 0)
            placedClusters[clName]--;

        allClusters.Remove(cluster);
        UpdateListOfRooms();
        UpdateText();
    }
    public void RemoveRoom(ProceduralCluster myCluster)
    {
        roomAreas.Remove(MyCluster(myCluster));

    }

    public void UpdateText()
    {
        //if entry = free just record the placed clusters

        txtMan.clusters = "";
        verticalCommunication = "";
        CheckLevels();
        if (entry == "Free")
        {

            if (transform.childCount > 0)
            {
                foreach (var item in placedClusters)
                {
                    if (placedClusters[item.Key] != 0)
                        txtMan.clusters += string.Format("placed: {1} {0}\r\n", item.Key, placedClusters[item.Key], item.Value);
                }
            }
            else if (transform.childCount == 0)
            {
                txtMan.clusters += "";
            }
        }
        //else turn text red when the placed clusters are less that the required
        else
        {
            foreach (var item in requiredClusters)
            {
                if (item.Key.Contains(entry) && !item.Key.Contains("Storage") && !item.Key.Contains("Corner") && !item.Key.Contains("Kitchen") && !item.Key.Contains("Small Hall"))
                {

                    if (item.Key.Contains("Stair") || item.Key.Contains("Lift"))
                    {
                        if (hasFloors)
                        {
                            if (placedClusters[item.Key] < item.Value)
                            {
                                verticalCommunication += "<mark=#ffffff35><color=#ff0000ff>" + item.Key + ": p: " + placedClusters[item.Key] + ", r: " + item.Value + "\r\n" + "</color></mark>";
                            }
                            else if (placedClusters[item.Key] > item.Value)
                            {
                                verticalCommunication += "<mark=#ffffff35><color=#ffa500ff>" + item.Key + ": p: " + placedClusters[item.Key] + ", r: " + item.Value + "\r\n" + "</color></mark>";
                            }
                            else
                            {
                                verticalCommunication += string.Format("{0}: p: {1}, r: {2}\r\n", item.Key, placedClusters[item.Key], item.Value);
                            }
                        }
                        else
                        {
                            verticalCommunication = string.Empty;
                        }
                    }
                    else
                    {
                        if (placedClusters[item.Key] < item.Value)
                        {
                            txtMan.clusters += "<mark=#ffffff35><color=#ff0000ff>" + item.Key + ": p: " + placedClusters[item.Key] + ", r: " + item.Value + "\r\n" + "</color></mark>";
                        }
                        else if (placedClusters[item.Key] > item.Value)
                        {
                            txtMan.clusters += "<mark=#ffffff35><color=#ffa500ff>" + item.Key + ": p: " + placedClusters[item.Key] + ", r: " + item.Value + "\r\n" + "</color></mark>";
                        }
                        else
                        {
                            txtMan.clusters += "<mark=#ffffff35>" + item.Key + ": p: " + placedClusters[item.Key] + ", r: " + item.Value + "\r\n";

                        }
                    }
                }
            }
            foreach (var item in placedClusters)
            {
                if (!item.Key.Contains(entry))
                {
                    if (placedClusters[item.Key] != 0)
                        txtMan.clusters += "<mark=#ffffff35>" + "\r\n" + item.Key + ": p: " + placedClusters[item.Key] + "</ mark >";
                }
            }
        }
        txtMan.clusters += verticalCommunication;
        if (clusterInfo != null && clusterInfo.gameObject.activeSelf)
        {
            clusterInfo.text = txtMan.clusters;
        }

        txtMan.UpdateText();
        txtMan.UpdateUIText();
        externalManager.UpdateInfoText();
    }

    public bool TryGetRowPrefab(string name, out DataRow row, out GameObject prefab)
    {
        //row = clusterTable.Select("name='" + name + "'")[0];
        row = currentRows.Where(x => x[name].ToString() == name).ToList()[0];
        prefab = clusterPrefab;
        if (row != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void CheckLevels()
    {
        int count = 0;
        if (configurationParent.childCount > 0)
            foreach (Transform child in configurationParent)
            {
                if (child.GetComponent<PlaceablePrefab>().level > 0)
                {
                    count++;
                }
            }
        if (count > 0)
        {
            hasFloors = true;
        }
        else
        {
            hasFloors = false;
        }
    }

    public string MyCluster(ProceduralCluster myPrefab)
    {
        string temp = "";

        if (!myPrefab.name.Contains("Infant"))
        {
            if (myPrefab.isResolved)
            {
                temp = "_Resolved";
            }
            else
            {
                temp = "_Unresolved";
            }
        }
        else
        {
            if (!myPrefab.halfLeft && !myPrefab.halfRight)
            {
                temp = "_Resolved";
            }
            else if (!myPrefab.halfLeft && myPrefab.halfRight)
            {
                temp = "_Unresolved_R";
            }
            else if (myPrefab.halfLeft && !myPrefab.halfRight)
            {
                temp = "_Unresolved_L";
            }
            else
            {
                temp = "_Unresolved";
            }
        }
        return myPrefab.name + temp;
    }
}
