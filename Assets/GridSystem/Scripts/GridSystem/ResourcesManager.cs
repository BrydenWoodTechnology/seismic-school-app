
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;
using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using UnityEngine.SceneManagement;
using System.Data;
using BrydenWood.Interop;
using UnityEngine.UI;
using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity;
using BrydenWoodUnity.Grid;

namespace BrydenWood.Interop
{
    public class ResourcesManager : Tagged<ResourcesManager>
    {

        public enum ResourcesLocation
        {
            GetFromURL,
            GetFromStreamingAssets
        }

        #region WebGL Methods
        [DllImport("__Internal")]
        private static extern string GetURLFromPage();

        [DllImport("__Internal")]
        public static extern void DownloadText(string data, string fileName, string type);

        [DllImport("__Internal")]
        private static extern void TextUploaderCaptureClick(string objectName, string methodName, string fileName);

        [DllImport("__Internal")]
        private static extern void PopupOpenerCaptureClick(string link);

        [DllImport("__Internal")]
        private static extern void DisplayNotification(string notification);

        [DllImport("__Internal")]
        private static extern void DownloadFile(byte[] array, int bytLength, string fileName);

        #endregion
        public ResourcesLocation resourcesLocation = ResourcesLocation.GetFromURL;
        public string filePath;

        public string content { get; set; }

        public ConfigurationSaveState lastSavedState { get; private set; }
        public ConfigurationSaveStateSite lastSavedStateSite { get; private set; }
        public ConfigurationSaveStatePrefab lastSavedStatePrefab { get; private set; }

        private string projectData;
        public string exportData;
        public string entry;
        private OpenFileDialog ofd { get; set; }
        private SaveFileDialog sfd { get; set; }
        private bool waitingData { get; set; }

        public delegate void OnLoadedData();
        public event OnLoadedData dataLoaded;
        const int arrayIndex = 1;

        public GameObject dataHolder;

        public PolygonDrawer polygonMan;
        public MeshlessGridManager meshMan;
        // Use this for initialization
        void Awake()
        {

            waitingData = true;
#if UNITY_STANDALONE_WIN
        ofd = new OpenFileDialog();
        sfd = new SaveFileDialog();
#elif UNITY_WEBGL
            switch (resourcesLocation)
            {
                case ResourcesLocation.GetFromURL:

                    break;
                case ResourcesLocation.GetFromStreamingAssets:
                    ofd = new OpenFileDialog();
                    sfd = new SaveFileDialog();
                    break;
            }
#endif
        }
        private void OnEnable()
        {
            DataRecorder hold = (DataRecorder)FindObjectOfType(typeof(DataRecorder));
            if (hold != null)
            {

                SplitProjectData(hold.data, out projectData, out exportData);
                GetComponent<ResetAll>().programSelection.isOn = false;
                GetComponent<ResetAll>().landingP1.SetActive(false);
                GetComponent<ResetAll>().landingP2.SetActive(false);
                GetComponent<ResetAll>().setGrid.interactable = true;
                GetComponent<ResetAll>().setSite.interactable = false;
                GetComponent<ResetAll>().clusterMenu.interactable = true;
                polygonMan.OnLoadingPolygon();
                Destroy(hold.gameObject);

            }
        }
        // Update is called once per frame
        void Update()
        {

        }

        /// <summary>
        /// Deserializes a save file from a givel path
        /// </summary>
        /// <param name="filePath"></param> the path the file is on
        /// <returns></returns>
        public ConfigurationSaveState ReadSaveState(string filePath)
        {
            return JsonConvert.DeserializeObject<ConfigurationSaveState>(File.ReadAllText(filePath));
        }

        /// <summary>
        /// saves a configuration file
        /// </summary>
        /// <param name="saveState"></param> the ConfigurationSaveState to be saved
        /// <param name="projectName"></param> the name of the project
        /// <param name="library"></param> the datatable with all data
        /// <param name="export"></param> tha version of the save
        public void WriteSaveState(ConfigurationSaveState saveState, string projectName, DataTable library, string export)
        {

            lastSavedState = saveState;
            string data = CombineProjectData(saveState, export);
            switch (resourcesLocation)
            {
                case ResourcesLocation.GetFromStreamingAssets:
                    sfd.Filter = "json files (*.json)|*.json|All files (*.*)|*.*";
                    sfd.Title = "Save Buildings";
                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        File.WriteAllText(sfd.FileName, data);
                    }
                    break;
                case ResourcesLocation.GetFromURL:
                    DownloadText(data, projectName + ".json", "text/plain");
                    break;
            }
        }
        /// <summary>
        /// Reads a save file 
        /// </summary>
        public void SetProjectFileBuildings()
        {
            switch (resourcesLocation)
            {
                case ResourcesLocation.GetFromURL:
                    DisplayNotification("A button will appear at the top center of your screen");
                    TextUploaderCaptureClick(gameObject.name, "ProjectFilesSelected", "savedFiles");
                    break;
                case ResourcesLocation.GetFromStreamingAssets:
                    ofd.Filter = "json files (*.json)|*.json|All files (*.*)|*.*";
                    if (ofd.ShowDialog() == DialogResult.OK)
                    {
                        var data = File.ReadAllText(ofd.FileName);
                        GameObject dh = GameObject.Instantiate(dataHolder);
                        DontDestroyOnLoad(dh);
                        dh.GetComponent<DataRecorder>().data = data;
                        LoadScene();

                    }
                    break;
            }
        }


        /// <summary>
        /// deserialize data from a loaded saved file
        /// </summary>
        /// <param name="data"></param>
        /// <param name="proj"></param>
        /// <param name="export"></param>
        public void SplitProjectData(string data, out string proj, out string export)
        {

            proj = data.Split(new string[] { "project:" }, StringSplitOptions.None)[arrayIndex];
            export = data.Split(new string[] { "export:" }, StringSplitOptions.None)[arrayIndex];
            lastSavedState = JsonConvert.DeserializeObject<ConfigurationSaveState>(projectData);

        }


        /// <summary>
        /// Serialize the data to be saved
        /// </summary>
        /// <param name="state"></param> tha ConfigurationSaveState 
        /// <param name="export"></param> the version of the save
        /// <returns></returns>
        public string CombineProjectData(ConfigurationSaveState state, string export)
        {
            string data = "";
            string project = JsonConvert.SerializeObject(state, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, ReferenceLoopHandling = ReferenceLoopHandling.Serialize });
            data += "project:" + project + "project:\r\n";
            data += "export:" + export + "export:\r\n";
            return data;
        }

        /// <summary>
        /// Loads a specified scene
        /// </summary>
        public void LoadScene()
        {

            SceneManager.LoadScene(0);

        }
        /// <summary>
        /// Start the loading of files
        /// </summary>
        /// <param name="url"></param> the url to load from
        public void ProjectFilesSelected(string url)
        {
            StartCoroutine(LoadProjectText(url));
        }


        /// <summary>
        /// load a save file based on url
        /// </summary>
        /// <param name="url"></param> the url to load from
        /// <returns></returns>
        IEnumerator LoadProjectText(string url)
        {
            WWW sav = new WWW(url);
            yield return sav;
            GameObject dh = GameObject.Instantiate(dataHolder);
            DontDestroyOnLoad(dh);
            dh.GetComponent<DataRecorder>().data = sav.text;
            dh.GetComponent<DataRecorder>().saveState = lastSavedState;
            LoadScene();

        }



        public void OnReset()
        {
            projectData = "";
            waitingData = true;
        }
        /// <summary>
        /// open a browser window
        /// </summary>
        /// <param name="link"></param> the link to open
        public void OnLoadWebsite(string link)
        {
            PopupOpenerCaptureClick(link);
        }
        /// <summary>
        /// Downloads an image
        /// </summary>
        /// <param name="imgData"></param> the image in bytes
        public void ExportScreenShot(byte[] imgData)
        {

#if UNITY_EDITOR
            sfd.Filter = "png files (*.png)|*.png";
            sfd.Title = "Save Screen Shot";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllBytes(sfd.FileName, imgData);
            }
#elif UNITY_STANDALONE_WIN
            sfd.Filter = "png files (*.png)|*.png";
            sfd.Title = "Save Screen Shot";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllBytes(sfd.FileName, imgData);
            }
#elif UNITY_WEBGL
            string fileName = "SEISMIC-Schools" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss")+"_img.png";
            DownloadFile(imgData, imgData.Length, fileName);
#endif
        }
    }
}

 
