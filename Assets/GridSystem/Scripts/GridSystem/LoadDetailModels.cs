﻿using BrydenWoodUnity.Navigation;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using BrydenWoodUnity.GeometryManipulation;

public class LoadDetailModels : MonoBehaviour
{

    GameObject currentObject;
    public Transform cameraTargetCluster,camtargetAdditional;
    public Transform camCluster,camAdditional;
    public Transform onHoverInstances;
    public Text clusterDescritpion;

    public string activeCluster { get; set; }
    

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    /// <summary>
    /// Load a detailed model
    /// </summary>
    public void OnActiveDetailed()
    {
        OnClusterDeselected();
        OnHoverUI(activeCluster);

    }
    /// <summary>
    /// Load an external additional model
    /// </summary>
    public void OnActiveExternal()
    {
        OnClusterDeselected();
        OnHoverExternal(activeCluster);
    }
    /// <summary>
    /// Load external additional models by name
    /// </summary>
    /// <param name="name"></param> The name of the model to load
    public void OnHoverExternal(string name)
    {
        if (currentObject != null)
        {
            OnClusterDeselected();
        }

        var prefab = Resources.Load<GameObject>("Exterior/" + name);
        if (prefab != null)
        {
            currentObject = Instantiate(prefab);
            currentObject.transform.localEulerAngles = new Vector3(0, 0, 0);
            currentObject.transform.localPosition = Vector3.zero;
            RecursivelySetLayers(currentObject.transform, 8);
            currentObject.transform.SetParent(onHoverInstances);
            Bounds bn = currentObject.GetComponent<BoxCollider>().bounds;
            camtargetAdditional.position = bn.center;
            camtargetAdditional.eulerAngles = new Vector3(-45, 40, 0);
            camAdditional.GetComponent<OrbitCamera>().distance =3.5f*currentObject.GetComponent<BoxCollider>().bounds.extents.x;
            camtargetAdditional.transform.position = camtargetAdditional.position + camtargetAdditional.forward * 3.5f * currentObject.GetComponent<BoxCollider>().bounds.extents.x;
            camAdditional.transform.LookAt(camtargetAdditional);
        }
        else
        {
            camAdditional.GetChild(0).gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// Load a model by name
    /// </summary>
    /// <param name="name"></param>Tha name of the model
    public void OnHoverUI(string name)
    {
        if (currentObject != null)
        {
            OnClusterDeselected();
        }

        if (!AssetBundleManager.TaggedObject.loading)
        {

            var prefab = AssetBundleManager.TaggedObject.GetAsset("Unresolved_" + name);
            if (prefab != null)
            {
               
                clusterDescritpion.text=ProceduralClusterManager.TaggedObject.clusterTable.AsEnumerable().Where(r => r["DetailedClusterName"].ToString().Contains(name)).ToList()[0]["Description"].ToString();
                currentObject = Instantiate(prefab);
                currentObject.transform.localEulerAngles = new Vector3(-90, 0, 0);
                currentObject.transform.localPosition = Vector3.zero;
                currentObject.transform.localScale = Vector3.one;
                Bounds clBnd = new Bounds();
                RecursivelySetLayers(currentObject.transform, 15);
                currentObject.transform.SetParent(onHoverInstances);
                cameraTargetCluster.localEulerAngles = Vector3.zero;
                if (currentObject.GetComponent<MeshRenderer>() != null)
                    clBnd = currentObject.GetComponent<MeshRenderer>().bounds;
                else if (currentObject.GetComponent<BoxCollider>() != null)
                    clBnd = currentObject.GetComponent<BoxCollider>().bounds;

                camCluster.transform.position = new Vector3(clBnd.center.x, 78, clBnd.center.z);
                cameraTargetCluster.position = clBnd.center;
                cameraTargetCluster.eulerAngles = new Vector3(-90,0,0);

            }
            else
            {
                camCluster.GetChild(0).gameObject.SetActive(true);

            }
        }
        else
        {
            camCluster.GetChild(0).gameObject.SetActive(true);

        }

    }
    /// <summary>
    /// Load a model when a cluster is selected
    /// </summary>
    /// <param name="cluster"></param> The selected cluster

    public void OnClusterSelected(GameObject cluster)
    {
        if (currentObject != null)
        {
            OnClusterDeselected();
        }
        if (cluster.GetComponent<PlaceablePrefab>() != null)
        {
            if (!AssetBundleManager.TaggedObject.loading)
            {
                var prefab = AssetBundleManager.TaggedObject.GetAsset("Unresolved_" + cluster.GetComponent<ProceduralCluster>().detailedName);
               
                if (prefab != null)
                {
                    camCluster.GetComponent<OrbitCamera>().CCP.isOn = true;
                    currentObject = Instantiate(prefab);
                    currentObject.transform.localEulerAngles = new Vector3(-90, 0, 0);
                    currentObject.transform.localPosition = Vector3.zero;
                    currentObject.transform.localScale = Vector3.one;
                    Bounds clBnd = new Bounds();
                    RecursivelySetLayers(currentObject.transform, 15);
                    currentObject.transform.SetParent(onHoverInstances);
                    cameraTargetCluster.localEulerAngles = Vector3.zero;
                    if (currentObject.GetComponent<MeshRenderer>() != null)
                        clBnd = currentObject.GetComponent<MeshRenderer>().bounds;
                    else if (currentObject.GetComponent<BoxCollider>() != null)
                        clBnd = currentObject.GetComponent<BoxCollider>().bounds;

                    camCluster.transform.position = new Vector3(clBnd.center.x, 78, clBnd.center.z);
                    cameraTargetCluster.position = clBnd.center;
                    cameraTargetCluster.eulerAngles = new Vector3(-90, 0, 0);

                }
                else
                {
                    camCluster.GetChild(0).gameObject.SetActive(true);
                }
            }
            else
            {
                camCluster.GetChild(0).gameObject.SetActive(true);
            }


            clusterDescritpion.text = cluster.GetComponent<ProceduralCluster>().description;
        }
        else
        {
            var prefab = Resources.Load<GameObject>("Exterior/" + cluster.name.Replace("(Clone)", string.Empty).Split('_')[0]);

            if (prefab != null)
            {
                camAdditional.GetComponent<OrbitCamera>().CCP.isOn = true;
                currentObject = Instantiate(prefab);
                currentObject.transform.localEulerAngles = new Vector3(0, 0, 0);
                currentObject.transform.localPosition = Vector3.zero;
                RecursivelySetLayers(currentObject.transform, 8);
                currentObject.transform.SetParent(onHoverInstances);
                camtargetAdditional.position = currentObject.GetComponent<BoxCollider>().bounds.center;
                camtargetAdditional.eulerAngles = new Vector3(-45,40,0);
                camAdditional.GetComponent<OrbitCamera>().distance = 3.5f * currentObject.GetComponent<BoxCollider>().bounds.extents.x;
                camtargetAdditional.transform.position = camtargetAdditional.position + camtargetAdditional.forward * 3.5f * currentObject.GetComponent<BoxCollider>().bounds.extents.x;
                transform.LookAt(camtargetAdditional);
            }
            else
            {
                camAdditional.GetChild(0).gameObject.SetActive(true);
                print(name + ".prefab");
            }
        }
       
    }

    /// <summary>
    /// Called when a cluster is deselected to destroy detailed models
    /// </summary>
    public void OnClusterDeselected()
    {
       
        camtargetAdditional.eulerAngles = new Vector3(-45, 40, 0);
        cameraTargetCluster.eulerAngles = new Vector3(-90, 0, 0);

        if (currentObject != null)
            Destroy(currentObject);
        currentObject = null;
        camAdditional.GetChild(0).gameObject.SetActive(false);
        camCluster.GetChild(0).gameObject.SetActive(false);
        clusterDescritpion.text = "";
    }

    /// <summary>
    /// Change the layer of and object and its' children
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="layer"></param>
    public void RecursivelySetLayers(Transform obj, int layer)
    {
        obj.gameObject.layer = layer;
        for (int i = 0; i < obj.childCount; i++)
        {
            if (obj.GetChild(i).GetComponent<MeshRenderer>() != null)
            {
                obj.GetChild(i).GetComponent<MeshRenderer>().receiveShadows = false;
                obj.GetChild(i).GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            }
            RecursivelySetLayers(obj.GetChild(i), layer);
        }
    }


}
