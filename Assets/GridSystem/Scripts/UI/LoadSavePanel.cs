﻿using BrydenWoodUnity.Three;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UI
{
    public class LoadSavePanel : MonoBehaviour
    {

        public Text projectTitle;
        public Text log;

        private OpenFileDialog ofd;
        private SaveFileDialog sfd;

        private string data;

        private DateTime start;
        private DateTime end;

        // Use this for initialization
        void Start()
        {
            ThreeConfiguration.gameObjectsLoaded += OnModelLoaded;
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void LoadProject()
        {
            if (ofd == null)
            {
                ofd = new OpenFileDialog();
                if (sfd != null)
                {
                    ofd.InitialDirectory = (new FileInfo(sfd.FileName)).DirectoryName;
                }
                else
                {
                    ofd.InitialDirectory = "C:\\";
                }
            }
            ofd.Filter = "json files (*.json)|*.json|All files (*.*)|*.*";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //data = File.ReadAllText(ofd.FileName);
                start = DateTime.Now;
                var configuration = new GameObject(new FileInfo(ofd.FileName).Name.Split('.')[0]).AddComponent<ThreeConfiguration>();
                configuration.millimeters = false;
                configuration.Initialize(File.ReadAllText(ofd.FileName));
                projectTitle.text = new FileInfo(ofd.FileName).Name.Split('.')[0];
            }
        }

        public void SaveProject()
        {
            if (sfd == null)
            {
                sfd = new SaveFileDialog();
                if (ofd != null)
                {
                    sfd.InitialDirectory = (new FileInfo(ofd.FileName)).DirectoryName;
                }
                else
                {
                    sfd.InitialDirectory = "C:\\";
                }
            }
        }

        private void OnModelLoaded(ThreeConfiguration configuration)
        {
            if (log != null)
            {
                end = DateTime.Now;
                log.text = string.Format("Loaded in {0} seconds.", (end - start).TotalSeconds);
            }
        }
    }
}
