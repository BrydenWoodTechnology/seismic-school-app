﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeToggleColor : MonoBehaviour
{


    public Color on;
    public Color off;

    private Toggle m_toggle { get; set; }
    private Graphic targetGraphic { get; set; }

    // Use this for initialization
    void Start()
    {
        m_toggle = GetComponent<Toggle>();
        targetGraphic = m_toggle.targetGraphic;
        targetGraphic.color = on;
        OnValueChanged(m_toggle.isOn);
        //GetComponentInChildren<Text>().color = off;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnValueChanged(bool toggle)
    {
        if (toggle)
        {
            targetGraphic.color = on;
            GetComponentInChildren<Text>().color = off;
        }
        else
        {
            targetGraphic.color = off;
            GetComponentInChildren<Text>().color = on;
        }
    }
}
