﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;
using UnityEngine.UI;

public class Selectable : MonoBehaviour {

    public Text selectedValue;
    public Color32 selected;
    public Color32 unselected;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void onSelectedValues(bool isOn)
    {
        if (isOn)
        {
            selectedValue.color = selected;
        }
        else
        {
            selectedValue.color = unselected;
        }
    }
}
