﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuiSelect : MonoBehaviour {

    public bool initializeOnStart = false;
    public Color color = Color.white;
    public Color deSelect;

    private Image m_image;

    public delegate void OnSelect(GuiSelect sender);
    public event OnSelect selected;

	// Use this for initialization
	void Start () {
        if (initializeOnStart)
        {
            Initialize();
        }
	}
	
	// Update is called once per frame
	void Update () {

    }

    public virtual void Initialize()
    {
        m_image = GetComponent<Image>();
        m_image.color = color;
        deSelect = color * 0.5f;
    }

    public void Select()
    {
        m_image.color = color;
        if (selected != null)
        {
            selected(this);
        }
    }

    public void SetAsCurrent()
    {
        m_image.color = color;
    }

    public void Deselect()
    {
        m_image.color = deSelect;
    }
}
