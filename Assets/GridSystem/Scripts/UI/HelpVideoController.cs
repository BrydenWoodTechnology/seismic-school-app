﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class HelpVideoController : MonoBehaviour {

    public VideoClip[] clips;
    public RenderTexture renderTexture;
    public RawImage rawImage;
    public VideoPlayer videoPlayer;

	// Use this for initialization
	void Start () {
        videoPlayer.renderMode = VideoRenderMode.RenderTexture;
        videoPlayer.loopPointReached += OnStopped;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetVideo(int index)
    {
        videoPlayer.gameObject.SetActive(true);
        videoPlayer.clip = clips[index];
        videoPlayer.targetTexture = renderTexture;
        rawImage.texture = renderTexture;
        videoPlayer.Play();
        
    }

    public void OnStopped(VideoPlayer player)
    {
        videoPlayer.Stop();
        videoPlayer.gameObject.SetActive(false);
    }
}
