﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BrydenWoodUnity.GeometryManipulation;

public class IncompatibilityResolveButton : MonoBehaviour
{

    public PlaceablePrefab cluster;
    public string side;
    public Vector3 worldPos;
    public Vector3 canvasOffset = new Vector3(0, 20, 0);
    public Transform myPosition;
    private Vector3 prevPos;
    private Camera mainCamera;
    private Vector3 pos;
    // Use this for initialization
    void Start()
    {
        mainCamera = Camera.main;
        UpdatePosition();
    }

    // Update is called once per frame
    void Update()
    {
        if (mainCamera.WorldToScreenPoint(worldPos) != prevPos||(myPosition!=null&&myPosition.position!=pos))
        {
            pos = myPosition.position;
           // print(pos);
            prevPos = mainCamera.WorldToScreenPoint(pos);
            prevPos = new Vector3(prevPos.x, prevPos.y, 0);
            UpdatePosition();
        }
    }

    public void ResolveIssue()
    {
        cluster.Expand(side);
        cluster.GetComponent<ProceduralCluster>().UpdateFloorplanResolved();
    }

    public void UnresolveIssue()
    {
        cluster.ExpandBack(side);
        cluster.GetComponent<ProceduralCluster>().UpdateFloorplanUnResolved();

    }

    public void UpdatePosition()
    {
        var newPos = mainCamera.WorldToScreenPoint(pos);
        newPos = new Vector3(newPos.x, newPos.y, 0);
        GetComponent<RectTransform>().position = newPos + canvasOffset;
    }
}
