﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EntrySelect : MonoBehaviour
{

    public GameObject gridManager;
    public string currentEntry = "Free";

    public Toggle[] entryToggles;
    public string[] entries;
    public Image entryPreview;
    public Image entryPreviewMagnifier;
    public Sprite[] previews;

    int allOff = 0;
    public Transform clusterButtonParent;
    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {

    }
    /// <summary>
    /// Select program 
    /// </summary>
    public void OnContinue()
    {

        gridManager.GetComponent<ProceduralClusterManager>().entry = currentEntry;
        gridManager.GetComponent<ProceduralClusterManager>().UpdateProgram();
        gridManager.GetComponent<ProceduralClusterManager>().UpdateText();
        if (currentEntry != "Free")
            clusterButtonParent.Find(currentEntry).SetSiblingIndex(0);
    }
    /// <summary>
    /// swap preview image of each program
    /// </summary>
    /// <param name="toggle"></param> each toggle that represents a program
    public void OnValueChanged(bool toggle)
    {
        allOff = 0;
        for (int i = 0; i < entryToggles.Length; i++)
        {
            if (entryToggles[i].isOn)
            {
                allOff++;
                currentEntry = entries[i];
                entryPreview.sprite = previews[i];
                //entryPreviewMagnifier.sprite = previews[i];
            }
        }
        if (allOff == 0)
        {
            currentEntry = "Free";
        }
    }
}
