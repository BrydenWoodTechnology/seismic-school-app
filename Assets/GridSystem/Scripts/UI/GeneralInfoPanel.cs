﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.Grid;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWood.Interop
{

    public class GeneralInfoPanel : MonoBehaviour
    {
        [Header("GUI references:")]
        public Text siteAreaText;
        public TextMeshProUGUI clusterSiteInfo;

        [Space(10)]
        [Header("Configuration Information")]
        public float siteArea;
        public float footprintArea;

        private Polygon sitePolygon { get; set; }

        TextManager txtMan;

        // Use this for initialization
        void Start()
        {
            PolygonDrawer.polygonEnded += OnSitePlaced;
            MeshlessGridManager.prefabAdded += OnPrefabAdded;
            txtMan = GetComponent<TextManager>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnEnable()
        {
            //siteAreaText.text = txtMan.grid;
        }

        private void OnDestroy()
        {

            PolygonDrawer.polygonEnded -= OnSitePlaced;
            MeshlessGridManager.prefabAdded -= OnPrefabAdded;
        }

        #region Public Methods
        /// <summary>
        /// Called when the site polygon has been placed
        /// </summary>
        /// <param name="sitePolygon"></param>
        public void OnSitePlaced(Polygon sitePolygon, Polygon offset)
        {
            this.sitePolygon = offset;
            siteArea = sitePolygon.area;
            UpdateAreaInfo();
        }

        public void OnPrefabAdded(MeshlessGridManager sender)
        {
            //if (gameObject.activeSelf)
            StartCoroutine(UpdateArea(sender));


        }
        #endregion

        #region Private Methods
        private void UpdateAreaInfo()
        {
            txtMan.grid = string.Format("<mark=#ffffff35>Site Area: {0} m\xB2\r\nFootprint Area: {1} m\xB2\r\nRemaining Area: {2} m\xB2</mark>\r\n", siteArea.ToString("0.00"), footprintArea.ToString("0.00"), (siteArea - footprintArea).ToString("0.00"));
            if (siteAreaText != null)
                siteAreaText.text = txtMan.grid.Replace("<mark=#ffffff35>", string.Empty).Replace("</mark>", string.Empty);
            if (clusterSiteInfo != null)
            {
                clusterSiteInfo.text = txtMan.grid;
            }


            txtMan.UpdateText();


        }

        private IEnumerator UpdateArea(MeshlessGridManager sender)
        {
            yield return new WaitForEndOfFrame();
            footprintArea = sender.GetPrefabFootprint();
            UpdateAreaInfo();
        }


        #endregion
    }
}
