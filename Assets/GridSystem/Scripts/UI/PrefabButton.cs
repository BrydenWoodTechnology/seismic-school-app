﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrefabButton : MonoBehaviour
{
    public GameObject[] clusters;
    public GameObject cluster;
    public Text remaining;
    int count;
    public delegate void OnClick(PrefabButton sender);
    public static event OnClick onClick;
    public bool selectRandom;
    public string batch { get; set; }
    

    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        
    }

  

    public void OnPlacePrefab(string name)
    {
        count++;

        //gameObject.GetComponentInChildren<Text>().text = count.ToString();

    }

    public void OnClicked()
    {
        if (selectRandom)
            cluster = clusters[Random.Range(0, clusters.Length - 1)];
        if (cluster.GetComponent<BatchPlacement>() != null)
        {
            cluster.GetComponent<BatchPlacement>().CreateBatch(batch);
        }

        //if (onClick != null)
        //{
        //    onClick(this);
        //}
      //  print(cluster.name);
       
       // cluster.SendMessage("CreateBatch", batch, SendMessageOptions.DontRequireReceiver);
       
    }


}
