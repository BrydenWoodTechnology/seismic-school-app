﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using UnityEngine;
using UnityEngine.UI;

//Based on: https://stackoverflow.com/questions/35183253/unity3d-upload-a-image-from-pc-memory-to-webgl-app
public class GetImageButton : MonoBehaviour
{
    public GameObject imagePlane;
    public Transform Dim;
    public InputField width;
    public InputField height;
    public InputField x_scale;
    public InputField y_scale;

    


    static string s_dataUrlPrefix = "data:image/png;base64,";
    public void ReceiveImage(string dataUrl)
    {
        Debug.Log("Receiving Image");
        if (dataUrl.StartsWith(s_dataUrlPrefix))
        {
            byte[] pngData = System.Convert.FromBase64String(dataUrl.Substring(s_dataUrlPrefix.Length));

            // Create a new Texture (or use some old one?)
            Texture2D tex = new Texture2D(1, 1); // does the size matter?
            if (tex.LoadImage(pngData))
            {
                Renderer renderer = imagePlane.GetComponent<Renderer>();

                renderer.material.mainTexture = tex;
            }
            else
            {
                Debug.LogError("could not decode image");
            }
        }
        else
        {
            Debug.LogError("Error getting image:" + dataUrl);
        }
    }

    public void OnClick()
    {
        imagePlane.SetActive(true);
        imagePlane.transform.localScale = new Vector3(float.Parse(width.text) / 10.0f,1, float.Parse(height.text) / 10.0f);
        x_scale.text = (imagePlane.transform.localScale.x).ToString();
        y_scale.text = (imagePlane.transform.localScale.z ).ToString();
#if UNITY_EDITOR
        GetImageDektop();
#elif UNITY_WEBGL
        GetImage.GetImageFromUserAsync(gameObject.name, "ReceiveImage");
         Dim.gameObject.SetActive(true);
#endif
    }

    public void GetImageDektop()
    {
        OpenFileDialog ofd = new OpenFileDialog();
        if (ofd.ShowDialog() == DialogResult.OK)
        {
            byte[] pngData = File.ReadAllBytes(ofd.FileName);
            Texture2D tex = new Texture2D(1, 1); // does the size matter?
            if (tex.LoadImage(pngData))
            {
                Renderer renderer = imagePlane.GetComponent<Renderer>();

                renderer.material.mainTexture = tex;
                Dim.gameObject.SetActive(true);
            }
            else
            {
                Debug.LogError("could not decode image");
            }
        }
    }


}
