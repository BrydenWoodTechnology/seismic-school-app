﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.Navigation
{
    public class DrawGLines : MonoBehaviour
    {
        public Material polygonMat;
        public Material gridMat;
        public Material selectedMat;
        public Material firstSelectedMat, normalSelected;
        public Material moduleEdgesMat;
        public Text Grid_OnOff;
        [HideInInspector]
        public Vector3[] polygonPoints;
        [HideInInspector]
        public Vector3[] offsetPolyPoints;
        [HideInInspector]
        public Vector3[] siteBoundaryPoints;
        [HideInInspector]
        public List<Material> gridColors = new List<Material>();
        [HideInInspector]
        public List<Vector3[]> gridPoints = new List<Vector3[]>();
        [HideInInspector]
        public Vector3[] orientationLine = new Vector3[0];
        [HideInInspector]
        public int currentGridIndex = 0;
        [HideInInspector]
        public List<Vector3[]> selectedPrefab;
        [HideInInspector]
        public List<List<Vector3[]>> modulesCorners = new List<List<Vector3[]>>();

        public bool showGrid = true;

        private void Start()
        {

        }

        public void ToggleGrid()
        {
            showGrid = !showGrid;
        }

        public void ChangeButtonText()
        {
            if (showGrid)
            {
                Grid_OnOff.text = "Hide Grid";
            }
            else
            {
                Grid_OnOff.text = "Show Grid";
            }
        }


        public void SwitchMaterial(bool first)
        {
            if (first)
            {
                selectedMat = firstSelectedMat;

            }
            else
            {
                selectedMat = normalSelected;
            }

        }
        // Connect all of the `points` to the `mainPoint`
        public void DrawConnectingLines()
        {
            if (showGrid)
            {
                if (gridPoints.Count > 0)
                {
                    for (int j = 0; j < gridPoints.Count; j++)
                    {
                        if (j == currentGridIndex)
                        {
                            if (gridPoints[j].Length > 0)
                            {
                                for (int i = 0; i < gridPoints[j].Length; i += 4)
                                {
                                    var p1 = gridPoints[j][i];
                                    var p2 = gridPoints[j][i + 1];
                                    var p3 = gridPoints[j][i + 2];
                                    var p4 = gridPoints[j][i + 3];
                                    GL.Begin(GL.LINES);
                                    gridColors[j].SetPass(0);
                                    GL.Color(new Color(gridColors[j].color.r, gridColors[j].color.g, gridColors[j].color.b, gridColors[j].color.a));
                                    GL.Vertex3(p1.x, p1.y, p1.z);
                                    GL.Vertex3(p2.x, p2.y, p2.z);
                                    GL.Vertex3(p3.x, p3.y, p3.z);
                                    GL.Vertex3(p4.x, p4.y, p4.z);
                                    GL.Vertex3(p2.x, p2.y, p2.z);
                                    GL.Vertex3(p3.x, p3.y, p3.z);
                                    GL.Vertex3(p1.x, p1.y, p1.z);
                                    GL.Vertex3(p4.x, p4.y, p4.z);
                                    GL.End();
                                }
                            }
                        }
                    }
                }
            }


            if (siteBoundaryPoints.Length > 0)
            {
                for (int i = 0; i < siteBoundaryPoints.Length; i += 4)
                {
                    var p1 = siteBoundaryPoints[i];
                    var p2 = siteBoundaryPoints[i + 1];
                    var p3 = siteBoundaryPoints[i + 2];
                    var p4 = siteBoundaryPoints[i + 3];
                    GL.Begin(GL.LINES);
                    gridMat.SetPass(0);
                    GL.Color(new Color(gridMat.color.r, gridMat.color.g, gridMat.color.b, gridMat.color.a));
                    GL.Vertex3(p1.x, p1.y, p1.z);
                    GL.Vertex3(p2.x, p2.y, p2.z);
                    GL.Vertex3(p3.x, p3.y, p3.z);
                    GL.Vertex3(p4.x, p4.y, p4.z);
                    GL.Vertex3(p2.x, p2.y, p2.z);
                    GL.Vertex3(p3.x, p3.y, p3.z);
                    GL.Vertex3(p1.x, p1.y, p1.z);
                    GL.Vertex3(p4.x, p4.y, p4.z);
                    GL.End();
                }
            }

            if (polygonPoints != null && polygonPoints.Length > 0)
            {
                for (int i = 1; i < polygonPoints.Length; i++)
                {
                    Vector3 firstPoint = polygonPoints[i - 1];
                    Vector3 secondPoint = polygonPoints[i];
                    GL.Begin(GL.LINES);
                    polygonMat.SetPass(0);
                    GL.Color(new Color(polygonMat.color.r, polygonMat.color.g, polygonMat.color.b, polygonMat.color.a));
                    GL.Vertex3(firstPoint.x, firstPoint.y, firstPoint.z);
                    GL.Vertex3(secondPoint.x, secondPoint.y, secondPoint.z);
                    GL.End();
                }
            }

            if (offsetPolyPoints != null && offsetPolyPoints.Length > 0)
            {
                for (int i = 1; i < offsetPolyPoints.Length; i++)
                {
                    Vector3 firstPoint = offsetPolyPoints[i - 1];
                    Vector3 secondPoint = offsetPolyPoints[i];
                    GL.Begin(GL.LINES);
                    polygonMat.SetPass(0);
                    GL.Color(new Color(polygonMat.color.r, polygonMat.color.g, polygonMat.color.b, polygonMat.color.a));
                    GL.Vertex3(firstPoint.x, firstPoint.y, firstPoint.z);
                    GL.Vertex3(secondPoint.x, secondPoint.y, secondPoint.z);
                    GL.End();
                }
            }

            if (orientationLine != null && orientationLine.Length > 0)
            {
                GL.Begin(GL.LINES);
                gridMat.SetPass(0);
                GL.Color(new Color(gridMat.color.r, gridMat.color.g, gridMat.color.b, gridMat.color.a));
                GL.Vertex3(orientationLine[0].x, orientationLine[0].y, orientationLine[0].z);
                GL.Vertex3(orientationLine[1].x, orientationLine[1].y, orientationLine[1].z);
                GL.End();
            }

            if (selectedPrefab != null && selectedPrefab.Count > 0)
            {
                for (int j = 0; j < selectedPrefab.Count; j++)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        Vector3 firstPoint = selectedPrefab[j][(i + 1) % 4];
                        Vector3 secondPoint = selectedPrefab[j][i];
                        GL.Begin(GL.LINES);
                        selectedMat.SetPass(0);
                        GL.Color(new Color(selectedMat.color.r, selectedMat.color.g, selectedMat.color.b, selectedMat.color.a));
                        GL.Vertex3(firstPoint.x, firstPoint.y, firstPoint.z);
                        GL.Vertex3(secondPoint.x, secondPoint.y, secondPoint.z);
                        GL.End();
                    }

                    for (int i = 0; i < 4; i++)
                    {
                        Vector3 firstPoint = selectedPrefab[j][4 + ((i + 1) % 4)];
                        Vector3 secondPoint = selectedPrefab[j][4 + i];
                        GL.Begin(GL.LINES);
                        selectedMat.SetPass(0);
                        GL.Color(new Color(selectedMat.color.r, selectedMat.color.g, selectedMat.color.b, selectedMat.color.a));
                        GL.Vertex3(firstPoint.x, firstPoint.y, firstPoint.z);
                        GL.Vertex3(secondPoint.x, secondPoint.y, secondPoint.z);
                        GL.End();
                    }

                    for (int i = 0; i < 4; i++)
                    {
                        Vector3 firstPoint = selectedPrefab[j][i + 4];
                        Vector3 secondPoint = selectedPrefab[j][i];
                        GL.Begin(GL.LINES);
                        selectedMat.SetPass(0);
                        GL.Color(new Color(selectedMat.color.r, selectedMat.color.g, selectedMat.color.b, selectedMat.color.a));
                        GL.Vertex3(firstPoint.x, firstPoint.y, firstPoint.z);
                        GL.Vertex3(secondPoint.x, secondPoint.y, secondPoint.z);
                        GL.End();
                    }
                }
            }

            if (modulesCorners != null && modulesCorners.Count > 0)
            {

                for (int j = 0; j < modulesCorners.Count; j++)
                {
                    if (modulesCorners[j] != null)
                    {
                        for (int k = 0; k < modulesCorners[j].Count; k++)
                        {
                            if (modulesCorners[j][k] != null)
                            {
                                for (int i = 0; i < 4; i++)
                                {
                                    Vector3 firstPoint = modulesCorners[j][k][(i + 1) % 4];
                                    Vector3 secondPoint = modulesCorners[j][k][i];
                                    GL.Begin(GL.LINES);
                                    moduleEdgesMat.SetPass(0);
                                    GL.Color(new Color(moduleEdgesMat.color.r, moduleEdgesMat.color.g, moduleEdgesMat.color.b, moduleEdgesMat.color.a));
                                    GL.Vertex3(firstPoint.x, firstPoint.y, firstPoint.z);
                                    GL.Vertex3(secondPoint.x, secondPoint.y, secondPoint.z);
                                    GL.End();
                                }

                                for (int i = 0; i < 4; i++)
                                {
                                    Vector3 firstPoint = modulesCorners[j][k][4 + ((i + 1) % 4)];
                                    Vector3 secondPoint = modulesCorners[j][k][4 + i];
                                    GL.Begin(GL.LINES);
                                    moduleEdgesMat.SetPass(0);
                                    GL.Color(new Color(moduleEdgesMat.color.r, moduleEdgesMat.color.g, moduleEdgesMat.color.b, moduleEdgesMat.color.a));
                                    GL.Vertex3(firstPoint.x, firstPoint.y, firstPoint.z);
                                    GL.Vertex3(secondPoint.x, secondPoint.y, secondPoint.z);
                                    GL.End();
                                }

                                for (int i = 0; i < 4; i++)
                                {
                                    Vector3 firstPoint = modulesCorners[j][k][i + 4];
                                    Vector3 secondPoint = modulesCorners[j][k][i];
                                    GL.Begin(GL.LINES);
                                    moduleEdgesMat.SetPass(0);
                                    GL.Color(new Color(moduleEdgesMat.color.r, moduleEdgesMat.color.g, moduleEdgesMat.color.b, moduleEdgesMat.color.a));
                                    GL.Vertex3(firstPoint.x, firstPoint.y, firstPoint.z);
                                    GL.Vertex3(secondPoint.x, secondPoint.y, secondPoint.z);
                                    GL.End();
                                }
                            }
                        }
                    }
                }
            }
        }

        // To show the lines in the game window whne it is running
        void OnPostRender()
        {
            DrawConnectingLines();
        }


        // To show the lines in the editor
        void OnDrawGizmos()
        {
            DrawConnectingLines();
        }

    }
}
