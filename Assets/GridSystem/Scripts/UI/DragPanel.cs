﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragPanel : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{

    public bool moveParent = false;
    private Vector3 initPos;
    private Vector3 diff;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    public void OnDrag(PointerEventData eventData)
    {
        if (moveParent)
        {
            transform.parent.GetComponent<RectTransform>().localPosition = Input.mousePosition + diff;
        }
        else
        {
            GetComponent<RectTransform>().localPosition = Input.mousePosition + diff;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (moveParent)
        {
            diff = transform.parent.GetComponent<RectTransform>().localPosition - Input.mousePosition;
        }
        else
        {

            diff = GetComponent<RectTransform>().localPosition - Input.mousePosition;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        diff = Vector3.zero;
    }
}
