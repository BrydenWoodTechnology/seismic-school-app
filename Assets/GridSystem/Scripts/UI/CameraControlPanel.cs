﻿using BrydenWoodUnity.Navigation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


namespace BrydenWoodUnity.Navigation
{
    public class CameraControlPanel : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
    {


        public bool isOn;
        bool checkTouch, checkMouse;


        void Update()
        {
            if (checkTouch || checkMouse)
            {
                isOn = true;
            }
            else if (!checkTouch && !checkMouse)
            {
                isOn = false;
            }

        }
        public void OnPointerEnter(PointerEventData eventData)
        {

            checkMouse = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {

            checkMouse = false;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            checkTouch = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            checkTouch = false;
        }

        public void TurnCCPOn(bool b)
        {
            isOn = b;
        }
    }
}
