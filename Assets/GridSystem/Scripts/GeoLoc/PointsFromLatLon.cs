﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsFromLatLon : MonoBehaviour {

    public Vector2[] latLonLocations;
    public Vector2 latLonCentre;

	// Use this for initialization
	void Start () {

        for (int i=0; i<latLonLocations.Length; i++)
        {
            GameObject obj = new GameObject("Location_" + i);
            obj.transform.position = BrydenWoodUnity.BrydenWoodUtils.XZFromLatLon(latLonCentre.x, latLonCentre.y, latLonLocations[i].x, latLonLocations[i].y);
        }
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
