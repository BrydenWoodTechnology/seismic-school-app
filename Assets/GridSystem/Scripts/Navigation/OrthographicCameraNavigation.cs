﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrthographicCameraNavigation : MonoBehaviour {

    public Transform target;
    public float rotationStep = 90;

    private Camera m_camera;
    private Vector3 rotationCentre;
    private float currentRotation;

	// Use this for initialization
	void Start () {
        m_camera = GetComponent<Camera>();
        UpdateTarget();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnGUI()
    {
        if (GUI.Button(new Rect(230, 10, 200, 20), "Rotate CLockWise"))
        {
            RotateAntiClockWise();
        }

        if (GUI.Button(new Rect(230, 40, 200, 20), "Rotate Anticlockwise"))
        {
            RotateClockWise();
        }
    }

    public void RotateAntiClockWise()
    {
        transform.RotateAround(rotationCentre, Vector3.up, rotationStep);
    }

    public void RotateClockWise()
    {
        transform.RotateAround(rotationCentre, Vector3.up, -rotationStep);
    }

    public void UpdateTarget()
    {
        if (target == null)
        {
            Ray r = m_camera.ScreenPointToRay(new Vector3(m_camera.pixelWidth / 2, m_camera.pixelHeight, 0));
            Plane plane = new Plane(Vector3.up,Vector3.zero);
            float dist = 0;
            if (plane.Raycast(r, out dist))
            {
                rotationCentre = r.GetPoint(dist);
            }
        }
        else
        {
            rotationCentre = target.position;
        }
    }
}
