﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapMove : MonoBehaviour
{

    public Camera orthographicCamera;
    public float zoomStep = 5;
    public float moveStep = 5;



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ZoomIn()
    {
        if (orthographicCamera.orthographicSize > 0)
        {
            orthographicCamera.orthographicSize -= zoomStep;
        }
        else
        {
            orthographicCamera.orthographicSize = 0;
        }
    }
           

    public void ZoomOut()
    {
        orthographicCamera.orthographicSize += zoomStep;
    }

    public void MoveLeftRight(bool right)
    {
        if (right)
            transform.Translate(transform.right.normalized * moveStep);
        else
            transform.Translate(transform.right.normalized * -moveStep);
    }

    public void MoveForwardBack(bool forward)
    {
        if (forward)
            transform.Translate(transform.forward.normalized * moveStep);
        else
            transform.Translate(transform.forward.normalized * -moveStep);
    }



}
