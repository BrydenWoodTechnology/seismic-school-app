﻿mergeInto(LibraryManager.library, {
    
    GetURLFromPage: function () {
        var returnStr = window.top.location.href;
        var buffer = _malloc(lengthBytesUTF8(returnStr) + 1);
        writeStringToMemory(returnStr, buffer);
        return buffer;
    },
	TextUploaderCaptureClick: function(objectNamePar,methodName,elementName) {

                var method = Pointer_stringify(methodName);

                var elementN = Pointer_stringify(elementName);

                var objectName = Pointer_stringify(objectNamePar);

    if (!document.getElementById(elementN))

                {

                      var fileInput = document.createElement('input');

                      fileInput.setAttribute('type', 'file');

                      fileInput.setAttribute('id', elementN);

                      fileInput.setAttribute('accept', 'text/txt');

                      //fileInput.style.visibility = 'hidden';

                      fileInput.onclick = function (event) { this.value = null; };

                      fileInput.onchange = function (event) {

                                fileInput.style.visibility = 'hidden';

                                SendMessage(objectName, method, URL.createObjectURL(event.target.files[0]));

                                }

 

                      document.body.appendChild(fileInput);

                                console.log('file load before clicked');

                                fileInput.click();

                                console.log('file load clicked');

                }

                else

                {

                                document.getElementById(elementN).style.visibility = 'visible';

                                document.getElementById(elementN).click();

                }

  },

DownloadText: function (data, filename, type) {
	var dat = Pointer_stringify(data);
	var fileN = Pointer_stringify(filename);
    var file = new Blob([dat], {type: type});
    if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, fileN);
    else { // Others
        var a = document.createElement("a"),
                url = URL.createObjectURL(file);
        a.href = url;
        a.download = fileN;
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);  
        }, 0); 
    }
},
PopupOpenerCaptureClick: function(link) {
	var lnk = Pointer_stringify(link);
    var OpenPopup = function() {
      window.open(lnk, null, 'width=500,height=500');
      document.getElementById('gameContainer').removeEventListener('click', OpenPopup);
    };
    document.getElementById('gameContainer').addEventListener('click', OpenPopup, false);
  },
  DisplayNotification: function (str) {
    window.alert(Pointer_stringify(str));
  },
  
  DownloadFile : function(array, size, fileNamePtr)
{
    var fileName = UTF8ToString(fileNamePtr);

    var bytes = new Uint8Array(size);
    for (var i = 0; i < size; i++)
    {
       bytes[i] = HEAPU8[array + i];
    }

    var file = new Blob([bytes]);
if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, fileName);
    else { // Others
        var a = document.createElement("a"),
                url = URL.createObjectURL(file);
        a.href = url;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        }, 0);
    }
},

PromptInputField: function(objectNamePar,methodName,title)
	{
		var txt;
		var title = Pointer_stringify(title);
		var method = Pointer_stringify(methodName);
		var objectName = Pointer_stringify(objectNamePar);
	
		var input = prompt(title, "");
		if (input == null || input == "") {
		    txt = "";
		} else {
		    txt = input;
  		}
		SendMessage(objectName, method, txt);
		
	},

});
