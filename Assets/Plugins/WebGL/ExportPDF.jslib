mergeInto(LibraryManager.library, {

ImportPDFLibrary: function()
	{
		var my_awesome_script = document.createElement('script');
		my_awesome_script.setAttribute('src','https://unpkg.com/jspdf@latest/dist/jspdf.min.js');
		document.head.appendChild(my_awesome_script);
	},

ExportPdfReport: function(dataProgram,dataClusters,dataResolve,dataSite,dataExterior,dataModules,dataLocation,dataRooms,fileName)
	{
		var fName = Pointer_stringify(fileName);
		var dtStr = Pointer_stringify(dataClusters);
		var program = Pointer_stringify(dataProgram);
var site = Pointer_stringify(dataSite);
var exterior = Pointer_stringify(dataExterior);
var resolved = Pointer_stringify(dataResolve);
var modules = Pointer_stringify(dataModules);
var location = Pointer_stringify(dataLocation);
var rooms = Pointer_stringify(dataRooms);


		var dc = new jsPDF('portrait');
	      // Positions and sizes are in mm

      //Setting the first page of the document
	   dc.setLineWidth(0.2);
      dc.setDrawColor(150);
      dc.line(8,8,8,288);
	  dc.setLineWidth(0.6);
      dc.line(12,8,12,288);
	  
      dc.setFontSize(20);
      dc.setTextColor(100);
      dc.text('Executive Report', 20,25);
	  
	  dc.setFontSize(18);
      dc.setTextColor(100);
      dc.text('Building Information', 20,35);
	  dc.setFontSize(14);
	  dc.text('Building Program: '+program, 20,43);
	  dc.text( dtStr, 20,58);
	  
	  
	  dc.setFontSize(18);
      dc.setTextColor(100);
      dc.text('Alerts', 20,245);
	  dc.setFontSize(14);
	   dc.text(resolved, 20,255);
	  
	  
	  //Setting the 2nd page of the document
	  dc.addPage();
	  dc.setLineWidth(0.2);
      dc.setDrawColor(150);
      dc.line(8,8,8,288);
	  dc.setLineWidth(0.6);
      dc.line(12,8,12,288);
	  
      dc.setFontSize(20);
      dc.setTextColor(100);
      dc.text('Executive Report', 20,25);
	  
	  
	  
	 dc.setFontSize(18);
      dc.setTextColor(100);
      dc.text('Module Information', 20,35);
	   dc.setFontSize(14);
	   dc.text(modules, 20,43);
	  
	   dc.setFontSize(18);
      dc.setTextColor(100);
      dc.text('Alerts', 20,245);
	  dc.setFontSize(14);
	   dc.text(resolved, 20,255);
	  
	  //Setting the 3rd page of the document
	  dc.addPage();
	  
	  dc.setLineWidth(0.2);
      dc.setDrawColor(150);
      dc.line(8,8,8,288);
	  dc.setLineWidth(0.6);
      dc.line(12,8,12,288);
	  
      dc.setFontSize(20);
      dc.setTextColor(100);
      dc.text('Executive Report', 20,25);
	  
	  
	  
	 dc.setFontSize(18);
      dc.setTextColor(100);
      dc.text('Room Areas', 20,35);
	  dc.setFontSize(14);
	   dc.text(rooms, 20,43);
	  
	  //Setting the 4th page of the document
	  dc.addPage();
	  
	  dc.setLineWidth(0.2);
      dc.setDrawColor(100);
      dc.line(8,8,8,288);
	  dc.setLineWidth(0.6);
      dc.line(12,8,12,288);
	  
	    dc.setFontSize(20);
      dc.setTextColor(120);
      dc.text('Executive Report', 20,25);
      
	  dc.setFontSize(18);
      dc.setTextColor(100);
      dc.text('Site', 20,40);
	   dc.setFontSize(14);
	   dc.text(location, 20,50); 
	   dc.text(site, 20,60);
	   
	  dc.setFontSize(18);
      dc.setTextColor(100);
      dc.text('School Ground', 20,125);
	  dc.setFontSize(14);
	   dc.text(exterior, 20,135);
	  
	   dc.setFontSize(18);
      dc.setTextColor(100);
      dc.text('Alerts', 20,245);
	   
      dc.save(fName+'.pdf');

	},

});