﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.DesignData.Editor
{
    public class WebGLWindow : EditorWindow
    {
        [MenuItem("WebGLHelp/EditorCOmmands")]
        public static void ShowWindow()
        {
            GetWindow<WebGLWindow>(false, "WebGL", true);
        }

        private void OnGUI()
        {
            if (GUILayout.Button("emscriptenargs"))
            {
                PlayerSettings.WebGL.emscriptenArgs = "-s DEMANGLE_SUPPORT=1 -s ASSERTIONS=2 -s -Werror";
            }
        }
    }
}
