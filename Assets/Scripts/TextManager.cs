﻿using BrydenWoodUnity.GeometryManipulation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWood.Interop
{
    public class TextManager : MonoBehaviour
    {
        public GameObject gridManager;
        public GameObject gridText;
        public string clusters;
        public string program = "Free";
        public string grid;
        public string roomAreas;
        public TextMeshProUGUI programText;
        public ClusterManager cMan;

        public Transform configurationParent, detailedConfiguration, mapParent, siteParent;
        public string exterior;
        string resolveAlerts;
        public TextMeshProUGUI exteriorAvailability, totalAreaUI;
        string moduleTypes;
        public string lat { get; set; }
        public string lon { get; set; }
        public string postcode { get; set; }
        string location;
        bool nextPage;



        [DllImport("__Internal")]
        private static extern void ImportPDFLibrary();

        [DllImport("__Internal")]
        private static extern void ExportPdfReport(string dataProgram, string dataClusters, string alert, string dataSite, string dataExterior, string modules, string location, string dataRooms, string filename);

        // Use this for initialization
        void Start()
        {
#if !UNITY_EDITOR
        ImportPDFLibrary();
#endif

            clusters = "";
            grid = "";
            exterior = "";
            postcode = "SW1A 2JR";
            lat = (51.501255).ToString();
            lon = (-0.125004).ToString();
        }

        // Update is called once per frame
        void Update()
        {

        }
        /// <summary>
        /// Finds all meshes of children of a specific parent
        /// </summary>
        /// <param name="parent"></param> hte parent to check
        /// <returns></returns>
        List<GameObject> RecursivelyFindMesh(Transform parent)
        {
            List<GameObject> toExport = new List<GameObject>();
            foreach (Transform c in parent)
            {
                if (c.GetComponent<MeshFilter>() != null)
                {
                    c.name.Replace("(clone)", string.Empty);
                    toExport.Add(c.gameObject);
                }

                if (c.childCount > 0)
                {
                    toExport.AddRange(RecursivelyFindMesh(c));
                }

            }
            return toExport;
        }
        /// <summary>
        /// Export all geometries in obj format
        /// </summary>
        public void ExportOBJ()
        {
            List<GameObject> toExport = new List<GameObject>();
            detailedConfiguration.GetComponent<DetailedConfiguration>().ArrangeDetailedClusters(true);
            toExport.AddRange(RecursivelyFindMesh(detailedConfiguration));
            toExport.AddRange(RecursivelyFindMesh(cMan.transform));
            toExport.AddRange(RecursivelyFindMesh(siteParent));
            string configuration = BW_ObjExporter.ExportMeshes(toExport);
#if UNITY_EDITOR
            File.WriteAllText(Application.streamingAssetsPath + "/obj.obj", configuration);
#else
        ResourcesManager.DownloadText(configuration, "SEISMIC_" + DateTime.Now.ToString() + ".obj", "text/plain");
       
#endif
            detailedConfiguration.GetComponent<DetailedConfiguration>().DestroyAllDetailed();
        }
        /// <summary>
        /// Updates text records based on program selected
        /// </summary>
        public void UpdateText()
        {
            program = gridManager.GetComponent<ProceduralClusterManager>().entry;
            if (programText != null)
            {
                programText.text = "";

                programText.text = "<mark=#ffffff35>School type: " + program + "</mark>";
            }
            clusters = clusters.Replace("<color=#ff0000ff>", string.Empty).Replace("</color>", string.Empty).Replace(" <color =#ffa500ff>", string.Empty).Replace(" <mark=#ffffff35>", string.Empty).Replace("</mark>", string.Empty);

            cMan.GenerateRequired(program);
        }
        /// <summary>
        /// Updates UI text
        /// </summary>
        public void UpdateUIText()
        {
            exteriorAvailability.text = exterior;
            totalAreaUI.text = roomAreas;
        }

        public void OnReset()
        {
            UpdateText();

        }
        /// <summary>
        /// Gathers all info and saves as a pdf
        /// </summary>
        public void OnExportPdf()
        {
            int halfcount = 0;
            int moduleCount = 0;
            int stairCount = 0;
            int moduleNoCircCount = 0;
            foreach (Transform c in configurationParent)
            {
                c.GetComponent<PlaceablePrefab>().CheckAdjacencies(1.8f, false);
                if (c.GetComponent<ProceduralCluster>().halfRight && c.GetComponent<PlaceablePrefab>().adjacent[0] == null || c.GetComponent<ProceduralCluster>().halfLeft && c.GetComponent<PlaceablePrefab>().adjacent[2] == null)
                {
                    halfcount++;
                }
                c.GetComponent<PlaceablePrefab>().ClearAdjacencies();
                if (c.GetComponent<ProceduralCluster>().hasCirculation && !c.name.Contains("Stair"))
                {
                    moduleCount += c.GetComponent<ProceduralCluster>().modules.Count;
                }
                else if (!c.GetComponent<ProceduralCluster>().hasCirculation && !c.name.Contains("Stair"))
                {
                    moduleNoCircCount += c.GetComponent<ProceduralCluster>().modules.Count;
                }
                else if (c.name.Contains("Stair"))
                {
                    stairCount++;
                }


            }
            if (halfcount > 0)
            {
                resolveAlerts = string.Format("There are still {0} half modules that need to be resolved", halfcount.ToString());
            }

            else
            {
                resolveAlerts = string.Empty;
            }

            moduleTypes = string.Format("The configuration contains:{3}{3}{0} number of modules with circulation{3}{1} number of modules without circulation{3}{2} number of stair modules", moduleCount.ToString(), moduleNoCircCount.ToString(), stairCount.ToString(), "\n");

            string location = "Location: " + postcode + "/" + lat + "," + lon;
            string filename = "Executive Report_" + DateTime.Now.ToString();

            clusters = clusters.Replace("<color=#ff0000ff>", string.Empty).Replace("</color>", string.Empty).Replace(" <color =#ffa500ff>", string.Empty).Replace("<mark=#ffffff35>", string.Empty).Replace("</mark>", string.Empty);
            program = program.Replace("<color=#ff0000ff>", string.Empty).Replace("<color =#ffa500ff>", string.Empty).Replace("<mark=#ffffff35>", string.Empty).Replace("</mark>", string.Empty).Replace("</color>", string.Empty);
            grid = grid.Replace("<color=#ff0000ff>", string.Empty).Replace("<color =#ffa500ff>", string.Empty).Replace("<mark=#ffffff35>", string.Empty).Replace("</mark>", string.Empty).Replace("</color>", string.Empty);
            exterior = exterior.Replace("<color=#ff0000ff>", string.Empty).Replace("<color =#ffa500ff>", string.Empty).Replace("<mark=#ffffff35>", string.Empty).Replace("</mark>", string.Empty).Replace("</color>", string.Empty);
            location = location.Replace("<color=#ff0000ff>", string.Empty).Replace("<color =#ffa500ff>", string.Empty).Replace("<mark=#ffffff35>", string.Empty).Replace("</mark>", string.Empty).Replace("</color>", string.Empty);
            roomAreas = roomAreas.Replace("<color=#ff0000ff>", string.Empty).Replace("<color =#ffa500ff>", string.Empty).Replace("<mark=#ffffff35>", string.Empty).Replace("</mark>", string.Empty).Replace("</color>", string.Empty);

            ExportPdfReport(program, clusters, resolveAlerts, grid, exterior, moduleTypes, location, roomAreas, filename);
        }
    }
}

