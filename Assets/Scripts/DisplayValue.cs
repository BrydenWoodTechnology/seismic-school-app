﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayValue : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowValue(float value)
    {
        GetComponent<Text>().text = value.ToString()+ "\xBo";
    }

    public void ShowValues(float value)
    {
        GetComponent<Text>().text = (value-1).ToString();
    }
}
