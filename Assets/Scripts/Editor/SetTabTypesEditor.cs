﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SetTabType))]
[CanEditMultipleObjects]
public class SetTabTypesEditor : Editor {

    [SerializeField]
    private int index { get; set; }
    //int index;

    private void OnEnable()
    {
        SetTabType setType = (SetTabType)target;
        index = (int)setType.tabtype;
    }

    public override void OnInspectorGUI()
    {

        base.OnInspectorGUI();

        SetTabType setType = (SetTabType)target;

        //string[] types = new string[setType.tab_types.Length];
        //int types_size = setType.tab_types.Length;
        //for(int i =0; i<types_size; i++)
        //{
        //    types[i] = setType.tab_types[i].name;
        //}
        //index = EditorGUILayout.Popup("Select Tab Type", index, types);
        //setType.TabSelecttor(index);

        if(index != (int)setType.tabtype)
        {
            setType.TabSelecttor((int)setType.tabtype);
            index = (int)setType.tabtype;
        }
    }

}
