﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;


[CustomEditor(typeof(RibbonSetup))]
[System.Serializable]
public class RibbonSetupEditor : Editor
{
    [SerializeField]
    int tab_num = 0;
    Color32 bookmark_color = Color.yellow;
    string title = "Tab Title";
    [SerializeField]
    bool expand = true;

    [SerializeField]
    List<bool> tab_drop_values = new List<bool>();

    TabTypes type;

    SerializedProperty TemplateImage;
    SerializedProperty TabTypes;
    SerializedProperty createdTabs;
    SerializedProperty bookmarkCols;
    SerializedProperty bookmarkTitles;
    SerializedProperty bookmarkBools;
    SerializedProperty bookmarIcons;


    private void OnEnable()
    {
        TemplateImage = serializedObject.FindProperty("ColorTemplate");
        TabTypes = serializedObject.FindProperty("_tabs");
        createdTabs = serializedObject.FindProperty("current_tabs");
        bookmarkCols = serializedObject.FindProperty("tab_colors");
        bookmarkTitles = serializedObject.FindProperty("tab_titles");
        bookmarkBools = serializedObject.FindProperty("tab_drops");
        bookmarIcons = serializedObject.FindProperty("tab_icons");
    }
    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        //base.OnInspectorGUI();

        RibbonSetup ribbon = (RibbonSetup)target;

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Populate Ribbon Menu", EditorStyles.boldLabel);
        EditorGUILayout.Space();

        EditorGUILayout.PropertyField(TabTypes, true);
        GUILayout.Space(10);

        if (GUILayout.Button("Populate Ribbon"))
        {

            if (ribbon.transform.childCount > 0)
            {
                ribbon.ClearRibbon();
                for (int i = ribbon.transform.childCount; i > 0; i--)
                {
                    DestroyImmediate(ribbon.transform.GetChild(0).gameObject);
                }
            }

            if (ribbon.transform.childCount == 0)
            {
                ribbon.PopulateRibbon();
                tab_num += ribbon._tabs.Count;
                Debug.Log("Tab_num: " + tab_num.ToString());
            }
            Debug.Log("Tabs " + tab_num.ToString());

        }

        GUILayout.BeginHorizontal();


        if (GUILayout.Button("Add Tab"))
        {

            ribbon.AddOneRibbonItem(type);
            tab_num++;
            Debug.Log("Tab_num: " + tab_num.ToString());
        }

        type = (TabTypes)EditorGUILayout.EnumPopup(type);

        GUILayout.EndHorizontal();

        if (GUILayout.Button("Clear Ribbon"))
        {
            ribbon.ReadyToPlay(true);
            ribbon.ClearRibbon();
            for (int i = ribbon.transform.childCount; i > 0; i--)
            {
                DestroyImmediate(ribbon.transform.GetChild(0).gameObject);
            }
            tab_num = 0;
            tab_drop_values.Clear();
            ribbon._tabs.Clear();

        }
        GUILayout.Space(15);

        if (GUILayout.Button("Colorize with Image"))
        {

            ribbon.DominantColors(ribbon.current_tabs.Count);
            for (int t = 0; t < ribbon.current_tabs.Count; t++)
            {
                bookmarkCols.GetArrayElementAtIndex(t).colorValue = ribbon.temp_centroids[t];
            }
        }

        EditorGUILayout.PropertyField(TemplateImage);


        GUILayout.Space(15);
        EditorGUILayout.LabelField("Tabs Menu", EditorStyles.boldLabel);
        EditorGUILayout.Space();

       // PopulateTabsMenu(ribbon);
        //ApplyTabAttributes(ribbon);

        EditorUtility.SetDirty(ribbon);

        serializedObject.ApplyModifiedProperties();
    }

    public void PopulateTabsMenu(RibbonSetup myribbon)
    {
        int tab_length = myribbon.current_tabs.Count;
        if (tab_length != 0)
        {
            expand = EditorGUILayout.Foldout(expand, "Current Tabs");


            if (bookmarkCols.arraySize == myribbon.current_tabs.Count)
            {
                for (int j = 0; j < tab_length; j++)
                {

                    try
                    {
                        tab_drop_values.Add(bookmarkBools.GetArrayElementAtIndex(j).boolValue);

                    }
                    catch (Exception e)
                    {
                        int bookmarks = bookmarkBools.arraySize;
                        Debug.Log("ERROR: " + e);

                    }

                }

                //Debug.Log(tab_drop_values.Count.ToString());
                if (expand)
                {
                    EditorGUI.indentLevel++;
                    EditorGUILayout.Space();

                    for (int i = 0; i < tab_length; i++)
                    {
                        tab_drop_values[i] = EditorGUILayout.Foldout(tab_drop_values[i], myribbon.current_tabs[i].name);
                        //tab_drop_values[i] = EditorGUILayout.Foldout(tab_drop_values[i], createdTabs.GetArrayElementAtIndex(i).name);


                        if (tab_drop_values[i])
                        {
                            //tab_Colors[i] = EditorGUILayout.ColorField("bookmark color", tab_Colors[i]);
                            //tab_titles[i] = EditorGUILayout.TextField("Bookmark Title", tab_titles[i]);
                            EditorGUILayout.PropertyField(bookmarkCols.GetArrayElementAtIndex(i), new GUIContent("bookmark color"));
                            EditorGUILayout.PropertyField(bookmarkTitles.GetArrayElementAtIndex(i), new GUIContent("bookmark title"));
                            EditorGUILayout.PropertyField(bookmarIcons.GetArrayElementAtIndex(i), new GUIContent("bookmark icon"));


                        }

                        //----TRY CHANGE == tab_num with == myribbon.current_tabs.Count
                        //if (bookmarkCols.arraySize == tab_num)
                        if (bookmarkCols.arraySize == myribbon.current_tabs.Count)
                        {
                            try
                            {
                                Color32 book_col = bookmarkCols.GetArrayElementAtIndex(i).colorValue;
                                myribbon.transform.GetChild(i).GetChild(0).GetComponent<Image>().color = book_col;
                                Color32 checkmark_col = new Color32(book_col.r, book_col.g, book_col.b, 100);
                                myribbon.transform.GetChild(i).GetComponentInChildren<Toggle>().graphic.color = checkmark_col;

                                Toggle current_tog = myribbon.transform.GetChild(i).GetComponentInChildren<Toggle>();
                                current_tog.targetGraphic.GetComponent<Image>().sprite = (Sprite)bookmarIcons.GetArrayElementAtIndex(i).objectReferenceValue;

                                myribbon.transform.GetChild(i).GetChild(1).GetComponent<Text>().text = bookmarkTitles.GetArrayElementAtIndex(i).stringValue;

                            }
                            catch (Exception e)
                            {
                                int bookColors = bookmarkCols.arraySize;
                                Debug.Log(e);
                            }

                        }


                        EditorGUILayout.Space();

                    }
                }
            }
        }
    }



    public void ApplyTabAttributes(RibbonSetup myribbon)
    {
        for (int i = 0; i < /*tab_num - 1*/ myribbon.current_tabs.Count; i++)
        {
            //myribbon.transform.GetChild(i).GetChild(0).GetComponent<Image>().color = tab_Colors[i];
            //myribbon.transform.GetChild(i).GetChild(1).GetComponent<Text>().text = tab_titles[i];
            //Color32 checkmark_col = new Color32(tab_Colors[i].r, tab_Colors[i].g, tab_Colors[i].b, 100);
            //myribbon.transform.GetChild(i).GetComponentInChildren<Toggle>().graphic.color = checkmark_col;

            //Debug.Log("ColorAtIndex"+ i.ToString()+"   " +bookmarkCols.GetArrayElementAtIndex(i).colorValue.ToString());
            //Debug.Log("ColorSize" + i.ToString() + "   " + bookmarkCols.arraySize.ToString());

            if (bookmarkCols.arraySize == myribbon.current_tabs.Count)
            {

                try
                {
                    Color32 book_col = bookmarkCols.GetArrayElementAtIndex(i).colorValue;
                    myribbon.transform.GetChild(i).GetChild(0).GetComponent<Image>().color = book_col;
                    Color32 checkmark_col = new Color32(book_col.r, book_col.g, book_col.b, 100);
                    myribbon.transform.GetChild(i).GetComponentInChildren<Toggle>().graphic.color = checkmark_col;
                }
                catch (Exception e)
                {
                    int bookColors = bookmarkCols.arraySize;
                    Debug.Log(e);
                }
                myribbon.transform.GetChild(i).GetChild(1).GetComponent<Text>().text = bookmarkTitles.GetArrayElementAtIndex(i).stringValue;
                //Debug.Log("Titles:" +bookmarkTitles.arraySize.ToString());


            }

        }

    }

    public void ApplyTabAttributes()
    {
        for (int i = 0; i < createdTabs.arraySize; i++)
        {
            var ribbon_child = createdTabs.GetArrayElementAtIndex(i);
            var ribbon_tab = ribbon_child.exposedReferenceValue;


        }
    }


    public void DefineTabTitle(RibbonSetup myribbon)
    {
        for (int i = 0; i < myribbon.current_tabs.Count; i++)
        {

        }
    }





}
