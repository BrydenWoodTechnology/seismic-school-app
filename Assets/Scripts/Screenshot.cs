﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using BrydenWood.Interop;

namespace BrydenWoodUnity.UIElements
{
    public class Screenshot : MonoBehaviour
    {
        public GameObject[] uiElements;
        public ResourcesManager resourcesLoader;
         Camera cam;
        public bool hasRenderTexture;

       
        private bool[] alreadyOn;

        private void Start()
        {
            alreadyOn = new bool[uiElements.Length];
            cam = GetComponent<Camera>();
        }
        /// <summary>
        /// Downloads a screen capture of the set camera 
        /// </summary>
        public void ScreenShot()
        {
            StartCoroutine(TakeScreenShot());
        }
        /// <summary>
        /// Saves camera view as 2D texture
        /// </summary>
        /// <returns></returns>
        private IEnumerator TakeScreenShot()
        {
            
            for (int i = 0; i < uiElements.Length; i++)
            {
                alreadyOn[i] = uiElements[i].activeSelf;
                uiElements[i].SetActive(false);
            }
            yield return new WaitForEndOfFrame();
            Texture2D screenShot = new Texture2D(cam.pixelWidth, cam.pixelHeight, TextureFormat.RGB24, false);
            if (hasRenderTexture)
            {
                RenderTexture currentRT = RenderTexture.active;
                RenderTexture.active = cam.targetTexture;

                cam.Render();

                screenShot = new Texture2D(cam.pixelWidth, cam.pixelHeight, TextureFormat.RGB24, false);
                screenShot.ReadPixels(new Rect(0, 0, cam.pixelWidth, cam.pixelHeight), 0, 0);
                screenShot.Apply();
                RenderTexture.active = currentRT;
            }
            else
            {
                screenShot = new Texture2D(cam.pixelWidth, cam.pixelHeight, TextureFormat.RGB24, false);
                screenShot.ReadPixels(new Rect(0, 0, cam.pixelWidth, cam.pixelHeight), 0, 0);
                screenShot.Apply();
            }
            

            resourcesLoader.ExportScreenShot(screenShot.EncodeToPNG());

            yield return new WaitForEndOfFrame();
            for (int i = 0; i < uiElements.Length; i++)
            {
                uiElements[i].SetActive(alreadyOn[i]);
            }
        }
    }
}
