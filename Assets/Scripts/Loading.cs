﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour {

    public GameObject loadingCanvas;
    public Slider LoadingBar;

    AsyncOperation async;
    WaitForSeconds delay = new WaitForSeconds(1f);

	// Use this for initialization
	void Start () {
        StartCoroutine(LoadingScreen());
        //async = SceneManager.LoadSceneAsync(0);
        //async.allowSceneActivation = false;

    }
	
	// Update is called once per frame
	void Update () {

    }

    public IEnumerator LoadingScreen()
    {
        yield return delay;
        loadingCanvas.SetActive(true);
        async = SceneManager.LoadSceneAsync(1);
        //async.allowSceneActivation = false;


        while(async.isDone == false)
        {
            //LoadingBar.value = async.progress;
            //if (async.progress == 0.9f)
            //{
            //    //LoadingBar.value = 1f;
            //    async.allowSceneActivation = true;
            //}
            yield return null;
        }
        //if (async.isDone == true)
        //{
        //    async.allowSceneActivation = true;
        //}
            
    }
    
}
