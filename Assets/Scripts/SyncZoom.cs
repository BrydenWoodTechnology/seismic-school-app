﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyncZoom : MonoBehaviour {

    public Camera m_camera;

    private float prevZoom;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (prevZoom != m_camera.orthographicSize)
        {
            GetComponent<Camera>().orthographicSize = m_camera.orthographicSize;
            prevZoom = m_camera.orthographicSize;
        }
	}
}
