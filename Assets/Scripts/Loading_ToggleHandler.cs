﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Loading_ToggleHandler : MonoBehaviour {

    [Header("Referenced Elements")]
    [Space(5)]
    public RectTransform three_toggle;
    public RectTransform model_toggle;

    [Space(10)]
    [Header("UI Elements")]
    [Space(5)]
    public Sprite pressed;
    public Sprite unPressed;

    // Use this for initialization
    void Start () {
        Screen_Manager.ToggleStates += ChangeToggles;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ChangeToggles(bool One, bool Two)
    {
        //three_toggle.GetComponent<Toggle>().isOn = One;
        //model_toggle.GetComponent<Toggle>().isOn = Two;

        if (One == true)
        {
            three_toggle.GetComponent<Toggle>().image.sprite = pressed;
            model_toggle.GetComponent<Toggle>().image.sprite = unPressed;
        }
        if (Two == true)
        {
            three_toggle.GetComponent<Toggle>().image.sprite = unPressed;
            model_toggle.GetComponent<Toggle>().image.sprite = pressed;
        }

        Debug.Log("three = " + One.ToString());
        Debug.Log("model = " + Two.ToString());
    }
}
