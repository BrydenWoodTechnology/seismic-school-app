﻿using BrydenWoodUnity.Navigation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Recalculates touch inlut for webgl
/// </summary>
public class DoubleTouchScript : MonoBehaviour
{

    public Vector2 prevPos;
    public Vector2 currentPos;
    public bool started = false;
    public Text debugText;
    public float mouseDeltaX;
    public float mouseDeltaY;

    private float storedX;
    private float storedY;
    private float prevStoredX;
    private float prevStoredY;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.touchCount >0)
        {
            if (!started)
            {
                prevPos = Input.GetTouch(0).position;
                started = true;
            }
            currentPos = Input.GetTouch(0).position;

            storedX = (currentPos - prevPos).x;
            storedY = (currentPos - prevPos).y;
            mouseDeltaX = storedX - prevStoredX;
            mouseDeltaY = storedY - prevStoredY;
            if (debugText != null)
                debugText.text = "Mouse Delta X : " + (currentPos - prevPos).x + "\r\nMouseDelta Y : " + (currentPos - prevPos).y;
            prevStoredX = storedX;
            prevStoredY = storedY;
        }
        else
        {
            if (debugText != null)
                debugText.text = "No Touch";
            mouseDeltaX = 0;
            mouseDeltaY = 0;
            storedX = 0;
            prevStoredX = 0;
            storedY = 0;
            prevStoredY = 0;
            started = false;
        }
    }
}
