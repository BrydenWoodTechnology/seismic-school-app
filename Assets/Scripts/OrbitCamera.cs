using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using BrydenWoodUnity.Grid;
using UnityEngine.EventSystems;
using System.Diagnostics;

namespace BrydenWoodUnity.Navigation
{
    /// <summary>
    /// Navigation modes for mouse interaction
    /// </summary>
    public enum NavigationMode
    {
        Select = 0,
        Pan = 1,
        Zoom = 2,
        Rotate = 3,
        Full = 4,
    }
    public enum CameraType
    {
        mainCamera,
        extraCamera
    }

    [RequireComponent(typeof(Camera))]
    public class OrbitCamera : MonoBehaviour
    {
        public Transform target;
        public float orbitScale = 2;
        public float panScale = 0.5f;
        public float zoomScale = 3;
        public float orthographicZoomScale = 1;
        public float minDistance = 1;
        public float maxYAngle = 80;
        public float minOrthographicSize = 1;
        public float YOrbitLimit = 1;
        public NavigationMode navigationMode = NavigationMode.Select;
        public CameraType cameratype;

        [Header("Scene References:")]
        public Selector selector;
        public CanvasGroup cpCanvasGroup;
        public MeshlessGridManager gridManager;
        public CameraControlPanel CCP;
        public bool allowNav { get; set; }
        private Plane targetPlane;
        public float distance;
        private Vector3 projectedForward;
        private Vector3 prev_euler;
        bool creation = true;
        Vector3 lastPos;
        Vector3 lastRotation;
        float prevZoomRate = 40;
        bool move = false;
        bool orbit = true;
        bool allowzoom, allowPan, allowOrbit;
        float initialZoomScale, initialPanScale, initialOrbitScale;
        Vector3 targetRot, targetInitialPos, initialPos, endPos;

        Vector2 initialDist, initialTouch;
        Vector2 previousTouch;
        
        
        bool start;
        
        
        Vector3 startRotTarget;
        float theta, phi;

        // Use this for initialization
        void Start()
        {
            distance = Vector3.Distance(transform.position, target.position);
            target.LookAt(transform);
            targetRot = new Vector3();
            initialZoomScale = zoomScale;
            initialOrbitScale = orbitScale;
            initialPanScale = panScale;
            initialPos = transform.position;
            targetInitialPos = target.position;
            allowOrbit = true;
           

        }

        // Update is called once per frame
        void Update()
        {
            bool eventCheck = Input.touchCount > 0 ? !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId) : !EventSystem.current.IsPointerOverGameObject(-1);
            if (gridManager.currentGrid != null)
            {
                creation = !gridManager.currentGrid.placingAdditional;
            }
            switch (cameratype)
            {
                case CameraType.mainCamera:
                    if (eventCheck && creation)
                    {
                        allowNav = true;
                    }
                    else
                    {
                        allowNav = false;
                    }

                    break;
                case CameraType.extraCamera:
                    if (CCP.isOn == true)
                    {
                        allowNav = true;
                    }
                    else
                    {
                        allowNav = false;
                    }

                    break;
            }
            if (allowNav)
            {

                if (Selector.TaggedObject.selected != null && Input.touchCount > 0)
                {
                    allowPan = false;

                }
                else
                {
                    allowPan = true;

                }
                switch (navigationMode)
                {
                    case NavigationMode.Select:
                        SelectNavigationMode();
                        break;
                    case NavigationMode.Pan:
                        PanNavigationMode();
                        break;
                    case NavigationMode.Rotate:
                        OrbitNavigationMode();
                        break;
                    case NavigationMode.Zoom:
                        ZoomNavigationMode();
                        break;
                    case NavigationMode.Full:
                        FullNavigationMode();
                        break;
                }
                // target.eulerAngles = new Vector3(target.eulerAngles.x, target.eulerAngles.y, 0);
                transform.position = target.position + target.forward * distance;
                transform.LookAt(target, Vector3.up);
            }

        }

        public void SetNavigationMode(int index)
        {
            navigationMode = (NavigationMode)index;
            selector.enabled = navigationMode == NavigationMode.Select;
            cpCanvasGroup.blocksRaycasts = navigationMode == NavigationMode.Select;
        }

        public void TogglePlanView(bool plan)
        {
            if (plan)
            {
                orbit = false;
                lastPos = transform.position;
                lastRotation = transform.eulerAngles;
                targetRot = target.eulerAngles;
                target.eulerAngles = new Vector3(-88, targetRot.y, targetRot.z);
                transform.position = target.position + new Vector3(0, 100, 0);
                transform.eulerAngles = new Vector3(90, 0, 0);
            }
            else
            {
                orbit = true;
                transform.position = lastPos;
                transform.eulerAngles = lastRotation;
                target.eulerAngles = targetRot;

            }
            if (gridManager.sitePolygon != null)
            {
                target.transform.position = gridManager.sitePolygon.Center;
            }
        }

        private void SelectNavigationMode()
        {
            if (Input.touchCount > 0)
            {

                if (Input.touchCount > 1)
                {
                    switch (Input.GetTouch(0).phase)
                    {
                        case TouchPhase.Began:
                          
                            initialDist = Input.GetTouch(0).position - Input.GetTouch(1).position;
                            break;
                        case TouchPhase.Moved:
                            var newdist = Input.GetTouch(0).position - Input.GetTouch(1).position;
                            if (Mathf.Abs(initialDist.magnitude - newdist.magnitude) > Mathf.Abs(initialDist.magnitude) * 0.2f)
                            {
                                allowzoom = true;
                                allowOrbit = false;
                            }
                            else
                            {
                                allowzoom = false;
                                allowOrbit = true;
                            }

                            break;
                        case TouchPhase.Ended:
                           
                            break;
                    }
                }
            }
            else
            {
                allowOrbit = true;
                allowzoom = true;
                allowPan = true;
            }
            Orbit(1);
            Pan(2);
            Zoom(-1);
        }

        private void PanNavigationMode()
        {
            Pan(0);
            Zoom(-1);
        }

        private void ZoomNavigationMode()
        {
            allowzoom = true;
            Zoom(0);
        }

        private void OrbitNavigationMode()
        {
            if (Input.touchCount > 0)
            {

                if (Input.touchCount == 1)
                {

                    allowzoom = false;
                    allowOrbit = true;
                }
                else if (Input.touchCount > 1)
                {
                    allowzoom = true;
                    allowOrbit = false;
                }

            }
            else
            {
                allowzoom = true;
                allowOrbit = true;
            }
            Orbit(0);
            Zoom(-1);
        }

        private void FullNavigationMode()
        {
            if (Input.touchCount > 0)
            {

                if (Input.touchCount > 1)
                {

                    allowzoom = true;
                    allowOrbit = false;
                }
                else
                {
                    allowzoom = false;
                    allowOrbit = true;
                }

            }
            else
            {
                allowzoom = true;
                allowOrbit = true;
            }
            Orbit(0);
            Pan(1);
            Zoom(-1);
        }

        private void Pan(int mouseButtonIndex)
        {
            if (allowPan)
            {
                if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    panScale = 0.2f;

                    target.position += -target.right * Input.GetTouch(0).deltaPosition.x * panScale;

                    target.position += -target.up * -Input.GetTouch(0).deltaPosition.y * panScale;
                }
                else if (Input.GetMouseButton(mouseButtonIndex))
                {
                    panScale = initialPanScale;
                    var deltaX = Input.GetAxis("Mouse X");
                    target.position += target.right * deltaX * panScale;
                    var deltaY = -Input.GetAxis("Mouse Y");
                    target.position += target.up * deltaY * panScale;
                }
            }
        }

        private void Orbit(int mouseButtonIndex)
        {
            if (allowOrbit)
            {
                if ((Input.touchCount == 2  || mouseButtonIndex == 0&& Input.touchCount>0) && orbit )
                {
                    theta += GetComponent<DoubleTouchScript>().mouseDeltaY;
                    if (theta > 85)
                        theta = 85;
                    if (theta < 5)
                        theta = 5;

                    phi -= GetComponent<DoubleTouchScript>().mouseDeltaX * 0.5f;



                    float x = distance * Mathf.Sin(Mathf.Deg2Rad * theta) * Mathf.Cos(Mathf.Deg2Rad * phi);
                    float z = distance * Mathf.Sin(Mathf.Deg2Rad * theta) * Mathf.Sin(Mathf.Deg2Rad * phi);
                    float y = distance * Mathf.Cos(Mathf.Deg2Rad * theta);



                    target.LookAt(new Vector3(x, y, z), Vector3.up);
                }
                else if (Input.touchCount == 0 && Input.GetMouseButton(mouseButtonIndex) && orbit)
                {
                    orbitScale = initialOrbitScale;
                    projectedForward = new Vector3(target.transform.forward.x, 0, target.transform.forward.z);
                    float prevAngle = Vector3.Angle(target.transform.forward, projectedForward);
                    if (transform.position.y >= YOrbitLimit)
                        prev_euler = target.eulerAngles;
                    var deltaX = Input.GetAxis("Mouse X");
                    target.RotateAround(target.position, Vector3.up, deltaX * orbitScale);
                    var deltaY = Input.GetAxis("Mouse Y");
                    target.RotateAround(target.position, target.right, deltaY * orbitScale);
                    projectedForward = new Vector3(target.transform.forward.x, 0, target.transform.forward.z);
                    if (Vector3.Angle(target.transform.forward, projectedForward) > maxYAngle && prevAngle < maxYAngle)
                    {
                        target.eulerAngles = prev_euler;
                    }
                    if (transform.position.y < YOrbitLimit)
                    {
                        transform.position = new Vector3(transform.position.x, YOrbitLimit, transform.position.z);
                        target.eulerAngles = prev_euler;
                    }
                }
            }
        }
        private void Zoom(int mouseButtonIndex)
        {
            if (allowzoom)
            {
                if (!GetComponent<Camera>().orthographic)
                {
                    if (Input.touchCount == 2 && Input.GetTouch(0).phase == TouchPhase.Moved && Input.GetTouch(1).phase == TouchPhase.Moved&&mouseButtonIndex!=0)
                    {
                        zoomScale = 0.2f;
                        Touch touchzero = Input.GetTouch(0);
                        Touch touchone = Input.GetTouch(1);
                        Vector2 prevZero = touchzero.position - touchzero.deltaPosition;
                        Vector2 prevOne = touchone.position - touchone.deltaPosition;


                        var delta = (touchzero.position - touchone.position).magnitude - (prevZero - prevOne).magnitude;
                        var newDist = distance - delta * zoomScale;
                        if (newDist < minDistance - 1)
                        {
                            distance = minDistance + 1;
                        }
                        else
                        {
                            distance += delta * zoomScale;
                        }
                    }
                    else if (mouseButtonIndex == -1)
                    {
                        zoomScale = initialZoomScale * 3;
                        var delta = -Input.GetAxis("Mouse ScrollWheel");
                        var newDist = distance + delta * zoomScale;
                        if (newDist < minDistance - 1)
                        {
                            distance = minDistance;
                        }
                        else
                        {
                            distance += delta * zoomScale;
                        }
                    }
                    else
                    {
                        if (Input.GetMouseButton(mouseButtonIndex))
                        {
                            zoomScale = initialZoomScale;
                            var delta = Input.touchCount > 0 ? Input.GetTouch(0).deltaPosition.y * 0.1f : -Input.GetAxis("Mouse Y");
                            var newDist = distance + delta * zoomScale;
                            if (newDist < minDistance - 1)
                            {
                                distance = minDistance;
                            }
                            else
                            {
                                distance += delta * zoomScale;
                            }
                        }

                    }
                }
                else
                {
                    if (Input.touchCount == 2 && Input.GetTouch(0).phase == TouchPhase.Moved && Input.GetTouch(1).phase == TouchPhase.Moved&&mouseButtonIndex!=0)
                    {
                        zoomScale = 0.7f;
                        Touch touchzero = Input.GetTouch(0);
                        Touch touchone = Input.GetTouch(1);
                        Vector2 prevZero = touchzero.position - touchzero.deltaPosition;
                        Vector2 prevOne = touchone.position - touchone.deltaPosition;


                        var delta = (touchzero.position - touchone.position).magnitude - (prevZero - prevOne).magnitude;
                        var newOrthoSize = GetComponent<Camera>().orthographicSize + delta * zoomScale;
                        if (newOrthoSize < minOrthographicSize)
                        {
                            GetComponent<Camera>().orthographicSize = minOrthographicSize + 1;
                        }
                        else
                        {
                            GetComponent<Camera>().orthographicSize += delta * zoomScale;

                        }
                    }
                    else if (mouseButtonIndex == -1)
                    {
                        zoomScale = initialZoomScale;
                        var delta = -Input.GetAxis("Mouse ScrollWheel");
                        var newOrthoSize = GetComponent<Camera>().orthographicSize + delta * zoomScale;
                        if (newOrthoSize < minOrthographicSize - 1)
                        {
                            GetComponent<Camera>().orthographicSize = minOrthographicSize + 1;
                        }
                        else
                        {
                            GetComponent<Camera>().orthographicSize += delta * zoomScale;
                        }
                    }
                    else
                    {
                        if (Input.GetMouseButton(mouseButtonIndex))
                        {
                            zoomScale = initialZoomScale;
                            var delta =Input.touchCount>0?Input.GetTouch(0).deltaPosition.y*0.1f:-Input.GetAxis("Mouse Y");
                           
                            var newOrthoSize = GetComponent<Camera>().orthographicSize + delta * zoomScale;
                            if (newOrthoSize < minOrthographicSize - 1)
                            {
                                GetComponent<Camera>().orthographicSize = minOrthographicSize + 1;
                            }
                            else
                            {
                                GetComponent<Camera>().orthographicSize += delta * zoomScale;
                            }
                        }
                    }
                }
            }
        }
    }
}
