﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BrydenWoodUnity.Navigation;
using BrydenWood.Interop;


public class ResetAll : MonoBehaviour {

    public Transform[] parents;
    public TextManager textMan;
    public ProceduralClusterManager clusterMan;
    public DrawGLines gLines;
    public Toggle programSelection;
    public GameObject landingP1, landingP2;
    public Button setSite, setGrid;
    public Toggle clusterMenu;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		
	}
    public void OnReset()
    {
        
        clusterMan.entry = "Free";
        clusterMan.UpdateProgram();
        clusterMan.UpdateText();
        textMan.OnReset();
        

    }

}
