﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InputType
{
    ParentOfPoints,
    PointsAsGameObjects,
    PointsAsVectors
}

public class PolylineInterpolation : MonoBehaviour
{

    #region Public Fields
    [Header("Inputs for creating the polyline:")]
    public InputType inputType = InputType.ParentOfPoints;
    [Tooltip("The parent of the gameObjects which represent the points (set when input type is ParentOfPoints)")]
    public Transform parentOfPoints;
    [Tooltip("A list of gameObjects which represent the points (set when input type is PointsAsGameObjects")]
    public List<Transform> pointsAsGameObjects;
    [Tooltip("A list of vectors for the points (set when input type is PointsAsVectors")]
    public List<Vector3> pointsAsVectors;

    [Space(10)]
    [Header("Input for Interpolation")]
    [Tooltip("The parameter along the polyline (float from 0 to PointNumber - 1)")]
    public float parameter = 0.0f;
    [Tooltip("The current position along the polyline")]
    public Vector3 currentPosition = Vector3.zero;

    [HideInInspector]
    public float minPar = 0.0f;
    [HideInInspector]
    public float maxPar = 1.0f;
    #endregion

    // Use this for initialization
    void Start()
    {

        //switch (inputType)
        //{
        //    case InputType.ParentOfPoints:
        //        SetPoints(parentOfPoints);
        //        break;
        //    case InputType.PointsAsGameObjects:
        //        SetPoints(pointsAsGameObjects);
        //        break;
        //    case InputType.PointsAsVectors:
        //        SetPoints(pointsAsVectors);
        //        break;
        //}
    }


    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// Gets the position along a polyline
    /// </summary>
    /// <param name="par">The length parameter along a polyline</param>
    /// <param name="polyline">The polyline as a list of vectors</param>
    /// <returns>The world position along the polyline</returns>
    public Vector3 GetPositionOnPolyline(float par, List<Vector3> polyline)
    {
        if (polyline.Count != 0)
        {

            Vector3 m_pos = Vector3.zero;

            int parStart = Mathf.FloorToInt(par);
            int parEnd = Mathf.CeilToInt(par);

            Vector3 start = polyline[parStart];
            Vector3 end = polyline[parEnd];
            Vector3 dif = end - start;

            float parDif = par - parStart;

            m_pos = start + dif * parDif;

            return m_pos;
        }
        else
        {
            return Vector3.zero;
        }
    }

    /// <summary>
    /// Sets the parameter along the polyline and recalculates the current position
    /// </summary>
    /// <param name="par">The parameter along the polyline</param>
    public void SetParameter(float par)
    {
        try
        {
            parameter = par;
            currentPosition = GetPositionOnPolyline(parameter, pointsAsVectors);
            transform.position = currentPosition;
        }
        catch (Exception e)
        {
            print(parameter + ":" + pointsAsVectors.Count);
        }

    }

    public void SetParameterNormalized(float par)
    {
        SetParameter(par * (pointsAsVectors.Count-1));

    }

    /// <summary>
    /// Sets new points from a parent GameObject
    /// </summary>
    /// <param name="parent">The parent GameObject</param>
    public void SetPoints(Transform parent)
    {
        pointsAsVectors = new List<Vector3>();
        for (int i = 0; i < parent.childCount; i++)
        {
            pointsAsVectors.Add(parent.GetChild(i).position);
        }
        maxPar = pointsAsVectors.Count;
        currentPosition = GetPositionOnPolyline(parameter, pointsAsVectors);
        transform.position = currentPosition;
    }

    /// <summary>
    /// Sets new points from a list of gameObjects
    /// </summary>
    /// <param name="gameObjects">The list of gameObjects</param>
    public void SetPoints(List<Transform> gameObjects)
    {
        pointsAsVectors = new List<Vector3>();
        for (int i = 0; i < gameObjects.Count; i++)
        {
            pointsAsVectors.Add(gameObjects[i].position);
        }
        maxPar = pointsAsVectors.Count;
        currentPosition = GetPositionOnPolyline(parameter, pointsAsVectors);
        transform.position = currentPosition;
    }

    /// <summary>
    /// Sets new points from a list of Vectors
    /// </summary>
    /// <param name="points">A list of Vectors</param>
    public void SetPoints(List<Vector3> points)
    {
        pointsAsVectors = new List<Vector3>();
        pointsAsVectors = points;
        maxPar = pointsAsVectors.Count;
        currentPosition = GetPositionOnPolyline(parameter, pointsAsVectors);
        transform.position = currentPosition;
    }
}
