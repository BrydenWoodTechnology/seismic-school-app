﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BatchPlacement : MonoBehaviour
{



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void MyMessage()
    {


    }

    public void CreateBatch(string input)
    {
        
        if (transform.childCount > 2)
        {
            GameObject[] slots = new GameObject[transform.childCount];
            for (int i = 2; i < transform.childCount; i++)
            {
                slots[i] = transform.GetChild(i).gameObject;
            }
            for(int j=2;j< transform.childCount; j++)
            {
                DestroyImmediate(slots[j].gameObject);
            }
        }
       
        int n = int.Parse(input);
        if (n > 20)
        {
            n = 20;
        }

        Transform myslot = transform.GetChild(0);
        for (int i = 1; i < n; i++)
        {
            Transform slot = Instantiate(myslot, transform);
            slot.localPosition = new Vector3(myslot.localPosition.x + (i * 2.85f), myslot.localPosition.y, myslot.localPosition.z);

        }
        var box = gameObject.GetComponent<BoxCollider>();
        box.center = new Vector3((n - 1) * 1.5f, box.center.y, box.center.z);
        box.size = new Vector3(3 * (n - 1), box.size.y, box.size.z);
    }


}
