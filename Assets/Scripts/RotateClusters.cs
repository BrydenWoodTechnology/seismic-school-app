﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using BrydenWoodUnity.Grid;
using BrydenWoodUnity.GeometryManipulation;


public class RotateClusters : MonoBehaviour, IPointerUpHandler, IPointerDownHandler
{
    public Transform configurationParent, meshlessGridManager, clusterManager;
    public Vector3 origGridPos;
    public List<Vector3> additionalPos;
    List<Transform> children;

    public void OnPointerDown(PointerEventData eventData)
    {
        additionalPos.Clear();
        
        foreach (Transform cluster in configurationParent)
        {
            if (cluster.GetComponent<PlaceablePrefab>().grid == meshlessGridManager.GetComponent<MeshlessGridManager>().currentGrid)
            {
                cluster.GetComponent<PlaceablePrefab>().SelectGroup();
            }
        }
        //foreach (Transform additional in clusterManager)
        //{
        //    GameObject temp = new GameObject();
        //    temp.transform.position = additional.GetComponent<BoxCollider>().center;
        //    additional.SetParent(temp.transform);
        //    temp.transform.SetParent(clusterManager);
        //    additionalPos.Add(additional.GetComponent<BoxCollider>().center);
        //}

        // origGridPos= meshlessGridManager.GetComponent<MeshlessGridManager>().currentGrid.transform.position;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        meshlessGridManager.SendMessage("OnDeselect",SendMessageOptions.DontRequireReceiver);
        foreach (Transform cluster in configurationParent)
        {
            if (cluster.GetComponent<PlaceablePrefab>().grid == meshlessGridManager.GetComponent<MeshlessGridManager>().currentGrid)
            {
                meshlessGridManager.GetComponent<MeshlessGridManager>().currentGrid.PlaceCluster(cluster.position, cluster.gameObject);
            }
                
            // cluster.SendMessage("DeSelect");
        }
        foreach (Transform cluster in configurationParent)
        {
            //meshlessGridManager.GetComponent<MeshlessGridManager>().currentGrid.PlaceCluster(cluster.position, cluster.gameObject);
            cluster.SendMessage("DeSelect",SendMessageOptions.DontRequireReceiver);
            
        }
        for (int i=0;i<clusterManager.childCount;i++)
        {
            var additional = clusterManager.GetChild(i);
            //var add = additional.GetChild(0);
            //additional.GetChild(0).SetParent(clusterManager);
            //Destroy(additional);
            additional.transform.localEulerAngles =new Vector3(additional.transform.localEulerAngles.x, meshlessGridManager.GetComponent<MeshlessGridManager>().currentGrid.transform.localEulerAngles.y, additional.transform.localEulerAngles.z);
            meshlessGridManager.GetComponent<MeshlessGridManager>().currentGrid.PlaceAdditional(additional.position, additional.gameObject);
            //additional.transform.position = additionalPos[i];
        }

            // meshlessGridManager.GetComponent<MeshlessGridManager>().currentGrid.transform.position= origGridPos;
        }

    public void RotateAdditional(float angle)
    {
        foreach (Transform additional in clusterManager)
        {
            var rot = additional.transform.localEulerAngles;// + new Vector3(0, angle, 0);
                                    //  Debug.Log(currentObject.transform.localEulerAngles.ToString());
            //additional.transform.localEulerAngles = new Vector3(rot.x, rot.y + angle, rot.z);
            additional.transform.Rotate(additional.GetComponent<BoxCollider>().center+Vector3.up, angle);
        }

    }
    // Use this for initialization
    void Start()
    {
        additionalPos = new List<Vector3>();
    }

    // Update is called once per frame
    void Update()
    {


    }
}
