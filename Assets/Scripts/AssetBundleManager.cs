﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using BrydenWoodUnity;

namespace BrydenWoodUnity.GeometryManipulation
{
    public class AssetBundleManager : Tagged<AssetBundleManager>
    {

        public string bundleName = "DetailedClusters";
        public Text loadingDescription;
        public Toggle configurationViewer;
        public Toggle clusterViewer;

        public AssetBundle bundle { get; set; }
        public bool loading { get; private set; }

        private Coroutine loadingCoroutine;


        // Use this for initialization
        void Start()
        {
            configurationViewer.interactable = false;
            clusterViewer.interactable = false;
#if UNITY_EDITOR
            if (!String.IsNullOrEmpty(bundleName))
            {

                var path1 = Path.Combine(Path.Combine(Application.streamingAssetsPath, "AssetsBundles"), bundleName);
                if (File.Exists(path1))
                {
                    //if (bundle == null)
                    LoadFromAssetBundle(path1);
                    loadingDescription.text = "Loading Assets";
                }
            }
#elif UNITY_WEBGL
        if (!String.IsNullOrEmpty(bundleName))
        {
            loadingCoroutine = StartCoroutine(LoadFromAssetBundleWeb(Path.Combine(Path.Combine(Application.streamingAssetsPath, "AssetsBundles"), bundleName)));
            loadingDescription.text = "Loading Assets";
        }
#endif
        }

        private new void OnDestroy()
        {
            if (loadingCoroutine != null)
                StopCoroutine(loadingCoroutine);
            if (bundle != null)
                bundle.Unload(true);
        }

        // Update is called once per frame
        void Update()
        {
            if (loading)
            {
                if (Time.frameCount % 150 == 0)
                {
                    loadingDescription.text = "Loading Assets";
                }
                else if (Time.frameCount % 30 == 0)
                {
                    loadingDescription.text += ".";
                }
            }
            else if (!loading && !String.IsNullOrEmpty(loadingDescription.text))
            {
                loadingDescription.text = String.Empty;
            }


        }
        /// <summary>
        /// Load a bundle
        /// </summary>
        /// <param name="path"></param> the path to load from
        public void LoadFromAssetBundle(string path)
        {

            bundle = AssetBundle.LoadFromFile(path);
            if (bundle == null)
            {
                Debug.Log("Failed to load AssetBundle!");
                return;
            }
            configurationViewer.interactable = true;
            clusterViewer.interactable = true;
        }
        /// <summary>
        /// Load a bundle(web)
        /// </summary>
        /// <param name="path"></param>the path to load from
        /// <returns></returns>
        public IEnumerator LoadFromAssetBundleWeb(string path)
        {
            if (path.Contains("://") || path.Contains(":///"))
            {
                loading = true;
                UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(path);
                yield return request.SendWebRequest();
                if (request.isNetworkError || request.isHttpError)
                {
                    Debug.Log(request.error);
                }
                else
                {
                    bundle = DownloadHandlerAssetBundle.GetContent(request);
                }
                loading = false;
                configurationViewer.interactable = true;
                clusterViewer.interactable = true;
            }
        }
        /// <summary>
        /// load a specific asset from a bundle
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public GameObject GetAsset(string name)
        {
            GameObject temp = bundle.LoadAsset<GameObject>(name);
            return temp;
        }
    }
}
