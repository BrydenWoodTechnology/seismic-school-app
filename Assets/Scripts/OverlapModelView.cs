﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class OverlapModelView : MonoBehaviour
{


    public Material clusterMain;
    public Transform configurationParent;
    List<Material> myMat;
    public ProceduralClusterManager clManager;
    public Material mat;
    // Use this for initialization
    void Start()
    {
      //  myMat = clManager.clusterMaterials.Values.ToList();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OverlapModels(bool isOver)
    {

        if (!isOver)
        {
            GetComponent<Camera>().cullingMask &= ~(1 << 13);
            GetComponent<Camera>().cullingMask &= ~(1 << 24);
            // mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, 90 / 255);
            //for (int i = 0; i < myMat.Count; i++)
            //{
            //    myMat[i].SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
            //    myMat[i].SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            //    myMat[i].SetInt("_ZWrite", 0);
            //    myMat[i].DisableKeyword("_ALPHATEST_ON");
            //    myMat[i].DisableKeyword("_ALPHABLEND_ON");
            //    myMat[i].EnableKeyword("_ALPHAPREMULTIPLY_ON");
            //    myMat[i].renderQueue = 3000;
            //    myMat[i].color = new Color(myMat[i].color.r, myMat[i].color.g, myMat[i].color.b,127);
            //}
            // clusterMain.color = new Color(clusterMain.color.r, clusterMain.color.g, clusterMain.color.b,50/255);
        }
        else
        {
            GetComponent<Camera>().cullingMask |= (1 << 13);
            GetComponent<Camera>().cullingMask |= (1 << 24);

            // mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, 255 / 255);
            //for (int i = 0; i < myMat.Count; i++)
            //{
            //    myMat[i].SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
            //    myMat[i].SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
            //    myMat[i].SetInt("_ZWrite", 1);
            //    myMat[i].DisableKeyword("_ALPHATEST_ON");
            //    myMat[i].DisableKeyword("_ALPHABLEND_ON");
            //    myMat[i].DisableKeyword("_ALPHAPREMULTIPLY_ON");
            //    myMat[i].renderQueue = -1;
            //    //myMat[i].color = new Color(myMat[i].color.r, myMat[i].color.g, myMat[i].color.b, 255 / 255);
            //}
            // clusterMain.color = new Color(clusterMain.color.r, clusterMain.color.g, clusterMain.color.b, 255/255);
        }

    }
}
