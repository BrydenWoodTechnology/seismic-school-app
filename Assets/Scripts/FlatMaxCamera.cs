﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BrydenWoodUnity.Navigation
{
    public class FlatMaxCamera : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {




        public Camera topCam, topCam2;
        bool onMinimap = false;
        GameObject pointed;

        public float panSpeed = 15;
        public float zoomRate = 40;

        float originalZoomRate;
        float originalPanSpeed;
        public GameObject topViewImage, topViewImage2;


        // Use this for initialization
        void Start()
        {
            originalZoomRate = zoomRate;
            originalPanSpeed = panSpeed;
        }

        // Update is called once per frame
        void Update()
        {

            if (onMinimap)
            {
                if (Input.GetMouseButton(2) || (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved))
                {
                    Pan();
                }
                Zoom();
                topCam2.transform.position = topCam.transform.position;
                topCam2.orthographicSize = topCam.orthographicSize;
            }
        }

        void Pan()
        {

            if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved)
            {

                panSpeed = 0.1f;
                topCam.transform.Translate(Vector3.right * Input.GetTouch(0).deltaPosition.x * panSpeed);
                topCam.transform.Translate(transform.forward * Input.GetTouch(0).deltaPosition.y * panSpeed, Space.World);
                topCam2.transform.position = topCam.transform.position;
            }
            else
            {
                panSpeed = originalPanSpeed;
                topCam.transform.Translate(Vector3.right * -Input.GetAxis("Mouse X") * panSpeed);
                topCam.transform.Translate(transform.forward * -Input.GetAxis("Mouse Y") * panSpeed, Space.World);
                topCam2.transform.position = topCam.transform.position;
            }


        }
        void Zoom()
        {
            if (Input.touchCount == 2 && Input.GetTouch(0).phase == TouchPhase.Moved && Input.GetTouch(1).phase == TouchPhase.Moved)
            {
                zoomRate = 0.1f;
                Touch touchzero = Input.GetTouch(0);
                Touch touchone = Input.GetTouch(1);
                Vector2 prevZero = touchzero.position - touchzero.deltaPosition;
                Vector2 prevOne = touchone.position - touchone.deltaPosition;
                var delta = (touchzero.position - touchone.position).magnitude - (prevZero - prevOne).magnitude;
                var newOrthoSize = topCam.orthographicSize + delta * zoomRate;
                if (newOrthoSize < 5)
                {
                    topCam.orthographicSize = 5;
                    topCam2.orthographicSize = 5;
                }
                else
                {
                    topCam.orthographicSize += delta * zoomRate;
                    topCam2.orthographicSize += delta * zoomRate;
                }
            }
            else
            {
                zoomRate = originalZoomRate;
                topCam.orthographicSize -= Input.GetAxis("Mouse ScrollWheel") * zoomRate;
                topCam2.orthographicSize = topCam.orthographicSize;

            }

        }


        public void OnPointerEnter(PointerEventData eventData)
        {

            onMinimap = true;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            onMinimap = false;

        }

    }
}
