﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ThrowUINotification : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public enum UIType
    {
        grid,
        clusters,
        mapData,
        site,
        circulation,
        detailed,
        delSite

    }

    public UIType myType;

    public Text notification;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    /// <summary>
    /// If a UI element is not interactable throw a notification why
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerEnter(PointerEventData eventData)
    {

        switch (myType)
        {
            case UIType.grid:
                if (!GetComponent<Button>().IsInteractable())

                    notification.text = "You need to set a site first";
                StartCoroutine(ErrorText());

                break;

            case UIType.clusters:
                if (GetComponent<Toggle>()!=null&&!GetComponent<Toggle>().IsInteractable()|| GetComponent<Slider>()!=null&&!GetComponent<Slider>().IsInteractable())
                    notification.text = "You need to add a grid first";
                StartCoroutine(ErrorText());
                break;
            case UIType.mapData:
                if (!GetComponent<Toggle>().IsInteractable())
                    notification.text = "You need to load a map first";
                StartCoroutine(ErrorText());
                break;
            case UIType.site:
                if (!GetComponent<Button>().IsInteractable())
                    notification.text = "You have already placed a site";
                StartCoroutine(ErrorText());
                break;

            case UIType.circulation:
                if (!GetComponent<Toggle>().IsInteractable())
                    notification.text = "This cluster does not have circulation";
                StartCoroutine(ErrorText());
                break;
            case UIType.detailed:
                if (!GetComponent<Toggle>().IsInteractable())
                    notification.text = "Assets are still loading ---->";
                StartCoroutine(ErrorText());
                break;
            case UIType.delSite:
                if (!GetComponent<Button>().IsInteractable())
                    notification.text = "You started building on this site";
                break;


        }
    }
    IEnumerator ErrorText()
    {
        yield return new WaitForSeconds(2f);
        notification.text = "";

    }
    public void OnPointerExit(PointerEventData eventData)
    {
        notification.text = "";
    }
}
