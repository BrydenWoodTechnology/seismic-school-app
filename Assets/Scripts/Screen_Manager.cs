﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Screen_Manager : MonoBehaviour
{

    public RectTransform Intro;
    public RectTransform Select_loading;
    public RectTransform Load_three;
    public RectTransform Load_model;

    int childcount;
    WaitForSeconds delay;
    bool three;
    bool model;
    //UnityEvent switchScreen = new UnityEvent();

    public delegate void OnToggleDelegate(bool togOne, bool togTwo);
    public static event OnToggleDelegate ToggleStates;

    // Use this for initialization
    void Start()
    {
        childcount = transform.childCount;
        delay = new WaitForSeconds(0.15f);
        three = false;
        model = false;

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnIntroSelection()
    {
        Select_loading.SetSiblingIndex(childcount - 1);
    }




    public void OnLoadingSelection(Toggle load_method)
    {

        if (load_method.isOn == true)
        {
            if (load_method.name == "3D.model_toggle")
            {

                //StartCoroutine(SwitchScreen(Load_model, Load_three));
                Load_model.SetSiblingIndex(childcount - 1);
                if (Load_model.gameObject.activeSelf == false)
                {
                    Load_model.gameObject.SetActive(true);
                    Load_three.gameObject.SetActive(true);
                }


                model = true;
                three = false;
                ToggleStates(three, model);

                //Debug.Log("ModelView" + three.ToString() + ","+model.ToString());


            }
            else if (load_method.name == "Three.js_toggle")
            {

                //StartCoroutine(SwitchScreen(Load_three, Load_model));
                Load_three.SetSiblingIndex(childcount - 1);
                if (Load_three.gameObject.activeSelf == false)
                {
                    Load_three.gameObject.SetActive(true);
                    Load_model.gameObject.SetActive(true);
                }


                model = false;
                three = true;
                ToggleStates(three, model);
                //Debug.Log("ThreeJSView" + three.ToString() + "," + model.ToString());
            }
        }
    }
    public void OnModelLoaded()
    {
        //change scene
        SceneManager.LoadScene("GLA_UI_Guidelines_Model_Scene",LoadSceneMode.Single);
    }

    public IEnumerator SwitchScreen(RectTransform front, RectTransform other)
    {
        yield return delay;
        front.SetSiblingIndex(childcount - 1);
        if (front.gameObject.activeSelf == false)
        {
            front.gameObject.SetActive(true);
            other.gameObject.SetActive(true);
        }
        Debug.Log(front.gameObject.name);
    }
}
