﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BrydenWoodUnity.GeometryManipulation
{
    /// <summary>
    /// A MonoBehaviour component for exporting obj data
    /// </summary>
    public class BW_ObjExporter : MonoBehaviour
    {
        private static int counter;

        /// <summary>
        /// Returns the string data of a list of meshes in Obj format
        /// </summary>
        /// <param name="meshes">The list of meshes to be exported</param>
        /// <returns>String</returns>
        public static string ExportMeshes(List<GameObject> meshes)
        {
            StringBuilder sb = new StringBuilder();
            int meshCount = meshes.Count;
            counter = 0;
            for (int i = 0; i < meshCount; i++)
            {
                GameObject current_mesh = meshes[i];
                Mesh m_now = new Mesh();
               // m_now = CombineMeshes(current_mesh);//current_mesh.GetComponent<MeshFilter>().mesh;
                m_now =myMesh(meshes[i]);
                //current_mesh.GetComponent<MeshFilter>().transform.position = current_mesh.GetComponent<MeshFilter>().transform.localToWorldMatrix;

                MeshToString(m_now, sb, current_mesh.name,current_mesh.transform);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Converts a mesh into a sting of Obj format
        /// </summary>
        /// <param name="m">The mesh to be converted</param>
        /// <param name="sb">The stringbuilder on to which the data will be attached</param>
        /// <param name="name">The name of the mesh</param>
        public static void MeshToString(Mesh m, StringBuilder sb, string name, Transform kid)
        {
            sb.Append("g ").Append(name).Append("\r\n");
            
            foreach (Vector3 v in m.vertices)
            {
                Vector3 localVertex = kid.TransformPoint(v);
                sb.Append(string.Format("v {0} {1} {2}\r\n", localVertex.x, localVertex.y, -localVertex.z));
            }
            sb.Append("\r\n");
            foreach (Vector3 v in m.normals)
            {
                sb.Append(string.Format("vn {0} {1} {2}\r\n", v.x, v.y, -v.z));
            }
            sb.Append("\r\n");
            foreach (Vector3 v in m.uv)
            {
                sb.Append(string.Format("vt {0} {1}\r\n", v.x, v.y));
            }
            for (int ma = 0; ma < m.subMeshCount; ma++)
            {
                sb.Append("\r\n");
                int[] triangles = m.GetTriangles(ma);

                for (int i = 0; i < triangles.Length; i += 3)
                {
                    sb.Append(string.Format("f {1}/{1}/{1} {0}/{0}/{0} {2}/{2}/{2}\r\n",
                        counter + triangles[i] + 1, counter + triangles[i + 1] + 1, counter + triangles[i + 2] + 1));
                }
            }
            counter += m.vertexCount;

            sb.Append("\r\n");
        }

        /// <summary>
        /// Combines the meshes of the children of an object into a single mesh
        /// </summary>
        /// <param name="thisChild">The parent object</param>
        /// <returns>Unity Mesh</returns>
        public static Mesh CombineMeshes(GameObject thisChild)
        {
            Mesh this_mesh = new Mesh();
            List<MeshFilter> meshFilters = new List<MeshFilter>();
            meshFilters.AddRange(thisChild.GetComponentsInChildren<MeshFilter>(true));
            if (thisChild.GetComponent<MeshFilter>() != null)
            {
                MeshFilter my_mesh = thisChild.GetComponent<MeshFilter>();
                meshFilters.Add(my_mesh);
            }
            CombineInstance[] combine = new CombineInstance[meshFilters.Count];

            int i = 0;
            while (i < meshFilters.Count)
            {
                combine[i].mesh = meshFilters[i].sharedMesh;
                combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
                
                i++;
            }
            this_mesh.CombineMeshes(combine,false);
            thisChild.SetActive(true);
            return this_mesh;
        }

        public static Mesh myMesh (GameObject myObj)
        {
            Mesh this_mesh = new Mesh();
            this_mesh = myObj.GetComponent<MeshFilter>().sharedMesh;

            return this_mesh;
        }
    }
}