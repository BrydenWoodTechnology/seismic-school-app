﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using BrydenWood.Interop;

public class LoadScene : MonoBehaviour {
    public ResourcesManager resourcesManager;
    public string currentEntry = "Free";
    public GameObject loading;
    public GameObject project;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void OnContinue()
    {
        resourcesManager.entry = currentEntry;
        StartCoroutine(Loading(1));
    }
    IEnumerator Loading(int level)
    {
        if (loading != null)
            loading.SetActive(true);
        project.SetActive(false);
        AsyncOperation async = SceneManager.LoadSceneAsync(level);
        yield return async;
    }
}
