﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.Navigation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SiteZoom : MonoBehaviour
{
    RectTransform[] myPoints;
    public float width,height;
    public Coroutine zoomOnSite;
    public PolygonDrawer site;
    public Transform polygonParent;
    // Use this for initialization
    void Start()
    {
        myPoints = new RectTransform[2];
    }

    // Update is called once per frame
    void Update()
    {
       if(Input.GetKey(KeyCode.L))
        {

            //FindLongerDistance(transform);
            StartCoroutine(ZoomOnSite());
        }

    }

    

    public IEnumerator ZoomOnSite()
    {
        FindLongerDistance(transform);
        Vector3 cam_pos_now = Camera.main.transform.position;
        for (int i=0;i<100;i++)
        {
            if(Mathf.Abs(myPoints[0].position.x - myPoints[1].position.x)<width && Mathf.Abs(myPoints[0].position.y - myPoints[1].position.y)<height)
            //if (Vector2.Distance(myPoints[0].position, myPoints[1].position) <Vector2.Distance(new Vector2(width,height),Vector2.zero))
            {
                Camera.main.GetComponent<OrbitCamera>().target.transform.position = polygonParent.GetChild(0).GetComponent<MeshRenderer>().bounds.center;
                Camera.main.orthographicSize -= 5;

               
              
            }
            yield return null;
        }

        
    }

    void FindLongerDistance(Transform parent)
    {
        float distance = 0;
        myPoints = new RectTransform[2];
        for (int i = 0; i < parent.childCount ; i++)
        {
            if(i== parent.childCount-1)
            {
                float d =Vector2.Distance( parent.GetChild(i).GetComponent<RectTransform>().position , parent.GetChild(0).GetComponent<RectTransform>().position);
                if (d > distance)
                {
                    distance = d;

                    myPoints[0] = parent.GetChild(i).GetComponent<RectTransform>();
                    myPoints[1] = parent.GetChild(0).GetComponent<RectTransform>();
                }
            }
            else
            {
                float d = Vector2.Distance(parent.GetChild(i).GetComponent<RectTransform>().position, parent.GetChild(0).GetComponent<RectTransform>().position);
                if (d > distance)
                {
                    distance = d;

                    myPoints[0] = parent.GetChild(i).GetComponent<RectTransform>();
                    myPoints[1] = parent.GetChild(i + 1).GetComponent<RectTransform>();
                }
            }
           
        }

    }
}
