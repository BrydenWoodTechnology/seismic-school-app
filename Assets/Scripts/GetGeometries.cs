﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using Mapbox.Geocoding;
using System.Globalization;
using System;
using Mapbox.Unity;
using Mapbox.Examples;
using Mapbox.Json;
using Mapbox.Utils.JsonConverters;

public class GetGeometries : MonoBehaviour
{
    [Header("Mapbox Objects")]
    [Tooltip("The gameobject with the abstract map script")]
    public Transform citySimulator;
    string _searchInput = "";
    List<Feature> _features;
    ForwardGeocodeResource _resource;
    public ForwardGeocodeUserInput _searchLocation;
    bool _isSearching = false;
    [Header("UI Objects")]
    public Button load;

    public InputField longitude, latitude, postcode;


    public float currentLat { get; set; }
    public float currentLon { get; set; }

    [Header("Site & Existing building")]
    public Button delExisting;
    public int layerMask;
    List<Transform> existing;
    bool hasSetFocus = false;

    private Vector2 latLon;
    //----UK bounding box
    float maxLatitude = 60.89f;
    float minLatitude = 49.95f;
    float maxLongitude = 2.38f;
    float minLongitude = -6.51f ;

    public bool mapLoaded;
    public Toggle mapdata;
    public bool existingDeleted;

    public delegate void OnLocationChanged(float lat, float lon);
    public static event OnLocationChanged locationChanged;

    public Text notification;



    private void OnDestroy()
    {
        if (locationChanged != null)
        {
            foreach (Delegate del in locationChanged.GetInvocationList())
            {
                locationChanged -= (OnLocationChanged)del;
            }
        }
    }



    // Use this for initialization
    void Start()
    {
        load.onClick.AddListener(LoadMap);
        delExisting.onClick.AddListener(DeleteExisting);
        _searchLocation.OnGeocoderResponse += SearchLocation_OnGeocoderResponse;
        longitude.text = (-0.1257383).ToString();
        latitude.text = (51.499466).ToString();
        layerMask = 1 << 31;
        existing = new List<Transform>();
        _resource = new ForwardGeocodeResource("");
    }


    // Update is called once per frame
    void Update()
    {

        if (existingDeleted)
        {
            if (existing.Count > 0)
            {
                if (existing[0].position.y < -100)
                {
                    for (int i = 0; i < existing.Count; i++)
                    {

                        DestroyImmediate(existing[i].gameObject);

                    }

                    existing.Clear();
                    existingDeleted = false;
                }
            }
        }
    }
    public void SetLatLon(float lat, float lon)
    {
        latLon = new Vector2((float)lat, (float)lon);
        latitude.text = latLon.x.ToString();
        longitude.text = latLon.y.ToString();
        LoadMap();
        currentLat = latLon.x;
        currentLon = latLon.y;
        if (locationChanged != null)
        {
            locationChanged(latLon.x, latLon.y);
        }
    }
    IEnumerator ErrorText()
    {
        yield return new WaitForSeconds(4f);
        notification.text = "";

    }
    public void LoadMap()
    {
        float lat = 0;
        float lon = 0;

        if (latitude.text != null && longitude.text != null)
        {
            if (float.Parse(latitude.text.Replace(" ", string.Empty)) <= maxLatitude && float.Parse(latitude.text.Replace(" ", string.Empty)) >= minLatitude&& float.Parse(longitude.text.Replace(" ", string.Empty)) >= minLongitude&& float.Parse(longitude.text.Replace(" ", string.Empty))<=maxLongitude)
            {
                lat = float.Parse(latitude.text.Replace(" ", string.Empty));
                lon = float.Parse(longitude.text.Replace(" ", string.Empty));
                print("within uk");
                notification.text = "";
            }
            else
            {
                lat = 51.499466f;
                lon = -0.1257383f;
                notification.text = "The location you set is not in the UK";
                StartCoroutine(ErrorText());
            } 

            

            Vector2d latlong = new Vector2d(lat, lon);
            citySimulator.GetComponent<AbstractMap>().SetCenterLatitudeLongitude(latlong);
        }

        if (citySimulator.childCount == 0)
        {
            citySimulator.GetComponent<AbstractMap>().SetUpMap();
        }
        else
        {
            Vector2d latlong = new Vector2d(lat, lon);
            citySimulator.GetComponent<AbstractMap>().UpdateMap(latlong, citySimulator.GetComponent<AbstractMap>().Zoom);
        }
        mapdata.interactable = true;
        mapLoaded = true;

    }

    public void DeleteExisting()
    {

        foreach (Transform tile in citySimulator)
        {

            foreach (Transform b in tile)
            {
                if (Physics.Raycast(b.GetComponent<MeshRenderer>().bounds.center, -Vector3.up, Mathf.Infinity, layerMask))
                {
                    existing.Add(b);

                    b.gameObject.AddComponent<Rigidbody>();
                    b.gameObject.AddComponent<MeshCollider>();
                    b.GetComponent<MeshCollider>().convex = true;
                    b.GetComponent<MeshCollider>().isTrigger = true;
                    b.GetComponent<Rigidbody>().isKinematic = false;
                    b.GetComponent<Rigidbody>().useGravity = true;

                }
            }
        }
        existingDeleted = true;


    }
    #region LOAD MAP WITH POSTCODE
    void SearchLocation_OnGeocoderResponse(ForwardGeocodeResponse response)
    {
        
        latLon = new Vector2((float)_searchLocation.Coordinate.x, (float)_searchLocation.Coordinate.y);
        latitude.text = latLon.x.ToString();
        longitude.text = latLon.y.ToString();
        LoadMap();
        currentLat = latLon.x;
        currentLon = latLon.y;
        if (locationChanged != null)
        {
            locationChanged(latLon.x, latLon.y);
        }
    }

    #endregion
}
