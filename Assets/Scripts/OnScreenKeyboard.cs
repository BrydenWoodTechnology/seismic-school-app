using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class OnScreenKeyboard : MonoBehaviour, IPointerClickHandler
{
    public enum TypeOfInput
    {
        normal,
        numbers,
        letters

    }
    public TypeOfInput myInput;
    TouchScreenKeyboard keyboard;
    System.Diagnostics.Process myPro;
    private InputField inputField;
    public string title = "Test Input Field";

    [DllImport("__Internal")]
    private static extern void PromptInputField(string objectName, string methodName, string title);

    // Use this for initialization
    void Start()
    {
        // myPro= System.Diagnostics.Process.Start("osk.exe");
#if UNITY_EDITOR
#elif UNITY_WEBGL
        inputField = GetComponent<InputField>();
#endif
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    public void OnPopKeyboard()
    {
        switch (myInput)
        {
            case TypeOfInput.normal:
                keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default);
                // System.Diagnostics.Process.Start("osk.exe");
                break;
            case TypeOfInput.numbers:
                keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.NumberPad);
                // System.Diagnostics.Process.Start("osk.exe");
                break;
            case TypeOfInput.letters:
                keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.ASCIICapable);
                // System.Diagnostics.Process.Start("osk.exe");
                break;


        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
#if UNITY_EDITOR
        OnPopKeyboard();
#elif UNITY_WEBGL
         PromptInputField(gameObject.name, "OnValueReceived", title);
#endif
    }

    public void OnValueReceived(string txt)
    {
        inputField.text = txt;
    }
}
