﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleImages : MonoBehaviour {
    public GameObject image1;
    public GameObject image2;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnToggle(bool onOff)
    {
        image1.SetActive(onOff);
        image2.SetActive(!onOff);
    }
}
