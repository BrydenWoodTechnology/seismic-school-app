﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ActivatetheTraffic : MonoBehaviour {

    Camera cam;
   // public bool trafficIstrue,greenTrue,buildTrue;
    public int traffic, green, buildings,flood, floodHist;
    //int number0 = 0;
    //int number1 = 1;
    //int number2 = 2;
    //int number3 = 3;
    //int number4 = 4;
    //int number5 = 5;

    //int number8 = 8;
    //int number9 = 9;
    //int number10 = 10;
    //int number11 = 11;
    //int number12 = 12;
    //int number13 = 13;
    //int number14 = 14;
    //int number16 = 16;
    //int number17 = 17;
    //int number19 = 19;

   // public Toggle trafficOn,greenOn,buildOn,floodOn,histFloodOn;


    void Start () {
        //trafficIstrue = trafficOn.isOn;
        //greenTrue = greenOn.isOn;
        //buildTrue = buildOn.isOn;
        cam = GetComponent<Camera>();
    }
	
	// Update is called once per frame
	void Update () {

        //int layermask0 = 1 << number0;
        //int layermask1 = 1 << number1;
        //int layermask2 = 1 << number2;
        //int layermask3 = 1 << number3;
        //int layermask4 = 1 << number4;
        //int layermask5 = 1 << number5;

        //int layermask8 = 1 << number8;
        //int layermask9 = 1 << number9;
        //int layermask10 = 1 << number10;
        //int layermask11 = 1 << number11;
        //int layermask12 = 1 << number12;
        //int layermask13 = 1 << number13;
        //int layermask14 = 1 << number14;
        //int layermask16 = 1 << number16;
        //int layermask17 = 1 << number17;
        //int layermask19 = 1 << number19;

        //int ActivateWIthout19 = layermask0 | layermask1 | layermask2 | layermask3 | layermask4 | layermask5 | layermask8 | layermask9 | layermask10 | layermask11 | layermask12 | layermask13 | layermask14 | layermask16 | layermask17;
        //int ActivateAllWith19 = layermask0 | layermask1 | layermask2 | layermask3 | layermask4 | layermask5 | layermask8 | layermask9 | layermask10 | layermask11 | layermask12 | layermask13 | layermask14 | layermask16 | layermask17 | layermask19;

        //if (trafficIstrue == true)
        //{
        //    cam.cullingMask = ActivateAllWith19;
        //}

        //else if (trafficIstrue == false)
        //{
        //    cam.cullingMask = ActivateWIthout19;
        //}
	}

    public void TurnTrafficOn(bool t)
    {
       
        if (t != true)
        {
            cam.cullingMask &= ~(1 << traffic);
        }
        else
        {
            cam.cullingMask |= (1 << traffic);
        }
    }
    public void TurnGreenOn(bool t)
    {
        
        if(t!=true)
        {
            cam.cullingMask &= ~(1 << green);
        }
        else
        {
            cam.cullingMask |= (1 << green);
        }
        
    }
    public void TurnBuildOn(bool t)
    {
        
        if (t != true)
        {
            cam.cullingMask &= ~(1 << buildings);
        }
        else
        {
            cam.cullingMask |= (1 << buildings);
        }
    }

    public void TurnFloodWarnOn(bool t)
    {
        
        if (t != true)
        {
            cam.cullingMask &= ~(1 << flood);
        }
        else
        {
            cam.cullingMask |= (1 << flood);
        }
    }

    public void TurnFloodHistOn(bool t)
    {

        if (t != true)
        {
            cam.cullingMask &= ~(1 << floodHist);
        }
        else
        {
            cam.cullingMask |= (1 << floodHist);
        }
    }
}
