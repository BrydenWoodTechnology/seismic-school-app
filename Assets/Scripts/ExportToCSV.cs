﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;
using BrydenWoodUnity.GeometryManipulation;
using BrydenWood.Interop;

public class ExportToCSV : MonoBehaviour
{



    StringBuilder sb;
    void Start()
    {
        sb = new StringBuilder();
        sb.AppendLine("Name,posX,posY,posZ,rotX,rotY,rotZ,scaleX,scaleY,scaleZ,isResolved,hasCirculation,Type");


    }

    // Update is called once per frame
    void Update()
    {

    }

/// <summary>
/// Exports a csv file with all cluster transformations
/// </summary>
    public void ExportCSV()
    {
        for (int i = 0; i < transform.childCount; i++)
        {

            var c = transform.GetChild(i);
            string family = "";
            if (!c.name.Contains("Infant"))
            {
                if (c.GetComponent<ProceduralCluster>().isResolved)
                {
                    family = "Resolved";
                }
                else
                {
                    family = "Unresolved";
                }
            }
            else
            {
                if (c.GetComponent<ProceduralCluster>().halfLeft&&!c.GetComponent<ProceduralCluster>().halfRight)
                {
                    family = "Unresolved_L";
                }
                else if(!c.GetComponent<ProceduralCluster>().halfLeft && c.GetComponent<ProceduralCluster>().halfRight)
                {
                    family = "Unresolved_R";
                }
                else if(!c.GetComponent<ProceduralCluster>().halfLeft && !c.GetComponent<ProceduralCluster>().halfRight)
                {
                    family = "Resolved";
                }
                else
                {
                    family = "Unresolved";
                }
            }
            if (c.GetComponent<ProceduralCluster>().hasCirculation)
            {
                family += "_C";
            }
            Vector3 offsetPosition = new Vector3();
            if (c.GetComponent<ProceduralCluster>().halfLeft|| c.GetComponent<PlaceablePrefab>().resolvedLeft)
            {
                offsetPosition = c.position - (c.transform.right * 3.6f);
            }
            else
            {
                offsetPosition = c.position;
            }

            string type = c.GetComponent<ProceduralCluster>().type + "_" + family;
           
            sb.AppendLine(c.name.Split('_')[0] + "," + offsetPosition.x.ToString() + "," + offsetPosition.z.ToString() + "," + offsetPosition.y.ToString() + "," + c.eulerAngles.x.ToString() + "," + c.eulerAngles.z.ToString() + "," + c.eulerAngles.y.ToString() + "," + c.localScale.x + "," + c.localScale.z + "," + c.localScale.y + "," + c.GetComponent<ProceduralCluster>().isResolved.ToString() + "," + c.GetComponent<PlaceablePrefab>().hasCirculation.ToString()+","+type);

        }



#if UNITY_EDITOR
        File.WriteAllText(Application.streamingAssetsPath + "/Saved_data.csv", sb.ToString());

#else
        ResourcesManager.DownloadText(sb.ToString(), "Saved_data" + DateTime.Now.ToString() + ".csv", "text/plain");
        
#endif
    }


}
