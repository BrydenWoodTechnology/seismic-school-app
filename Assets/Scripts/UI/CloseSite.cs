﻿using BrydenWoodUnity.GeometryManipulation;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloseSite : MonoBehaviour
{
    public PolygonDrawer polyDraw;
    public Text buttonText;
    public bool siteStarted = false;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //print(siteStarted);
    }
    public void OnStartSite()
    {
        buttonText.text = "Close Site";
        if(!siteStarted)
        {
            polyDraw.StartPolygon();
        }
        else if (siteStarted == true)
        {
            siteStarted = false;
            buttonText.text = "Site Placed";
            polyDraw.EndPolygon();
           


        }
        siteStarted = true;

    }
}
