﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

[System.Serializable]
public class RibbonSetup : MonoBehaviour
{
 
    public GameObject tab_prefab;

    public GameObject popupPrefab;

    public GameObject slidingPrefab;
    public Sprite ColorTemplate;
    [HideInInspector]
    public List<GameObject> current_tabs;
    [HideInInspector]
    public List<Color32> tab_colors;
    [HideInInspector]
    public List<string> tab_titles;
    [HideInInspector]
    public List<Sprite> tab_icons;
    [HideInInspector]
    public List<bool> tab_drops;




    Vector2 SpriteDimensions;
    int sizeX;
    int sizeY;
    Texture2D myTexture;
    List<Color32>[] AllClusters;
    [HideInInspector]
    public Color32[] centroids;
    public Color32[] temp_centroids;

    //Vasilis edit
    public List <TabTypes> _tabs = new List<TabTypes>();


    List<float> distances = new List<float>();
    int r_value,g_value,b_value =0;
    int recursion = 0;

    Vector3 start_tab_position = new Vector3(32, 0, 0);

    List<Vector3> tabs = new List<Vector3>();

    // Use this for initialization
    void Start()
    {
        //for (int i = 0; i <transform.childCount; i++)
        //{
        //    transform.GetChild(i).gameObject.AddComponent<LayoutElement>();
        //    transform.GetChild(i).gameObject.GetComponent<LayoutElement>().ignoreLayout = true;
        //    if (i == 0)
        //    {
        //        transform.GetChild(i).transform.localPosition = start_tab_position;
        //    }
        //    else
        //    {
        //        transform.GetChild(i).transform.localPosition = new Vector3(transform.GetChild(i - 1).transform.localPosition.x + 73, 0, 0);
        //    }
        //}

        ReadyToPlay(false);

    }

    // Update is called once per frame
    void Update()
    {

    }

    

    public void PopulateRibbon(int tabs)
    {
        for (int i = 0; i < tabs; i++)
        {
            GameObject new_tab = Instantiate(tab_prefab);
            new_tab.name = "tab_" + i.ToString();
            new_tab.transform.SetParent(transform);
            new_tab.transform.localScale = new Vector3(1, 1, 1);
            current_tabs.Add(new_tab);
            Color32 current_col = new_tab.transform.GetChild(0).GetComponent<Image>().color;
            tab_colors.Add(current_col);
        }
        Debug.Log(current_tabs.Count.ToString());
    }

    public void PopulateRibbon()
    {
        if(current_tabs.Count == 0)
        {
            current_tabs = new List<GameObject>();
            tab_colors = new List<Color32>();
            tab_titles = new List<string>();
            tab_drops = new List<bool>();
            tab_icons = new List<Sprite>();
        }
        for (int i = 0; i < _tabs.Count; i++)
        {
            switch (_tabs[i])
            {
                case TabTypes.PopUp:
                    GameObject new_tab = Instantiate(popupPrefab);
                    new_tab.name = "tab_" + i.ToString();
                    new_tab.transform.SetParent(transform);
                    new_tab.transform.localScale = new Vector3(1, 1, 1);
                    //TabTypes new_type = new_tab.GetComponent<TabTypes>();           
                    current_tabs.Add(new_tab);

                    Color32 current_col = new_tab.transform.GetChild(0).GetComponent<Image>().color;
                    tab_colors.Add(current_col);

                    string current_title = "Bookmark Title";
                    tab_titles.Add(current_title);

                    bool drop_now = false;
                    tab_drops.Add(drop_now);

                    Sprite icon_now = new_tab.transform.GetChild(2).GetChild(0).GetChild(0).GetChild(1).GetComponent<Image>().sprite;
                    tab_icons.Add(icon_now);

                    break;

                case TabTypes.Sliding:
                    GameObject m_tab = Instantiate(slidingPrefab);
                    m_tab.name = "tab_" + i.ToString();
                    m_tab.transform.SetParent(transform);
                    m_tab.transform.localScale = new Vector3(1, 1, 1);
                    current_tabs.Add(m_tab);

                    Color32 current_col2 = m_tab.transform.GetChild(0).GetComponent<Image>().color;
                    tab_colors.Add(current_col2);

                    string current_title2 = "Bookmark Title";
                    tab_titles.Add(current_title2);

                    bool drop_now2 = false;
                    tab_drops.Add(drop_now2);

                    Sprite icon_now2 = m_tab.transform.GetChild(2).GetChild(0).GetChild(0).GetChild(1).GetComponent<Image>().sprite;
                    tab_icons.Add(icon_now2);

                    break;
            }
        }

    }

    public void AddOneRibbonItem()
    {
        //GameObject new_tab = Instantiate(tab_prefab);
        GameObject new_tab = Instantiate(slidingPrefab);
        new_tab.name = "tab_" + (current_tabs.Count).ToString();
        new_tab.transform.SetParent(transform);
        new_tab.transform.localScale = new Vector3(1, 1, 1);
        _tabs.Add(TabTypes.Sliding);
        current_tabs.Add(new_tab);
        Color32 current_col = new_tab.transform.GetChild(0).GetComponent<Image>().color;
        tab_colors.Add(current_col);
        Debug.Log(current_tabs.Count.ToString());
   
    }

    public void AddOneRibbonItem(TabTypes selected_type)
    {
        if (current_tabs.Count == 0)
        {
            current_tabs = new List<GameObject>();
            tab_colors = new List<Color32>();
            tab_titles = new List<string>();
            tab_drops = new List<bool>();
            tab_icons = new List<Sprite>();
        }
        switch (selected_type)
        {
            case TabTypes.PopUp:
                GameObject new_tab = Instantiate(popupPrefab);
                new_tab.name = "tab_" + (current_tabs.Count).ToString();
                new_tab.transform.SetParent(transform);
                new_tab.transform.localScale = new Vector3(1, 1, 1);
                _tabs.Add(TabTypes.PopUp);
                current_tabs.Add(new_tab);
                Color32 current_col = new_tab.transform.GetChild(0).GetComponent<Image>().color;
                tab_colors.Add(current_col);
                string current_title = "Bookmark Title";
                tab_titles.Add(current_title);
                bool drop_now = false;
                tab_drops.Add(drop_now);
                Sprite icon_now = new_tab.transform.GetChild(2).GetChild(0).GetChild(0).GetChild(1).GetComponent<Image>().sprite;
                tab_icons.Add(icon_now);
                break;
            case TabTypes.Sliding:
                GameObject other_tab = Instantiate(slidingPrefab);
                other_tab.name = "tab_" + (current_tabs.Count).ToString();
                other_tab.transform.SetParent(transform);
                other_tab.transform.localScale = new Vector3(1, 1, 1);
                _tabs.Add(TabTypes.Sliding);
                current_tabs.Add(other_tab);
                Color32 current_col_other = other_tab.transform.GetChild(0).GetComponent<Image>().color;
                tab_colors.Add(current_col_other);
                string current_title2 = "Bookmark Title";
                tab_titles.Add(current_title2);
                bool drop_now2 = false;
                tab_drops.Add(drop_now2);
                Sprite icon_now2 = other_tab.transform.GetChild(2).GetChild(0).GetChild(0).GetChild(1).GetComponent<Image>().sprite;
                tab_icons.Add(icon_now2);

                break;

        }

    }

    public void ClearRibbon()
    {
        tab_colors.Clear();
        tab_titles.Clear();
        tab_drops.Clear();
        tab_icons.Clear();
        current_tabs.Clear();
    }



    public void ColorizeTabSimple(Color32 my_col,GameObject thisTab)
    {
        thisTab.transform.GetChild(0).GetComponent<Image>().color = my_col;
    }

    public void DominantColors(int color_num)
    {
        SpriteDimensions = ColorTemplate.rect.size;
        sizeX = (int)SpriteDimensions.x;
        sizeY = (int)SpriteDimensions.y;
        Debug.Log("width: " + SpriteDimensions.x + ",  " + "height: " + SpriteDimensions.y);
        myTexture = ColorTemplate.texture;
        centroids = new Color32[color_num];
        temp_centroids = new Color32[color_num];
        AllClusters = new List<Color32>[color_num];

        for (int i=0; i<color_num; i++)
        {
            Color32 randomColor = myTexture.GetPixel((int)UnityEngine.Random.Range(0, sizeX - 1), (int)UnityEngine.Random.Range(0, sizeY - 1));
            //Debug.Log("Color_" + i.ToString() + ": " + randomColor.r.ToString() + "," + randomColor.g.ToString() + "," + randomColor.b.ToString());
            List<Color32> cluster = new List<Color32>();
            centroids[i] = randomColor;
            AllClusters[i] = cluster;
        }


        ClusteringRecursion(color_num,centroids);



    }

    public void ClusteringRecursion(int color_num, Color32[] centers)
    {
        recursion++;
        //Debug.Log(recursion.ToString());

        Color32[] _tempcentroids = new Color32[color_num];
        for (int j = 0; j < sizeX; j++)
        {
            for (int k = 0; k < sizeY; k++)
            {
                for (int c = 0; c < color_num; c++)
                {
                    Color32 currentPixel = myTexture.GetPixel(j, k);
                    float distance = RGBdistance(centers[c], currentPixel);
                    distances.Add(distance);
                    if (c == color_num - 1)
                    {
                        float minDistance = distances.Min();
                        //int minIndex = distances.ToList().IndexOf(minDistance);
                        int minIndex = distances.IndexOf(minDistance);
                        AllClusters[minIndex].Add(currentPixel);
                        //Array.Clear(distances, 0, distances.Length);
                        distances.Clear();
                    }
                }
            }
        }

        for (int l = 0; l < color_num; l++)
        {
            
            r_value = g_value = b_value = 0;
            int clusterSize = AllClusters[l].Count;

            if (clusterSize != 0)
            {
                for (int m = 0; m < clusterSize; m++)
                {
                    r_value += AllClusters[l][m].r;
                    g_value += AllClusters[l][m].g;
                    b_value += AllClusters[l][m].b;
                }
                //Debug.Log("R: "+(r_value/clusterSize).ToString());
                Color32 new_centroid = new Color32((byte)(r_value / clusterSize), (byte)(g_value / clusterSize), (byte)(b_value / clusterSize), 255);
                _tempcentroids[l] = new_centroid;
            }
            else
            {
                Color32 new_centroid = myTexture.GetPixel((int)UnityEngine.Random.Range(0, sizeX - 1), (int)UnityEngine.Random.Range(0, sizeY - 1));
                _tempcentroids[l] = new_centroid;
            }


        }

        int _dominanceCounter = 0;

        for (int g = 0; g < color_num; g++)
        {
            if (_tempcentroids[g].r == centers[g].r && _tempcentroids[g].g == centers[g].g && _tempcentroids[g].b == centers[g].b)

            {
                _dominanceCounter++;
            }
        }
        if (_dominanceCounter == color_num)
        {
            Debug.Log("Dominant Colors Found!!");
            temp_centroids = _tempcentroids;
        }
        if(_dominanceCounter != color_num)
        {
            ClusteringRecursion(color_num, _tempcentroids);
        }

    }

    public float RGBdistance(Color32 a,Color32 b)
    {
        float distance = Mathf.Sqrt(Mathf.Pow((a.r - b.r), 2)+ Mathf.Pow((a.g - b.g), 2)+ Mathf.Pow((a.b - b.b), 2));
        return distance;
    }

    public void ReadyToPlay(bool ready)
    {
        transform.GetComponent<HorizontalLayoutGroup>().enabled = ready;
    }
}
