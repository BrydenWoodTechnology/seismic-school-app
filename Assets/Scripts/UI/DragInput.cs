﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class DragInput : MonoBehaviour {

    public InputField input;
    public float value;
    //public RectTransform handle;

    Vector3 startMouse;
    Vector3 currentMouse;
    float distance;
    float prevDistance;
    float finalDIstance;

	// Use this for initialization
	void Start () {
        //input.text = value.ToString();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TrackMouse()
    {
        if (input.text != "")
        {
            prevDistance = float.Parse(input.text);
        }
        else
        {
            prevDistance = 0;
        }

        startMouse = Input.mousePosition;
        //Debug.Log(startMouse);
    }

    public void UpdateFiled()
    {
        currentMouse = Input.mousePosition;      
        distance = currentMouse.x - startMouse.x;



        finalDIstance = prevDistance + distance;
        input.text = /*((int)finalDIstance)*/finalDIstance.ToString();
    }
}

