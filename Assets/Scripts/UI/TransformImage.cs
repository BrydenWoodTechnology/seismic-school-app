﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransformImage : MonoBehaviour
{

    public Transform imagePlane;
    public InputField x_m;
    public InputField y_m;
    public InputField x_s;
    public InputField y_s;
    public InputField r;
    public bool lockedScale { get; set; }
    //public TranformMode mode;

    Vector3 new_pos;
    Vector3 new_scale;
    Vector3 new_rot;
    float _xM;
    float _yM;
    float _xS;
    float _yS;
    float _r;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateTransform(int mode)
    {
        if (mode == 0)
        {
            if (!float.TryParse(x_m.text, out _xM))
            {
                _xM = 0f;
            }
            if (!float.TryParse(y_m.text, out _yM))
            {
                _yM = 0f;
            }
            new_pos = new Vector3(_xM, 0, _yM);
            imagePlane.position = new_pos;
        }
        else if (mode == 1)
        {
            if (!float.TryParse(x_s.text, out _xS))
            {
                _xS = imagePlane.localScale.x;
            }
            if (!float.TryParse(y_s.text, out _yS))
            {
                _yS = imagePlane.localScale.z;
            }
            //_xS = (float)double.Parse(x_s.text);
            //_yS = (float)double.Parse(y_s.text);
            if (lockedScale)
                new_scale = new Vector3(_xS, 1, imagePlane.localScale.y + _xS);
            else
                new_scale = new Vector3(_xS, 1, _yS);
            imagePlane.localScale = new_scale;
        }
        else if (mode == 2)
        {
            _r = float.Parse(r.text);
            new_rot = new Vector3(0, 180 + _r, 0);
            imagePlane.eulerAngles = new_rot;
        }
    }
}

public enum TranformMode
{
    Move,
    Scale,
    Rotate
};

