using UnityEngine;
using System.Collections;
using BrydenWoodUnity.Lighting;


namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component from controlling the su position through a UI clock
    /// </summary>
    public class Clock : MonoBehaviour
    {
        #region Public Fields and Properties
        //-- set start time 00:00
        public int minutes = 0;
        public int hour = 0;
        public int seconds = 0;
        public bool realTime = true;

        public GameObject pointerSeconds;
        public GameObject pointerMinutes;
        public GameObject pointerHours;
        public SunControlUI_ sunScript;

        //-- time speed factor
        public float clockSpeed = 1.0f;     // 1.0f = realtime, < 1.0f = slower, > 1.0f = faster
        #endregion

        #region Private Fields and Properties
        //-- internal vars
        private float msecs = 0;
        private float rotationMinutes;
        private float rotationHours;
        #endregion

        #region MonoBehaviour Methods
        void Start()
        {
            hour = sunScript.this_is_now.Hour;
            minutes = sunScript.this_is_now.Minute;
        }

        void Update()
        {
            hour = sunScript.this_is_now.Hour;
            minutes = sunScript.this_is_now.Minute;

            rotationMinutes = (int)((360.0f / 60.0f) * minutes);
            rotationHours = (int)(((360.0f / 12.0f) * hour) + ((360.0f / (60.0f * 12.0f)) * minutes));
            

            pointerMinutes.transform.localEulerAngles = new Vector3(0.0f, 0.0f, -rotationMinutes);
            pointerHours.transform.localEulerAngles = new Vector3(0.0f, 0.0f, -rotationHours);
        }
        #endregion
    }
}
