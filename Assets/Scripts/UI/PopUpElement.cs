﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpElement : MonoBehaviour {

    [Header("Referenced Elements")]
    [Space(5)]
    public RectTransform popper;
    public Transform popper_content;


    public Toggle popper_toggle;
    bool tog_state;
    [Space(10)]
    [Header("UI Elements")]
    [Space(5)]
    [Tooltip("Define pop-up window color")]
    public Color32 color_start = new Color32();
    WaitForSeconds delay = new WaitForSeconds(0.35f);


    


    // Use this for initialization
    void Start () {
        tog_state = popper_toggle.isOn;

        color_start = popper.GetComponent<Image>().color;
        //Debug.Log(popper.gameObject.name+color_start.ToString());

        if (popper_content== null)
        {
            //Debug.Log("Content null");
            GameObject content = new GameObject("popper_content");
            content.transform.SetParent(popper);

            int pop_children = popper.childCount;
            for(int i = pop_children-1; i > 0; i--)
            {
                popper.GetChild(0).SetParent(content.transform);
            }

            
            popper_content = content.transform;
            popper_content.gameObject.SetActive(false);
        }
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartPopping(Toggle mytog)
    {
        tog_state = !tog_state;
        StartCoroutine(SmoothPop(popper, 1f, tog_state));
    }

    public void JustPop()
    {

    }



    public IEnumerator SmoothPop(RectTransform pop, float seconds,bool activeState)
    {
        float elapsedTime = 0;
        
        Image image = pop.GetComponent<Image>();
        
        color_start = image.color;
        //Debug.Log(color_start.ToString());
        
        float start_a = color_start.a;
        float end_a;
        if (activeState == true)
        {
            pop.gameObject.SetActive(true);
            end_a = 220;

            while (elapsedTime < seconds)
            {
                float a_now = (Mathf.Lerp(start_a, end_a, (elapsedTime / seconds)));
                image.color = new Color32(color_start.r, color_start.g, color_start.b, (byte)a_now);
                elapsedTime += 0.03f;

                if (elapsedTime >= seconds)
                {
                    image.color = new Color32(color_start.r, color_start.g, color_start.b, (byte)end_a);
                    popper_content.gameObject.SetActive(true);
                }
                yield return null;
            }
        }
        if(activeState ==false)
        {
            end_a = 0;
            popper_content.gameObject.SetActive(false);

            yield return delay;
            while (elapsedTime < seconds)
            {
                float a_now = (Mathf.Lerp(start_a, end_a, (elapsedTime / seconds)));
                image.color = new Color32(color_start.r, color_start.g, color_start.b, (byte)a_now);
                elapsedTime += 0.05f;

                if (elapsedTime >= seconds)
                {
                    image.color = new Color32(color_start.r, color_start.g, color_start.b, (byte)end_a);
                    pop.gameObject.SetActive(false);

                }
                yield return null;
            }
        }

    }
}
