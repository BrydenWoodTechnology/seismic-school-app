﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    public class MovePanelOnCanvas : MonoBehaviour
    {

        public Vector3 onPosition;
        public Vector3 offPosition;
        public bool inCoroutine = false;
        bool place;
        

        // Use this for initialization
        void Start()
        {
            place = false;
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void TakePlace()
        {
            place = !place;
            if (place==true)
            {
                GetComponent<RectTransform>().anchoredPosition = onPosition;
              
            }
            else
            {
                GetComponent<RectTransform>().anchoredPosition = offPosition;
                
            }

        }

        public void OnToggle(bool show)
        {
            
            if (inCoroutine) { }
            else
            {
                if (show)
                {
                    
                    GetComponent<RectTransform>().anchoredPosition = onPosition;
                }
                else
                {
                   
                    GetComponent<RectTransform>().anchoredPosition = offPosition;
                }
            }
        }


    }
}

