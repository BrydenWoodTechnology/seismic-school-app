﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlideOnRibbon : MonoBehaviour
{

    public RectTransform container;
    public Transform MainContainer;
    public List<GameObject> elementsToSlide = new List<GameObject>();
    List<Vector3> initialPositions = new List<Vector3>();
    float distance;
    int moverItems;
    GameObject moverParent;
    public bool slideComplete;
    bool toggleValue;


    // Use this for initialization
    void Start()
    {
        MainContainer = transform.parent.parent.parent;
        int myIndex = transform.parent.parent.GetSiblingIndex();
        int allIndices = MainContainer.childCount;
        if (container != null)
        {
            distance = container.rect.width;
        }
        else
        {
            distance = 0;
        }
       // Debug.Log(distance.ToString());
        slideComplete = false;
        toggleValue = false;

        if (myIndex != allIndices - 1)
        {
            for (int i = myIndex + 1; i < allIndices; i++)
            {
                elementsToSlide.Add(MainContainer.transform.GetChild(i).gameObject);
                initialPositions.Add(MainContainer.transform.GetChild(i).localPosition);
            }
        }
        //moverItems = elementsToSlide.Count;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GroupSlidingElements(Toggle pressedTog)
    {
        toggleValue = !toggleValue;
       // Debug.Log(toggleValue.ToString());
        if (toggleValue)
        {
            moverParent = new GameObject("MoverParent");
            moverParent.transform.SetParent(MainContainer);
            moverParent.transform.localPosition = transform.localPosition;
           // Debug.Log(moverParent.transform.localPosition.ToString());
            //slideComplete = false;
            
            if (elementsToSlide.Count != 0)
            {
                for (int i = 0; i < elementsToSlide.Count; i++)
                {
                    elementsToSlide[i].transform.SetParent(moverParent.transform);
                }
            }
            StartCoroutine(SlideElements(1f, moverParent.transform,container, true));
        }
        else if (toggleValue == false)
        {
            //recreate parent
            moverParent = new GameObject("MoverParent");
            moverParent.transform.SetParent(MainContainer);
            moverParent.transform.localPosition = transform.localPosition;

            if (elementsToSlide.Count != 0)
            {
                for (int i = 0; i < elementsToSlide.Count; i++)
                {
                    elementsToSlide[i].transform.SetParent(moverParent.transform);
                }
            }
            //slideComplete = false;
            StartCoroutine(SlideElements(1f, moverParent.transform,container, false));
        }


    }

    public IEnumerator SlideElements(float seconds, Transform mover, RectTransform placeHolder, bool leftRight)
    {
        
        Vector3 moverStartPos = mover.localPosition;
       // Debug.Log(moverStartPos.ToString());

        Vector3 moverEndPos;
        if (leftRight)
        {
            float elapsedTime = 0;
            moverEndPos = new Vector3(moverStartPos.x + distance, moverStartPos.y, 0);

            while (elapsedTime < seconds)
            {
                mover.localPosition = Vector3.Lerp(moverStartPos, moverEndPos, (elapsedTime / seconds));
                elapsedTime += 0.04f * (335/distance);
                if (elapsedTime >= seconds)
                {
                    mover.localPosition = moverEndPos;

                    if (elementsToSlide.Count != 0)
                    {
                        for (int k = 0; k < elementsToSlide.Count; k++)
                        {
                            elementsToSlide[k].transform.SetParent(MainContainer.transform);
                        }
                    }
                    if (mover.childCount == 0)
                    {
                        Destroy(mover.gameObject);
                    }

                    placeHolder.gameObject.SetActive(true);
                }
                yield return null;
            }
        }


        else
        {
            float elapsedTimeOther = 0;
            moverEndPos = new Vector3(moverStartPos.x - distance, moverStartPos.y, 0);
            placeHolder.gameObject.SetActive(false);
            //Debug.Log("StartPos: " + moverStartPos.ToString() + "__EndPos" + moverEndPos.ToString());

            while (elapsedTimeOther < seconds)
            {
                container.gameObject.SetActive(false);
                mover.localPosition = Vector3.Lerp(moverStartPos, moverEndPos, (elapsedTimeOther / seconds));
                elapsedTimeOther += 0.04f*(335 / distance);
                if (elapsedTimeOther >= seconds)
                {
                    //Debug.Log("GOT INTO FINAL STATE");
                    mover.localPosition = moverEndPos;
                    if (elementsToSlide.Count != 0)
                    {
                        for (int k = 0; k < elementsToSlide.Count; k++)
                        {
                            elementsToSlide[k].transform.SetParent(MainContainer.transform);
                        }
                    }
                    if (mover.childCount == 0)
                    {
                        Destroy(mover.gameObject);
                    }

                }
                yield return null;
            }
        }
        
    }


}
