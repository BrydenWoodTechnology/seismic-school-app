﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchView : MonoBehaviour {
    public bool isEnvironment;
    public GameObject viewer;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Click()
    {
        transform.SetSiblingIndex(transform.parent.childCount - 2);
        if (isEnvironment)
            viewer.SetActive(false);
        else
            viewer.SetActive(true);


    }
}
