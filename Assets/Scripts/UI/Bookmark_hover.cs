﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bookmark_hover : MonoBehaviour {

    public RectTransform BookmarkText;
    public GameObject textBackground;
    RectTransform rect;
    Vector3 originalPos;
    float xPos;
    WaitForSeconds displayDelay;
    WaitForSeconds scaleDelay;
    bool hovering;
   
    

	// Use this for initialization
	void Start () {
        rect = transform.GetComponent<RectTransform>();
        originalPos = transform.localPosition;
        xPos = transform.localPosition.x;
        displayDelay = new WaitForSeconds(0.3f);
        scaleDelay = new WaitForSeconds(0.2f);
        hovering = false;
        BookmarkText.gameObject.SetActive(false);
        textBackground.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnMouseOver()
    {
        hovering = true;
        StartCoroutine(MouseOver());
        StartCoroutine(ShowText(BookmarkText, true));
    }

    public void OnMouseExit()
    {
        hovering = false;
        transform.localScale = new Vector3(1, 1, 1);
        transform.localPosition = originalPos;
        BookmarkText.gameObject.SetActive(false);
        textBackground.SetActive(false);
    }

    public IEnumerator ShowText(RectTransform text, bool ONOFF)
    {
        yield return displayDelay;
        if (hovering == true)
        {
            text.gameObject.SetActive(ONOFF);
            textBackground.SetActive(ONOFF);
        }
    }

    public IEnumerator MouseOver()
    {
        yield return scaleDelay;
        if (hovering)
        {
            transform.localScale = new Vector3(1.2f, 1.31f, 1);
            float width = rect.sizeDelta.x;
            float new_width = width * 1.2f;
            float diff = new_width - width;
            transform.localPosition = new Vector3(xPos - diff, originalPos.y, originalPos.z);
        }
    }
}
