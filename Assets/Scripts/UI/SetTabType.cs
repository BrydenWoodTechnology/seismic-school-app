﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetTabType : MonoBehaviour {

    public GameObject[] tab_types;
    public RectTransform tab_container;
    public TabTypes tabtype = TabTypes.PopUp;

    [HideInInspector]
    public int tabTypeIndex = 0;

	// Use this for initialization
	void Start () {
        //TabSelecttor(1);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TabSelecttor(int tab_num)
    {
        if(tab_container.childCount != 0)
        {
            DestroyImmediate(tab_container.GetChild(0).gameObject);
        }
        GameObject new_tab_type = Instantiate(tab_types[tab_num]);
        new_tab_type.name = tab_types[tab_num].name;
        new_tab_type.transform.SetParent(tab_container);
        new_tab_type.transform.localScale = new Vector3(1, 1, 1);
        new_tab_type.transform.localPosition = Vector3.zero;
        
    }
}

public enum TabTypes
{
    Sliding = 1,
    PopUp=0
}
