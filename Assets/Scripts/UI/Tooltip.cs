﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using TMPro;

namespace BrydenWoodUnity.UIElements
{
    public class Tooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [Header("Tooltip related info:")]
        [Tooltip("The text to be desplayed")]
        public string text;
        [Tooltip("The color of the text")]
        public Color textColor = Color.black;
        [Tooltip("The offset in the position pof the text (local)")]
        public Vector2 offset = new Vector2(30, 70);
        [Tooltip("The delay in appearing (milliseconds)")]
        public float delay = 1000;

        [Space(10)]
        [Header("Prefabs and materials:")]
        [Tooltip("The Text prefab to be activated")]
        public GameObject textPrefab;

        private GameObject m_tooltip;
        private DateTime entered;
        private bool over = false;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (over)
            {
                
                if ((DateTime.Now - entered).TotalMilliseconds > 500)
                {

                    textPrefab.SetActive(true);

                    
                }
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            over = true;
            entered = DateTime.Now;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            over = false;
            if (textPrefab != null)
            {
                textPrefab.SetActive(false);
            }
        }
    }
}