﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
/// <summary>
/// Simulates a keyboard button with a UI element
/// </summary>
public class SimulateInput : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
   public enum ButtonType
    {
        Escape,
        Delete,
        Zed
    }

    public ButtonType buttonType;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

   

    public void OnPointerDown(PointerEventData eventData)
    {
        switch(buttonType)
        {
            case ButtonType.Escape:

                CustomInput.SetAxis("GUIEscape", 3);
                break;
            case ButtonType.Delete:
                CustomInput.SetAxis("GUIDelete", 3);

                break;
            case ButtonType.Zed:
                CustomInput.SetAxis("GUIZed", 3);

                break;

        }
        
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        CustomInput.SetAxis("GUIEscape", 0);
        CustomInput.SetAxis("GUIDelete", 0);
        CustomInput.SetAxis("GUIZed", 0);
    }
}
