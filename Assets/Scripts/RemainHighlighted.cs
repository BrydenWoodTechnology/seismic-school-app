﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RemainHighlighted : MonoBehaviour
{
    public bool isClicked;
    public Color clicked, unClicked;
    public Transform[] buttonParents;
    // Use this for initialization
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(IsPressed);
        if (isClicked)
            GetComponent<Image>().color = clicked;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void IsPressed()
    {

        isClicked = true;
        GetComponent<Image>().color = clicked;
        SwitchClicked();
    }

    public void SwitchClicked()
    {
        for (int i = 0; i < buttonParents.Length; i++)
        {
            foreach (Transform c in buttonParents[i])
            {
                if (c != transform && c.GetComponent<RemainHighlighted>() != null)
                {
                    c.GetComponent<RemainHighlighted>().isClicked = false;
                    c.GetComponent<Image>().color = unClicked;
                }
            }


        }


    }
}
