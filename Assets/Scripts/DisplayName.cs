﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using BrydenWoodUnity.GeometryManipulation;

public class DisplayName : MonoBehaviour {
    Transform hovered;
    Ray ray;
    RaycastHit hit;
    Vector3 origPos;
    public TextMeshProUGUI text;
    // Use this for initialization
    void Start () {
        origPos = GetComponent<RectTransform>().position;
        
    }
	
	// Update is called once per frame
	void Update () {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            if(hit.collider.gameObject.GetComponent<PlaceablePrefab>()!=null)
            {
                hovered = hit.collider.transform;
                text.text = "<mark=#00000020>" + hovered.GetComponent<ProceduralCluster>().name.Replace("Cluster",string.Empty)+ "</mark>";
                UpdatePosition();

            }
            else
            {
                GetComponent<RectTransform>().position= origPos;
            }
        }
        else
        {
            GetComponent<RectTransform>().position = origPos;
        }
    }

   void UpdatePosition()
    {
        var camPos = Camera.main.WorldToScreenPoint(hovered.position);
        camPos = new Vector3(camPos.x, camPos.y, 0);
        GetComponent<RectTransform>().position = camPos;
    }

  
}
