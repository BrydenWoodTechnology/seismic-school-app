﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BrydenWoodUnity.Grid;

namespace BrydenWoodUnity.GeometryManipulation
{
    public class MoveAlongSelected : MonoBehaviour
    {
        Transform sceneObject;
        public GameObject MeshlessGridMngr;
        private Vector3 initPosition;
        private Vector3 initMousePosition;
        private Vector3 prevCameraPosition;
        private float cameraPrevFoV;
        public Camera cam;
        Vector3 origPos;
        public Toggle circ, mirror;

        public bool isTranslation;
        // Use this for initialization
        void Start()
        {
            origPos = GetComponent<RectTransform>().position;
        }

        // Update is called once per frame
        void Update()
        {
            if (MeshlessGridMngr.GetComponent<MeshlessGridManager>().currentGrid != null)
            {

                if (MeshlessGridMngr.GetComponent<MeshlessGridManager>().currentGrid.GetComponent<MeshlessGrid>().hasSelected)
                {


                    if (prevCameraPosition != Camera.main.transform.position || cameraPrevFoV != Camera.main.orthographicSize)
                    {
                        UpdateUIPosition();

                    }
                }
                else
                {
                    GetComponent<RectTransform>().position = origPos;
                    if (isTranslation)
                        CloseToggles(transform);

                }
            }
            prevCameraPosition = Camera.main.transform.position;
            cameraPrevFoV = Camera.main.orthographicSize;
        }
        public void UpdateUIPosition()
        {
            if (MeshlessGridMngr.GetComponent<MeshlessGridManager>().currentGrid.currentObject != null)
            {
                var camPos = Camera.main.WorldToScreenPoint(MeshlessGridMngr.GetComponent<MeshlessGridManager>().currentGrid.currentObject.GetComponent<BoxCollider>().bounds.center + MeshlessGridMngr.GetComponent<MeshlessGridManager>().currentGrid.currentObject.GetComponent<BoxCollider>().bounds.extents);
                camPos = new Vector3(camPos.x, camPos.y, 0);
                GetComponent<RectTransform>().position = camPos;

            }
            else if (MeshlessGridMngr.GetComponent<MeshlessGridManager>().currentGrid.currentAdditional != null)
            {
                var camPos = Camera.main.WorldToScreenPoint(MeshlessGridMngr.GetComponent<MeshlessGridManager>().currentGrid.currentAdditional.GetComponent<BoxCollider>().bounds.center + MeshlessGridMngr.GetComponent<MeshlessGridManager>().currentGrid.currentAdditional.GetComponent<BoxCollider>().bounds.extents);
                camPos = new Vector3(camPos.x, camPos.y, 0);
                GetComponent<RectTransform>().position = camPos;
            }


        }
        public void OnSelect()
        {
            if (MeshlessGridMngr.GetComponent<MeshlessGridManager>().currentGrid.currentObject != null)
            {

                var camPos = Camera.main.WorldToScreenPoint(MeshlessGridMngr.GetComponent<MeshlessGridManager>().currentGrid.currentObject.GetComponent<BoxCollider>().bounds.center + MeshlessGridMngr.GetComponent<MeshlessGridManager>().currentGrid.currentObject.GetComponent<BoxCollider>().bounds.extents);
                camPos = new Vector3(camPos.x, camPos.y, 0);
                GetComponent<RectTransform>().position = camPos;
                mirror.interactable = true;
                ProceduralCluster pc = MeshlessGridMngr.GetComponent<MeshlessGridManager>().currentGrid.GetComponent<MeshlessGrid>().currentObject.GetComponent<ProceduralCluster>();
                if (isTranslation)
                {
                    if (pc.hasCirculation)
                    {
                        circ.isOn = false;
                        circ.interactable = true;
                    }
                    else
                    {
                        circ.isOn = true;
                        if (pc.gameObject.name.Contains("Hall") || pc.gameObject.name.Contains("Kitchen"))
                        {
                            circ.interactable = false;
                        }
                        else
                        {
                            circ.interactable = true;
                        }
                    }

                }
            }
            else if (MeshlessGridMngr.GetComponent<MeshlessGridManager>().currentGrid.currentAdditional != null)
            {

                var camPos = Camera.main.WorldToScreenPoint(MeshlessGridMngr.GetComponent<MeshlessGridManager>().currentGrid.currentAdditional.GetComponent<BoxCollider>().bounds.center + MeshlessGridMngr.GetComponent<MeshlessGridManager>().currentGrid.currentAdditional.GetComponent<BoxCollider>().bounds.extents);
                camPos = new Vector3(camPos.x, camPos.y, 0);
                GetComponent<RectTransform>().position = camPos;
                circ.interactable = false;
                mirror.interactable = false;
            }
        }
        void UpdatePosition()
        {


        }
        void CloseToggles(Transform parent)
        {
            foreach (Transform c in parent)
            {
                if (c.GetComponent<Toggle>() != null)
                {

                    c.GetComponent<Toggle>().isOn = false;
                }
                if (c.childCount > 0)
                {
                    CloseToggles(c);
                }

            }

        }
    }
}
