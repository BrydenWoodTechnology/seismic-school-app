﻿using BrydenWoodUnity.Navigation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EnableMaxCamera : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler {

    public Camera detailedCam;

    public void OnPointerEnter(PointerEventData eventData)
    {
        detailedCam.GetComponent<OrbitCamera>().enabled = true;
       // detailedCam.GetComponent<maxCamera>().allowOrbit = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        detailedCam.GetComponent<OrbitCamera>().enabled = false;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
