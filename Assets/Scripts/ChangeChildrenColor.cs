﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeChildrenColor : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void ColorChange(Color value)
    {
       for(int i=0;i<transform.childCount;i++)
        {
            transform.GetChild(i).GetComponent<MeshRenderer>().material.color = value;
        }
    }
}
