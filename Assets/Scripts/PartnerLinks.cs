﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class PartnerLinks : MonoBehaviour {
    [DllImport("_Internal")]
    private static extern void PopupOpenerCaptureClick(string link);
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnLinkOpen(string link)
    {

        PopupOpenerCaptureClick(link);
    }
}
