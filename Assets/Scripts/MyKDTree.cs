﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BrydenWoodUnity;

public class MyKDTree : Tagged<MyKDTree>
{
    public List<Vector3> KDTreeVertices(List<Vector3> listHeldPoints, List<Vector3> listPlacedPoints)
    {

        /*
         * For every held cluster a user holds, run through the for each loop. Essentially this code looks at the currently held cluster and finds
         * the nearest placed cluster. The for each loop runs through both clusters box collider to find all 8 verticies, the following for loops 
         * finds the nearest two vertices, the closest held cluster vertex and the closest placed cluster vertex. 
         * The debug draw line shows that the method works as it should
         */
        List<Vector3> closest = new List<Vector3>();

        Vector3 nearestHeld = Vector3.zero; //nearest vertex into a vector
        float closestHeldVertex = Mathf.Infinity; //this allows all gameobjects to be considered within the scene. e.g all placed/held

        Vector3 nearestPlaced = Vector3.zero;
        float closestPlacedVertex = Mathf.Infinity;

        //foreach (var heldcluster in listHeldClusters) //for each held cluster object within the held cluster list..
        //{
        Vector3 heldCorners=new Vector3(); //outputs list of vertiices to singular point
        Vector3 placedCorners=new Vector3(); //outputs list of vertices to singular point

        for (int i = 0; i < listHeldPoints.Count; i++) //count all the points within the held list
        {
            for (int x = 0; x < listPlacedPoints.Count; x++) //count all the points within the placed list
            {
                heldCorners = listHeldPoints[i];

                var heldClusterDistance = Vector3.Distance(heldCorners, placedCorners); //distance between held vertex and placed

                if (heldClusterDistance < closestHeldVertex) //if distance is less than all game objects in scene
                {
                    closestHeldVertex = heldClusterDistance;
                    nearestHeld = heldCorners;
                    //closest held corner is stored into nearest held, which stores the closet held vertex point to another object
                }

                placedCorners = listPlacedPoints[x];

                var placedClusterDistance = Vector3.Distance(heldCorners, placedCorners);

                if (placedClusterDistance < closestPlacedVertex)
                {
                    closestPlacedVertex = placedClusterDistance;
                    nearestPlaced = placedCorners;
                }

            }//list placed points end of loop

        }//list held points end of loop

        //}//placedcluster
        closest.Add(nearestPlaced);
        closest.Add(nearestHeld);
        // Debug.DrawLine(nearestHeld, nearestPlaced, Color.red); //draw red line
        return closest;
    }//heldcluster


}//end of method 
