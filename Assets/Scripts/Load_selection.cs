﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Load_selection : MonoBehaviour {
    [Header("Transition Values")]
    [Space(5)]
    public float TextscaleFactor;
    public float LogoScaleFactor;
    public float TransitionDelay;
    public float AppearDelay;

    [Space(10)]
    [Header("Referenced Elements")]
    [Space(5)]
    public RectTransform AppTitle;
    public RectTransform AppCredits;
    public RectTransform Logo;
    public RectTransform three_toggle;
    public RectTransform model_toggle;
    public List<RectTransform> Options = new List<RectTransform>();

    [Space(10)]
    [Header("UI Elements")]
    [Space(5)]
    public Sprite pressed;
    public Sprite unPressed;

    GameObject ScaleParent;
    GameObject MoveParent;
    WaitForSeconds delay;
    WaitForSeconds appearDelay;


    // Use this for initialization
    void Start () {

        Screen_Manager.ToggleStates += ChangeAllToggles;

        delay = new WaitForSeconds(TransitionDelay);
        appearDelay = new WaitForSeconds(AppearDelay);

        ScaleParent = new GameObject("ScaleParent");
        ScaleParent.transform.SetParent(transform);
        ScaleParent.transform.localPosition = Vector3.zero;
        ScaleParent.transform.localScale = new Vector3(1, 1, 1);

        MoveParent = new GameObject("MoveParent");
        MoveParent.transform.SetParent(transform);
        MoveParent.transform.localPosition = Vector3.zero;
        MoveParent.transform.localScale = new Vector3(1, 1, 1);

        AppTitle.transform.SetParent(ScaleParent.transform);
        AppCredits.transform.SetParent(ScaleParent.transform);

        //ScaleParent.transform.SetParent(MoveParent.transform);
        three_toggle.SetParent(MoveParent.transform);
        model_toggle.SetParent(MoveParent.transform);

        StartCoroutine(Transition( Logo, MoveParent.transform, ScaleParent.transform, 5f));
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public IEnumerator Transition(RectTransform logo,Transform mover, Transform scaler, float seconds)
    {
        float elapsedTime = 0;

        Vector3 logo_start_scale = logo.localScale;
        Vector3 logo_target_scale = new Vector3(LogoScaleFactor, LogoScaleFactor, 1);

        Vector3 scaler_start_scale = scaler.localScale;
        Vector3 scaler_target_scale = new Vector3(TextscaleFactor, TextscaleFactor, 1);

        Vector3 logo_start_pos = logo.localPosition;
        Vector3 logo_target_pos = new Vector3(0, 150, 0);

        Vector3 mover_start_pos = mover.localPosition;
        Vector3 mover_target_pos = new Vector3(0, mover_start_pos.y + 155, 0);

        Vector3 scaler_start_pos = scaler.localPosition;
        Vector3 scaler_target_pos = new Vector3(0, 150, 0);

        yield return delay;
        while (elapsedTime < seconds)
        {
            logo.localScale = Vector3.Lerp(logo_start_scale, logo_target_scale, (elapsedTime / seconds));
            logo.localPosition = Vector3.Lerp(logo_start_pos, logo_target_pos, (elapsedTime / seconds));
            scaler.localScale = Vector3.Lerp(scaler_start_scale, scaler_target_scale, (elapsedTime / seconds));
            scaler.localPosition = Vector3.Lerp(scaler_start_pos, scaler_target_pos, (elapsedTime / seconds));
            mover.localPosition = Vector3.Lerp(mover_start_pos, mover_target_pos, (elapsedTime / seconds));

            elapsedTime += 0.15f;
            yield return null;
        }
        yield return appearDelay;
        for(int i=0; i < Options.Count; i++)
        {
            Options[i].gameObject.SetActive(true);
        }
    }

    public void ChangeAllToggles(bool stateOne, bool stateTwo)
    {
        //three_toggle.GetComponent<Toggle>().isOn = stateOne;
        //model_toggle.GetComponent<Toggle>().isOn = stateTwo;
        if(stateOne == true)
        {
            three_toggle.GetComponent<Toggle>().image.sprite = pressed;
            model_toggle.GetComponent<Toggle>().image.sprite = unPressed;
        }if(stateTwo == true)
        {
            three_toggle.GetComponent<Toggle>().image.sprite = unPressed;
            model_toggle.GetComponent<Toggle>().image.sprite = pressed;
        }

        Debug.Log("three = " + stateOne.ToString());
        Debug.Log("model = " + stateTwo.ToString());

    }
}
