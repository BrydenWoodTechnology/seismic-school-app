﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZoomOnProgram : MonoBehaviour {
    public EntrySelect entry;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.Escape))
        {

            gameObject.SetActive(false);
        }
	}
    /// <summary>
    /// Loads an image by name
    /// </summary>
    /// <param name="index"></param> the name as integer
    public void LoadRespectiveImage(int index)
    {
        GetComponent<Image>().sprite = Resources.Load<Sprite>("ZoomUI/"+entry.currentEntry + "/" + index.ToString());
        //print("ZoomUI/" + entry + "/" + index.ToString());
        GetComponent<Image>().SetNativeSize();
    }
}
