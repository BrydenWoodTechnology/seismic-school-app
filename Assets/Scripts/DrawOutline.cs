﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawOutline : MonoBehaviour
{

    public GameObject camera;
    public LineRenderer ln;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            CreateOutline();
        }
    }

    public void CreateOutline()
    {
        var tempCorners = new List<Vector3>();
        foreach (Transform c in transform)
        {
            Bounds b = GetMaxBounds(c);
            tempCorners.Add(c.TransformPoint(b.center + new Vector3(b.size.x, -b.size.y, b.size.z) * 0.5f));
            tempCorners.Add(c.TransformPoint(b.center + new Vector3(-b.size.x, -b.size.y, b.size.z) * 0.5f));
            tempCorners.Add(c.TransformPoint(b.center + new Vector3(-b.size.x, -b.size.y, -b.size.z) * 0.5f));
            tempCorners.Add(c.TransformPoint(b.center + new Vector3(b.size.x, -b.size.y, -b.size.z) * 0.5f));


        }
        //var tree = KDTreeVec.MakeFromPoints(tempCorners.ToArray());

        ln.positionCount = FindClose(tempCorners.ToArray()).Length;
        ln.SetPositions((FindClose(tempCorners.ToArray())));
        //ln.SetWidth(0.5f,0.5f);
    }

    Vector3[] FindClose(Vector3[] myList)
    {
        var temp = new List<Vector3>();
        for (int i = 0; i < myList.Length - 1; i++)
        {
            print(myList[i]);
            if(Vector3.Distance(myList[i], myList[i+1])>2)
            {
                temp.Add(myList[i]);
            }


        }
        return temp.ToArray();
    }
    public Bounds GetMaxBounds(Transform g)
    {
        int counter = 0;
        Bounds bnds = new Bounds();
        foreach (Transform t in g)
        {

            if (t.GetComponent<BoxCollider>() != null)
            {
                if (counter == 0)
                {
                    //Debug.Log(t.gameObject.name);
                    bnds = t.GetComponent<MeshRenderer>().bounds;
                }
                else
                {
                    //Debug.Log(t.gameObject.name);
                    Bounds _b = t.GetComponent<MeshRenderer>().bounds;
                    bnds.Encapsulate(_b);
                }
                counter++;
            }


        }
        //Gizmos.color = Color.green;
        //Gizmos.DrawWireCube(bnds.center, bnds.size);
        return bnds;
    }
}
