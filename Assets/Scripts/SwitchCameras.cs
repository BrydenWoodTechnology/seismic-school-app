﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCameras : MonoBehaviour {

    public Camera mainCam;
    public Camera TopCam;
    public RenderTexture topview;
    bool switcher = false;
    Vector3 mainPos;
    Vector3 topPos;
    Quaternion mainRot;
    Quaternion topRot;

	// Use this for initialization
	void Start () {
        mainPos = mainCam.transform.position;
        mainRot = mainCam.transform.rotation;
        topPos = TopCam.transform.position;
        topRot = TopCam.transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SwitchView()
    {
        switcher = !switcher;

        if (switcher)
        {
            //----switch main camera to top view
            //mainCam.targetTexture = topview;
            //TopCam.targetTexture = null;
            mainCam.transform.position = topPos;
            mainCam.transform.rotation = topRot;
            mainCam.farClipPlane = 1000;

            TopCam.transform.position = mainPos;
            TopCam.transform.rotation = mainRot;
            TopCam.farClipPlane = 10000;

        }
        else
        {
            //-----switch main camera back
            //mainCam.targetTexture = null;
            //TopCam.targetTexture = topview;

            mainCam.transform.position = mainPos;
            mainCam.transform.rotation = mainRot;
            mainCam.farClipPlane = 10000;

            TopCam.transform.position = topPos;
            TopCam.transform.rotation = topRot;
            TopCam.farClipPlane = 1000;
        }

    }
}
