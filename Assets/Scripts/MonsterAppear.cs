﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAppear : MonoBehaviour {
    public GameObject model;
	// Use this for initialization
	void Start () {
		
	}

    private void OnEnable()
    {

        model.SetActive(true);
        InvokeRepeating("ToggleMonster",Random.Range(0,5),Random.Range(3,10));
    }

    // Update is called once per frame
    void Update () {



		
	}

    void ToggleMonster()
    {
        model.SetActive(!model.activeSelf);
    }
}
