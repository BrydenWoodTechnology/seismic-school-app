﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace BrydenWoodUnity.GeometryManipulation
{
    public class DetailedConfiguration : MonoBehaviour
    {

        public Transform configParent;

        bool isArranged = false;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void ArrangeDetailedClusters(bool tog)
        {

            if (tog)
            {
                isArranged = true;

                foreach (Transform c in configParent)
                {
                    if (c.name.Contains("Infant"))
                    {
                        if (c.GetComponent<ProceduralCluster>().isResolved && c.GetComponent<ProceduralCluster>().hasCirculation && c.GetComponent<ProceduralCluster>().halfLeft && !c.GetComponent<ProceduralCluster>().halfRight)
                        {
                            LoadRespectiveCluster(c, "Unresolved_Infant_L_Corridor");
                        }
                        else if (c.GetComponent<ProceduralCluster>().isResolved && !c.GetComponent<ProceduralCluster>().hasCirculation && c.GetComponent<ProceduralCluster>().halfLeft && !c.GetComponent<ProceduralCluster>().halfRight)
                        {
                            LoadRespectiveCluster(c, "Unresolved_Infant_L");
                        }
                        else if (c.GetComponent<ProceduralCluster>().isResolved && !c.GetComponent<ProceduralCluster>().hasCirculation && !c.GetComponent<ProceduralCluster>().halfLeft && c.GetComponent<ProceduralCluster>().halfRight)
                        {
                            LoadRespectiveCluster(c, "Unresolved_Infant_R");
                        }
                        else if (c.GetComponent<ProceduralCluster>().isResolved && c.GetComponent<ProceduralCluster>().hasCirculation && !c.GetComponent<ProceduralCluster>().halfLeft && c.GetComponent<ProceduralCluster>().halfRight)
                        {
                            LoadRespectiveCluster(c, "Unresolved_Infant_R_Corridor");
                        }
                        else if (!c.GetComponent<ProceduralCluster>().isResolved && c.GetComponent<ProceduralCluster>().hasCirculation)
                        {
                            LoadRespectiveCluster(c, "Unresolved_" + c.GetComponent<ProceduralCluster>().detailedName + "_Corridor");
                        }
                        else if (!c.GetComponent<ProceduralCluster>().isResolved && !c.GetComponent<ProceduralCluster>().hasCirculation)
                        {
                            LoadRespectiveCluster(c, "Unresolved_" + c.GetComponent<ProceduralCluster>().detailedName);
                        }
                        else if (c.GetComponent<ProceduralCluster>().isResolved && !c.GetComponent<ProceduralCluster>().hasCirculation && !c.GetComponent<ProceduralCluster>().halfLeft && !c.GetComponent<ProceduralCluster>().halfRight)
                        {
                            LoadRespectiveCluster(c, "Resolved_" + c.GetComponent<ProceduralCluster>().detailedName);
                        }
                        else if (c.GetComponent<ProceduralCluster>().isResolved && c.GetComponent<ProceduralCluster>().hasCirculation && !c.GetComponent<ProceduralCluster>().halfLeft && !c.GetComponent<ProceduralCluster>().halfRight)
                        {
                            LoadRespectiveCluster(c, "Resolved_" + c.GetComponent<ProceduralCluster>().detailedName + "_Corridor");
                        }

                    }
                    else
                    {
                        if (c.GetComponent<ProceduralCluster>().isResolved && c.GetComponent<ProceduralCluster>().hasCirculation)
                        {
                            LoadRespectiveCluster(c, "Resolved_" + c.GetComponent<ProceduralCluster>().detailedName + "_Corridor");
                        }
                        else if (c.GetComponent<ProceduralCluster>().isResolved && !c.GetComponent<ProceduralCluster>().hasCirculation)
                        {
                            LoadRespectiveCluster(c, "Resolved_" + c.GetComponent<ProceduralCluster>().detailedName);
                        }
                        else if (!c.GetComponent<ProceduralCluster>().isResolved && c.GetComponent<ProceduralCluster>().hasCirculation)
                        {
                            LoadRespectiveCluster(c, "Unresolved_" + c.GetComponent<ProceduralCluster>().detailedName + "_Corridor");
                        }
                        else if (!c.GetComponent<ProceduralCluster>().isResolved && !c.GetComponent<ProceduralCluster>().hasCirculation)
                        {
                            LoadRespectiveCluster(c, "Unresolved_" + c.GetComponent<ProceduralCluster>().detailedName);
                        }
                    }



                }
            }
            else
            {
                DestroyAllDetailed();
            }

        }

        public void KeepAranging(bool b)
        {



        }
        /// <summary>
        /// load an asset by path with specific transformation
        /// </summary>
        /// <param name="c"></param> the transform to get transformation from
        /// <param name="path"></param>path of asset

        void LoadRespectiveCluster(Transform c, string path)
        {
            if (!AssetBundleManager.TaggedObject.loading)
            {

                GameObject d = AssetBundleManager.TaggedObject.GetAsset(path) as GameObject;
                if (d != null)
                {
                    GameObject temp = Instantiate(d, c.localPosition, c.localRotation, transform);

                    if (c.tag == "MirroredX")
                    {

                        temp.transform.localScale = new Vector3(-temp.transform.localScale.x, temp.transform.localScale.y, temp.transform.localScale.z);
                        temp.transform.position = c.position;


                    }
                    else if (c.tag == "MirroredZ")
                    {
                        temp.transform.localScale = new Vector3(temp.transform.localScale.x, -temp.transform.localScale.y, temp.transform.localScale.z);
                        temp.transform.position = c.position;

                    }
                    else if (c.tag == "Mirrored")
                    {
                        temp.transform.localScale = new Vector3(-temp.transform.localScale.x, -temp.transform.localScale.y, temp.transform.localScale.z);
                        temp.transform.position = c.position;


                    }
                    else
                    {
                        temp.transform.position = c.position;
                    }



                    temp.transform.localEulerAngles = new Vector3(temp.transform.localEulerAngles.x - 90, temp.transform.localEulerAngles.y + 180, temp.transform.localEulerAngles.z);
                }
                else
                {
                    print(path);

                }
            }
        }
        /// <summary>
        /// Destroy all loaded assets
        /// </summary>
        public void DestroyAllDetailed()
        {
            isArranged = false;
            foreach (Transform c in transform)
            {
                Destroy(c.gameObject);

            }
        }
        /// <summary>
        /// activates objects per level
        /// </summary>
        /// <param name="floor"></param>the level number
        public void FloorToFloorVisualization(float floor)
        {
            foreach (Transform c in transform)
            {
                if (c.position.y > floor * 3.5f)
                {
                    c.gameObject.SetActive(false);
                }
                else
                {
                    c.gameObject.SetActive(true);
                }

            }

        }
    }
}
