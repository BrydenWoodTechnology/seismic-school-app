﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.Navigation;

namespace BrydenWoodUnity.GeometryManipulation
{
    public class AdditionalPrefab : SelectableGeometry
    {

        public Vector3 size = new Vector3();
        [HideInInspector]
        public bool isSelected = false;
        public Transform originalParent;

        public int places;
        public delegate void OnDelete(AdditionalPrefab sender);
        public static event OnDelete deleted;
        // Use this for initialization
        void Start()
        {
            size = GetComponent<BoxCollider>().size;
            originalParent = transform.parent;
        }

        // Update is called once per frame
        void Update()
        {
            if (isSelected)
            {
                SelectHighlight(true);
            }

        }
        private void OnDestroy()
        {
            Delete();
            if (deleted != null)
            {
                foreach (System.Delegate del in deleted.GetInvocationList())
                {
                    deleted -= (OnDelete)del;
                }

            }

        }
        public void Delete()
        {
            if (deleted != null)
            {
                deleted(this);
                if (Camera.main != null)
                    if (Camera.main.GetComponent<DrawGLines>().selectedPrefab != null)
                        Camera.main.GetComponent<DrawGLines>().selectedPrefab = null;
            }

        }
        public override void SelectHighlight(bool select)
        {
            if (select)
            {
                Camera.main.GetComponent<DrawGLines>().selectedPrefab = new List<Vector3[]>();
                Camera.main.GetComponent<DrawGLines>().selectedPrefab.Add(GetBoundingCorners());
                for (int i = 0; i < transform.childCount; i++)
                {
                    if (transform.GetChild(i).GetComponent<AdditionalPrefab>() != null)
                    {

                        Camera.main.GetComponent<DrawGLines>().selectedPrefab.Add(transform.GetChild(i).GetComponent<AdditionalPrefab>().GetBoundingCorners());
                    }
                }
            }
            else
            {
                Camera.main.GetComponent<DrawGLines>().selectedPrefab = null;
            }
        }
        public override void Select()
        {

            if (!isSelected)
            {
                base.Select();
                isSelected = true;
            }
        }
        public override void DeSelect()
        {
            base.DeSelect();
            isSelected = false;
            transform.SetParent(originalParent);
        }
        private Vector3[] GetBoundingCorners()
        {
            List<Vector3> corners = new List<Vector3>();
            Vector3 point = new Vector3();
            var b = GetComponent<BoxCollider>();
            corners.Add(transform.TransformPoint(b.center + new Vector3(b.size.x, -b.size.y, b.size.z) * 0.5f));
            corners.Add(transform.TransformPoint(b.center + new Vector3(-b.size.x, -b.size.y, b.size.z) * 0.5f));
            corners.Add(transform.TransformPoint(b.center + new Vector3(-b.size.x, -b.size.y, -b.size.z) * 0.5f));
            corners.Add(transform.TransformPoint(b.center + new Vector3(b.size.x, -b.size.y, -b.size.z) * 0.5f));
            corners.Add(transform.TransformPoint(b.center + new Vector3(b.size.x, b.size.y, b.size.z) * 0.5f));
            corners.Add(transform.TransformPoint(b.center + new Vector3(-b.size.x, b.size.y, b.size.z) * 0.5f));
            corners.Add(transform.TransformPoint(b.center + new Vector3(-b.size.x, b.size.y, -b.size.z) * 0.5f));
            corners.Add(transform.TransformPoint(b.center + new Vector3(b.size.x, b.size.y, -b.size.z) * 0.5f));

            return corners.ToArray();
        }
    }
}
