﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using BrydenWoodUnity.Grid;



public class ShowMirrorLine : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    public MeshlessGridManager grigManager;
    public GameObject mirrorLine;

    public bool Z;


    public void OnPointerEnter(PointerEventData eventData)
    {
        mirrorLine.layer = 0;
        mirrorLine.transform.position = grigManager.currentGrid.currentObject.GetComponent<BoxCollider>().bounds.center + new Vector3(0, 2, 0);
        if (Z)
        {
            mirrorLine.transform.eulerAngles = grigManager.currentGrid.currentObject.transform.eulerAngles + new Vector3(0, 90, 0);
            mirrorLine.transform.localScale = new Vector3(grigManager.currentGrid.currentObject.GetComponent<BoxCollider>().size.x/10, 1, grigManager.currentGrid.currentObject.GetComponent<BoxCollider>().size.z/10);
        }
        else
        {
            mirrorLine.transform.eulerAngles = grigManager.currentGrid.currentObject.transform.eulerAngles;
            mirrorLine.transform.localScale = new Vector3(grigManager.currentGrid.currentObject.GetComponent<BoxCollider>().size.x/10, 1, grigManager.currentGrid.currentObject.GetComponent<BoxCollider>().size.z/10);
        }
           
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        mirrorLine.layer = 30;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


}
