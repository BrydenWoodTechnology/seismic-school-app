The application was made using Unity 2018.1.0f2.

All elements were developed in-house by Bryden Wood,besides the following external assets:

Modern UI Pack: https://assetstore.unity.com/packages/tools/gui/modern-ui-pack-114792

JsonDotNet: https://assetstore.unity.com/packages/tools/input-management/json-net-for-unity-11347

Better Unity WebGL Template: https://github.com/greggman/better-unity-webgl-template

MapBox Unity SDK: https://docs.mapbox.com/unity/maps/overview/

These are not present in the source code due to copyright and distribution restrictions.